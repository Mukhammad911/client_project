<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;
use Teleglobal\Accounting\Traits\EnvValueEditable;
use Illuminate\Support\Facades\Schema;

class PurgeDatabaseTables extends Command
{
    use EnvValueEditable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:purgeDatabaseTables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'purgeDatabaseTables is runing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Schema::connection('accounting_mysql')->dropAllTables();
    }
}
