<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;
use Teleglobal\Accounting\Traits\EnvValueEditable;

class PurgeAccounting extends Command
{
    use EnvValueEditable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:purge {--key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate accounting app key.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->option('key'))
        {
            if (env("ACCOUNTING_KEY"))
            {
                if (!$this->setEnvironmentValue(["ACCOUNTING_KEY" => '']))
                {
                    echo("Package deployment error: environment variable setting failed. \n");
                    exit;
                }
                exec('php artisan optimize:clear', $output);
                echo "Accounting encryption key removed. \n";
            }
            else
            {
                echo "Accounting encryption key already removed. \n";
            }
        }
    }
}
