<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;
use Teleglobal\Accounting\Traits\EnvValueEditable;

class InstallAccounting extends Command
{
    use EnvValueEditable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running package deployment.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!env("ACCOUNTING_DEPLOY_STATUS", null))
        {
            if (!$this->setEnvironmentValue(["ACCOUNTING_DEPLOY_STATUS" => true]))
            {
                echo "Package deployment error: environment variable setting failed.\n";
                exit;
            }
            exec('php artisan vendor:publish --tag=accounting-config --force', $output);
            exec('php artisan vendor:publish --tag=accounting-assets --force', $output);
            exec('php artisan accounting:migrate --initial', $output);
            exec('php artisan accounting:seed --initial', $output);
        }
        else
        {
            echo("Package application is already deployed.\n");
        }
    }
}
