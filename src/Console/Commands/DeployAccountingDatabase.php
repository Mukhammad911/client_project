<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;
use Teleglobal\Accounting\Traits\EnvValueEditable;

class DeployAccountingDatabase extends Command
{
    use EnvValueEditable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:deployAccountingDatabase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running package deployment.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('php artisan accounting:purgeDatabaseTables', $output);
        exec('php artisan migrate --path=vendor/teleglobal/accounting/src/database/migrations/', $output);
        exec('php artisan accounting:seed --initial', $output);
    }
}
