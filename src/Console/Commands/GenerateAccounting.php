<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Teleglobal\Accounting\Traits\EnvValueEditable;

class GenerateAccounting extends Command
{
    use EnvValueEditable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:generate {--key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate accounting app key.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        if($this->option('key'))
        {
            if (!env("ACCOUNTING_KEY", null))
            {
                exec('php artisan key:generate --show', $output);

                if (!isset($output[0]) && empty($output[0])) {
                    echo("Package deployment error: application key generate failed. \n");
                    exit;
                }
                echo "Please store your encryption key: \n" . $output[0] . "\n";

                if (!$this->setEnvironmentValue(["ACCOUNTING_KEY" => $output[0]]))
                {
                    echo("Package deployment error: environment variable setting failed (KEY). \n");
                    exit;
                }

                $cypherMethod = 'AES-128-CBC';

                $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cypherMethod));
                
                $bin_to_hex = bin2hex($iv);

                if (!$this->setEnvironmentValue(["ACCOUNTING_IV" => $bin_to_hex]))
                {
                    echo("Package deployment error: environment variable setting failed (IV). \n");
                    exit;
                }
                exec('php artisan optimize:clear', $output);
            }
            else
            {
                echo "Accounting encryption key already exists: \n" . env("ACCOUNTING_KEY") . "\n";
            }
        }
    }

}
