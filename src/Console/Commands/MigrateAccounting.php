<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;

class MigrateAccounting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:migrate {migration=all} {--initial}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running migrations.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->option('initial'))
        {
            exec('php artisan migrate:install', $output);
        }

        exec('php artisan migrate --path=vendor/teleglobal/accounting/src/database/migrations/', $output);
        var_dump($output);
    }
}
