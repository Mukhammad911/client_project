<?php

namespace Teleglobal\Accounting\Console\Commands;

use Illuminate\Console\Command;
use Teleglobal\Accounting\Database\Seeds\AccountingDatabaseSeeder;

class SeedAccounting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting:seed {seeder=all} {--initial}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running seeders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->option('initial'))
        {
            // Run all seeders
            exec('php artisan db:seed --class="Teleglobal\\Accounting\\Database\\Seeds\\AccountingDatabaseSeeder"', $output);
            var_dump($output);
        }
        else
        {
            // Run a specific seeder
            exec('php artisan db:seed --class="Teleglobal\\Accounting\\Database\\Seeds\\'. $this->argument('seeder') .'"', $output);
        }
    }
}
