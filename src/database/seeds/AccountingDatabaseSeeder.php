<?php

namespace Teleglobal\Accounting\Database\Seeds;

use Illuminate\Database\Seeder;
use Teleglobal\Accounting\Database\Seeds\UsersTableSeeder as UsersTableSeeder;
use Teleglobal\Accounting\Database\Seeds\CurrenciesTableSeeder as CurrenciesTableSeeder;
use Teleglobal\Accounting\Database\Seeds\WarehouseTableSeeder as WarehouseTableSeeder;
use Teleglobal\Accounting\Database\Seeds\SettingsTableSeeder as SettingsTableSeeder;
use Teleglobal\Accounting\Database\Seeds\CashboxesTableSeeder as CashboxesTableSeeder;
use Teleglobal\Accounting\Database\Seeds\HistoryOfEditsTableSeeder as HistoryOfEditsTableSeeder;

class AccountingDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(WarehouseTableSeeder::class);
        $this->call(CashboxesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);

        $this->call(HistoryOfEditsTableSeeder::class);
    }
}
