<?php

namespace Teleglobal\Accounting\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Client;
use Teleglobal\Accounting\Models\Transport;
use Teleglobal\Accounting\Models\Dealer;
use Teleglobal\Accounting\Models\Warehouse;
use Teleglobal\Accounting\Models\Product;
use Teleglobal\Accounting\Models\WarehouseProductPivot;

class WarehouseTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('accounting_mysql')->table(Warehouse::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Склад №1'),
            'b' => Crypt::encrypt('Комментарий к складу №1'),
            'c' => Crypt::encrypt('all'),
        ]);
        DB::connection('accounting_mysql')->table(Warehouse::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Склад №2'),
            'b' => Crypt::encrypt('Комментарий к складу №2'),
            'c' => Crypt::encrypt('all'),
        ]);
        DB::connection('accounting_mysql')->table(Warehouse::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Склад №3'),
            'b' => Crypt::encrypt('Комментарий к складу №3'),
            'c' => Crypt::encrypt('all'),
        ]);

        DB::connection('accounting_mysql')->table(Product::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Товар №1'),
            'b' => Crypt::encrypt('Комментарий к товару №1'),
        ]);
        DB::connection('accounting_mysql')->table(Product::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Товар №2'),
            'b' => Crypt::encrypt('Комментарий к товару №2'),
        ]);
        DB::connection('accounting_mysql')->table(Product::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Товар №3'),
            'b' => Crypt::encrypt('Комментарий к товару №3'),
        ]);

        DB::connection('accounting_mysql')->table(WarehouseProductPivot::TABLE_NAME)->insert([
            'a' => 2,
            'b' => 2,
        ]);
        DB::connection('accounting_mysql')->table(WarehouseProductPivot::TABLE_NAME)->insert([
            'a' => 2,
            'b' => 3,
        ]);
        DB::connection('accounting_mysql')->table(WarehouseProductPivot::TABLE_NAME)->insert([
            'a' => 1,
            'b' => 3,
        ]);
        DB::connection('accounting_mysql')->table(WarehouseProductPivot::TABLE_NAME)->insert([
            'a' => 3,
            'b' => 3,
        ]);

        DB::connection('accounting_mysql')->table(Client::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Клиент №1'),
            'b' => Crypt::encrypt('Коментарий к клиенту №1'),
        ]);
        DB::connection('accounting_mysql')->table(Client::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Клиент №2'),
            'b' => Crypt::encrypt('Коментарий к клиенту №2'),
        ]);
        DB::connection('accounting_mysql')->table(Client::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Клиент №3'),
            'b' => Crypt::encrypt('Коментарий к клиенту №3'),
        ]);

        DB::connection('accounting_mysql')->table(Transport::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Транспорт №1'),
            'b' => Crypt::encrypt('Коментарий к транспорту №1'),
        ]);
        DB::connection('accounting_mysql')->table(Transport::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Транспорт №2'),
            'b' => Crypt::encrypt('Коментарий к транспорту №2'),
        ]);
        DB::connection('accounting_mysql')->table(Transport::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Транспорт №3'),
            'b' => Crypt::encrypt('Коментарий к транспорту №3'),
        ]);

        DB::connection('accounting_mysql')->table(Dealer::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Посредник №1'),
            'b' => Crypt::encrypt('Коментарий к посреднику №1'),
        ]);
        DB::connection('accounting_mysql')->table(Dealer::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Посредник №2'),
            'b' => Crypt::encrypt('Коментарий к посреднику №2'),
        ]);
        DB::connection('accounting_mysql')->table(Dealer::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Посредник №3'),
            'b' => Crypt::encrypt('Коментарий к посреднику №3'),
        ]);
    }
}
