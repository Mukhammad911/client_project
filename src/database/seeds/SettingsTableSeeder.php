<?php

namespace Teleglobal\Accounting\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('accounting_mysql')->table(Setting::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('currencies'),
            'b' => Crypt::encrypt(json_encode([0 => '77', 1 => '253', 2 => '249'])),
        ]);

    }
}
