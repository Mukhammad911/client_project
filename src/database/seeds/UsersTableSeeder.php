<?php

namespace Teleglobal\Accounting\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\User;
use Teleglobal\Accounting\Models\UserGroup;
use Teleglobal\Accounting\Models\UserGroupUser;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Inserting users
        DB::connection('accounting_mysql')->table(User::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('admin'),
            'e' => null,
            'b' => Crypt::encrypt('dev@gmail.com'),
            'd' => Hash::make('devadmin'),
            'f' => Crypt::encrypt('a'),
            'g' => Crypt::encrypt('seeds generated for users'),
        ]);
        DB::connection('accounting_mysql')->table(User::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('rom'),
            'e' => null,
            'b' => Crypt::encrypt('romcrazy13@gmail.com'),
            'd' => Hash::make('crazy13'),
            'f' => Crypt::encrypt('a'),
            'g' => Crypt::encrypt('seeds generated for users'),
        ]);
        DB::connection('accounting_mysql')->table(User::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('John Smith'),
            'e' => null,
            'b' => Crypt::encrypt('developer@gmail.com'),
            'd' => Hash::make('secret'),
            'f' => Crypt::encrypt('a'),
            'g' => Crypt::encrypt('seeds generated for users'),
        ]);

        // Inserting new userGroups
        DB::connection('accounting_mysql')->table(UserGroup::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Группа №1'),
            'b' => Crypt::encrypt(''),
        ]);
        DB::connection('accounting_mysql')->table(UserGroup::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Группа №2'),
            'b' => Crypt::encrypt(''),
        ]);
        DB::connection('accounting_mysql')->table(UserGroup::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Группа №3'),
            'b' => Crypt::encrypt(''),
        ]);

        // Adding all existing users to "Группа №1"
        $users = DB::connection('accounting_mysql')->table(User::TABLE_NAME)->get();
        $userGroup = DB::connection('accounting_mysql')
            ->table(UserGroup::TABLE_NAME)
            ->where(UserGroup::FIELD_NAME, '=', Crypt::encrypt('Группа №1'))
            ->first();
        foreach ($users as $user){
            DB::connection('accounting_mysql')->table(UserGroupUser::TABLE_NAME)->insert([
                'a' => $user->id,
                'b' => $userGroup->id,
            ]);
        }
    }
}
