<?php

namespace Teleglobal\Accounting\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\ReportCategory;
use Teleglobal\Accounting\Models\ReportSubCategory;
use Teleglobal\Accounting\Models\ReportAccount;
use Teleglobal\Accounting\Models\ReportCurrency;

class CashboxesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('accounting_mysql')->table(ReportCategory::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Категория №1'),
            'b' => Crypt::encrypt('Комментарий к категории №1'),
        ]);
        DB::connection('accounting_mysql')->table(ReportCategory::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Категория №2'),
            'b' => Crypt::encrypt('Комментарий к категории №2'),
        ]);
        DB::connection('accounting_mysql')->table(ReportCategory::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Категория №3'),
            'b' => Crypt::encrypt('Комментарий к категории №3'),
        ]);

        DB::connection('accounting_mysql')->table(ReportSubCategory::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Подкатегория №1'),
            'b' => Crypt::encrypt('Комментарий к подкатегории №1'),
            'c' => Crypt::encrypt(ReportSubCategory::TYPE_OF_LINK_ALL),
        ]);
        DB::connection('accounting_mysql')->table(ReportSubCategory::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Подкатегория №2'),
            'b' => Crypt::encrypt('Комментарий к подкатегории №2'),
            'c' => Crypt::encrypt(ReportSubCategory::TYPE_OF_LINK_ALL),
        ]);
        DB::connection('accounting_mysql')->table(ReportSubCategory::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Подкатегория №3'),
            'b' => Crypt::encrypt('Комментарий к подкатегории №3'),
            'c' => Crypt::encrypt(ReportSubCategory::TYPE_OF_LINK_ALL),
        ]);

        DB::connection('accounting_mysql')->table(ReportAccount::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Счет №1'),
            'b' => Crypt::encrypt('Комментарий к счету №1'),
        ]);
        DB::connection('accounting_mysql')->table(ReportAccount::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Счет №2'),
            'b' => Crypt::encrypt('Комментарий к счету №2'),
        ]);
        DB::connection('accounting_mysql')->table(ReportAccount::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Счет №3'),
            'b' => Crypt::encrypt('Комментарий к счету №3'),
        ]);

        DB::connection('accounting_mysql')->table(ReportCurrency::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Валюта №1'),
            'b' => Crypt::encrypt('Комментарий к валюте №1'),
        ]);
        DB::connection('accounting_mysql')->table(ReportCurrency::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Валюта №2'),
            'b' => Crypt::encrypt('Комментарий к валюте №2'),
        ]);
        DB::connection('accounting_mysql')->table(ReportCurrency::TABLE_NAME)->insert([
            'a' => Crypt::encrypt('Валюта №3'),
            'b' => Crypt::encrypt('Комментарий к валюте №3'),
        ]);
    }
}
