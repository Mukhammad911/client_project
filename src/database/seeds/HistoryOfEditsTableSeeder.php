<?php

namespace Teleglobal\Accounting\Database\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\User;
use Teleglobal\Accounting\Models\UserCashbox;
use Teleglobal\Accounting\Models\UserGroup;
use Teleglobal\Accounting\Models\UserGroupUser;
use Teleglobal\Accounting\Models\Warehouse;

class HistoryOfEditsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = DB::connection('accounting_mysql')->table(User::TABLE_NAME)->where(User::FIELD_NAME, '=', Crypt::encrypt('admin'))->first();

        $warehouses = DB::connection('accounting_mysql')->table(Warehouse::TABLE_NAME)->get();
        foreach ($warehouses as $warehouse){
            DB::connection('accounting_mysql')->table(HistoryOfEdits::TABLE_NAME)->insert([
                'a' => $adminUser->id,
                'b' => Carbon::now('utc'),
                'c' => Crypt::encrypt('auto generating'),
                'd' => Crypt::encrypt('auto generating'),
                'e' => Crypt::encrypt(Warehouse::SOURCE_NAME),
                'f' => $warehouse->id,
                'g' => Crypt::encrypt('auto generating'),
            ]);
        }

        $users = DB::connection('accounting_mysql')->table(User::TABLE_NAME)->get();
        foreach ($users as $user){
            $groupIds = DB::connection('accounting_mysql')
                ->table(UserGroupUser::TABLE_NAME)
                ->select(UserGroupUser::FIELD_USER_GROUP_ID)
                ->where(UserGroupUser::FIELD_USER_ID, '=', $user->id)
                ->get()
                ->pluck(UserGroupUser::FIELD_USER_GROUP_ID)
                ->toArray();
            $groups = DB::connection('accounting_mysql')
                ->table(UserGroup::TABLE_NAME)
                ->whereIn(UserGroup::FIELD_ID, $groupIds)
                ->get()->toArray();
            $groupsArray = $groups;

            foreach ($groups as $group){
                $groupsArray[] = [
                    '_scenario' => HistoryOfEdits::SCENARIO_CREATE,
                    'id'        => $group->id,
                    'name'      => Crypt::decrypt($group->a),
                    'comment'   => Crypt::decrypt($group->b),
                ];
            }

            DB::connection('accounting_mysql')->table(HistoryOfEdits::TABLE_NAME)->insert([
                'a' => $adminUser->id,                      // user_id
                'b' => Carbon::now('utc'),                  // date_edited
                'c' => Crypt::encrypt('auto generating'),    // ip
                'd' => Crypt::encrypt('auto generating'),    // user_agent
                'e' => Crypt::encrypt(User::SOURCE_NAME),   // source
                'f' => $user->id,                           // source_id
                'g' => Crypt::encrypt(                      // changes
                    json_encode([
                        '_scenario' => HistoryOfEdits::SCENARIO_CREATE,
                        'name'      => Crypt::decrypt($user->a),
                        'email'     => Crypt::decrypt($user->b),
                        'password'  => '**********',
                        'type'      => Crypt::decrypt($user->f),
                        'comment'   => Crypt::decrypt($user->g),
                        'groups'    => $groupsArray,
                    ])
                ),
            ]);
        }

    }
}
