<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\SalaryRecipient;

class CreateSalaryRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * name = a
     * comment = b
     * amount = c
     * currency_id = d
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(SalaryRecipient::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(SalaryRecipient::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->text('a');
            $table->text('b');
            $table->text('c');
            $table->integer('d');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(SalaryRecipient::TABLE_NAME);
    }
}
