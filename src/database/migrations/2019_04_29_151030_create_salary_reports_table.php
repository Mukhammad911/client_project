<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\SalaryReport;

class CreateSalaryReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * warehouse_id = a
     * recipient_id = b
     * date_from = c
     * date_to = d
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(SalaryReport::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(SalaryReport::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->string('a');
            $table->string('b');
            $table->timestamp('c')->nullable();
            $table->timestamp('d')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(SalaryReport::TABLE_NAME);
    }
}
