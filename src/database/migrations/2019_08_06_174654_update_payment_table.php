<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\Payment;

class UpdatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->table(Payment::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('d');
        });

        Schema::connection('accounting_mysql')->table(Payment::TABLE_NAME, function (Blueprint $table) {
            $table->integer('d')->unsigned()->after('c');   // currency_id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
