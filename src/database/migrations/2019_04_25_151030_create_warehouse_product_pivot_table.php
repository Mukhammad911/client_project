<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\WarehouseProductPivot;

class CreateWarehouseProductPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * warehouse_id = a
     * product_id = b
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(WarehouseProductPivot::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(WarehouseProductPivot::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('a');
            $table->integer('b');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(WarehouseProductPivot::TABLE_NAME);
    }
}
