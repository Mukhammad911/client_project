<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\UserGroup;

class CreateUserGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * name = a
     * comment = b
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(UserGroup::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->text('a');
            $table->text('b')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(UserGroup::TABLE_NAME);
    }
}
