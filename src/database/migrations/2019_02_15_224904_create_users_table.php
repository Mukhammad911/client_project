<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * name = a
     * email = b
     * email_verified_at = c
     * password = d
     * remember_token = e
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(User::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(User::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->text('a');                  // name
            $table->text('b');                  // email
            $table->timestamp('c')->nullable(); // email_verified_at
            $table->string('d');                // password
            $table->string('e')->nullable();    // remember_token
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(User::TABLE_NAME);
    }
}
