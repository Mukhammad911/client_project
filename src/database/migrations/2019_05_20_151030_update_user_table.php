<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\User;
use Teleglobal\Accounting\Models\UserCashbox;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists('c');

        Schema::connection('accounting_mysql')->table(User::TABLE_NAME, function (Blueprint $table) {
            $table->text('f');              // type
            $table->text('g')->nullable();  // comment
        });

        Schema::connection('accounting_mysql')->table(UserCashbox::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('b');
            $table->dropColumn('c');
            $table->dropColumn('d');
        });

        Schema::connection('accounting_mysql')->table(UserCashbox::TABLE_NAME, function (Blueprint $table) {
            $table->integer('b')->unsigned()->after('a');   // currency_id
            $table->timestamp('f')->nullable();             // deleted
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
