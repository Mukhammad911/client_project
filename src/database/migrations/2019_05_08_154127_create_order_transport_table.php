<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\OrderTransport;

class CreateOrderTransportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(OrderTransport::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->text(OrderTransport::FIELD_NAME);
            $table->text(OrderTransport::FIELD_COMMENT)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(OrderTransport::TABLE_NAME);
    }
}
