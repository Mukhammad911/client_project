<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\OrderPosition;

class CreateOrderPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(OrderPosition::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->text(OrderPosition::FIELD_NAME);
            $table->text(OrderPosition::FIELD_UNIT);
            $table->decimal(OrderPosition::FIELD_COST, 8, 2)->unsigned();
            $table->integer(OrderPosition::FIELD_CURRENCY_ID)->unsigned();
            $table->text(OrderPosition::FIELD_COMMENT)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(OrderPosition::TABLE_NAME);
    }
}
