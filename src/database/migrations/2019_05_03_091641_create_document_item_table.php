<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\DocumentItem;

class CreateDocumentItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(DocumentItem::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(DocumentItem::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('a')->unsigned();
            $table->text('b');
            $table->integer('c')->unsigned();
            $table->text('d')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(DocumentItem::TABLE_NAME);
    }
}
