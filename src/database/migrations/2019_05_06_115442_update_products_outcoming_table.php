<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ProductsOutcoming;

class UpdateProductsOutcomingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->table(ProductsOutcoming::TABLE_NAME, function (Blueprint $table) {
            $table->decimal('e', 8, 2)->nullable()->unsigned();
            $table->integer('f')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->table(ProductsOutcoming::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('e');
            $table->dropColumn('f');
        });
    }
}
