<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ReportCategory;

class CreateReportCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(ReportCategory::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->text(ReportCategory::FIELD_NAME);
            $table->text(ReportCategory::FIELD_COMMENT)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ReportCategory::TABLE_NAME);
    }
}
