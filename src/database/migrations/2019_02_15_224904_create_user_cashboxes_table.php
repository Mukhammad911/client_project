<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\UserCashbox;

class CreateUserCashboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * user_id = a
     * balance = b
     * currency = c
     * date = d
     * alias = e
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(UserCashbox::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->integer('a')->unsigned();   // user_id
            $table->text('b');                  // balance
            $table->text('c');                  // currency
            $table->timestamp('d')->nullable();             // date
            $table->text('e');                  // alias
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(UserCashbox::TABLE_NAME);
    }
}
