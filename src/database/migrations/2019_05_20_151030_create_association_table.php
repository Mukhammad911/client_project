<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\Association;

class CreateAssociationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(Association::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments(Association::FIELD_ID);
            $table->integer(Association::FIELD_DOC_ID)->unsigned();
            $table->text(Association::FIELD_NAME);
            $table->timestamp(Association::FIELD_DATE)->nullable();
            $table->double(Association::FIELD_AMOUNT, 15, 2)->nullable();
            $table->integer(Association::FIELD_CURRENCY_ID)->nullable()->unsigned();
            $table->integer(Association::FIELD_CASHBOX_ID)->nullable()->unsigned();
            $table->text(Association::FIELD_COMMENT)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(Association::TABLE_NAME);
    }
}
