<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ProductsIncoming;
use Teleglobal\Accounting\Models\ProductsOutcoming;

class UpdateProductsIncomingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->table(ProductsIncoming::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('b');
            $table->dropColumn('c');
            $table->dropTimestamps();
            $table->renameColumn('a', ProductsIncoming::FIELD_PIVOT_ID);
        });
        Schema::connection('accounting_mysql')->table(ProductsIncoming::TABLE_NAME, function (Blueprint $table) {
            $table->integer(ProductsIncoming::FIELD_DOC_ID)->after(ProductsIncoming::FIELD_ID)->unsigned();
            $table->timestamp(ProductsIncoming::FIELD_DATE)->after(ProductsIncoming::FIELD_DOC_ID)->nullable();
        });

        Schema::connection('accounting_mysql')->table(ProductsOutcoming::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('b');
            $table->dropColumn('c');
            $table->dropTimestamps();
            $table->renameColumn('a', ProductsIncoming::FIELD_PIVOT_ID);
        });
        Schema::connection('accounting_mysql')->table(ProductsOutcoming::TABLE_NAME, function (Blueprint $table) {
            $table->integer(ProductsIncoming::FIELD_DOC_ID)->after(ProductsIncoming::FIELD_ID)->unsigned();
            $table->timestamp(ProductsIncoming::FIELD_DATE)->after(ProductsIncoming::FIELD_DOC_ID)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
