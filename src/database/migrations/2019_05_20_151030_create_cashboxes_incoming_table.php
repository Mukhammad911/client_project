<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\CashboxesIncoming;

class CreateCashboxesIncomingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(CashboxesIncoming::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->integer('a')->unsigned()->nullable();   // doc_id
            $table->timestamp('b')->nullable();                         // date
            $table->text('c');                              // key
            $table->integer('d')->unsigned();               // cashbox_id
            $table->double('e', 15, 2);                     // amount
            $table->text('f')->nullable();                  // comment
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(CashboxesIncoming::TABLE_NAME);
    }
}
