<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\HistoryOfEdits;

class CreateEditsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * user_id = a
     * date_edited = b
     * ip = c
     * user_agent = d
     * source = e
     * edited_by_id = f
     * changes = g
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(HistoryOfEdits::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(HistoryOfEdits::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments(HistoryOfEdits::FIELD_ID);
            $table->integer(HistoryOfEdits::FIELD_USER_ID);
            $table->timestamp(HistoryOfEdits::FIELD_DATE_EDITED)->nullable();
            $table->text(HistoryOfEdits::FIELD_IP);
            $table->text(HistoryOfEdits::TABLE_NAME);
            $table->text(HistoryOfEdits::FIELD_SOURCE);
            $table->integer(HistoryOfEdits::FIELD_SOURCE_ID);
            $table->text(HistoryOfEdits::FIELD_CHANGES);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(HistoryOfEdits::TABLE_NAME);
    }
}
