<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * user_id = a
     * user_group_id = b
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(\Teleglobal\Accounting\Models\UserGroupUser::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(\Teleglobal\Accounting\Models\UserGroupUser::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->integer('a');
            $table->integer('b');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(\Teleglobal\Accounting\Models\UserGroupUser::TABLE_NAME);
    }
}
