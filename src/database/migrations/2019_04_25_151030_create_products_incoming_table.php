<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ProductsIncoming;

class CreateProductsIncomingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * pivot_id = a
     * source = b
     * source_id = c
     * amount = d
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ProductsIncoming::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(ProductsIncoming::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('a');
            $table->text('b');
            $table->integer('c');
            $table->double('d', 15, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ProductsIncoming::TABLE_NAME);
    }
}
