<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ReportCategorySubCategoryPivot;

class CreateReportCategorySubCategoryPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(ReportCategorySubCategoryPivot::TABLE_NAME, function (Blueprint $table) {
            $table->increments(ReportCategorySubCategoryPivot::FIELD_ID);
            $table->integer(ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID)->unsigned();
            $table->integer(ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ReportCategorySubCategoryPivot::TABLE_NAME);
    }
}
