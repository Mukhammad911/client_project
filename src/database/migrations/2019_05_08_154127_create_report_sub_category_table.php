<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ReportSubCategory;

class CreateReportSubCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(ReportSubCategory::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->text(ReportSubCategory::FIELD_NAME);
            $table->text(ReportSubCategory::FIELD_COMMENT)->nullable();
            $table->text(ReportSubCategory::FIELD_TYPE_OF_LINK);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ReportSubCategory::TABLE_NAME);
    }
}
