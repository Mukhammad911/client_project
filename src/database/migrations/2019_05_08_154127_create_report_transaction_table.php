<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ReportTransaction;

class CreateReportTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(ReportTransaction::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->integer(ReportTransaction::FIELD_USER_ID)->unsigned();                  // user_id
            $table->integer(ReportTransaction::FIELD_CASHBOX_ID)->unsigned();               // cashbox_id
            $table->integer(ReportTransaction::FIELD_ACCOUNT_ID)->unsigned();               // account_id
            $table->text(ReportTransaction::FIELD_DESCRIPTION)->nullable();                 // description
            $table->integer(ReportTransaction::FIELD_CATEGORY_ID)->unsigned();              // category_id
            $table->integer(ReportTransaction::FIELD_SUBCATEGORY_ID)->unsigned();           // subcategory_id
            $table->decimal(ReportTransaction::FIELD_AMOUNT, 8, 2)->unsigned();             // amount
            $table->integer(ReportTransaction::FIELD_CURRENCY_ID)->unsigned();              // currency_id
            $table->timestamp(ReportTransaction::FIELD_DATE)->nullable();                               // date
            $table->text(ReportTransaction::FIELD_COMMENT)->nullable();                     // comment
            $table->text(ReportTransaction::FIELD_SOURCE);                                  // source
            $table->integer(ReportTransaction::FIELD_SOURCE_ID)->unsigned()->nullable();    // source_id
            $table->integer(ReportTransaction::FIELD_CASHBOX_OUTCOMING_ID)->unsigned();     // cashbox_outcoming_id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ReportTransaction::TABLE_NAME);
    }
}
