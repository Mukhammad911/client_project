<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ReportTransaction;

class UpdateReportTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->table(ReportTransaction::TABLE_NAME, function (Blueprint $table) {
            $table->boolean('n')->default(false);           // 'validated'
            $table->integer('o')->unsigned()->nullable();   // 'validated_by'
            $table->timestamp('p')->nullable();             // 'validated_at'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
