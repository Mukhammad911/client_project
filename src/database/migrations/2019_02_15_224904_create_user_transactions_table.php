<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\UserTransaction;

class CreateUserTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * user_id = a
     * from_cashbox = b
     * to_cashbox = c
     * amount = d
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(UserTransaction::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(UserTransaction::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id');
            $table->integer('a');
            $table->integer('b');
            $table->integer('c');
            $table->text('d');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(UserTransaction::TABLE_NAME);
    }
}
