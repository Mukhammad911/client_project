<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\Payment;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * doc_id = a
     * date = b
     * amount = c
     * currency = d
     * comment = e
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(Payment::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(Payment::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('a')->unsigned();
            $table->timestamp('b')->nullable();
            $table->text('c');
            $table->text('d');
            $table->text('e')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(Payment::TABLE_NAME);
    }
}
