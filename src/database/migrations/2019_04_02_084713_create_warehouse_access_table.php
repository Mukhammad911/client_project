<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\WarehouseAccess;

class CreateWarehouseAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * warehouse_id = a
     * source_type = b
     * source_id = c
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(WarehouseAccess::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(WarehouseAccess::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->integer(WarehouseAccess::FIELD_WAREHOUSE_ID);
            $table->text(WarehouseAccess::FIELD_SOURCE);
            $table->integer(WarehouseAccess::FIELD_SOURCE_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(WarehouseAccess::TABLE_NAME);
    }
}
