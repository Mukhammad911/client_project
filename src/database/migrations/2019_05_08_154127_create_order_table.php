<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\Order;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(Order::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->integer(Order::FIELD_ACCOUNT_ID)->unsigned();
            $table->integer(Order::FIELD_POSITION_ID)->unsigned();
            $table->decimal(Order::FIELD_QUANTITY, 8, 3)->unsigned();
            $table->decimal(Order::FIELD_AMOUNT, 8, 2)->unsigned();
            $table->timestamp(Order::FIELD_DATE_ORDER)->nullable();
            $table->text(Order::FIELD_COMMENT)->nullable();
            $table->timestamp(Order::FIELD_DATE_EXECUTION)->nullable();
            $table->integer(Order::FIELD_ASSOC_TRANSPORT_ID)->unsigned()->nullable();
            $table->decimal(Order::FIELD_ASSOC_TRANSPORT_AMOUNT, 8, 2)->nullable();
            $table->decimal(Order::FIELD_ASSOC_DELIVERY_AMOUNT, 8, 2)->nullable();
            $table->decimal(Order::FIELD_ASSOC_SERVICE_AMOUNT, 8, 2)->nullable();
            $table->decimal(Order::FIELD_ASSOC_DOCUMENTS_AMOUNT, 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(Order::TABLE_NAME);
    }
}
