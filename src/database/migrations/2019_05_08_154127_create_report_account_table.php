<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\ReportAccount;

class CreateReportAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->create(ReportAccount::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->text(ReportAccount::FIELD_NAME);
            $table->text(ReportAccount::FIELD_COMMENT)->nullable();
            $table->text(ReportAccount::FIELD_STATUS)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(ReportAccount::TABLE_NAME);
    }
}
