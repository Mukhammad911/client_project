<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Teleglobal\Accounting\Models\Document;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * doc_number = a
     * doc_type = b
     * source = c
     * source_id = d
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting_mysql')->dropIfExists(Document::TABLE_NAME);

        Schema::defaultStringLength(191);
        Schema::connection('accounting_mysql')->create(Document::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->text('a');
            $table->text('b');
            $table->text('c')->nullable();
            $table->integer('d')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting_mysql')->dropIfExists(Document::TABLE_NAME);
    }
}
