<?php

namespace Teleglobal\Accounting\Listeners;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function onUserLogin($event)
    {
        /*$visit = new UserVisit();
        $visit->userid = $event->user->id;
        $visit->ip = $_SERVER['REMOTE_ADDR'];
        $visit->useragent = $_SERVER['HTTP_USER_AGENT'];
        $visit->time = date("Y-m-d H:i:s");
        $visit->timestamps = false;
        $visit->save();*/

    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event)
    {

    }

    /**
     * Handle user created events.
     */
    public function onUserCreated($event)
    {

    }

    /**
     * Handle user updated events.
     */
    public function onUserUpdated($event)
    {
        #dd($event->user);
    }

    /**
     * Handle user deleted events.
     */
    public function onUserDeleted($event)
    {

    }

    /**
     * Handle user saved events.
     */
    public function onUserSaved($event)
    {

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Teleglobal\Accounting\Events\UserCreated',
            'Teleglobal\Accounting\Listeners\UserEventSubscriber@onUserCreated'
        );
        $events->listen(
            'Teleglobal\Accounting\Events\UserUpdated',
            'Teleglobal\Accounting\Listeners\UserEventSubscriber@onUserUpdated'
        );
        $events->listen(
            'Teleglobal\Accounting\Events\UserDeleted',
            'Teleglobal\Accounting\Listeners\UserEventSubscriber@onUserDeleted'
        );
    }
}