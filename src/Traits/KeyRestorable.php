<?php

namespace Teleglobal\Accounting\Traits;

use Illuminate\Http\Request;
use RuntimeException;

trait KeyRestorable
{
    /**
     * Application encryption key
     *
     * @var string
     */
    protected $accountingEncryptionKey;

    /**
     * Restore application encryption key
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function restoreEncryptionKey(Request $request)
    {

        if(is_null(config('accounting.key'))) {
            if (!$this->accountingEncryptionKey && $request->post('key')) {
                $this->accountingEncryptionKey = 'base64:' . base64_decode($request->post('key')) . '=';

            }elseif (!$this->accountingEncryptionKey && $request->session()->has('acc_e_key')) {
                $this->accountingEncryptionKey = $request->session()->get('acc_e_key');

//            }elseif(!$this->accountingEncryptionKey && $request->hasCookie('acc_e_key')) {
//
//                dd($request->cookie('acc_e_key'));
//                $this->accountingEncryptionKey = $request->cookie('acc_e_key');
//
            }

            $this->validateEncryptionKey();

            config(['accounting.key' => $this->accountingEncryptionKey]);

            $request->session()->put('acc_e_key', $this->accountingEncryptionKey);
            $request->session()->save();
        }

        return true;
    }

    /**
     * Validate application key.
     *
     * @return bool
     */
    protected function validateEncryptionKey()
    {
        if(! $this->accountingEncryptionKey || is_null($this->accountingEncryptionKey)) {
            return redirect()
                ->route('accounting:auth.get.login')
                ->withErrors(['key' => 'Accounting application key is not defined.']);

//             return redirect()
//                ->route('accounting:auth.get.logout')
//                ->withErrors(['key' => 'Accounting application key is not defined.']);

//            throw new RuntimeException('Accounting application key is not defined.');
        }

        // TODO: реализовать валидацию ключа
        return true;
    }
}
