<?php

namespace Teleglobal\Accounting\Traits;

trait EnvValueEditable
{
    /**
     * Set environment variable
     *
     * @param array $values
     *
     * @return bool
     */
    public function setEnvironmentValue($values)
    {
        $envFile = base_path().'/.env';
        $str = file_get_contents($envFile);

        if (0 < count($values)) {
            foreach ($values as $envKey => $envValue) {
                $str .= "\n"; // In case the searched variable is in the last line without \n
                $keyPosition = strpos($str, "{$envKey}=");
                $endOfLinePosition = strpos($str, "\n", $keyPosition);
                $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

                // If key does not exist, add it
                if (!$keyPosition || !$endOfLinePosition || !$oldLine) {
                    $str .= "{$envKey}={$envValue}\n";
                } else {
                    $str = str_replace($oldLine, "{$envKey}={$envValue}", $str);
                }
            }
        }

        $str = substr($str, 0, -1);
        if (!file_put_contents($envFile, $str)) return false;
        return true;
    }
}
