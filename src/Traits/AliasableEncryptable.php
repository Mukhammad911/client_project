<?php

namespace Teleglobal\Accounting\Traits;

use Teleglobal\Accounting\Facades\Encrypter as Crypt;

trait AliasableEncryptable
{
    /**
     * Retrieves a field by reference and decrypt model field value
     *
     * @param $key
     *
     * @return mixed|string|array
     */
    public function getAttribute($key)
    {
        if (in_array($key, array_keys($this->aliasable)))
        {
            $key = $this->aliasable[$key];
        }

        $value = parent::getAttribute($key);

        if (in_array($key, $this->encryptable) && (! is_null($value)))
        {
            // TODO: не должно быть здесь, но пока не понял почему провайдер шифрования не инициализирован на момент первого обращения к модели.
            if(!config('accounting.key'))
            {
                config(['accounting.key' => session()->get('acc_e_key')]);
            }
            //----------------------------------------------------------
            $value = Crypt::decrypt($value);
        }

        if(!empty($this->json) && in_array($key, $this->json) && (!empty($value))){
            $value = json_decode($value, true);
        }

        return $value;
    }

    /**
     * Retrieves a field by reference and decrypt model field value
     *
     * @param $key
     *
     * @return mixed|string
     */
    public function getOriginal($key = NULL, $default = NULL)
    {
        if (in_array($key, array_keys($this->aliasable)))
        {
            $key = $this->aliasable[$key];
        }

        $value = parent::getOriginal($key);

        if (in_array($key, $this->encryptable) && (! is_null($value)))
        {
            // TODO: не должно быть здесь, но пока не понял почему провайдер шифрования не инициализирован на момент первого обращения к модели.
            if(!config('accounting.key'))
            {
                config(['accounting.key' => session()->get('acc_e_key')]);
            }
            //----------------------------------------------------------
            $value = Crypt::decrypt($value);
        }

        if(!empty($this->json) && in_array($key, $this->json) && (!empty($value))){
            $value = json_decode($value, true);
        }

        return $value;
    }

    /**
     * Retrieves a field by reference and encrypt model field value
     *
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, array_keys($this->aliasable)))
        {
            $key = $this->aliasable[$key];
        }

        if(!empty($this->json) && in_array($key, $this->json) && (!empty($value))){
            $value = json_encode($value, true);
        }

        if (in_array($key, $this->encryptable))
        {
            // TODO: не должно быть здесь, но пока не понял почему провайдер шифрования не инициализирован на момент первого обращения к модели.
            if(!config('accounting.key'))
            {
                config(['accounting.key' => session()->get('acc_e_key')]);
            }
            //----------------------------------------------------------
            $value = Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }

    public function toArray()
    {
        $result = [];

        foreach ($this->getAttributes() as $key => $value){

            if(!empty($this->json) && in_array($key, $this->json) && (!empty($value))){
                $value = json_encode($value, true);
            }

            if (in_array($key, $this->encryptable) && (! is_null($value)))
            {
                // TODO: не должно быть здесь, но пока не понял почему провайдер шифрования не инициализирован на момент первого обращения к модели.
                if(!config('accounting.key'))
                {
                    config(['accounting.key' => session()->get('acc_e_key')]);
                }
                //----------------------------------------------------------
                $value = Crypt::decrypt($value);
            }

            $alterKey = $key;
            if($r = array_search($key, $this->aliasable)){
                $alterKey = $r;
            }

            $result[$alterKey] = $value;
        }

        return $result;
    }

    /**
     * @param array|null $attributes
     *
     * @return array
     */
    public function getChanges()
    {
        $attributes = $this->getAttributes();
        $original = $this->getOriginal();

        $changes = [];

        foreach ($attributes as $key => $attribute){
            if (in_array($key, $this->encryptable)){
                // TODO: не должно быть здесь, но пока не понял почему провайдер шифрования не инициализирован на момент первого обращения к модели.
                if(!config('accounting.key'))
                {
                    config(['accounting.key' => session()->get('acc_e_key')]);
                }
                //----------------------------------------------------------
                $attribute = !empty($attribute) ? Crypt::decrypt($attribute) : null;
                $original[$key] = !empty($original[$key]) ? Crypt::decrypt($original[$key]) : null;
            }
            if($attribute != $original[$key]){
                $changes[$key] = $attribute;
            }
        }

        $aliaseble = collect($this->aliasable)->flip()->toArray();

        $result = [];
        foreach ($changes as $key => $value){
            $result[isset($aliaseble[$key]) ? $aliaseble[$key] : $key] = $value;
        }

        return $result;
    }
}
