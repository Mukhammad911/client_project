<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\WarehouseCreated;
use Teleglobal\Accounting\Events\WarehouseUpdated;
use Teleglobal\Accounting\Events\WarehouseDeleted;
use Illuminate\Support\Facades\DB;

class Warehouse extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'i';
    const SOURCE_NAME   = 'Warehouse';

    const FIELD_ID          = 'id';
    const FIELD_NAME        = 'a';
    const FIELD_COMMENT     = 'b';
    const FIELD_ACCESS_TYPE = 'c';

    const ACCESS_TYPE_ALL       = 'all';
    const ACCESS_TYPE_GROUPS    = 'groups';
    const ACCESS_TYPE_USERS     = 'users';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    private $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => WarehouseCreated::class,
        'updated' => WarehouseUpdated::class,
        'deleted' => WarehouseDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
        self::FIELD_ACCESS_TYPE,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'comment'       => self::FIELD_COMMENT,
        'access_type'   => self::FIELD_ACCESS_TYPE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
        'access_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this
            ->hasMany(HistoryOfEdits::class, HistoryOfEdits::FIELD_SOURCE_ID, self::FIELD_ID)
            ->where(HistoryOfEdits::FIELD_SOURCE, Crypt::encrypt(self::SOURCE_NAME))
            ->orderByDesc(HistoryOfEdits::FIELD_DATE_EDITED);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            WarehouseProductPivot::TABLE_NAME,
            WarehouseProductPivot::FIELD_WAREHOUSE_ID,
            WarehouseProductPivot::FIELD_PRODUCT_ID
        )->withPivot(WarehouseProductPivot::FIELD_ID . ' as pivot_id', WarehouseProductPivot::FIELD_WAREHOUSE_ID . ' as warehouse_id');
    }

    /**
     * @param Collection $params
     *
     * @return self
     */
    public static function insertModel(Collection $params)
    {
        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
            'access_type'   => $params->get('access_type'),
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'name'          => ['required', 'max:255'],
            'comment'       => ['nullable'],
            'access_type'   => ['required', 'in:'.self::ACCESS_TYPE_ALL.','.self::ACCESS_TYPE_USERS.','.self::ACCESS_TYPE_GROUPS],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['required', 'unique:'.self::TABLE_NAME.','.self::FIELD_NAME],
        ])->validate();

        $model = new self();
        $model->fill($attributes)->save();

        if(in_array($model->access_type, [self::ACCESS_TYPE_GROUPS, self::ACCESS_TYPE_USERS]) ){
            $sources = [];
            if($model->access_type == self::ACCESS_TYPE_GROUPS){
                $sources = json_decode($params->get('access_user_groups'), false);
            }elseif ($model->access_type == self::ACCESS_TYPE_USERS){
                $sources = json_decode($params->get('access_users'), false);
            }
            foreach ($sources as $source){
                WarehouseAccess::insertModel(collect([
                    'warehouse_id'  => $model->id,
                    'source'        => $model->access_type,
                    'source_id'     => $source,
                ]));
            }
        }

        /**
         * Save changes into the history
         */
        $attributes['_scenario'] = HistoryOfEdits::SCENARIO_CREATE;
        $model->historyOfEdits->setRequiredParams(
            self::SOURCE_NAME,
            $model->getAttribute(self::FIELD_ID),
            $attributes)->save();

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $params
     *
     * @return self
     */
    public static function updateModel(int $id, Collection $params)
    {
        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
            'access_type'   => $params->get('access_type'),
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'name'          => ['required', 'max:255'],
            'comment'       => ['nullable'],
            'access_type'   => ['required', 'in:'.self::ACCESS_TYPE_ALL.','.self::ACCESS_TYPE_USERS.','.self::ACCESS_TYPE_GROUPS],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['required', 'unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$id.','.self::FIELD_ID],
        ])->validate();

        $model = self::find($id);
        $model->fill($attributes)->save();

        WarehouseAccess::where(WarehouseAccess::FIELD_WAREHOUSE_ID, '=', $id)->delete();

        if(in_array($model->access_type, [self::ACCESS_TYPE_GROUPS, self::ACCESS_TYPE_USERS]) ){
            $sources = [];
            if($model->access_type == self::ACCESS_TYPE_GROUPS){
                $sources = json_decode($params->get('access_user_groups'), false);
            }elseif ($model->access_type == self::ACCESS_TYPE_USERS){
                $sources = json_decode($params->get('access_users'), false);
            }
            if(!empty($sources)){
                foreach ($sources as $source){
                    WarehouseAccess::insertModel(collect([
                        'warehouse_id'  => $model->id,
                        'source'        => $model->access_type,
                        'source_id'     => $source,
                    ]));
                }
            }
        }

        return $model;
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function deleteModel(int $id)
    {
        WarehouseAccess::where(WarehouseAccess::FIELD_WAREHOUSE_ID, '=', $id)->delete();
        self::find($id)->delete();

        return true;
    }

    public static function getParamsData(Request $request)
    {
        return [
            'route'         => $request->route()->getName(),
            'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
            'query'         => is_null($request->getQueryString()) ? '' : '?'. $request->getQueryString(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionIndexParams(Request $request)
    {
        $params = [
            'warehouses'    => Warehouse::with('history')->get(),
            'data'          => self::getParamsData($request),
        ];

        return $params;
    }

    /**
     * Prepared parameters for Create and Update forms
     *
     * @return array
     */
    private static function prepareResponseParams()
    {
        $params = [
            'userGroups'    => UserGroup::select()->get(),
            'users'         => User::select()->get(),
        ];

        return $params;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionCreateParams(Request $request)
    {
        return array_merge(
            self::prepareResponseParams(),
            [
                'data' => self::getParamsData($request),
            ]
        );
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return array
     */
    public static function getActionEditParams(int $id, Request $request)
    {
        $accessSources = WarehouseAccess::select(WarehouseAccess::FIELD_SOURCE_ID)->where(WarehouseAccess::FIELD_WAREHOUSE_ID, '=', $id)->get();
        $sources = [];
        foreach ($accessSources as $accessSource){
            $sources[] = $accessSource->source_id;
        }

        return array_merge(
            self::prepareResponseParams(),
            [
                'warehouse' => self::find($id),
                'sources'   => $sources,
                'data'      => self::getParamsData($request),
            ]
        );
    }

    /**
     * Return all products stocks from $this warehouse
     *
     * @return array
     */
    public function getStocksOfWarehouse()
    {
        // todo переделать под нормальные реляции
        $products = $this->products()->get();
        $pivots = WarehouseProductPivot::where(WarehouseProductPivot::FIELD_WAREHOUSE_ID, '=', $this->getAttribute(self::FIELD_ID))->get();

        $pivotsId = [];
        foreach ($pivots as $pivot){
            $pivotsId[] = $pivot->getAttribute(WarehouseProductPivot::FIELD_ID);
        }

        $incoming = ProductsIncoming::select(
            ProductsIncoming::FIELD_PIVOT_ID . ' as pivot_id', DB::raw('SUM(' . ProductsIncoming::FIELD_AMOUNT . ') as amount'))
            ->whereIn(ProductsIncoming::FIELD_PIVOT_ID, $pivotsId)
            ->groupBy(ProductsIncoming::FIELD_PIVOT_ID)->get()->toArray();
        $incoming = collect($incoming)->pluck('amount', 'pivot_id')->toArray();

        $outcoming = ProductsOutcoming::select(
            ProductsOutcoming::FIELD_PIVOT_ID . ' as pivot_id', DB::raw('SUM(' . ProductsOutcoming::FIELD_AMOUNT . ') as amount'))
            ->whereIn(ProductsOutcoming::FIELD_PIVOT_ID, $pivotsId)
            ->groupBy(ProductsOutcoming::FIELD_PIVOT_ID)->get();
        $outcoming = collect($outcoming)->pluck('amount', 'pivot_id')->toArray();

        $result = [];
        foreach ($products as $product){

            $in = (!empty($incoming[$product->pivot->id])) ? $incoming[$product->pivot->id] : 0;
            $out = (!empty($outcoming[$product->pivot->id])) ? $outcoming[$product->pivot->id] : 0;

            $product->setAttribute('stock', $in - $out);
            $result[] = $product;
        }

        return $result;
    }

    /**
     * Writing off all products from current warehouse
     *
     * @param Document $docOutcoming
     *
     * @return array
     */
    public function clearBalance(Document $docOutcoming)
    {
        $stocks = $this->getStocksOfWarehouse();

        $writtenOffProducts = [];
        foreach ($stocks as $stock) {
            $writtenOffProducts[] = ProductsOutcoming::insertModel(
                $docOutcoming->id,
                $docOutcoming->created_at,
                $this->id,
                $stock->getAttribute(Product::FIELD_ID),
                $stock->stock
            );
        }

        return $writtenOffProducts;
    }

    /**
     * Add wrote off products to the specified warehouse
     *
     * @param Document $docIncoming
     * @param int $recipientWarehouseId
     * @param array $writtenOffProducts
     *
     * @return bool
     */
    public static function setOnBalance(Document $docIncoming, int $recipientWarehouseId, array $writtenOffProducts = [])
    {
        foreach ($writtenOffProducts as $writtenOffProduct) {
            ProductsIncoming::insertModel(
                $docIncoming->id,
                $docIncoming->created_at,
                $recipientWarehouseId,
                $writtenOffProduct->id,
                $writtenOffProduct->amount
            );
        }

        return true;
    }



}
