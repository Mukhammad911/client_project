<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\UserCreated;
use Teleglobal\Accounting\Events\UserUpdated;
use Teleglobal\Accounting\Events\UserDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'a';
    const SOURCE_NAME = 'User';

    const FIELD_ID                  = 'id';
    const FIELD_NAME                = 'a';
    const FIELD_EMAIL               = 'b';
    const FIELD_EMAIL_VERIFIED_AT   = 'c';
    const FIELD_PASSWORD            = 'd';
    const FIELD_REMEMBER_TOKEN      = 'e';
    const FIELD_TYPE                = 'f';
    const FIELD_COMMENT             = 'g';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    private $historyOfEdits = null;

    const TYPE_A    = 'a';
    const TYPE_GA   = 'ga';

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => UserCreated::class,
        'updated' => UserUpdated::class,
        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_TYPE,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'name'              => self::FIELD_NAME,
        'email'             => self::FIELD_EMAIL,
        'email_verified_at' => self::FIELD_EMAIL_VERIFIED_AT,
        'password'          => self::FIELD_PASSWORD,
        'remember_token'    => self::FIELD_REMEMBER_TOKEN,
        'type'              => self::FIELD_TYPE,
        'comment'           => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
        'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visits()
    {
        return $this
            ->hasMany(UserVisit::class,UserVisit::FIELD_USER_ID, self::FIELD_ID)
            ->orderByDesc(UserVisit::FIELD_TIME);
    }

    /**
     * History of editing entity current user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this
            ->hasMany(HistoryOfEdits::class, HistoryOfEdits::FIELD_SOURCE_ID, self::FIELD_ID)
            ->where(HistoryOfEdits::FIELD_SOURCE, Crypt::encrypt(self::SOURCE_NAME))
            ->orderByDesc(HistoryOfEdits::FIELD_DATE_EDITED);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cashboxes()
    {
        return $this
            ->hasMany(UserCashbox::class, UserCashbox::FIELD_USER_ID, 'id')
            ->whereNull(UserCashbox::FIELD_DELETED);
    }

    public function groupsPivot()
    {
        return $this->hasMany(UserGroupUser::class, UserGroupUser::FIELD_USER_ID, self::FIELD_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function groups()
    {
        return $this->hasManyThrough(
            UserGroup::class,
            UserGroupUser::class,
            UserGroupUser::FIELD_USER_ID,
            UserGroup::FIELD_ID,
            self::FIELD_ID,
            UserGroupUser::FIELD_USER_ID
        );
    }

    /**
     * @param Collection $params
     *
     * @return $this
     *
     * @throws \Exception
     */
    public static function insertModel(Collection $params)
    {
        // Костыль для сокрытия от клиента типа пользователя
        $params->put('type', 'a');

        /* validate params */
        Validator::make([
            'name'                  => $params->get('name'),
            'email'                 => $params->get('email'),
            'password'              => $params->get('password'),
            'password_confirmation' => $params->get('password_confirmation'),
            'type'                  => $params->get('type'),
            'comment'               => $params->get('comment'),
        ], [
            'name'      => ['required', 'max:255'],
            'email'     => ['required', 'email', 'max:255'],
            'password'  => ['required', 'confirmed', 'min:6','max:255'],
            'type'      => ['required', 'in:'.self::TYPE_A.','.self::TYPE_GA],
            'comment'   => ['nullable', 'max:255'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
            'email' => Crypt::encrypt($params->get('email')),
        ], [
            'name'  => ['required', 'unique:'.User::TABLE_NAME.','.User::FIELD_NAME],
            'email' => ['required', 'unique:'.User::TABLE_NAME.','.User::FIELD_EMAIL],
        ])->validate();

        /* save new user */
        $model = new self();
        $model
            ->fill([
                'name'      => $params->get('name'),
                'email'     => $params->get('email'),
                'password'  => Hash::make($params->get('password')),
                'type'      => $params->get('type'),
                'comment'   => $params->get('comment'),
            ])
            ->save();
        /* set user changes into history */
        $model->historyOfEdits->addChangedParam([
            '_scenario' => HistoryOfEdits::SCENARIO_CREATE,
            'name'      => $model->name,
            'email'     => $model->email,
            'password'  => '**********',
            'type'      => $model->type,
            'comment'   => $model->comment,
        ]);

        /* save groups */
//        $userGroupsParams = collect(json_decode($params->get('user_groups')));
//
//        $model = self::updateGroups($model, $userGroupsParams);

        /* save cashboxes */
        $newCashboxes = $params->get('new_cashboxes');

        $model = self::updateCashboxes($model, $newCashboxes);

        /* save changes into history */
        $model->historyOfEdits
            ->setRequiredParams(User::SOURCE_NAME, $model->id)
            ->save();

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $params
     *
     * @return mixed
     * @throws \Exception
     */
    public static function updateModel(int $id, Collection $params)
    {
        // Костыль для сокрытия от клиента типа пользователя
        $params->put('type', 'a');

        $attributes = [
            'name'                  => $params->get('name'),
            'email'                 => $params->get('email'),
            'password'              => $params->get('password'),
            'password_confirmation' => $params->get('password_confirmation'),
            'type'                  => $params->get('type'),
            'comment'               => $params->get('comment'),
        ];

        /* validate data */
        Validator::make($attributes, [
            'name'      => ['required', 'max:255'],
            'email'     => ['required', 'email', 'max:255'],
            'password'  => ['nullable', 'confirmed', 'min:6','max:255'],
            'type'      => ['required', 'min:1','max:2'],
            'comment'   => ['nullable', 'max:255'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
            'email' => Crypt::encrypt($params->get('email')),
        ], [
            'name'  => 'unique:'.User::TABLE_NAME.','.User::FIELD_NAME.','.$id.','.User::FIELD_ID,
            'email' => 'unique:'.User::TABLE_NAME.','.User::FIELD_EMAIL.','.$id.','.User::FIELD_ID,
        ])->validate();

        if(empty($attributes['password'])){ unset($attributes['password']); }

        /* save new user */
        $model = self::find($id);
        $model->fill($attributes);

        $changes = $model->getChanges();

        $model->save();

        /* set user changes into history */
        $model->historyOfEdits->addChangedParam(array_merge(
            ['_scenario' => HistoryOfEdits::SCENARIO_UPDATE],
            $changes
        ));

        /* save groups */
        $userGroupsParams = collect(json_decode($params->get('user_groups')));
        $model = self::updateGroups($model, $userGroupsParams);

        /* save cashboxes */
        $newCashboxes = $params->get('new_cashboxes');
        $oldCashboxes = $params->get('old_cashboxes');
        $model = self::updateCashboxes($model, $newCashboxes, $oldCashboxes);

        /* save changes into history */
        $model->historyOfEdits
            ->setRequiredParams(User::SOURCE_NAME, $model->id)
            ->save();

        return $model;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public static function deleteModel(int $id)
    {
        /*
         * каскадное удаление не используется, так как
         * для большей анонимности связей в БД нет
         * все удаляем вручную
         */
        $model = self::find($id);
        $model->groupsPivot()->delete();
        $model->cashboxes()->delete();
        $model->visits()->delete();
        $model->history()->delete();
        $model->delete();

        return true;
    }

    private static function updateGroups(User $model, Collection $userGroupsParams)
    {
        $oldUserGroups = collect($model->groupsPivot()->get()->toArray())->pluck('user_group_id');
        $newUserGroups = $userGroupsParams;

        $diffOldIds = $oldUserGroups->isNotEmpty() ? $oldUserGroups->diff($newUserGroups) : [];
        $diffNewIds = $newUserGroups->isNotEmpty() ? $newUserGroups->diff($oldUserGroups) : [];

        $groupsChanges = [];
        foreach ($diffNewIds as $diffNewId){
            if(UserGroupUser::insertModel($model->id, (int)$diffNewId) == true ){
                $userGroupModel = UserGroup::find((int)$diffNewId);
                $groupsChanges[] = [
                    '_scenario' => HistoryOfEdits::SCENARIO_CREATE,
                    'id'        => $userGroupModel->id,
                    'name'      => $userGroupModel->name,
                    'comment'   => $userGroupModel->comment,
                ];
            }
        }
        foreach ($diffOldIds as $diffOldId){
            if(UserGroupUser::deleteModel($model->id, (int)$diffOldId) == true ){
                $userGroupModel = UserGroup::find((int)$diffOldId);
                $groupsChanges[] = [
                    '_scenario' => HistoryOfEdits::SCENARIO_DELETE,
                    'id'        => $userGroupModel->id,
                    'name'      => $userGroupModel->name,
                    'comment'   => $userGroupModel->comment,
                ];
            }
        }
        /* set groups changes into history */
        $model->historyOfEdits->addChangedParam(['groups' => $groupsChanges]);

        return $model;
    }

    private static function updateCashboxes(User $model, $newCashboxesParams, $oldCashboxesParams = [])
    {
        $cashboxesChanges = [];
        $originCashboxIds = collect($model->cashboxes()->select('id')->get()->toArray())->pluck('id');

        /* create new cashboxes */
        if(!empty($newCashboxesParams)){
            $i = 0;
            while(isset($newCashboxesParams['balance'][$i])
                && isset($newCashboxesParams['currency_id'][$i])
                && isset($newCashboxesParams['datetime'][$i])) {

                /* save new cashbox */
                $cashboxParams = [
                    'user_id'       => $model->id,
                    'currency_id'   => $newCashboxesParams['currency_id'][$i],
                    'alias'         => $newCashboxesParams['alias'][$i],
                ];
                $cashbox = UserCashbox::insertModel(collect($cashboxParams));

                /* save start_balance in cashboxIncoming */
                $cashboxIncomingParams = [
                    'doc_id'        => null,
                    'date'          => (new \DateTime($newCashboxesParams['datetime'][$i]))->format('Y-m-d H:i:s'),
                    'key'           => CashboxesIncoming::KEY_START_BALANCE,
                    'cashbox_id'    => $cashbox->id,
                    'amount'        => $newCashboxesParams['balance'][$i],
                    'comment'       => null,
                ];
                $cashboxIncoming = CashboxesIncoming::insertModel(collect($cashboxIncomingParams));

                $cashboxesChanges[] = [
                    '_scenario'     => HistoryOfEdits::SCENARIO_CREATE,
                    'balance'       => $cashboxIncoming->amount,
                    'currency'      => $cashbox->currency->code . ' - ' . $cashbox->currency->country,
                    'date'          => $cashboxIncoming->date,
                    'alias'         => $cashbox->alias,
                ];

                $i++;
            }
        }

        $oldCashboxes = $oldCashboxesParams;

        /* delete cashboxes */
        $oldCashboxIds = !empty($oldCashboxes) ? collect($oldCashboxes['id']) : collect([]);

        $mustDeleted = $originCashboxIds->diff($oldCashboxIds);
        foreach ($mustDeleted as $key => $id){
            UserCashbox::deleteModel($id);
            $deletedCashbox = UserCashbox::select()
                ->with('incomingStartBalance')
                ->with('currency')
                ->where(UserCashbox::FIELD_ID, '=', $id)
                ->first();
            CashboxesIncoming::deleteModel($id);
            $cashboxesChanges[] = [
                '_scenario' => HistoryOfEdits::SCENARIO_DELETE,
                'balance'   => $deletedCashbox->incomingStartBalance->first()->amount,
                'currency'  => $deletedCashbox->currency->code . ' - ' . $deletedCashbox->currency->country,
                'date'      => $deletedCashbox->incomingStartBalance->first()->date,
                'alias'     => $deletedCashbox->alias,
            ];

        }

        /* update exists cashboxes */
        if (!empty($oldCashboxes)) {
            $i = 0;
            while (isset($oldCashboxes['balance'][$i])
                && isset($oldCashboxes['alias'][$i])
                && isset($oldCashboxes['datetime'][$i])) {

                $partCashboxChages = [];

                $cashbox = UserCashbox::find($oldCashboxes['id'][$i]);
                if($cashbox->alias !== $oldCashboxes['alias'][$i]){
                    $cashbox->alias = $oldCashboxes['alias'][$i];
                    $cashbox->save();
                    $partCashboxChages['balance'] = number_format($oldCashboxes['balance'][$i], 2);
                    $partCashboxChages['currency'] = $cashbox->currency->code . ' - ' . $cashbox->currency->country;
                    $partCashboxChages['date'] = (new \DateTime($oldCashboxes['datetime'][$i]))->format('Y-m-d H:i:s');
                    $partCashboxChages['alias'] = $oldCashboxes['alias'][$i];
                }
                $incomingStartBalance = $cashbox->incomingStartBalance()->first();
                if(number_format($incomingStartBalance->amount, 2) !== number_format($oldCashboxes['balance'][$i], 2)){
                    $incomingStartBalance->amount = number_format($oldCashboxes['balance'][$i], 2);
                    $incomingStartBalance->save();
                    $partCashboxChages['balance'] = number_format($oldCashboxes['balance'][$i], 2);
                    $partCashboxChages['currency'] = $cashbox->currency->code . ' - ' . $cashbox->currency->country;
                    $partCashboxChages['date'] = (new \DateTime($oldCashboxes['datetime'][$i]))->format('Y-m-d H:i:s');
                    $partCashboxChages['alias'] = $oldCashboxes['alias'][$i];
                }
                if((new \DateTime($incomingStartBalance->date))->format('Y-m-d H:i:s') !== (new \DateTime($oldCashboxes['datetime'][$i]))->format('Y-m-d H:i:s')){
                    $incomingStartBalance->date = (new \DateTime($oldCashboxes['datetime'][$i]))->format('Y-m-d H:i:s');
                    $incomingStartBalance->save();
                    $partCashboxChages['balance'] = number_format($oldCashboxes['balance'][$i], 2);
                    $partCashboxChages['currency'] = $cashbox->currency->code . ' - ' . $cashbox->currency->country;
                    $partCashboxChages['date'] = (new \DateTime($oldCashboxes['datetime'][$i]))->format('Y-m-d H:i:s');
                    $partCashboxChages['alias'] = $oldCashboxes['alias'][$i];
                }
                if(!empty($partCashboxChages)){
                    $incomingStartBalance
                        ->historyOfEdits
                        ->setRequiredParams(
                            CashboxesIncoming::SOURCE_NAME,
                            $incomingStartBalance->id,
                            array_merge(
                                ['_scenario' => HistoryOfEdits::SCENARIO_UPDATE],
                                $partCashboxChages
                            ));
                    $cashboxesChanges[] = array_merge(
                        ['_scenario' => HistoryOfEdits::SCENARIO_UPDATE],
                        $partCashboxChages
                    );
                }
                $i++;
            }
        }

        /* set cashboxes changes into history */
        $model->historyOfEdits->addChangedParam(['cashboxes' => $cashboxesChanges]);

        return $model;
    }

    /**
     * @return array
     */
    public static function getFields()
    {
        $fields = [
            'fields' => [
                'action'            => __('accounting::users.page.list.fields_of_history.action'),
                'edited_by_user'    => __('accounting::users.page.list.fields_of_history.edited_by_user'),
                'action_date'       => __('accounting::users.page.list.fields_of_history.edited_date'),
                'name'              => __('accounting::users.page.list.fields_of_history.name'),
                'email'             => __('accounting::users.page.list.fields_of_history.email'),
                'password'          => __('accounting::users.page.list.fields_of_history.password'),
                'type'              => __('accounting::users.page.list.fields_of_history.type'),
                'comment'           => __('accounting::users.page.list.fields_of_history.comment'),
            ],
            'groups_fields' => [
                'action'    => __('accounting::users.page.list.fields_of_history.group.action'),
                'name'      => __('accounting::users.page.list.fields_of_history.group.name'),
            ],
            'cashboxes_fields' => [
                'action'    => __('accounting::users.page.list.fields_of_history.cashbox.action'),
                'balance'   => __('accounting::users.page.list.fields_of_history.cashbox.balance'),
                'currency'  => __('accounting::users.page.list.fields_of_history.cashbox.currency'),
                'date'      => __('accounting::users.page.list.fields_of_history.cashbox.date'),
                'alias'     => __('accounting::users.page.list.fields_of_history.cashbox.alias'),
            ],
        ];

        return $fields;
    }

    /**
     * @return array
     */
    public static function getUsersList()
    {
        $userList = self::select('*')
            ->with(['visits' => function($query){
                return $query->first();
            }])
            ->with(['history' => function($query){
                return $query->with('user');
            }])
            ->with(['cashboxes' => function($query){
                return $query;
            }])
            ->get();

        $editsHistory = [];
        foreach ($userList as $user){
            foreach ($user->history as $item) {

                $changes = json_decode($item->getAttribute(HistoryOfEdits::FIELD_CHANGES));

                $time = strtotime($item->date_edited);

                $change = [
                    'edited_by_user'    => $item->user->name,
                    'edited_date'       => Carbon::createFromTimestamp($time)->format(config('accounting.app.datetime_format')),
                ];

                foreach ($changes as $key => $value){
                    if($key === 'password'){
                        $change[$key] = '**********';
                    }elseif ($key === 'type'){
                        $change[$key] = strtoupper($value);
                    }elseif (!is_array($value)){
                        $change[$key] = $value;
                    }else{
                        foreach ($value as $arrayKey => $arrayValue){
                            if($key === 'cashboxes'){

                                try{
                                    $date = $arrayValue->date->date;
                                    $parse_unix = strtotime($arrayValue->date->date);
                                }catch (\Exception $e){
                                    $date = $arrayValue->date;
                                    $parse_unix = strtotime($arrayValue->date);
                                }

                                $arrayValue->date = Carbon::createFromTimestamp($parse_unix)->format(config('accounting.app.date_format'));
                            }
                            $change[$key][$arrayKey] = $arrayValue;
                        }
                    }
                }
                $editsHistory[$user->id][] = $change;
            }
        }
        return [
            'users'     => $userList,
            'history'   => $editsHistory
        ];
    }
}
