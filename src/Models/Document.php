<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Nexmo\Call\Collection;
use phpDocumentor\Reflection\Types\Integer;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class Document extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'u';
    const SOURCE_NAME = 'Document';

    const FIELD_ID          = 'id';
    const FIELD_DOC_NUMBER  = 'a';
    const FIELD_DOC_TYPE    = 'b';
    const FIELD_SOURCE      = 'c';
    const FIELD_SOURCE_ID   = 'd';
    const FIELD_CREATED_AT  = 'created_at';
    const FIELD_UPDATED_AT  = 'updated_at';

    const SOURCE_WAREHOUSE  = 'warehouse';
    const SOURCE_CLIENT     = 'client';

    const DOC_TYPE_INCOMES              = 'incomes';
    const DOC_TYPE_OUTCOMES             = 'outcomes';
    const DOC_TYPE_TRANSFERING_STOCKS   = 'transfering_stocks';
    const DOC_TYPE_REPORT_TRANSACTION   = 'report_transaction';
    const DOC_TYPE_CASHBOXES_INCOMING   = 'cashboxes_incoming';

    protected $table = self::TABLE_NAME;

    public $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_DOC_NUMBER,
        self::FIELD_DOC_TYPE,
        self::FIELD_SOURCE,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'doc_number'    => self::FIELD_DOC_NUMBER,
        'doc_type'      => self::FIELD_DOC_TYPE,
        'source'        => self::FIELD_SOURCE,
        'source_id'     => self::FIELD_SOURCE_ID,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_DOC_NUMBER,
        self::FIELD_DOC_TYPE,
        'doc_number',
        'doc_type',
        'source',
        'source_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Document constructor.
     *
     * @param array $attributes
     */


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    public function productsIncoming(){
        return $this->hasMany(ProductsIncoming::class, ProductsIncoming::FIELD_DOC_ID, self::FIELD_ID);
    }

    public function productsOutcoming(){
        return $this->hasMany(ProductsOutcoming::class, ProductsOutcoming::FIELD_DOC_ID, self::FIELD_ID);
    }

    public function payment(){
        return $this->hasOne(Payment::class, Payment::FIELD_DOC_ID, self::FIELD_ID);
    }

    public function payments(){
        return $this->hasMany(Payment::class, Payment::FIELD_DOC_ID, self::FIELD_ID);
    }

    public function history(){
        return $this
            ->hasMany(HistoryOfEdits::class, HistoryOfEdits::FIELD_SOURCE_ID, self::FIELD_ID)
            ->where(HistoryOfEdits::FIELD_SOURCE, Crypt::encrypt(self::SOURCE_NAME))
            ->orderByDesc(HistoryOfEdits::FIELD_DATE_EDITED);
    }

    public function docItems()
    {
        return $this->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID);
    }

    public function docItemsUser()
    {
        return $this
            ->hasOne(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(User::SOURCE_NAME));
    }

    public function docItemsUsers()
    {
        return $this
            ->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(User::SOURCE_NAME));
    }

    public function docItemsUserCashboxes()
    {
        return $this
            ->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(UserCashbox::SOURCE_NAME));
    }

    public function docItemsAccount()
    {
        return $this
            ->hasOne(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(ReportAccount::SOURCE_NAME));
    }

    public function docItemsAccounts()
    {
        return $this
            ->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(ReportAccount::SOURCE_NAME));
    }

    public function docItemsClient()
    {
        return $this
            ->hasOne(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(DocumentItem::SOURCE_CLIENT));
    }

    public function docItemsClients()
    {
        return $this
            ->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(DocumentItem::SOURCE_CLIENT));
    }

    public function docItemsCashbox(){
        return $this
            ->hasOne(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(DocumentItem::SOURCE_USER_CASHBOX));
    }

    public function docItemsCashboxes(){
        return $this
            ->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(DocumentItem::SOURCE_USER_CASHBOX));
    }

    public function docItemsReportCurrency(){
        return $this
            ->hasOne(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(DocumentItem::SOURCE_REPORT_CURRENCY));
    }

    public function docItemsTransports(){
        return $this
            ->hasMany(DocumentItem::class, DocumentItem::FIELD_DOC_ID, self::FIELD_ID)
            ->where(DocumentItem::FIELD_SOURCE, '=', Crypt::encrypt(DocumentItem::SOURCE_TRANSPORT));
    }

    public function associations(){
        return $this->hasMany(Association::class, Association::FIELD_DOC_ID, self::FIELD_ID);
    }

    /**
     * @return string
     */
    private static function getNewDocNumber()
    {
        return (string)strtotime(new Carbon('utc'));
    }

    /**
     * @param $docType
     * @param $params
     *
     * @return bool|Document
     */
    public static function insertModel($docType, $params)
    {
        $attributes = [
            'doc_number'    => static::getNewDocNumber(),
            'doc_type'      => $docType,
            'source'        => $params->get('source'),
            'source_id'     => $params->get('source_id'),
        ];

        switch ($docType){

            case self::DOC_TYPE_INCOMES:
                $doc = new Document(['timestamps' => true]);
                $doc->fill($attributes)->save();
                $doc->historyOfEdits->setRequiredParams(self::SOURCE_NAME, $doc->id, $attributes)->save();

                $products = $params->get('products');

                if(!empty($products)){
                    $date = new Carbon($params->get('income_date'));

                    for ($i = 0; $i < count($products['id']); $i++){
                        ProductsIncoming::insertModel(
                            $doc->id,
                            $date,
                            $params->get('warehouse'),
                            $products['id'][$i],
                            $products['quantity'][$i]
                        );
                    }
                }
                if($params->get('payment') == 'on'){
                    Payment::insertModel(
                        $doc->id,
                        new Carbon($params->get('payment_date')),
                        $params->get('payment_amount'),
                        $params->get('payment_currency_id'),
                        null
                    );
                }

                break;

            case self::DOC_TYPE_OUTCOMES:

                $doc = new Document(['timestamps' => true]);
                $doc->fill($attributes)->save();
                $doc->historyOfEdits->setRequiredParams(self::SOURCE_NAME, $doc->id, $attributes)->save();

                $products = $params->get('products');

                $date = new Carbon($params->get('outcome_date'));

                if(!empty($products['new'])){
                    foreach ($products['new'] as $product){
                        ProductsOutcoming::insertModel(
                            $doc->id,
                            $date,
                            $params->get('warehouse_id'),
                            $product['product_id'],
                            $product['quantity'],
                            $params->get('currency_id'),
                            $product['cost']
                        );
                        if(!empty($product['addWarehouses'])){
                            foreach ($product['addWarehouses'] as $addWarehouse){
                                ProductsOutcoming::insertModel(
                                    $doc->id,
                                    $date,
                                    $addWarehouse['warehouse_id'],
                                    $product['product_id'],
                                    $addWarehouse['quantity'],
                                    $params->get('currency_id'),
                                    $product['cost']
                                );
                            }
                        }
                    }
                }

                if($params->get('client_id')){
                    DocumentItem::insertModel($doc->id, Client::SOURCE_NAME, $params->get('client_id'));
                }
                if($params->get('transport_id')){
                    DocumentItem::insertModel($doc->id, Transport::SOURCE_NAME, $params->get('transport_id'));
                }
                if($params->get('assoc_dealer_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_DEALER,
                        'date'          => $params->get('assoc_dealer_date'),
                        'amount'        => $params->get('assoc_dealer_amount'),
                        'currency_id'   => $params->get('assoc_dealer_currency'),
                    ]);
                }
                if($params->get('assoc_transport_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_TRANSPORT,
                        'date'          => $params->get('assoc_transport_date'),
                        'amount'        => $params->get('assoc_transport_amount'),
                        'currency_id'   => $params->get('assoc_transport_currency'),
                    ]);
                }
                if($params->get('assoc_travel_card_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_TRAVEL_CARD,
                        'date'          => $params->get('assoc_travel_card_date'),
                        'amount'        => $params->get('assoc_travel_card_amount'),
                        'currency_id'   => $params->get('assoc_travel_card_currency'),
                    ]);
                }
                if($params->get('assoc_documents_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_DOCUMENTS,
                        'date'          => $params->get('assoc_documents_date'),
                        'amount'        => $params->get('assoc_documents_amount'),
                        'currency_id'   => $params->get('assoc_documents_currency'),
                    ]);
                }
                if($params->get('assoc_seller_dealer_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_SELLER_DEALER,
                        'date'          => $params->get('assoc_seller_dealer_date'),
                        'amount'        => $params->get('assoc_seller_dealer_amount'),
                        'currency_id'   => $params->get('assoc_seller_dealer_currency'),
                    ]);
                }
                if($params->get('assoc_warehouse_payd_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_WAREHOUSE_PAYD,
                        'date'          => $params->get('assoc_warehouse_payd_date'),
                        'amount'        => $params->get('assoc_warehouse_payd_amount'),
                        'currency_id'   => $params->get('assoc_warehouse_payd_currency'),
                    ]);
                }
                if($params->get('assoc_outcome_ab_check') == 'on'){
                    Association::create([
                        'doc_id'        => $doc->id,
                        'name'          => Association::NAME_OUTCOME_AB,
                        'date'          => $params->get('assoc_outcome_ab_date'),
                        'amount'        => $params->get('assoc_outcome_ab_amount'),
                        'currency_id'   => $params->get('assoc_outcome_ab_currency'),
                        'cashbox_id'    => $params->get('assoc_outcome_ab_cashbox'),
                    ]);
                }

                break;

            case self::DOC_TYPE_TRANSFERING_STOCKS:
                $docOutcoming = new Document([
                    'timestamps'    => true,
                    'source'        => Warehouse::SOURCE_NAME,
                    'source_id'     => $params['recipientId']
                ]);
                $docOutcoming->fill($attributes)->save();
                $docOutcoming->historyOfEdits->setRequiredParams(self::SOURCE_NAME, $docOutcoming->id, $attributes)->save();

                $writtenOffProducts = Warehouse::where(Warehouse::FIELD_ID, '=', $params['senderId'])->first()->clearBalance($docOutcoming);

                $docIncoming = new Document([
                    'timestamps'    => true,
                    'source'        => Warehouse::SOURCE_NAME,
                    'source_id'     => $params['senderId']
                ]);
                $docIncoming->fill($attributes)->save();
                $docIncoming->historyOfEdits->setRequiredParams(self::SOURCE_NAME, $docIncoming->id, $attributes)->save();

                Warehouse::setOnBalance($docIncoming, $params['recipientId'], $writtenOffProducts);

                break;

            case self::DOC_TYPE_CASHBOXES_INCOMING:
                $doc = new Document(['timestamps' => true]);
                $doc->fill($attributes)->save();
                $doc->historyOfEdits->setRequiredParams(self::SOURCE_NAME, $doc->id, $attributes)->save();

                if($params->get('user_id') !== null){
                    DocumentItem::insertModel($doc->id, DocumentItem::SOURCE_USER, $params->get('user_id'));
                }
                if($params->get('account_id') !== null) {
                    DocumentItem::insertModel($doc->id, DocumentItem::SOURCE_ACCOUNT, $params->get('account_id'));
                }
                if($params->get('client_id') !== null) {
                    DocumentItem::insertModel($doc->id, DocumentItem::SOURCE_CLIENT, $params->get('client_id'));
                }
                if($params->get('currency_id') !== null) {
                    DocumentItem::insertModel($doc->id, DocumentItem::SOURCE_REPORT_CURRENCY, $params->get('currency_id'));
                }

                return $doc;
                break;

            default:
                break;
        }


        return true;

    }

    public function deleteModel()
    {
        $this->delete();
        return true;
    }


}
