<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Nexmo\Call\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\WarehouseCreated;
use Teleglobal\Accounting\Events\WarehouseUpdated;
use Teleglobal\Accounting\Events\WarehouseDeleted;
use Illuminate\Support\Facades\DB;

class SalaryReport extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 't';
    const SOURCE_NAME   = 'SalaryReport';

    const FIELD_ID              = 'id';
    const FIELD_WAREHOUSE_ID    = 'a';
    const FIELD_RECIPIENT_ID    = 'b';
    const FIELD_DATE_FROM       = 'c';
    const FIELD_DATE_TO         = 'd';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    private $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        /*'created' => WarehouseCreated::class,
        'updated' => WarehouseUpdated::class,
        'deleted' => WarehouseDeleted::class,*/
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [

    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'warehouse_id'  => self::FIELD_WAREHOUSE_ID,
        'recipient_id'  => self::FIELD_RECIPIENT_ID,
        'date_from'     => self::FIELD_DATE_FROM,
        'date_to'       => self::FIELD_DATE_TO,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'warehouse_id',
        'recipient_id',
        'date_from',
        'date_to',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @param $params
     *
     * @return $this
     */
    public function insertModel(Request $request)
    {
        $params = collect($request->input());

        $attributes = [
            'warehouse_id' => $params->get('warehouse'),
            'recipient_id' => $params->get('recipient'),
            'date_from' => $params->get('from_date'),
            'date_to'   => $params->get('to_date'),
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'warehouse_id'  => ['required', 'exists:'.Warehouse::TABLE_NAME.','.Warehouse::FIELD_ID],
            'recipient_id'  => ['required', 'exists:'.SalaryRecipient::TABLE_NAME.','.SalaryRecipient::FIELD_ID],
            'date_from'     => ['nullable', 'date'],
            'date_to'       => ['nullable', 'date'],
        ])->validate();

        /**
         * Save changes into the history
         */
        $this->setAttribute(self::FIELD_WAREHOUSE_ID, $params->get('warehouse'));
        $this->setAttribute(self::FIELD_RECIPIENT_ID, $params->get('recipient'));
        $this->setAttribute(self::FIELD_DATE_FROM, $params->get('from_date'));
        $this->setAttribute(self::FIELD_DATE_TO, $params->get('to_date'));
        $this->save();

        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteModel()
    {
        $this->delete();

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function warehouse()
    {
        return $this->hasOne(Warehouse::class, Warehouse::FIELD_ID, self::FIELD_WAREHOUSE_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function recipient()
    {
        return $this->hasOne(SalaryRecipient::class, SalaryRecipient::FIELD_ID, self::FIELD_RECIPIENT_ID);
    }
}
