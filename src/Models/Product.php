<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\ProductCreated;
use Teleglobal\Accounting\Events\ProductUpdated;
use Teleglobal\Accounting\Events\ProductDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Product extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'o';
    const SOURCE_NAME   = 'Product';

    const FIELD_ID          = 'id';
    const FIELD_NAME        = 'a';
    const FIELD_COMMENT     = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ProductCreated::class,
        'updated' => ProductUpdated::class,
        'deleted' => ProductDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'comment'       => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function warehouses()
    {
        return $this
            ->belongsToMany(
                Warehouse::class,
                WarehouseProductPivot::TABLE_NAME,
                WarehouseProductPivot::FIELD_PRODUCT_ID,
                WarehouseProductPivot::FIELD_WAREHOUSE_ID
            );
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function insertModel(Request $request)
    {
        $params = collect($request->input());

        $attributes = [
            'name'      => $params->get('name'),
            'comment'   => $params->get('comment'),
        ];

        /*
         * Validate data
         */
        Validator::make($attributes, [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME],
        ])->validate();

        $this->fill($attributes)->save();

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function updateModel(Request $request)
    {
        $params = collect($request->input());

        $attributes = [
            'name'      => $params->get('name'),
            'comment'   => $params->get('comment'),
        ];

        /**
         * Validate data
         */
        Validator::make($attributes, [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$this->getAttribute(self::FIELD_ID)],
        ])->validate();

        $this->fill($attributes)->save();

        return $this;
    }

    /**
     * @return bool
     *
     * @throws \Exception
     */
    public function deleteModel()
    {
        $this->delete();
        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function incomes()
    {
        return $this->belongsToMany(
            Income::class,
            ProductIncome::TABLE_NAME,
            ProductIncome::FIELD_PRODUCT_ID,
            ProductIncome::FIELD_INCOME_ID
        );
    }
}
