<?php

namespace Teleglobal\Accounting\Models;

class Breadcrumb
{

    private $prefix_rout = 'accounting:';
    private $prefix_title = 'accounting::';
    private $breadcrumb = [];

    private $schema = [
        /* Home */
        'home' => [
            'class'     => 'fa-home',
            'route'     => 'dashboard.index',
            'title'     => 'page.dashboard.breadcrumb',
            'parent'    => null
        ],
        /* Dashboard */
        'dashboard.index' => [
            'title'     => 'dashboard.breadcrumb.main',
            'parent'    => 'home'
        ],
        /* Users */
        'users.main' => [
            'class'     => 'fa-user',
            'route'     => 'users',
            'title'     => 'users.breadcrumb.main',
            'parent'    => 'home'
        ],
        'users' => [
            'title'     => 'users.breadcrumb.list',
            'parent'    => 'users.main'
        ],
        'user.create' => [
            'title'     => 'users.breadcrumb.create',
            'parent'    => 'users.main'
        ],
        'user.edit' => [
            'title'     => 'users.breadcrumb.edit',
            'parent'    => 'users.main'
        ],
        'user_groups' => [
            'class'     => 'fa-user',
            'route'     => 'user_groups',
            'title'     => 'user_groups.breadcrumb.main',
            'parent'    => 'users'
        ],
        'user_group.create' => [
            'title'     => 'user_groups.breadcrumb.create',
            'parent'    => 'user_groups'
        ],
        'user_group.edit' => [
            'title'     => 'user_groups.breadcrumb.edit',
            'parent'    => 'user_groups'
        ],

        /* Warehouses */
        'warehouses' => [
            'class'     => 'icon-grid',
            'route'     => 'warehouses.list',
            'title'     => 'warehouses.breadcrumb.main',
            'parent'    => 'home'
        ],
        'warehouses.list' => [
            'title'     => 'warehouses.breadcrumb.list',
            'parent'    => 'warehouses'
        ],
        'warehouse.create' => [
            'title'     => 'warehouses.breadcrumb.create',
            'parent'    => 'warehouses'
        ],
        'warehouse.edit_access' => [
            'title'     => 'warehouses.breadcrumb.edit_access',
            'parent'    => 'warehouses'
        ],
        'warehouse.edit' => [
            'title'     => 'warehouses.breadcrumb.edit',
            'parent'    => 'warehouses'
        ],
        /* Trademarks */
        'trademarks.list' => [
            'title'     => 'trademarks.breadcrumb.list',
            'parent'    => 'warehouses'
        ],
        'trademark.create' => [
            'title'     => 'trademarks.breadcrumb.create',
            'parent'    => 'warehouses'
        ],
        'trademark.edit' => [
            'title'     => 'trademarks.breadcrumb.edit',
            'parent'    => 'warehouses'
        ],
        /* Clients */
        'clients.list' => [
            'title'     => 'clients.breadcrumb.list',
            'parent'    => 'warehouses'
        ],
        'client.create' => [
            'title'     => 'clients.breadcrumb.create',
            'parent'    => 'warehouses'
        ],
        'client.edit' => [
            'title'     => 'clients.breadcrumb.edit',
            'parent'    => 'warehouses'
        ],
        /* Transports */
        'transports.list' => [
            'title'     => 'transports.breadcrumb.list',
            'parent'    => 'warehouses'
        ],
        'transport.create' => [
            'title'     => 'transports.breadcrumb.create',
            'parent'    => 'warehouses'
        ],
        'transport.edit' => [
            'title'     => 'transports.breadcrumb.edit',
            'parent'    => 'warehouses'
        ],
        /* Dealers */
        'dealers.list' => [
            'title'     => 'dealers.breadcrumb.list',
            'parent'    => 'warehouses'
        ],
        'dealer.create' => [
            'title'     => 'dealers.breadcrumb.create',
            'parent'    => 'warehouses'
        ],
        'dealer.edit' => [
            'title'     => 'dealers.breadcrumb.edit',
            'parent'    => 'warehouses'
        ],
        /* Products */
        'products.list' => [
            'title'     => 'products.breadcrumb.main',
            'parent'    => 'warehouses'
        ],
        'product.create' => [
            'title'     => 'products.breadcrumb.create',
            'parent'    => 'warehouses'
        ],
        'product.edit' => [
            'title'     => 'products.breadcrumb.edit',
            'parent'    => 'warehouses'
        ],

        'warehouse.move_remnants' => [
            'title'     => 'warehouses.breadcrumb.select_recepient',
            'parent'    => 'warehouses'
        ],
        /* Salary */
        'salary' => [
            'class'     => 'icon-wallet',
            'route'     => 'salary.recipients.list',
            'title'     => 'salary.breadcrumb.recipients.main',
            'parent'    => 'home'
        ],
        'salary.recipients.list' => [
            'title'     => 'salary.breadcrumb.recipients.list',
            'parent'    => 'salary'
        ],
        'salary.recipient.create' => [
            'title'     => 'salary.breadcrumb.recipients.create',
            'parent'    => 'salary'
        ],
        'salary.recipient.edit' => [
            'title'     => 'salary.breadcrumb.recipients.edit',
            'parent'    => 'salary'
        ],
        'salary.recipient.get' => [
            'title'     => 'salary.breadcrumb.recipients.get',
            'parent'    => 'salary'
        ],
        'salary.reports.list' => [
            'title'     => 'salary.breadcrumb.reports.list',
            'parent'    => 'salary'
        ],
        'salary.report.create' => [
            'title'     => 'salary.breadcrumb.reports.create',
            'parent'    => 'salary'
        ],
        'salary.report.edit' => [
            'title'     => 'salary.breadcrumb.reports.edit',
            'parent'    => 'salary'
        ],
        'salary.report.get' => [
            'title'     => 'salary.breadcrumb.reports.get',
            'parent'    => 'salary'
        ],
        'movement.list' => [
            'class'     => 'icon-grid',
            'route'     => 'movement.list',
            'title'     => 'movement.breadcrumb.main',
            'parent'    => 'home'
        ],
        'storage.reports' => [
            'class'     => 'icon-grid',
            'route'     => 'storage.reports',
            'title'     => 'warehouses.breadcrumb.reports',
            'parent'    => 'home'
        ],
        'incomes.list' => [
            'class'     => 'fa fa-sign-in',
            'route'     => 'incomes.list',
            'title'     => 'incomes.breadcrumb.main',
            'parent'    => 'warehouses'
        ],
        'income.create' => [
            'title'     => 'incomes.breadcrumb.create',
            'parent'    => 'incomes.list'
        ],
        'outcomes.list' => [
            'class'     => 'fa fa-sign-out',
            'route'     => 'outcomes.list',
            'title'     => 'outcomes.breadcrumb.main',
            'parent'    => 'warehouses'
        ],
        'outcome.create' => [
            'title'     => 'outcomes.breadcrumb.create',
            'parent'    => 'outcomes.list'
        ],

        /* Orders */
        'orders' => [
            'class'     => 'icon-basket-loaded',
            'route'     => 'orders.index',
            'title'     => 'orders.breadcrumb.main',
            'parent'    => 'home'
        ],
        'order.create' => [
            #'class'     => 'icon-grid',
            #'route'     => 'order.create',
            'title'     => 'orders.breadcrumb.create',
            'parent'    => 'orders'
        ],
        'orders.index' => [
            #'class'     => 'icon-grid',
            #'route'     => 'order.create',
            'title'     => 'orders.breadcrumb.list',
            'parent'    => 'orders'
        ],
        'orders.order.edit' => [
            #'class'     => 'icon-grid',
            #'route'     => 'order.edit',
            'title'     => 'orders.breadcrumb.edit',
            'parent'    => 'orders'
        ],
        'orders.positions' => [
            #'class'     => 'icon-grid',
            #'route'     => 'orders.position.list',
            'title'     => 'orders.breadcrumb.position',
            'parent'    => 'orders'
        ],
        'orders.position.create' => [
            #'class'     => 'icon-grid',
            #'route'     => 'orders.position.create',
            'title'     => 'order_position.breadcrumb.create',
            'parent'    => 'orders'
        ],
        'orders.position.edit' => [
            #'class'     => 'icon-grid',
            #'route'     => 'orders.position.edit',
            'title'     => 'order_position.breadcrumb.edit',
            'parent'    => 'orders'
        ],
        'orders.transports' => [
            #'class'     => 'icon-grid',
            #'route'     => 'orders.transport.list',
            'title'     => 'orders.breadcrumb.transport',
            'parent'    => 'orders'
        ],
        'orders.transport.create' => [
            #'class'     => 'icon-grid',
            #'route'     => 'orders.transport.create',
            'title'     => 'order_transport.breadcrumb.create',
            'parent'    => 'orders'
        ],
        'orders.transport.edit' => [
            #'class'     => 'icon-grid',
            #'route'     => 'orders.transport.edit',
            'title'     => 'order_transport.breadcrumb.edit',
            'parent'    => 'orders'
        ],

        /* Cashboxes */
        'cashboxes' => [
            'class'     => 'icon-bar-chart',
            'title'     => 'cashboxes.page_title',
            'parent'    => 'home'
        ],
        'cashboxes.categories' => [
            'class'     => 'fa-tag',
            'route'     => 'cashboxes.categories',
            'title'     => 'report_categories.breadcrumb.main',
            'parent'    => 'cashboxes'
        ],
        'cashboxes.category.create' => [
            'title'     => 'report_categories.breadcrumb.create',
            'parent'    => 'cashboxes.categories'
        ],
        'cashboxes.category.edit' => [
            'title'     => 'report_categories.breadcrumb.edit',
            'parent'    => 'cashboxes.categories'
        ],
        'cashboxes.subcategories' => [
            'class'     => 'fa-tags',
            'route'     => 'cashboxes.subcategories',
            'title'     => 'report_subcategories.breadcrumb.main',
            'parent'    => 'cashboxes'
        ],
        'cashboxes.subcategory.create' => [
            'title'     => 'report_subcategories.breadcrumb.create',
            'parent'    => 'cashboxes.subcategories'
        ],
        'cashboxes.subcategory.edit' => [
            'title'     => 'report_subcategories.breadcrumb.edit',
            'parent'    => 'cashboxes.subcategories'
        ],
        /**
         * Cashboxes Accounts
         */
        'cashboxes.accounts' => [
            'class'     => 'fa-bank',
            'route'     => 'cashboxes.accounts',
            'title'     => 'report_accounts.breadcrumb.list',
            'parent'    => 'cashboxes'
        ],
        'cashboxes.account.create' => [
            'title'     => 'report_accounts.breadcrumb.create',
            'parent'    => 'cashboxes.accounts'
        ],
        'cashboxes.account.edit' => [
            'title'     => 'report_accounts.breadcrumb.edit',
            'parent'    => 'cashboxes.accounts'
        ],
        /**
         * Cashboxes Currencies
         */
        'cashboxes.currencies' => [
            'class'     => 'fa-money',
            'route'     => 'cashboxes.currencies',
            'title'     => 'report_currencies.breadcrumb.list',
            'parent'    => 'cashboxes'
        ],
        'cashboxes.currency.create' => [
            'title'     => 'report_currencies.breadcrumb.create',
            'parent'    => 'cashboxes.currencies'
        ],
        'cashboxes.currency.edit' => [
            'title'     => 'report_currencies.breadcrumb.edit',
            'parent'    => 'cashboxes.currencies'
        ],
        /* Cashboxes - Cashboxes */
        'cashboxes.main' => [
            'class'     => 'fa-money',
            'route'     => 'cashboxes',
            'title'     => 'cashboxes.breadcrumb.main',
            'parent'    => 'home'
        ],
        'cashboxes' => [
            'class'     => 'fa-list',
            'route'     => 'cashboxes',
            'title'     => 'cashboxes.breadcrumb.list',
            'parent'    => 'cashboxes.main'
        ],
        'users.transactions' => [
            'class'     => 'fa-exchange',
            'route'     => 'cashboxes.transactions',
            'title'     => 'cashboxes.breadcrumb.transactions',
            'parent'    => 'cashboxes.main'
        ],
        'users.remains' => [
            'class'     => 'fa-money',
            'route'     => 'users.remains',
            'title'     => 'users.breadcrumb.remains',
            'parent'    => 'users.main'
        ],
        'users.remains.lists' => [
            'class'     => 'fa-money',
            'route'     => 'users.remains.lists',
            'title'     => 'users.breadcrumb.remains',
            'parent'    => 'users.main'
        ],

        'users.transaction.create' => [
            'title'     => 'cashboxes.breadcrumb.transaction_create',
            'parent'    => 'users.transactions'
        ],

        /* Incoming */
        'cashboxes.incoming' => [
            'class'     => 'fa',
            'route'     => 'cashboxes.incoming',
            'title'     => 'cashboxes_incoming.breadcrumb.list',
            'parent'    => 'cashboxes.main'
        ],
        'cashboxes.incoming.create' => [
            'title'     => 'cashboxes_incoming.breadcrumb.create',
            'parent'    => 'cashboxes.incoming'
        ],
        'cashboxes.incoming.edit' => [
            'title'     => 'cashboxes_incoming.breadcrumb.edit',
            'parent'    => 'cashboxes.incoming'
        ],

        /* Outcoming */
        'cashboxes.transactions' => [
            'class'     => 'fa',
            'route'     => 'cashboxes.transactions',
            'title'     => 'cashboxes_outcoming.breadcrumb.list',
            'parent'    => 'cashboxes.main'
        ],
        'cashboxes.transaction.create' => [
            'title'     => 'cashboxes_outcoming.breadcrumb.create',
            'parent'    => 'cashboxes.transactions'
        ],
        'cashboxes.transaction.edit' => [
            'title'     => 'cashboxes_outcoming.breadcrumb.edit',
            'parent'    => 'cashboxes.transactions'
        ],

        /* Settings */
        'settings' => [
            'class'     => 'icon-settings',
            'title'     => 'settings.breadcrumb.currencies.main',
            'parent'    => 'home'
        ],
        'settings.index' => [
            'title'     => 'settings.breadcrumb.currencies.list',
            'parent'    => 'settings'
        ],
        'settings.edit' => [
            'title'     => 'settings.breadcrumb.currencies.edit',
            'parent'    => 'settings'
        ],
    ];

    public function __construct($route)
    {
        $this->breadcrumb = $this->getBreadcrumbSchema(
            explode(':', $route)[1]
        );
    }

    public function getBreadcrumb()
    {
        return $this->breadcrumb;
    }

    private function getBreadcrumbSchema($route, $schema = [])
    {
        if(array_key_exists($route, $this->schema)) {
            array_unshift(
                $schema,
                [
                    'class' => isset($this->schema[$route]['class']) ? ' ' . $this->schema[$route]['class'] : '',
                    'route' => isset($this->schema[$route]['route']) ? $this->prefix_rout . $this->schema[$route]['route'] : '',
                    'title' => isset($this->schema[$route]['title']) ? $this->prefix_title . $this->schema[$route]['title'] : '',
                ]
            );
            if ($this->schema[$route]['parent']) {
                $schema = $this->getBreadcrumbSchema($this->schema[$route]['parent'], $schema);
            }
        }
        return $schema;
    }

}