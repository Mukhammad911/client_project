<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class UserTransaction extends Model
{
    use AliasableEncryptable;

    const TABLE_NAME = 'f';
    const SOURCE_NAME = 'UserTransaction';

    const FIELD_ID              = 'id';
    const FIELD_USER_ID         = 'a';
    const FIELD_FROM_CASHBOX    = 'b';
    const FIELD_TO_CASHBOX      = 'c';
    const FIELD_AMOUNT          = 'd';

    protected $table = self::TABLE_NAME;

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_AMOUNT
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'user_id'       => self::FIELD_USER_ID,
        'from_cashbox'  => self::FIELD_FROM_CASHBOX,
        'to_cashbox'    => self::FIELD_TO_CASHBOX,
        'amount'        => self::FIELD_AMOUNT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'from_cashbox', 'to_cashbox', 'amount',
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function toCashboxes()
    {
        return $this->hasMany(UserCashbox::class,'id', self::FIELD_TO_CASHBOX);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function fromCashboxes()
    {
        return $this->hasMany(UserCashbox::class,'id', self::FIELD_FROM_CASHBOX);
    }
}
