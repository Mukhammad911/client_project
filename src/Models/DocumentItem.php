<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Notifications\Notifiable;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Illuminate\Support\Facades\Validator;
use Nexmo\Call\Collection;
use phpDocumentor\Reflection\Types\Integer;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Transport;
use Teleglobal\Accounting\Models\UserCashbox;

class DocumentItem extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'x';
    const SOURCE_NAME = 'DocumentItem';

    const FIELD_ID          = 'id';
    const FIELD_DOC_ID      = 'a';
    const FIELD_SOURCE      = 'b';
    const FIELD_SOURCE_ID   = 'c';
    const FIELD_PARAMS      = 'd';

    const SOURCE_USER               = User::SOURCE_NAME;
    const SOURCE_USER_CASHBOX       = UserCashbox::SOURCE_NAME;
    const SOURCE_CLIENT             = Client::SOURCE_NAME;
    const SOURCE_TRANSPORT          = Transport::SOURCE_NAME;
    const SOURCE_ACCOUNT            = ReportAccount::SOURCE_NAME;
    const SOURCE_REPORT_CURRENCY    = ReportCurrency::SOURCE_NAME;
    const SOURCE_CASHBOXES_INCOMING = CashboxesIncoming::SOURCE_NAME;

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_SOURCE,
        self::FIELD_PARAMS,
    ];

    /**
     * The attributes that stored by JSON
     *
     * @var array
     */
    protected $json = [
        self::FIELD_PARAMS,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'doc_id'    => self::FIELD_DOC_ID,
        'source'    => self::FIELD_SOURCE,
        'source_id' => self::FIELD_SOURCE_ID,
        'params'    => self::FIELD_PARAMS,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_id',
        'source',
        'source_id',
        'params',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Document constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    private static function getClassOfModel($class)
    {
        return 'Teleglobal\\Accounting\\Models\\' . $class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, self::FIELD_DOC_ID, Document::FIELD_ID);
    }

    /**
     * @param $source
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function source($source)
    {
        return $this->belongsTo(
            self::getClassOfModel($source),
            self::SOURCE_NAME,
            self::getClassOfModel($source)::FIELD_ID
        );
    }

    public function user()
    {
        return $this->belongsTo(User::class, self::FIELD_SOURCE_ID, User::FIELD_ID);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, self::FIELD_SOURCE_ID, Client::FIELD_ID);
    }

    public function account()
    {
        return $this->belongsTo(ReportAccount::class, self::FIELD_SOURCE_ID, ReportAccount::FIELD_ID);
    }

    public function reportCurrency()
    {
        return $this->belongsTo(ReportCurrency::class, self::FIELD_SOURCE_ID, ReportCurrency::FIELD_ID);
    }

    public function transport()
    {
        return $this->belongsTo(Transport::class, self::FIELD_SOURCE_ID, Transport::FIELD_ID);
    }

    /**
     * @param int $docId
     * @param string $source
     * @param int $sourceId
     * @param array $params
     *
     * @return DocumentItem
     */
    public static function insertModel(int $docId, string $source, int $sourceId, array $params = []): DocumentItem
    {
        $attributes = [
            'doc_id'    => $docId,
            'source'    => $source,
            'source_id' => $sourceId,
            'params'    => $params,
        ];

        $model = DocumentItem::create($attributes);

        return $model;
    }
}
