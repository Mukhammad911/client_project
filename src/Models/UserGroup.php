<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Notifications\Notifiable;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\UserGroupCreated;
use Teleglobal\Accounting\Events\UserGroupUpdated;
use Teleglobal\Accounting\Events\UserGroupDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserGroup extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'g';

    const FIELD_ID      = 'id';
    const FIELD_NAME    = 'a';
    const FIELD_COMMENT = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => UserGroupCreated::class,
        'updated' => UserGroupUpdated::class,
        'deleted' => UserGroupDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'      => self::FIELD_NAME,
        'comment'   => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'comment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function users()
    {
        return $this->hasManyThrough(
            User::class,
            UserGroupUser::class,
            UserGroupUser::FIELD_USER_ID,
            User::FIELD_ID,
            self::FIELD_ID,
            UserGroupUser::FIELD_USER_GROUP_ID
        );
    }
}
