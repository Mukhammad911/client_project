<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Nexmo\Call\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class ProductsOutcoming extends BaseModel
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'q';
    const SOURCE_NAME = 'ProductsOutcoming';

    const FIELD_ID          = 'id';
    const FIELD_DOC_ID      = 'a';
    const FIELD_DATE        = 'b';
    const FIELD_PIVOT_ID    = 'c';
    const FIELD_CURRENCY    = 'f';
    const FIELD_AMOUNT      = 'd';
    const FIELD_COST        = 'e';

    const SOURCE_WAREHOUSE  = 'warehouse';
    const SOURCE_CLIENT     = 'client';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'doc_id'    => self::FIELD_DOC_ID,
        'date'      => self::FIELD_DATE,
        'pivot_id'  => self::FIELD_PIVOT_ID,
        'currency'  => self::FIELD_CURRENCY,
        'amount'    => self::FIELD_AMOUNT,
        'cost'      => self::FIELD_COST,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_id',
        'date',
        'pivot_id',
        'currency',
        'amount',
        'cost',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, self::FIELD_DOC_ID, Document::FIELD_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pivot()
    {
        return $this->hasOne(WarehouseProductPivot::class, WarehouseProductPivot::FIELD_ID, self::FIELD_PIVOT_ID);
    }

    /**
     * ProductsOutcoming constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param $docId
     * @param $date
     * @param $productId
     * @param $warehouseId
     * @param $amount
     *
     * @return ProductsOutcoming
     */
    static public function insertModel($docId, $date, $warehouseId, $productId, $amount, $currency = null, $cost = null): ProductsOutcoming
    {
        // todo Дописать валидацию

        $pivot = WarehouseProductPivot::insertModel($warehouseId, $productId);

        $amount = number_format((float)($amount ?? 0), 3);

        $attributes = [
            'doc_id'    => $docId,
            'date'      => $date,
            'pivot_id'  => $pivot->id,
            'currency'  => $currency,
            'amount'    => $amount,
            'cost'      => $cost,
        ];

        $model = new ProductsOutcoming();
        $model->fill($attributes)->save();

        return $model;
    }

    public function getDocumentsList(array $docTypes = [])
    {
        $query = Document::select();

        if(!empty($docTypes)){
            $cryptDocTypes = [];
            foreach ($docTypes as $docType){
                $cryptDocTypes[] = Crypt::encrypt($docType);
            }
            $query->whereIn(Document::FIELD_DOC_TYPE, $cryptDocTypes);
        }

        $query
            ->with(['productsOutcoming' => function($query){
                return $query->with(['pivot' => function($query){
                    return $query
                        ->with('warehouse')
                        ->with('product');
                }]);
            }])
            ->with('history')
            ->with(['docItemsClients' => function($query){
                return $query->with('client');
            }])
            ->with(['docItemsTransports' => function($query){
                return $query->with('transport');
            }]);

        $this->setPagination($query);

        $this->result = $query->get();

        return $this;
    }

}
