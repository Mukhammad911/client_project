<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\IncomeCreated;
use Teleglobal\Accounting\Events\IncomeUpdated;
use Teleglobal\Accounting\Events\IncomeDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Income extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'p';
    const SOURCE_NAME   = 'Income';

    const FIELD_ID              = 'id';
    const FIELD_WAREHOUSE_ID    = 'a';
    const FIELD_DATE            = 'b';
    const FIELD_AUTHOR          = 'j';
    const FIELD_PAYMENT         = 'c';
    const FIELD_PAYMENT_AMOUNT      = 'd';
    const FIELD_PAYMENT_CURRENCY    = 'e';
    const FIELD_PAYMENT_DATE        = 'f';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => IncomeCreated::class,
        'updated' => IncomeUpdated::class,
        'deleted' => IncomeDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_PAYMENT_AMOUNT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'warehouse_id'      => self::FIELD_WAREHOUSE_ID,
        'date'              => self::FIELD_DATE,
        'author'            => self::FIELD_AUTHOR,
        'payment'           => self::FIELD_PAYMENT,
        'payment_amount'    => self::FIELD_PAYMENT_AMOUNT,
        'payment_currency'  => self::FIELD_PAYMENT_CURRENCY,
        'payment_date'      => self::FIELD_PAYMENT_DATE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'warehouse_id',
        'date',
        'author',
        'payment',
        'payment_amount',
        'payment_currency',
        'payment_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function insertModel(Request $request)
    {
        $params = collect($request->input());

        /*
         * Validate data
         */
        if(is_null($params->get('payment'))) {
            $payment = false;

            Validator::make([
                'warehouse'     => $params->get('warehouse'),
                'date'          => $params->get('date'),
                'payment_amount'   => $params->get('payment_amount'),
                'payment_date'     => $params->get('payment_date'),
                'payment_currency' => $params->get('payment_currency'),
            ], [
                'warehouse'     => ['required', 'exists:'.Warehouse::TABLE_NAME.','.Warehouse::FIELD_ID],
                'date'          => ['required'],
                'payment_amount'   => ['nullable'],
                'payment_date'     => ['nullable'],
                'payment_currency' => ['nullable'],
            ])->validate();
        } else {
            $payment = true;

            Validator::make([
                'warehouse'     => $params->get('warehouse'),
                'date'          => $params->get('date'),
                'payment_amount'   => $params->get('payment_amount'),
                'payment_date'     => $params->get('payment_date'),
                'payment_currency' => $params->get('payment_currency'),
            ], [
                'warehouse'     => ['required', 'exists:'.Warehouse::TABLE_NAME.','.Warehouse::FIELD_ID],
                'date'          => ['required'],
                'payment_amount'   => ['required', 'numeric'],
                'payment_date'     => ['required'],
                'payment_currency' => ['required', 'exists:'.Currency::TABLE_NAME.','.Currency::FIELD_ID],
            ])->validate();
        }

        /*
         * Используем транзакции для контроля целостности
         * данных при сохраннении связанных моделей
         */
        DB::beginTransaction();

        $date = Carbon::now();

        $this->setAttribute(self::FIELD_WAREHOUSE_ID, $params->get('warehouse'));
        $this->setAttribute(self::FIELD_DATE, $params->get('date'));

        if($payment) {
            $this->setAttribute(self::FIELD_PAYMENT, $payment);
            $this->setAttribute(self::FIELD_PAYMENT_AMOUNT, $params->get('payment_amount'));
            $this->setAttribute(self::FIELD_PAYMENT_DATE, $params->get('payment_date'));
            $this->setAttribute(self::FIELD_PAYMENT_CURRENCY, $params->get('payment_currency'));
        }
        $this->setAttribute(self::FIELD_AUTHOR, Auth()->id());
        $this->setAttribute('created_at', $date);
        $this->setAttribute('updated_at', $date);
        $this->save();

        (new ProductIncome())->insertModel($request, $this);

        DB::commit();

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function updateModel(Request $request)
    {
        $params = collect($request->input());

        /**
         * Validate data
         */
        Validator::make([
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
        ], [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$this->getAttribute(self::FIELD_ID)],
        ])->validate();

        $this->setAttribute(self::FIELD_NAME, $params->get('name'));
        $this->setAttribute(self::FIELD_COMMENT, $params->get('comment'));
        $this->setAttribute('updated_at', Carbon::now());
        $this->save();

        return $this;
    }

    public function deleteModel()
    {
        $this->delete();
        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function warehouse()
    {
        return $this->hasOne(Warehouse::class, Warehouse::FIELD_ID, $this::FIELD_WAREHOUSE_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            ProductIncome::TABLE_NAME,
            ProductIncome::FIELD_INCOME_ID,
            ProductIncome::FIELD_PRODUCT_ID
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this->hasOne(
            User::class,
            User::FIELD_ID,
            self::FIELD_AUTHOR
        );
    }
}
