<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class ReportCategorySubCategoryPivot extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'aa';
    const SOURCE_NAME   = 'ReportCategorySubCategoryPivot';

    const FIELD_ID              = 'id';
    const FIELD_CATEGORY_ID     = 'a';
    const FIELD_SUBCATEGORY_ID  = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => DealerCreated::class,
//        'updated' => DealerUpdated::class,
//        'deleted' => DealerDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'category_id'       => self::FIELD_CATEGORY_ID,
        'subcategory_id'    => self::FIELD_SUBCATEGORY_ID,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'subcategory_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function categories()
    {
        return $this->belongsTo(ReportCategory::class, self::FIELD_CATEGORY_ID, ReportCategory::FIELD_ID);
    }

    public function subcategories()
    {
        return $this->belongsTo(ReportSubCategory::class, self::FIELD_SUBCATEGORY_ID, ReportSubCategory::FIELD_ID);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param int $categoryId
     * @param int $subCategoryId
     *
     * @return ReportCategorySubCategoryPivot
     */
    public static function updateModel(int $categoryId, int $subCategoryId)
    {
        $attributes = [
            'category_id'       => $categoryId,
            'subcategory_id'    => $subCategoryId,
        ];

        $model = self::select()
            ->where(self::FIELD_CATEGORY_ID, '=', $categoryId)
            ->where(self::FIELD_SUBCATEGORY_ID, '=', $subCategoryId)
            ->first();

        if(!$model){
            $model = new self();
            $model->fill($attributes)->save();
        }

        return $model;
    }

    public function deleteModel()
    {
        $this->delete();
        return true;
    }
}
