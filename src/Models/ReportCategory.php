<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class ReportCategory extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'y';
    const SOURCE_NAME   = 'ReportCategory';

    const FIELD_ID          = 'id';
    const FIELD_NAME        = 'a';
    const FIELD_COMMENT     = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => DealerCreated::class,
//        'updated' => DealerUpdated::class,
//        'deleted' => DealerDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'comment'       => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function subcategories()
    {
        return $this->belongsToMany(
            ReportSubCategory::class,
            ReportCategorySubCategoryPivot::TABLE_NAME,
            ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID,
            ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID,
            ReportCategory::FIELD_ID,
            ReportSubCategory::FIELD_ID
        );
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param Collection $params
     *
     * @return $this
     */
    public static function insertModel(Collection $params)
    {
        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
        ];

        /*
         * Validate data
         */
        Validator::make($attributes, [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME],
        ])->validate();

        $model = new self();
        $model->fill($attributes)->save();

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $params
     *
     * @return $this
     */
    public static function updateModel(int $id, Collection $params)
    {
        $model = self::find($id);

        if(empty($model)){
            return false;
        }

        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
        ];

        /**
         * Validate data
         */
        Validator::make($attributes, [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$model->getAttribute(self::FIELD_ID)],
        ])->validate();

        $model->fill($attributes)->save();

        return $model;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function deleteModel($id)
    {
        // todo дописать корректное удаление из связанных таблиц
        return self::find($id)->delete();
    }
}
