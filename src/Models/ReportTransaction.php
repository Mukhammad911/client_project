<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class ReportTransaction extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'ag';
    const SOURCE_NAME   = 'ReportTransaction';

    const FIELD_ID                      = 'id';
    const FIELD_USER_ID                 = 'a';
    const FIELD_CASHBOX_ID              = 'b';
    const FIELD_ACCOUNT_ID              = 'c';
    const FIELD_DESCRIPTION             = 'd';
    const FIELD_CATEGORY_ID             = 'e';
    const FIELD_SUBCATEGORY_ID          = 'f';
    const FIELD_AMOUNT                  = 'g';
    const FIELD_CURRENCY_ID             = 'h';
    const FIELD_DATE                    = 'i';
    const FIELD_COMMENT                 = 'j';
    const FIELD_SOURCE                  = 'k';
    const FIELD_SOURCE_ID               = 'l';
    const FIELD_CASHBOX_OUTCOMING_ID    = 'm';
    const FIELD_VALIDATED               = 'n';
    const FIELD_VALIDATED_BY            = 'o';
    const FIELD_VALIDATED_AT            = 'p';

    const SOURCE_NONE       = 'none';
    const SOURCE_WAREHOUSE  = 'warehouse';
    const SOURCE_ORDER      = 'order';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    private $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => DealerCreated::class,
//        'updated' => DealerUpdated::class,
//        'deleted' => DealerDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_DESCRIPTION,
        self::FIELD_COMMENT,
        self::FIELD_SOURCE,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'user_id'               => self::FIELD_USER_ID,
        'cashbox_id'            => self::FIELD_CASHBOX_ID,
        'account_id'            => self::FIELD_ACCOUNT_ID,
        'description'           => self::FIELD_DESCRIPTION,
        'category_id'           => self::FIELD_CATEGORY_ID,
        'subcategory_id'        => self::FIELD_SUBCATEGORY_ID,
        'amount'                => self::FIELD_AMOUNT,
        'currency_id'           => self::FIELD_CURRENCY_ID,
        'date'                  => self::FIELD_DATE,
        'comment'               => self::FIELD_COMMENT,
        'source'                => self::FIELD_SOURCE,
        'source_id'             => self::FIELD_SOURCE_ID,
        'cashbox_outcoming_id'  => self::FIELD_CASHBOX_OUTCOMING_ID,
        'validated'             => self::FIELD_VALIDATED,
        'validated_by'          => self::FIELD_VALIDATED_BY,
        'validated_at'          => self::FIELD_VALIDATED_AT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'cashbox_id',
        'account_id',
        'description',
        'category_id',
        'subcategory_id',
        'amount',
        'currency_id',
        'date',
        'comment',
        'source',
        'source_id',
        'cashbox_outcoming_id',
        'validated',
        'validated_by',
        'validated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user()
    {
        return $this->belongsTo(User::class, self::FIELD_USER_ID, User::FIELD_ID);
    }

    public function cashbox()
    {
        return $this->belongsTo(UserCashbox::class, self::FIELD_CASHBOX_ID, UserCashbox::FIELD_ID);
    }

    public function account()
    {
        return $this->belongsTo(ReportAccount::class, self::FIELD_ACCOUNT_ID, ReportAccount::FIELD_ID);
    }

    public function category()
    {
        return $this->belongsTo(ReportCategory::class, self::FIELD_CATEGORY_ID, ReportCategory::FIELD_ID);
    }

    public function subcategory()
    {
        return $this->belongsTo(ReportSubCategory::class, self::FIELD_SUBCATEGORY_ID, ReportSubCategory::FIELD_ID);
    }

    public function currency()
    {
        return $this->belongsTo(ReportCurrency::class, self::FIELD_CURRENCY_ID, ReportCurrency::FIELD_ID);
    }

    public function sourceWarehouse()
    {
        return $this->belongsTo(ProductsOutcoming::class, self::FIELD_SOURCE_ID, ProductsOutcoming::FIELD_ID);
    }

    public function sourceOrder()
    {
        return $this->belongsTo(Order::class, self::FIELD_SOURCE_ID, Order::FIELD_ID);
    }

    public function history()
    {
        return $this
            ->hasMany(HistoryOfEdits::class, HistoryOfEdits::FIELD_SOURCE_ID, self::FIELD_ID)
            ->where(HistoryOfEdits::FIELD_SOURCE, Crypt::encrypt(self::SOURCE_NAME))
            ->orderBy(HistoryOfEdits::FIELD_DATE_EDITED, 'DESC');
    }

    public function cashboxOutcoming()
    {
        return $this->belongsTo(CashboxesOutcoming::class, self::FIELD_CASHBOX_OUTCOMING_ID, CashboxesOutcoming::FIELD_ID);
    }

    public function validatedBy()
    {
        return $this->belongsTo(User::class, self::FIELD_VALIDATED_BY, User::FIELD_ID);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @param Collection $params
     *
     * @return $this
     */
    public static function insertModel(Collection $params)
    {
        $sourceId = null;
        if($params->get('source') === self::SOURCE_ORDER){ $sourceId = $params->get('order_id'); }
        if($params->get('source') === self::SOURCE_WAREHOUSE){ $sourceId = $params->get('outcome_id'); }

        $attributes = [
            'user_id'           => $params->get('user_id'),
            'cashbox_id'        => $params->get('cashbox_id'),
            'account_id'        => $params->get('account_id'),
            'description'       => $params->get('description'),
            'category_id'       => $params->get('category_id'),
            'subcategory_id'    => $params->get('subcategory_id'),
            'amount'            => $params->get('amount'),
            'currency_id'       => $params->get('currency_id'),
            'date'              => date_create_from_format(config('accounting.app.date_format'), $params->get('date')),
            'comment'           => $params->get('comment'),
            'source'            => $params->get('source'),
            'source_id'         => $sourceId,
        ];

        /*
         * Validate data
         */
        Validator::make($attributes, [
            'user_id'           => ['required','exists:'.User::TABLE_NAME.','.User::FIELD_ID],
            'cashbox_id'        => ['required','exists:'.UserCashbox::TABLE_NAME.','.UserCashbox::FIELD_ID],
            'account_id'        => ['required','exists:'.ReportAccount::TABLE_NAME.','.ReportAccount::FIELD_ID],
            'description'       => ['nullable','max:255'],
            'category_id'       => ['required','exists:'.ReportCategory::TABLE_NAME.','.ReportCategory::FIELD_ID],
            'subcategory_id'    => ['required','exists:'.ReportSubCategory::TABLE_NAME.','.ReportSubCategory::FIELD_ID],
            'amount'            => ['required','numeric'],
            'currency_id'       => ['required','exists:'.ReportCurrency::TABLE_NAME.','.ReportCurrency::FIELD_ID],
            'date'              => ['required'],
            'comment'           => ['nullable','max:255'],
            'source'            => ['required'],
            'source_id'         => ['nullable','integer'],
        ])->validate();

        $cashboxesOutcoming = CashboxesOutcoming::insertModel(collect([
            'doc_id'        => null,
            'date'          => $attributes['date'],
            'key'           => CashboxesOutcoming::KEY_OUTCOMING,
            'cashbox_id'    => $attributes['cashbox_id'],
            'amount'        => $attributes['amount'],
            'comment'       => $attributes['comment'],
        ]));

        $attributes['cashbox_outcoming_id'] = $cashboxesOutcoming->id;

        $model = new self();
        $model->fill($attributes)->save();

        $attributes['_scenario'] = HistoryOfEdits::SCENARIO_CREATE;
        $model->historyOfEdits->setRequiredParams(
            self::SOURCE_NAME,
            $model->getAttribute(self::FIELD_ID),
            $attributes)->save();

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $params
     *
     * @return self
     */
    public static function updateModel(int $id, Collection $params)
    {
        $model = self::find($id);

        if(empty($model)){
            return false;
        }

        $sourceId = null;
        if($params->get('source') === self::SOURCE_ORDER){ $sourceId = $params->get('order_id'); }
        if($params->get('source') === self::SOURCE_WAREHOUSE){ $sourceId = $params->get('outcome_id'); }

        $attributes = [
            'user_id'           => $params->get('user_id'),
            'cashbox_id'        => $params->get('cashbox_id'),
            'account_id'        => $params->get('account_id'),
            'description'       => $params->get('description'),
            'category_id'       => $params->get('category_id'),
            'subcategory_id'    => $params->get('subcategory_id'),
            'amount'            => $params->get('amount'),
            'currency_id'       => $params->get('currency_id'),
            'date'              => date_create_from_format(config('accounting.app.date_format'), $params->get('date')),
            'comment'           => $params->get('comment'),
            'source'            => $params->get('source'),
            'source_id'         => $sourceId,
        ];

        /**
         * Validate data
         */
        Validator::make($attributes, [
            'user_id'           => ['required','exists:'.User::TABLE_NAME.','.User::FIELD_ID],
            'cashbox_id'        => ['required','exists:'.UserCashbox::TABLE_NAME.','.UserCashbox::FIELD_ID],
            'account_id'        => ['required','exists:'.ReportAccount::TABLE_NAME.','.ReportAccount::FIELD_ID],
            'description'       => ['nullable','max:255'],
            'category_id'       => ['required','exists:'.ReportCategory::TABLE_NAME.','.ReportCategory::FIELD_ID],
            'subcategory_id'    => ['required','exists:'.ReportSubCategory::TABLE_NAME.','.ReportSubCategory::FIELD_ID],
            'amount'            => ['required','numeric'],
            'currency_id'       => ['required','exists:'.ReportCurrency::TABLE_NAME.','.ReportCurrency::FIELD_ID],
            'date'              => ['required'],
            'comment'           => ['nullable','max:255'],
            'source'            => ['required'],
            'source_id'         => ['nullable','integer'],
        ])->validate();

        $cashboxesOutcoming = CashboxesOutcoming::updateModel($model->cashbox_outcoming_id, collect([
            'doc_id'        => null,
            'date'          => $attributes['date'],
            'key'           => CashboxesOutcoming::KEY_OUTCOMING,
            'cashbox_id'    => $attributes['cashbox_id'],
            'amount'        => $attributes['amount'],
            'comment'       => $attributes['comment'],
        ]));

        $model->fill($attributes);
        $changes = $model->getChanges();
        if(isset($changes['date'])){
            $oDate = explode(' ', $model->getOriginal('date'))[0];
            $aDate = explode(' ', ($changes['date']->date))[0];
            if($oDate === $aDate){
                unset($changes['date']);
            }
        }
        $model->save();
        if(!empty($changes)){
            $changes['_scenario'] = HistoryOfEdits::SCENARIO_UPDATE;
            $model->historyOfEdits->setRequiredParams(
                self::SOURCE_NAME,
                $model->getAttribute(self::FIELD_ID),
                $changes)->save();

            self::setUnvalidated($id);
        }

        return $model;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function deleteModel($id)
    {
        return self::find($id)->delete();
    }

    public static function getParamsData(Request $request)
    {
        return [
            'route'         => $request->route()->getName(),
            'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
            'query'         => is_null($request->getQueryString())
                ? '' : '?'. $request->getQueryString(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionIndexParams(Request $request)
    {
        $transactions = self::select()
            ->with('user')
            ->with('cashbox')
            ->with('account')
            ->with('category')
            ->with('subcategory')
            ->with('currency')
            ->with('sourceWarehouse')
            ->with('sourceOrder')
            ->with('history')
            ->get();

        $historyFields = [
            'user'          => __('accounting::cashboxes_outcoming.history_fields.user'),
            'cashbox'       => __('accounting::cashboxes_outcoming.history_fields.cashbox'),
            'account'       => __('accounting::cashboxes_outcoming.history_fields.account'),
            'description'   => __('accounting::cashboxes_outcoming.history_fields.description'),
            'category'      => __('accounting::cashboxes_outcoming.history_fields.category'),
            'subcategory'   => __('accounting::cashboxes_outcoming.history_fields.subcategory'),
            'amount'        => __('accounting::cashboxes_outcoming.history_fields.amount'),
            'currency'      => __('accounting::cashboxes_outcoming.history_fields.currency'),
            'date'          => __('accounting::cashboxes_outcoming.history_fields.date'),
            'comment'       => __('accounting::cashboxes_outcoming.history_fields.comment'),
        ];

        $params = [
            'historyFields' => json_encode($historyFields),
            'transactions'  => $transactions,
            'data'          => self::getParamsData($request),
        ];

        return $params;
    }

    /**
     * Prepared parameters for Create and Update forms
     *
     * @return array
     */
    private static function prepareResponseParams()
    {
        $userCashboxes = UserCashbox::select(UserCashbox::FIELD_ID, UserCashbox::FIELD_USER_ID, UserCashbox::FIELD_ALIAS)->get()->groupBy('user_id')->toArray();
        foreach ($userCashboxes as &$cashboxes){
            foreach ($cashboxes as &$cashbox){
                $cashbox['text'] = $cashbox['alias'];
            }
        }

        $categories = ReportCategory::with('subcategories')->get();
        $subCategoriesForAllCategories = ReportSubCategory::select()
            ->where(ReportSubCategory::FIELD_TYPE_OF_LINK, '=', Crypt::encrypt(ReportSubCategory::TYPE_OF_LINK_ALL))
            ->get()->toArray();

        $subcategories = [];
        foreach ($categories as $category){
            $subcategories[$category->id] = collect(array_merge($subCategoriesForAllCategories, $category->subcategories->toArray()))->sortBy('name')->values()->toArray();
            foreach ($subcategories[$category->id] as &$subcategory){
                $subcategory['text'] = $subcategory['name'];
            }
        }

        $outcomes = ProductsOutcoming::select()
            ->with(['document' => function($query){
                return $query->with(['docItemsClients' => function($query){
                    return $query->with(['client' => function($query){
                        return $query;
                    }]);
                }]);
            }])
            ->get();

        $params = [
            'users'         => User::select()->with('cashboxes')->get(),
            'cashboxes'     => $userCashboxes,
            'accounts'      => ReportAccount::all(),
            'categories'    => $categories,
            'subcategories' => $subcategories,
            'currencies'    => ReportCurrency::all(),
            'outcomes'      => $outcomes,
            'orders'        => Order::all(),
        ];

        return $params;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionCreateParams(Request $request)
    {
        return array_merge(
            self::prepareResponseParams(),
            [
                'authUserId'    => HistoryOfEdits::$authUserId,
                'data'          => self::getParamsData($request),
            ]
        );
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return array
     */
    public static function getActionEditParams(int $id, Request $request)
    {
        return array_merge(
            self::prepareResponseParams(),
            [
                'transaction'   => self::find($id),
                'data'          => self::getParamsData($request),
            ]
        );
    }

    public static function setValidated(Collection $params)
    {
        $model = self::find($params->get('id'));
        if($model){
            $model->validated = true;
            $model->validated_by = HistoryOfEdits::$authUserId;
            $model->validated_at = (new Carbon('utc'));
            $model->save();
        }

        return $model;
    }

    public static function setUnvalidated(int $transactionId)
    {
        $model = self::find($transactionId);
        if($model){
            $model->validated = false;
            $model->save();
        }

        return $model;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public static function getHistory(int $id)
    {
        $data = self::select()
            ->where(self::FIELD_ID, '=', $id)
            ->with('history')
            ->first();

        $history = [];
        foreach ($data->history as $item){
            $part = [
                'edited_by' => $item->user->name,
                'edited_at' => (new Carbon($item->getAttribute(HistoryOfEdits::FIELD_DATE_EDITED)))->format(config('accounting.app.datetime_format')),
                'changes'   => json_decode($item->getAttribute(HistoryOfEdits::FIELD_CHANGES), true),
            ];

            $part['_scenario'] = $part['changes']['_scenario'];
            unset($part['changes']['_scenario']);
            if(isset($part['changes']['user_id'])){
                $part['changes']['user'] = User::find($part['changes']['user_id'])->name;
                unset($part['changes']['user_id']);
            }
            if(isset($part['changes']['cashbox_id'])){
                $part['changes']['cashbox'] = UserCashbox::find($part['changes']['cashbox_id'])->alias;
                unset($part['changes']['cashbox_id']);
            }
            if(isset($part['changes']['account_id'])){
                $part['changes']['account'] = ReportAccount::find($part['changes']['account_id'])->name;
                unset($part['changes']['account_id']);
            }
            if(isset($part['changes']['category_id'])){
                $part['changes']['category'] = ReportCategory::find($part['changes']['category_id'])->name;
                unset($part['changes']['category_id']);
            }
            if(isset($part['changes']['subcategory_id'])){
                $part['changes']['subcategory'] = ReportSubCategory::find($part['changes']['subcategory_id'])->name;
                unset($part['changes']['subcategory_id']);
            }
            if(isset($part['changes']['currency_id'])){
                $part['changes']['currency'] = ReportCurrency::find($part['changes']['currency_id'])->name;
                unset($part['changes']['currency_id']);
            }
            if(isset($part['changes']['date'])){
                $part['changes']['date'] = (date_create_from_format('Y-m-d H:i:s.u', $part['changes']['date']['date']))->format(config('accounting.app.date_format'));
            }
            // todo дописать вывод ассоциации
            unset($part['changes']['source'], $part['changes']['source_id'], $part['changes']['cashbox_outcoming_id']);
            if(isset($part['changes']['source']) && $part['changes']['source'] !== self::SOURCE_NONE){
                switch ($part['changes']['source']){
                    case self::SOURCE_WAREHOUSE :
                        break;
                    case self::SOURCE_ORDER :
                        break;
                    default :
                        break;
                }
                unset($part['changes']['source_id']);
            }

            $history[] = $part;
        }

        return $history;
    }

}
