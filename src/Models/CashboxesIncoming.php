<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class CashboxesIncoming extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'ah';
    const SOURCE_NAME = 'CashboxesIncoming';

    const FIELD_ID          = 'id';
    const FIELD_DOC_ID      = 'a';
    const FIELD_DATE        = 'b';
    const FIELD_KEY         = 'c';
    const FIELD_CASHBOX_ID  = 'd';
    const FIELD_AMOUNT      = 'e';
    const FIELD_COMMENT     = 'f';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    public $historyOfEdits = null;

    const KEY_START_BALANCE         = 'start_balance';          // must created when insert new userCashbox
    const KEY_CLIENT_TRANSFERRING   = 'client_transferring';    // transferring from client
    const KEY_USER_TRANSFERRING     = 'user_transferring';      // transferring from another user

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_KEY,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'doc_id' => self::FIELD_DOC_ID,
        'date' => self::FIELD_DATE,
        'key' => self::FIELD_KEY,
        'cashbox_id' => self::FIELD_CASHBOX_ID,
        'amount' => self::FIELD_AMOUNT,
        'comment' => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_id',
        'date',
        'key',
        'cashbox_id',
        'amount',
        'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * ProductsIncoming constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, self::FIELD_DOC_ID, Document::FIELD_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cashbox()
    {
        return $this->belongsTo(UserCashbox::class, self::FIELD_CASHBOX_ID, UserCashbox::FIELD_ID);
    }

    /**
     * @param Collection $params
     *
     * @return CashboxesIncoming
     */
    public static function insertModel(Collection $params)
    {
        $date = Carbon::parse($params->get('date'));
        $formated_date = $date->format('Y-m-d H:i:s');

        $attributes = [
            'doc_id' => $params->get('doc_id'),
            'date' => $formated_date,
            'key' => $params->get('key'),
            'cashbox_id' => $params->get('cashbox_id'),
            'amount' => $params->get('amount'),
            'comment' => json_encode([
                'comment'       => $params->get('comment'),
                'description'   => $params->get('description')
            ]),
        ];

        Validator::make($attributes, [
            'doc_id' => ['nullable', 'integer'],
            'date' => ['date'],
            'key' => ['in:' . self::KEY_START_BALANCE.','.self::KEY_CLIENT_TRANSFERRING.','.self::KEY_USER_TRANSFERRING],
            'cashbox_id' => ['integer'],
            'amount' => ['numeric'],
            'comment' => ['nullable', 'max:255'],
        ])->validate();

        $doc = Document::insertModel(Document::DOC_TYPE_CASHBOXES_INCOMING, $params);

        $attributes['doc_id'] = $doc->id;

        $model = new self();
        $model->fill($attributes)->save();

        /* Save changes into history */
        $changes = [
            '_scenario' => HistoryOfEdits::SCENARIO_CREATE,
            'id' => $model->id,
            'doc_id' => $model->doc_id,
            'date' => $model->date,
            'key' => $model->key,
            'currency' => $model->cashbox->currency->code . ' - ' . $model->cashbox->currency->country,
            'cashbox' => $model->cashbox->alias,
            'amount' => $model->amount,
        ];

        $model->historyOfEdits
            ->setRequiredParams(self::SOURCE_NAME, $model->id, $changes)
            ->save();

        return $model;
    }

    public static function updateModel(int $id, Collection $params)
    {
        $attributes = [
            'doc_id' => $params->get('doc_id'),
            'date' => date_create_from_format(config('accounting.app.date_format'), $params->get('date')),
            'key' => $params->get('key'),
            'cashbox_id' => $params->get('cashbox_id'),
            'amount' => $params->get('amount'),
            'comment' => $params->get('comment'),
        ];

        Validator::make($attributes, [
            'doc_id' => ['nullable', 'integer'],
            'date' => ['date'],
            'key' => ['in:' . self::KEY_START_BALANCE.','.self::KEY_CLIENT_TRANSFERRING.','.self::KEY_USER_TRANSFERRING],
            'cashbox_id' => ['integer'],
            'amount' => ['numeric'],
            'comment' => ['nullable', 'max:255'],
        ])->validate();

        $model = self::find($id);
        $model->fill($attributes);

        $changes = array_merge(
            ['_scenario' => HistoryOfEdits::SCENARIO_UPDATE],
            $model->getChanges()
        );

        $model->save();

        /* Save changes into history */
        $model->historyOfEdits
            ->setRequiredParams(self::SOURCE_NAME, $id, $changes)
            ->save();

        return $model;
    }

    public static function deleteModel(int $id)
    {
        $model = self::find($id);

        $changes = [
            '_scenario' => HistoryOfEdits::SCENARIO_DELETE,
            'id' => $model->id,
            'doc_id' => $model->doc_id,
            'date' => $model->date,
            'key' => $model->key,
            'currency' => $model->cashbox->currency->code . ' - ' . $model->cashbox->currency->country,
            'cashbox' => $model->cashbox->alias,
            'amount' => $model->amount,
        ];

        $model->delete();

        $model->historyOfEdits
            ->setRequiredParams(self::SOURCE_NAME, $model->id, $changes)
            ->save();
    }

    public static function getParamsData(Request $request)
    {
        return [
            'route'         => $request->route()->getName(),
            'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
            'query'         => is_null($request->getQueryString())
                ? '' : '?'. $request->getQueryString(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionIndexParams(Request $request)
    {
        $incomings = CashboxesIncoming::select()
            ->whereHas('cashbox', function($query){
                return $query->where(UserCashbox::FIELD_DELETED, '=', null);
            })
            ->with('cashbox')
            ->with(['document' => function($query){
                return $query
                    ->with(['docItemsUser' => function($query){
                        return $query->with('user');
                    }])
                    ->with(['docItemsAccount' => function($query){
                        return $query->with('account');
                    }])
                    ->with(['docItemsClient' => function($query){
                        return $query->with('client');
                    }])
                    ->with(['docItemsReportCurrency' => function($query){
                        return $query->with('reportCurrency');
                    }]);
            }])
            ->get();

        $params = [
            'incomings'  => $incomings,
            'data'          => self::getParamsData($request),
        ];

        return $params;
    }

    /**
     * Prepared parameters for Create and Update forms
     *
     * @return array
     */
    private static function prepareResponseParams()
    {
        $userCashboxes = UserCashbox::select()
            ->whereNull(UserCashbox::FIELD_DELETED)
            ->get()
            ->groupBy('user_id')
            ->toArray();
        foreach ($userCashboxes as &$cashboxes){
            foreach ($cashboxes as &$cashbox){
                $cashbox['text'] = $cashbox['alias'];
            }
        }
        $params = [
            'users'         => User::select()->get(),
            'cashboxes'     => $userCashboxes,
            'accounts'      => ReportAccount::select()->get(),
            'clients'       => Client::select()->get(),
            'currencies'    => ReportCurrency::select()->get(),
        ];

        return $params;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionCreateParams(Request $request)
    {
        return array_merge(
            self::prepareResponseParams(),
            [
                'authUserId'    => HistoryOfEdits::$authUserId,
                'data'          => self::getParamsData($request),
            ]
        );
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return array
     */
    public static function getActionEditParams(int $id, Request $request)
    {
        $doc = Document::select()
            ->where(Document::FIELD_ID, '=', $id)
            ->first();

        return array_merge(
            self::prepareResponseParams(),
            [
                'doc'   => $doc,
                'data'  => self::getParamsData($request),
            ]
        );
    }
}