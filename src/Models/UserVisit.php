<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class UserVisit extends Model
{
    use AliasableEncryptable;

    const TABLE_NAME        = 'b';
    const SOURCE_NAME       = 'userVisit';

    const FIELD_ID          = 'id';
    const FIELD_USER_ID     = 'a';
    const FIELD_TIME        = 'b';
    const FIELD_IP          = 'c';
    const FIELD_USER_AGENT  = 'd';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_IP,
        self::FIELD_USER_AGENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'user_id'    => self::FIELD_USER_ID,
        'time'       => self::FIELD_TIME,
        'ip'         => self::FIELD_IP,
        'user_agent' => self::FIELD_USER_AGENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'ip',
        'time',
        'user_agent',
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class,User::FIELD_ID, self::FIELD_USER_ID);
    }
}
