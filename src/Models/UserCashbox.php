<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;

class UserCashbox extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'e';
    const SOURCE_NAME = 'UserCashbox';

    const FIELD_ID = 'id';
    const FIELD_USER_ID = 'a';
    const FIELD_CURRENCY_ID = 'b';
    const FIELD_ALIAS = 'e';
    const FIELD_DELETED = 'f';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_ALIAS,
    ];

    protected $hidden = [
        self::FIELD_DELETED,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'user_id'       => self::FIELD_USER_ID,
        'currency_id'   => self::FIELD_CURRENCY_ID,
        'alias'         => self::FIELD_ALIAS,
        'deleted'       => self::FIELD_DELETED,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'currency_id',
        'alias',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function user()
    {
        return $this->belongsTo(User::class, self::FIELD_USER_ID, User::FIELD_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, self::FIELD_CURRENCY_ID, Currency::FIELD_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function incoming()
    {
        return $this
            ->hasMany(CashboxesIncoming::class, CashboxesIncoming::FIELD_CASHBOX_ID, self::FIELD_ID)
            ->orderByDesc(CashboxesIncoming::FIELD_DATE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outcoming()
    {
        return $this
            ->hasMany(CashboxesOutcoming::class, CashboxesOutcoming::FIELD_CASHBOX_ID, self::FIELD_ID)
            ->orderByDesc(CashboxesOutcoming::FIELD_DATE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function incomingStartBalance()
    {
        return $this
            ->hasMany(CashboxesIncoming::class, CashboxesIncoming::FIELD_CASHBOX_ID, self::FIELD_ID)
            ->where(CashboxesIncoming::FIELD_KEY, '=', Crypt::encrypt(CashboxesIncoming::KEY_START_BALANCE));
    }

    /**
     * @param Collection $params
     *
     * @return UserCashbox
     */
    public static function insertModel(Collection $params)
    {
        $attributes = [
            'user_id'       => $params->get('user_id'),
            'currency_id'   => $params->get('currency_id'),
            'alias'         => $params->get('alias'),
        ];

        Validator::make($attributes, [
            'user_id' => ['required', 'exists:' . User::TABLE_NAME . ',' . User::FIELD_ID],
            'currency_id' => ['required', 'exists:' . Currency::TABLE_NAME . ',' . Currency::FIELD_ID],
            'alias' => ['max:255'],
        ])->validate();
        // todo дописать проверку на уникальность для данного пользователя
//        Validator::make([
//            'alias' => Crypt::encrypt($params->get('alias')),
//        ], [
//            'alias' => Rule::unique('alias')->where(function ($query) use($params) {
//                $query->where('user_id', $params->get('user_id'));
//            }),
//        ])->validate();

        $model = new self();
        $model->fill($attributes)->save();

        return $model;
    }

    /**
     * @param Collection $params
     *
     * @return UserCashbox
     */
    public static function updateModel(int $id, Collection $params)
    {
        $attributes = [
            'user_id'       => $params->get('user_id'),
            'currency_id'   => $params->get('currency_id'),
            'alias'         => $params->get('alias'),
        ];

        Validator::make($attributes, [
            'user_id' => ['required', 'exists:' . User::TABLE_NAME . ',' . User::FIELD_ID],
            'currency_id' => ['required', 'exists:' . Currency::TABLE_NAME . ',' . Currency::FIELD_ID],
            'alias' => ['max:255'],
        ])->validate();
        // todo дописать проверку на уникальность для данного пользователя
//        Validator::make([
//            'alias' => Crypt::encrypt($params->get('alias')),
//        ], [
//            'alias' => Rule::unique('alias')->where(function ($query) use($params) {
//                $query->where('user_id', $params->get('user_id'));
//            }),
//        ])->validate();

        $model = self::find($id);
        $model->fill($attributes)->save();

        return $model;
    }

    public static function deleteModel($id)
    {
        $model = self::find($id);
        $model->deleted = new Carbon('utc');
        $model->save();

        return true;
    }

}