<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    const TABLE_NAME    = 'currencies';

    const FIELD_ID      = 'id';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cashboxes()
    {
        return $this->hasMany(UserCashbox::class, UserCashbox::FIELD_CURRENCY_ID, self::FIELD_ID);
    }
}
