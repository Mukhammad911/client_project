<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class Association extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'aj';

    const FIELD_ID          = 'id';
    const FIELD_DOC_ID      = 'a';
    const FIELD_NAME        = 'b';
    const FIELD_DATE        = 'c';
    const FIELD_AMOUNT      = 'd';
    const FIELD_CURRENCY_ID = 'e';
    const FIELD_CASHBOX_ID  = 'f';
    const FIELD_COMMENT     = 'g';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    const NAME_DEALER           = 'dealer';
    const NAME_TRANSPORT        = 'transport';
    const NAME_TRAVEL_CARD      = 'travel_card';
    const NAME_DOCUMENTS        = 'documents';
    const NAME_SELLER_DEALER    = 'seller_dealer';
    const NAME_WAREHOUSE_PAYD   = 'warehouse_payd';
    const NAME_OUTCOME_AB       = 'outcome_ab';

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserGroupCreated::class,
//        'updated' => UserGroupUpdated::class,
//        'deleted' => UserGroupDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'doc_id'        => self::FIELD_DOC_ID,
        'date'          => self::FIELD_DATE,
        'amount'        => self::FIELD_AMOUNT,
        'currency_id'   => self::FIELD_CURRENCY_ID,
        'cashbox_id'    => self::FIELD_CASHBOX_ID,
        'comment'       => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'doc_id',
        'date',
        'amount',
        'currency_id',
        'cashbox_id',
        'comment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function document()
    {
        return $this->belongsTo(Document::class, self::FIELD_DOC_ID, Document::FIELD_ID);
    }
}
