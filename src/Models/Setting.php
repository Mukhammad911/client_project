<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Nexmo\Call\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\WarehouseCreated;
use Teleglobal\Accounting\Events\WarehouseUpdated;
use Teleglobal\Accounting\Events\WarehouseDeleted;
use Illuminate\Support\Facades\DB;

class Setting extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'w';
    const SOURCE_NAME   = 'Setting';

    const FIELD_ID          = 'id';
    const FIELD_KEY         = 'a';
    const FIELD_VALUE       = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    const KEY_CURRENCIES = 'currencies';

    private $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        /*'created' => WarehouseCreated::class,
        'updated' => WarehouseUpdated::class,
        'deleted' => WarehouseDeleted::class,*/
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_KEY,
        self::FIELD_VALUE,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'key'         => self::FIELD_KEY,
        'value'       => self::FIELD_VALUE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @param Request $request
     * @param         $key
     *
     * @return $this
     */
    public function insertModel(Request $request, $key)
    {
        $value = $request->input($key); // JSON

        // TODO сделать валидацию входящих данных связанных с другими моделями

        $attributes = [
            'key'    => $key,
            'value'  => $value,
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'value'          => ['required'],
        ])->validate();

        Validator::make([
            'key'  => Crypt::encrypt($key),
        ], [
            'key'  => ['required', 'unique:'.self::TABLE_NAME.','.self::FIELD_KEY],
        ])->validate();

        /**
         * Save changes into the history
         */

        $this->setAttribute(self::FIELD_KEY, $key);
        $this->setAttribute(self::FIELD_VALUE, $value);
        $this->save();

        return $this;
    }

    /**
     * @param Request $request
     * @param         $key
     *
     * @return $this
     */
    public function updateModel(Request $request, $key)
    {
        $value = $request->input($key); // JSON

        // TODO сделать валидацию входящих данных связанных с другими моделями

        $attributes = [
            'value'  => $value,
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'value'          => ['required'],
        ])->validate();

        /**
         * Save changes into the history
         */

        $this->setAttribute(self::FIELD_VALUE, $value);
        $this->save();

        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteModel()
    {
        $this->delete();

        return true;
    }
}
