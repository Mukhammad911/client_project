<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\UserCreated;
use Teleglobal\Accounting\Events\UserUpdated;
use Teleglobal\Accounting\Events\UserDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserGroupUser extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'h';

    const FIELD_USER_ID         = 'a';
    const FIELD_USER_GROUP_ID   = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'user_id'       => self::FIELD_USER_ID,
        'user_group_id' => self::FIELD_USER_GROUP_ID,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_group_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'user_group_id',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', self::FIELD_USER_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function group()
    {
        return $this->hasOne(UserGroup::class, 'id', self::FIELD_USER_GROUP_ID);
    }

    /**
     * @param int $userId
     * @param int $userGroupId
     *
     * @return UserGroupUser
     */
    private static function getModel(int $userId, int $userGroupId)
    {
        $model = self::where([
            [self::FIELD_USER_ID, '=', $userId],
            [self::FIELD_USER_GROUP_ID, '=', $userGroupId],
        ])->first();

        return ($model) ? $model : new self();
    }

    /**
     * @param Collection $params
     * @param User $user
     *
     * @return bool
     */
    public static function updateModels(Collection $params, User $user){

        $oldUserGroups = $user->groups->flatten();
        $newUserGroups = $params->flatten();

        $diffOldIds = $oldUserGroups->diff($newUserGroups);
        $diffNewIds = $newUserGroups->diff($oldUserGroups);

        foreach ($diffOldIds as $diffOldId){
            self::deleteModel($user->id, $diffOldId);
        }
        foreach ($diffNewIds as $diffNewId){
            self::insertModel($user->id, $diffNewId);
        }

        return true;
    }

    /**
     * @param int $userId
     * @param int $userGroupId
     *
     * @return bool
     */
    public static function insertModel(int $userId, int $userGroupId){

        $attributes = [
            'user_id'       => $userId,
            'user_group_id' => $userGroupId,
        ];

        Validator::make($attributes, [
            'user_id'       => ['required', 'exists:'.User::TABLE_NAME.','.User::FIELD_ID],
            'user_group_id' => ['required', 'exists:'.UserGroup::TABLE_NAME.','.UserGroup::FIELD_ID],
        ])->validate();

        $model = new self();
        $model->fill($attributes)->save();

        return true;
    }

    /**
     * @param int $userId
     * @param int $userGroupId
     *
     * @return bool
     */
    public static function deleteModel(int $userId, int $userGroupId){

        self::where([
            [self::FIELD_USER_ID, '=', $userId],
            [self::FIELD_USER_GROUP_ID, '=', $userGroupId],
        ])->delete();

        return true;
    }
}
