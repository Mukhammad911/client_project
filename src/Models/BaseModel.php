<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use phpDocumentor\Reflection\Types\Integer;

class BaseModel extends Model
{
    const TABLE_NAME = null;
    const SOURCE_NAME = null;

    const FIELD_ID = 'id';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    protected $page = 1;
    protected $pagesCount = null;
    protected $recordsCount = null;
    protected $recordsPerPage = null;
    protected $space = null;

    protected $result = null;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->recordsPerPage = config('accounting.app.pagination.records_per_page');
        $this->space = config('accounting.app.pagination.space');
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return null
     */
    public function getPagesCount()
    {
        return $this->pagesCount;
    }

    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed|null
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * Добавление пагинации в запрос
     *
     * @param Builder $query
     *
     * @param int $pageNumber
     */
    protected function setPagination(Builder $query, int $pageNumber = 1)
    {
        $this->recordsCount = $query->get()->count();
        $this->page = $pageNumber;
        $this->pagesCount = ceil($this->recordsCount / $this->recordsPerPage);

        $query->offset(($pageNumber - 1) * $this->recordsPerPage);
        $query->limit($this->recordsPerPage);
    }

}
