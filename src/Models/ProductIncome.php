<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\IncomeCreated;
use Teleglobal\Accounting\Events\IncomeUpdated;
use Teleglobal\Accounting\Events\IncomeDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProductIncome extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'q';
    const SOURCE_NAME   = 'ProductIncome';

    const FIELD_ID          = 'id';
    const FIELD_INCOME_ID   = 'a';
    const FIELD_PRODUCT_ID  = 'b';
    const FIELD_QUANTITY    = 'c';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => IncomeCreated::class,
        'updated' => IncomeUpdated::class,
        'deleted' => IncomeDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [

    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'income_id'     => self::FIELD_INCOME_ID,
        'product_id'    => self::FIELD_PRODUCT_ID,
        'quantity'      => self::FIELD_QUANTITY,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'income_id',
        'product_id',
        'quantity',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param Request                              $request
     * @param \Teleglobal\Accounting\Models\Income $income
     *
     * @return $this
     */
    public function insertModel(Request $request, Income $income)
    {
        $params = collect($request->input());

        /*
         * Validate data
         */
        $products = $params->get('products');

        for($i=0; $i+1<=count($products['id']); $i++) {
            Validator::make([
                'id'        => $products['id'][$i],
                'quantity'  => $products['quantity'][$i],
            ], [
                'id'        => ['required', 'exists:'.Product::TABLE_NAME.','.Product::FIELD_ID],
                'quantity'  => ['required', 'numeric'],
            ])->validate();
        }

            for($i=0; $i+1<=count($products['id']); $i++) {
            $this->setAttribute(self::FIELD_INCOME_ID, $income->id);
            $this->setAttribute(self::FIELD_PRODUCT_ID, $products['id'][$i]);
            $this->setAttribute(self::FIELD_QUANTITY, $products['quantity'][$i]);
            $this->save();
        }

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function updateModel(Request $request)
    {
        $params = collect($request->input());

        /**
         * Validate data
         */
        Validator::make([
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
        ], [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$this->getAttribute(self::FIELD_ID)],
        ])->validate();

        $this->setAttribute(self::FIELD_NAME, $params->get('name'));
        $this->setAttribute(self::FIELD_COMMENT, $params->get('comment'));
        $this->setAttribute('updated_at', Carbon::now());
        $this->save();

        return $this;
    }

    public function deleteModel()
    {
        $this->delete();
        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function income()
    {
        return $this->belongsTo(Income::class, Income::FIELD_ID, $this::FIELD_INCOME_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, Product::FIELD_ID, $this::FIELD_PRODUCT_ID);
    }
}
