<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Nexmo\Call\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class Payment extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'v';
    const SOURCE_NAME = 'Payment';

    const FIELD_ID          = 'id';
    const FIELD_DOC_ID      = 'a';
    const FIELD_DATE        = 'b';
    const FIELD_AMOUNT      = 'c';
    const FIELD_CURRENCY_ID = 'd';
    const FIELD_COMMENT     = 'e';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'doc_id'        => self::FIELD_DOC_ID,
        'date'          => self::FIELD_DATE,
        'amount'        => self::FIELD_AMOUNT,
        'currency_id'   => self::FIELD_CURRENCY_ID,
        'comment'       => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_id',
        'date',
        'amount',
        'currency_id',
        'comment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Document constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, self::FIELD_DOC_ID, Document::FIELD_ID);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, self::FIELD_CURRENCY_ID, Currency::FIELD_ID);
    }

    /**
     * @param $docId
     * @param $date
     * @param $amount
     * @param $currencyId
     * @param $comment
     *
     * @return Payment
     */
    public static function insertModel($docId, $date, $amount, $currencyId, $comment = null)
    {
        $model = new Payment();

        $attributes = [
            'doc_id'        => $docId,
            'date'          => $date,
            'amount'        => $amount,
            'currency_id'   => $currencyId,
            'comments'      => $comment,
        ];

        $model->fill($attributes)->save();

        return $model;
    }

}
