<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use PhpParser\Comment\Doc;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class ProductsIncoming extends BaseModel
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'p';
    const SOURCE_NAME = 'ProductsIncoming';

    const FIELD_ID          = 'id';
    const FIELD_DOC_ID      = 'a';
    const FIELD_DATE        = 'b';
    const FIELD_PIVOT_ID    = 'c';
    const FIELD_AMOUNT      = 'd';

    const SOURCE_WAREHOUSE  = 'warehouse';
    const SOURCE_CLIENT     = 'client';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'doc_id'    => self::FIELD_DOC_ID,
        'date'      => self::FIELD_DATE,
        'pivot_id'  => self::FIELD_PIVOT_ID,
        'amount'    => self::FIELD_AMOUNT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_id',
        'date',
        'pivot_id',
        'amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * ProductsIncoming constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, self::FIELD_DOC_ID, Document::FIELD_ID);
    }

    public function pivot()
    {
        return $this->hasOne(WarehouseProductPivot::class, WarehouseProductPivot::FIELD_ID, self::FIELD_PIVOT_ID);
    }

    /**
     * @param $docId
     * @param $date
     * @param $warehouseId
     * @param $productId
     * @param $amount
     *
     * @return ProductsIncoming
     */
    public static function insertModel($docId, $date, $warehouseId, $productId, $amount)
    {
        // todo Дописать валидацию

        $pivot = WarehouseProductPivot::insertModel($warehouseId, $productId);

        $amount = number_format((float)($amount ?? 0), 3);

        $attributes = [
            'doc_id'    => $docId,
            'date'      => $date,
            'pivot_id'  => $pivot->id,
            'amount'    => $amount,
        ];

        $model = new ProductsIncoming();
        $model->fill($attributes)->save();

        return $model;
    }

    /**
     * @param Collection $params
     *
     * @return ProductsIncoming
     */
    public function getDocumentsList(Collection $params) : ProductsIncoming
    {
        $dTable = Document::TABLE_NAME;
        $dId = $dTable.'.'.Document::FIELD_ID;
        $dDocType = $dTable.'.'.Document::FIELD_DOC_TYPE;
        $dDocNumber = $dTable.'.'.Document::FIELD_DOC_NUMBER;
        $pTable = ProductsIncoming::TABLE_NAME;
        $pId = $pTable.'.'.ProductsIncoming::FIELD_ID;
        $pDocId = $pTable.'.'.ProductsIncoming::FIELD_DOC_ID;
        $pDate = $pTable.'.'.ProductsIncoming::FIELD_DATE;

        $query = Document::select(
            $dId . ' as id',
            $dDocNumber,
            $pDate
        )
            ->where($dDocType, '=', Crypt::encrypt(Document::DOC_TYPE_INCOMES))
            ->leftJoin($pTable, function($query) use ($dId, $pTable, $pId, $pDocId){
                return $query->on($pId, '=', DB::raw("(SELECT $pId FROM $pTable WHERE $pDocId = $dId LIMIT 1)"));
            });

// todo Подумать над сортировкой криптованных полей
        $query->orderBy(($params->get('order') ?? $pDate), 'desc');

        if(!empty($params->get('date_from')) && !empty($params->get('date_to'))){
            $dateFrom = date_create_from_format(config('accounting.app.date_format'), $params->get('date_from'));
            $dateTo = date_create_from_format(config('accounting.app.date_format'), $params->get('date_to'));

            $query
                ->whereDate($pDate, '>=', date_format($dateFrom, 'Y-m-d'))
                ->whereDate($pDate, '<=', date_format($dateTo, 'Y-m-d'));
        }

        // Устанавливаем пагинацию
        $this->setPagination($query, $params->get('page') ?? 1);

        $query->with(['productsIncoming' => function($query){
            return $query->with(['pivot' => function($query){
                return $query
                    ->with('warehouse')
                    ->with('product');
            }]);
        }])
            ->with('payment')
            ->with('history');

        $this->result = $query->get();

        return $this;
    }

    public function getDocumentsListAjax(Collection $params)
    {
        $this->getDocumentsList($params);
        $data = [];

        $documents = $this->result;
        foreach ($documents as $document){
            $products = [];
            foreach ($document->productsIncoming as $product){
                $products[] = [
                    'name'  => $product->pivot->product->name,
                    'amount' => number_format((float)$product->amount, 0),
                ];
            }
            $data[] = [
                'id'                => $document->id,
                'doc_number'        => $document->doc_number,
                'warehouse'         => $document->productsIncoming()->first()->pivot->warehouse->name,
                'date'              => (new Carbon($document->productsIncoming()->first()->date))->format(config('accounting.app.date_format')),
                'products'          => $products,
                'created_by'        => $document->history()->first()->user->name,
                'payment'           => ($t = $document->payment()->first()) ? __('accounting::incomes.pages.list.payed') : __('accounting::incomes.pages.list.not_payed'),
                'payment_date'      => ($t) ? (new Carbon($t->date))->format(config('accounting.app.date_format')) : '',
                'payment_amount'    => ($t) ? number_format((float)$t->amount, 2) : '',
                'payment_currency'  => ($t) ? $t->currency->code . ' - ' . $t->currency->country : '',
            ];
        }

        $this->result = $data;

        return $this;
    }

    /**
     * @param int $warehouseId
     * @param array $docTypes
     * @param null $dateFrom
     * @param null $dateTo
     *
     * @return mixed
     */
    public static function getIncomingProducts(int $warehouseId, array $docTypes = [], $dateFrom = null, $dateTo = null)
    {
        $query = Document::select();

        $dateFrom = ($dateFrom !== null) ? new Carbon($dateFrom) : null;
        $dateTo = ($dateTo !== null) ? new Carbon($dateTo) : null;

        if(!empty($docTypes)){
            $cryptDocTypes = [];
            foreach ($docTypes as $docType){
                $cryptDocTypes[] = Crypt::encrypt($docType);
            }
            $query->whereIn(Document::FIELD_DOC_TYPE, $cryptDocTypes);
        }

        $query
            ->whereHas('productsIncoming', function ($query) use ($warehouseId, $dateFrom, $dateTo) {
                if($dateFrom){
                    $query->whereRaw(ProductsIncoming::FIELD_DATE . '::date >=' . '\'' . $dateFrom . '\'');
                }
                if($dateTo){
                    $query->whereRaw(ProductsIncoming::FIELD_DATE . '::date <=' . '\'' . $dateTo . '\'');
                }
                $query->whereHas('pivot', function ($query) use ($warehouseId) {
                    $query->where(WarehouseProductPivot::FIELD_WAREHOUSE_ID, '=', $warehouseId);
                });
            })
            ->with(['productsIncoming' => function($query) use ($warehouseId, $dateFrom, $dateTo){
                if($dateFrom){
                    $query->whereRaw(ProductsIncoming::FIELD_DATE . '::date >=' . '\'' . $dateFrom . '\'');
                }
                if($dateTo){
                    $query->whereRaw(ProductsIncoming::FIELD_DATE . '::date <=' . '\'' . $dateTo . '\'');
                }
                $query
                    ->with(['pivot' => function($query) use ($warehouseId){
                        $query
                            ->select(WarehouseProductPivot::TABLE_NAME.'.*')
                            ->where(WarehouseProductPivot::FIELD_WAREHOUSE_ID, '=', $warehouseId)
                            ->with('warehouse')
                            ->with('product')
                        ;
                    }]);
            }]);

        return $query->get();
    }

    public static function getStatisticOfIncomingProducts(int $warehouseId, array $docTypes = [], $dateFrom = null, $dateTo = null)
    {
//        $query = WarehouseProductPivot::select();
//        $query
//            ->where(WarehouseProductPivot::FIELD_WAREHOUSE_ID, '=', $warehouseId)
//            ->whereHas(['incoming' => function($query) use ($dateFrom, $dateTo){
//                if($dateFrom){
//                    $query->whereRaw(ProductsIncoming::FIELD_DATE . '::date >=' . '\'' . $dateFrom . '\'');
//                }
//                if($dateTo){
//                    $query->whereRaw(ProductsIncoming::FIELD_DATE . '::date <=' . '\'' . $dateTo . '\'');
//                }
//            }]);
    }
}
