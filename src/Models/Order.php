<?php

namespace Teleglobal\Accounting\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class Order extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME  = 'af';
    const SOURCE_NAME = 'Order';

    const FIELD_ID                      = 'id';
    const FIELD_ACCOUNT_ID              = 'a';
    const FIELD_POSITION_ID             = 'b';
    const FIELD_QUANTITY                = 'c';
    const FIELD_AMOUNT                  = 'd';
    const FIELD_DATE_ORDER              = 'e';
    const FIELD_COMMENT                 = 'f';
    const FIELD_DATE_EXECUTION          = 'g';
    const FIELD_ASSOC_TRANSPORT_ID      = 'h';
    const FIELD_ASSOC_TRANSPORT_AMOUNT  = 'i';
    const FIELD_ASSOC_DELIVERY_AMOUNT   = 'j';
    const FIELD_ASSOC_SERVICE_AMOUNT    = 'k';
    const FIELD_ASSOC_DOCUMENTS_AMOUNT  = 'l';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    private $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => DealerCreated::class,
//        'updated' => DealerUpdated::class,
//        'deleted' => DealerDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'account_id'                => self::FIELD_ACCOUNT_ID,
        'position_id'               => self::FIELD_POSITION_ID,
        'quantity'                  => self::FIELD_QUANTITY,
        'amount'                    => self::FIELD_AMOUNT,
        'date_order'                => self::FIELD_DATE_ORDER,
        'comment'                   => self::FIELD_COMMENT,
        'date_execution'            => self::FIELD_DATE_EXECUTION,
        'assoc_transport_id'        => self::FIELD_ASSOC_TRANSPORT_ID,
        'assoc_transport_amount'    => self::FIELD_ASSOC_TRANSPORT_AMOUNT,
        'assoc_delivery_amount'     => self::FIELD_ASSOC_DELIVERY_AMOUNT,
        'assoc_service_amount'      => self::FIELD_ASSOC_SERVICE_AMOUNT,
        'assoc_documents_amount'    => self::FIELD_ASSOC_DOCUMENTS_AMOUNT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'position_id',
        'quantity',
        'amount',
        'date_order',
        'comment',
        'date_execution',
        'assoc_transport_id',
        'assoc_transport_amount',
        'assoc_delivery_amount',
        'assoc_service_amount',
        'assoc_documents_amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function account()
    {
        return $this->belongsTo(ReportAccount::class, self::FIELD_ACCOUNT_ID, ReportAccount::FIELD_ID);
    }

    public function position()
    {
        return $this->belongsTo(OrderPosition::class, self::FIELD_POSITION_ID, OrderPosition::FIELD_ID);
    }

    public function transport()
    {
        return $this->belongsTo(OrderTransport::class, self::FIELD_ASSOC_TRANSPORT_ID, OrderTransport::FIELD_ID);
    }

    public function history()
    {
        return $this
            ->hasMany(HistoryOfEdits::class, HistoryOfEdits::FIELD_SOURCE_ID, self::FIELD_ID)
            ->where(HistoryOfEdits::FIELD_SOURCE, Crypt::encrypt(self::SOURCE_NAME))
            ->orderBy(HistoryOfEdits::FIELD_DATE_EDITED);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @param Collection $params
     *
     * @return Document
     */
    public static function insertModel(Collection $params)
    {
        $attributes = [
            'account_id'                => $params->get('account_id'),
            'position_id'               => $params->get('position_id'),
            'quantity'                  => $params->get('quantity'),
            'amount'                    => $params->get('amount'),
            'date_order'                => new Carbon($params->get('date_order')),
            'comment'                   => $params->get('comment'),
            'date_execution'            => new Carbon($params->get('date_execution')),
            'assoc_transport_id'        => $params->get('assoc_transport_id'),
            'assoc_transport_amount'    => $params->get('assoc_transport_amount'),
            'assoc_delivery_amount'     => $params->get('assoc_delivery_amount'),
            'assoc_service_amount'      => $params->get('assoc_service_amount'),
            'assoc_documents_amount'    => $params->get('assoc_documents_amount'),
        ];

        /*
         * Validate data
         */
        Validator::make($attributes, [
            'account_id'                => ['required','exists:'.ReportAccount::TABLE_NAME.','.ReportAccount::FIELD_ID],
            'position_id'               => ['required','exists:'.OrderPosition::TABLE_NAME.','.OrderPosition::FIELD_ID],
            'quantity'                  => ['required','numeric'],
            'amount'                    => ['required','numeric'],
            'date_order'                => ['required'],
            'comment'                   => ['max:255'],
            'date_execution'            => ['required'],
            'assoc_transport_id'        => ['nullable','exists:'.OrderTransport::TABLE_NAME.','.OrderTransport::FIELD_ID],
            'assoc_transport_amount'    => ['nullable','numeric'],
            'assoc_delivery_amount'     => ['nullable','numeric'],
            'assoc_service_amount'      => ['nullable','numeric'],
            'assoc_documents_amount'    => ['nullable','numeric'],
        ])->validate();

        $model = new self();
        $model->fill($attributes)->save();

        $attributes['_scenario'] = HistoryOfEdits::SCENARIO_CREATE;
        $model->historyOfEdits->setRequiredParams(
            self::SOURCE_NAME,
            $model->getAttribute(self::FIELD_ID),
            $attributes)->save();

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $params
     *
     * @return Document|bool
     */
    public static function updateModel(int $id, Collection $params)
    {
        $model = self::find($id);

        if(empty($model)){
            return false;
        }

        $attributes = [
            'account_id'                => $params->get('account_id'),
            'position_id'               => $params->get('position_id'),
            'quantity'                  => $params->get('quantity'),
            'amount'                    => $params->get('amount'),
            'date_order'                => $params->get('date_order'),
            'comment'                   => $params->get('comment'),
            'date_execution'            => $params->get('date_execution'),
            'assoc_transport_id'        => $params->get('assoc_transport_id'),
            'assoc_transport_amount'    => $params->get('assoc_transport_amount'),
            'assoc_delivery_amount'     => $params->get('assoc_delivery_amount'),
            'assoc_service_amount'      => $params->get('assoc_service_amount'),
            'assoc_documents_amount'    => $params->get('assoc_documents_amount'),
        ];

        /*
         * Validate data
         */
        Validator::make($attributes, [
            'account_id'                => ['required','exists:'.ReportAccount::TABLE_NAME.','.ReportAccount::FIELD_ID],
            'position_id'               => ['required','exists:'.OrderPosition::TABLE_NAME.','.OrderPosition::FIELD_ID],
            'quantity'                  => ['required','numeric'],
            'amount'                    => ['required','numeric'],
            'date_order'                => ['required'],
            'comment'                   => ['max:255'],
            'date_execution'            => ['required'],
            'assoc_transport_id'        => ['nullable','exists:'.OrderTransport::TABLE_NAME.','.OrderTransport::FIELD_ID],
            'assoc_transport_amount'    => ['nullable','numeric'],
            'assoc_delivery_amount'     => ['nullable','numeric'],
            'assoc_service_amount'      => ['nullable','numeric'],
            'assoc_documents_amount'    => ['nullable','numeric'],
        ])->validate();

        $model->fill($attributes);
        $changes = $model->getDirty();
        $model->save();

        $changes['_scenario'] = HistoryOfEdits::SCENARIO_UPDATE;
        $model->historyOfEdits->setRequiredParams(
            self::SOURCE_NAME,
            $model->getAttribute(self::FIELD_ID),
            $changes)->save();

        return $model;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public static function deleteModel(int $id)
    {
        return self::find($id)->delete();
    }

    public static function getParamsData(Request $request)
    {
        return [
            'route'         => $request->route()->getName(),
            'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
            'query'         => is_null($request->getQueryString())
                ? '' : '?'. $request->getQueryString(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionIndexParams(Request $request)
    {
        $orders = Order::select()
            ->with('account')
            ->with('position')
            ->with('transport')
            ->with('history')
            ->get();
        $params = [
            'orders'      => $orders,
            'data'        => self::getParamsData($request),
        ];

        return $params;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public static function getActionCreateParams(Request $request)
    {
        $params = [
            'accounts'      => ReportAccount::all(),
            'positions'     => OrderPosition::all(),
            'transports'    => OrderTransport::all(),
            'data'          => self::getParamsData($request),
        ];

        return $params;
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return array
     */
    public static function getActionEditParams(int $id, Request $request)
    {
        $params = [
            'order'         => Order::find($id),
            'accounts'      => ReportAccount::all(),
            'positions'     => OrderPosition::all(),
            'transports'    => OrderTransport::all(),
            'data'      => self::getParamsData($request),
        ];

        return $params;
    }
}
