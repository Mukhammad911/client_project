<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Nexmo\Call\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\WarehouseCreated;
use Teleglobal\Accounting\Events\WarehouseUpdated;
use Teleglobal\Accounting\Events\WarehouseDeleted;
use Illuminate\Support\Facades\DB;

class SalaryRecipient extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 's';
    const SOURCE_NAME   = 'SalaryRecipient';

    const FIELD_ID          = 'id';
    const FIELD_NAME        = 'a';
    const FIELD_COMMENT     = 'b';
    const FIELD_AMOUNT      = 'c';
    const FIELD_CURRENCY    = 'd';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    private $historyOfEdits = null;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        /*'created' => WarehouseCreated::class,
        'updated' => WarehouseUpdated::class,
        'deleted' => WarehouseDeleted::class,*/
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
        self::FIELD_AMOUNT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'comment'       => self::FIELD_COMMENT,
        'amount'        => self::FIELD_AMOUNT,
        'currency_id'   => self::FIELD_CURRENCY,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
        'amount',
        'currency_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyOfEdits = new HistoryOfEdits();
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function insertModel(Request $request)
    {
        $params = collect($request->input());

        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
            'amount'        => $params->get('amount'),
            'currency'   => $params->get('currency'),
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'name'          => ['required', 'max:255'],
            'comment'       => ['nullable'],
            'amount'        => ['required','numeric','min:0.1'],
            'currency'      => ['required', 'exists:'.Currency::TABLE_NAME.','.Currency::FIELD_ID],
        ])->validate();

        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['required', 'unique:'.self::TABLE_NAME.','.self::FIELD_NAME],
        ])->validate();

        /**
         * Save changes into the history
         */

        $this->setAttribute(self::FIELD_NAME, $params->get('name'));
        $this->setAttribute(self::FIELD_COMMENT, $params->get('comment'));
        $this->setAttribute(self::FIELD_AMOUNT, (float)$params->get('amount'));
        $this->setAttribute(self::FIELD_CURRENCY, $params->get('currency'));
        $this->save();

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function updateModel(Request $request)
    {
        $params = collect($request->input());

        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
            'amount'        => $params->get('amount'),
            'currency'   => $params->get('currency'),
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'name'          => ['required', 'max:255'],
            'comment'       => ['nullable'],
            'amount'        => ['required','numeric','min:0.1'],
            'currency'      => ['required', 'exists:'.Currency::TABLE_NAME.','.Currency::FIELD_ID],
        ])->validate();

        /**
         * Save changes into the history
         */

        $this->setAttribute(self::FIELD_NAME, $params->get('name'));
        $this->setAttribute(self::FIELD_COMMENT, $params->get('comment'));
        $this->setAttribute(self::FIELD_AMOUNT, (float)$params->get('amount'));
        $this->setAttribute(self::FIELD_CURRENCY, $params->get('currency'));
        $this->save();

        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteModel()
    {
        $this->delete();

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currency()
    {
        return $this->hasOne(Currency::class, Currency::FIELD_ID, self::FIELD_CURRENCY);
    }
}
