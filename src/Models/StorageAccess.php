<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\UserCreated;
use Teleglobal\Accounting\Events\UserUpdated;
use Teleglobal\Accounting\Events\UserDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Storage extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'j';
    protected $table = self::TABLE_NAME;

    const FIELD_STORAGE_ID  = 'a';
    const FIELD_SOURCE_TYPE = 'b';
    const FIELD_SOURCE_ID   = 'c';

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => UserCreated::class,
        'updated' => UserUpdated::class,
        'deleted' => UserDeleted::class
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_STORAGE_ID,
        self::FIELD_SOURCE_TYPE,
        self::FIELD_SOURCE_ID
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'storage_id'    => self::FIELD_SOURCE_ID,
        'source_type'   => self::FIELD_SOURCE_TYPE,
        'source_id'     => self::FIELD_SOURCE_ID
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'storage_id', 'source_type', 'source_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


}
