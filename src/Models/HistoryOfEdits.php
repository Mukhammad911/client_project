<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Carbon\Carbon;

class HistoryOfEdits extends Model
{
    use AliasableEncryptable;

    const TABLE_NAME = 'd';

    const FIELD_ID              = 'id';
    const FIELD_USER_ID         = 'a';
    const FIELD_DATE_EDITED     = 'b';
    const FIELD_IP              = 'c';
    const FIELD_USER_AGENT      = 'd';
    const FIELD_SOURCE          = 'e';
    const FIELD_SOURCE_ID       = 'f';
    const FIELD_CHANGES         = 'g';

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_DELETE = 'delete';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    static public $authUserId = null;

    public $changedParams = [];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_IP,
        self::FIELD_USER_AGENT,
        self::FIELD_SOURCE,
        self::FIELD_CHANGES,
    ];

    /**
     * The attributes that are aliasable.
     *
     * @var array
     */
    protected $aliasable = [
        'user_id'       => self::FIELD_USER_ID,
        'date_edited'   => self::FIELD_DATE_EDITED,
        'ip'            => self::FIELD_IP,
        'user_agent'    => self::FIELD_USER_AGENT,
        'source'        => self::FIELD_SOURCE,
        'source_id'     => self::FIELD_SOURCE_ID,
        'changes'       => self::FIELD_CHANGES,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'date_edited',
        'ip',
        'user_agent',
        'source',
        'edited_by_id',
        'changes',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, self::FIELD_USER_ID, User::FIELD_ID);
    }

    /**
     * HistoryOfEdits constructor.
     *
     * @param string $source
     * @param int $editedById
     * @param array $changes
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param string $source
     * @param int $editedById
     * @param array $changedParams
     *
     * @return $this
     */
    public function setRequiredParams(string $source, int $editedById, array $changedParams = [])
    {
        $this->addChangedParam($changedParams);

        $this->setAttribute(self::FIELD_SOURCE, $source);
        $this->setAttribute(self::FIELD_SOURCE_ID, $editedById);
        $this->setAttribute(self::FIELD_CHANGES, json_encode($this->changedParams));

        $this->setAttribute(self::FIELD_USER_ID, self::$authUserId);
        $this->setAttribute(self::FIELD_DATE_EDITED, Carbon::now('utc'));
        $this->setAttribute(self::FIELD_IP, $_SERVER['REMOTE_ADDR']);
        $this->setAttribute(self::FIELD_USER_AGENT, $_SERVER['HTTP_USER_AGENT']);

        return $this;
    }

    /**
     * @param array $param
     *
     * @return $this
     */
    public function addChangedParam(array $param = [])
    {
        $this->changedParams = array_merge($this->changedParams, $param);
        return $this;
    }

    /**
     * @return array
     */
    public function getChangedParams(): array
    {
        return $this->changedParams;
    }
}
