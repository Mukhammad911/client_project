<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;
use Teleglobal\Accounting\Events\TransportCreated;
use Teleglobal\Accounting\Events\TransportUpdated;
use Teleglobal\Accounting\Events\TransportDeleted;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Transport extends Authenticatable
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'm';
    const SOURCE_NAME   = 'Transport';

    const FIELD_ID          = 'id';
    const FIELD_NAME        = 'a';
    const FIELD_COMMENT     = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => TransportCreated::class,
        'updated' => TransportUpdated::class,
        'deleted' => TransportDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'comment'       => self::FIELD_COMMENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function insertModel(Request $request)
    {
        $params = collect($request->input());

        /*
         * Validate data
         */
        Validator::make([
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
        ], [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME],
        ])->validate();

        $this->setAttribute(self::FIELD_NAME, $params->get('name'));
        $this->setAttribute(self::FIELD_COMMENT, $params->get('comment'));
        $this->save();

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function updateModel(Request $request)
    {
        $params = collect($request->input());

        /**
         * Validate data
         */
        Validator::make([
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
        ], [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$this->getAttribute(self::FIELD_ID)],
        ])->validate();

        $this->setAttribute(self::FIELD_NAME, $params->get('name'));
        $this->setAttribute(self::FIELD_COMMENT, $params->get('comment'));
        $this->save();

        return $this;
    }

    public function deleteModel()
    {
        $this->delete();
        return true;
    }
}
