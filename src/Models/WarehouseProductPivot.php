<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Nexmo\Call\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class WarehouseProductPivot extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME = 'r';
    const SOURCE_NAME = 'WarehouseProductPivot';

    const FIELD_ID              = 'id';
    const FIELD_WAREHOUSE_ID    = 'a';
    const FIELD_PRODUCT_ID      = 'b';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//        'updated' => UserUpdated::class,
//        'deleted' => UserDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    public $aliasable = [
        'warehouse_id' => self::FIELD_WAREHOUSE_ID,
        'product_id' => self::FIELD_PRODUCT_ID,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'warehouse_id',
        'product_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * @return BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, self::FIELD_WAREHOUSE_ID, Warehouse::FIELD_ID);
    }

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, self::FIELD_PRODUCT_ID, Product::FIELD_ID);
    }

    /**
     * @return HasMany
     */
    public function incoming()
    {
        return $this->hasMany(ProductsIncoming::class, ProductsIncoming::FIELD_PIVOT_ID, self::FIELD_ID);
    }

    /**
     * @return HasMany
     */
    public function outcoming()
    {
        return $this->hasMany(ProductsOutcoming::class, ProductsOutcoming::FIELD_PIVOT_ID, self::FIELD_ID);
    }

    /**
     * WarehouseProductPivot constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param int $warehouseId
     * @param int $productId
     *
     * @return WarehouseProductPivot|null
     */
    static public function getPivot(int $warehouseId, int $productId)
    {
        return WarehouseProductPivot::where([
            [WarehouseProductPivot::FIELD_WAREHOUSE_ID, '=', $warehouseId],
            [WarehouseProductPivot::FIELD_PRODUCT_ID, '=', $productId],
        ])->first();
    }

    /**
     * @param int $warehouseId
     * @param int $productId
     *
     * @return WarehouseProductPivot
     */
    static public function insertModel(int $warehouseId, int $productId): WarehouseProductPivot
    {
        $model = self::getPivot($warehouseId, $productId);

        if($model === null){

            $model = new WarehouseProductPivot();
            $model->setAttribute(self::FIELD_WAREHOUSE_ID, $warehouseId)
                ->setAttribute(self::FIELD_PRODUCT_ID, $productId)
                ->save();
        }

        return $model;
    }

}
