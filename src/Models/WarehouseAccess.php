<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class WarehouseAccess extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'j';
    const SOURCE_NAME   = 'WarehouseAccess';

    const FIELD_WAREHOUSE_ID    = 'a';
    const FIELD_SOURCE          = 'b';
    const FIELD_SOURCE_ID       = 'c';

    const SOURCE_TYPE_ALL       = 'all';
    const SOURCE_TYPE_GROUPS    = 'groups';
    const SOURCE_TYPE_USERS     = 'users';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => WarehouseCreated::class,
//        'updated' => WarehouseUpdated::class,
//        'deleted' => WarehouseDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_SOURCE,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'warehouse_id'  => self::FIELD_WAREHOUSE_ID,
        'source'        => self::FIELD_SOURCE,
        'source_id'     => self::FIELD_SOURCE_ID,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'warehouse_id',
        'source',
        'source_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, self::FIELD_WAREHOUSE_ID, Warehouse::FIELD_ID);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this
            ->belongsTo(User::class, self::FIELD_SOURCE_ID, User::FIELD_ID)
            ->where(self::FIELD_SOURCE, '=', Crypt::encrypt(self::SOURCE_TYPE_USERS));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userGroup()
    {
        return $this
            ->belongsTo(UserGroup::class, self::FIELD_SOURCE_ID, UserGroup::FIELD_ID)
            ->where(self::FIELD_SOURCE, '=', Crypt::encrypt(self::SOURCE_TYPE_GROUPS));
    }

    /**
     * @param Collection $params
     *
     * @return $this
     */
    public static function insertModel(Collection $params)
    {
        $attributes = [
            'warehouse_id'  => $params->get('warehouse_id'),
            'source'        => $params->get('source'),
            'source_id'     => $params->get('source_id'),
        ];
        /**
         * Validate params
         */
        Validator::make($attributes, [
            'warehouse_id'  => ['required', 'integer'],
            'source'        => ['required', 'in:'.self::SOURCE_TYPE_USERS.','.self::SOURCE_TYPE_GROUPS],
            'source_id'     => ['required', 'integer'],
        ])->validate();

        $model = self::where([
            [self::FIELD_WAREHOUSE_ID, '=', $params->get('warehouse_id')],
            [self::FIELD_SOURCE, '=', Crypt::encrypt($params->get('source'))],
            [self::FIELD_SOURCE_ID, '=', $params->get('source_id')],
        ])->first();

        if(empty($model)){
            $model = new self();
            $model->fill($attributes)->save();
        }

        return $model;
    }
}
