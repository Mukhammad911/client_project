<?php

namespace Teleglobal\Accounting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\AliasableEncryptable;

class ReportSubCategory extends Model
{
    use AliasableEncryptable;
    use Notifiable;

    const TABLE_NAME    = 'z';
    const SOURCE_NAME   = 'ReportSubCategory';

    const FIELD_ID              = 'id';
    const FIELD_NAME            = 'a';
    const FIELD_COMMENT         = 'b';
    const FIELD_TYPE_OF_LINK    = 'c';

    const TYPE_OF_LINK_ALL      = 'all';
    const TYPE_OF_LINK_CUSTOM   = 'custom';

    protected $table = self::TABLE_NAME;
    public $timestamps = false;

    /**
     * Bind model events
     *
     * @var array
     */
    protected $dispatchesEvents = [
//        'created' => DealerCreated::class,
//        'updated' => DealerUpdated::class,
//        'deleted' => DealerDeleted::class,
    ];

    /**
     * The attributes that are encryptable.
     *
     * @var array
     */
    protected $encryptable = [
        self::FIELD_NAME,
        self::FIELD_COMMENT,
        self::FIELD_TYPE_OF_LINK,
    ];

    /**
     * The attributes that have aliases.
     *
     * @var array
     */
    protected $aliasable = [
        'name'          => self::FIELD_NAME,
        'comment'       => self::FIELD_COMMENT,
        'typeOfLink'    => self::FIELD_TYPE_OF_LINK,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
        'typeOfLink',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function categories()
    {
        return $this->belongsToMany(
            ReportCategory::class,
            ReportCategorySubCategoryPivot::TABLE_NAME,
            ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID,
            ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID,
            ReportSubCategory::FIELD_ID,
            ReportCategory::FIELD_ID
        );
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param Collection $params
     *
     * @return $this
     */
    public static function insertModel(Collection $params)
    {
        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
            'typeOfLink'    => $params->get('typeOfLink'),
        ];

        /*
         * Validate data
         */
        Validator::make($attributes, [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
            'typeOfLink'    => ['required','in:all,custom']
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME],
        ])->validate();

        $model = new static();
        $model->fill($attributes)->save();

        if($attributes['typeOfLink'] == self::TYPE_OF_LINK_CUSTOM){
            foreach (json_decode($params->get('categories'), true) as $key => $categoryId){
                ReportCategorySubCategoryPivot::updateModel($categoryId, $model->id);
            }
        }

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $params
     *
     * @return $this
     */
    public static function updateModel(int $id, Collection $params)
    {
        $model = self::find($id);

        if(empty($model)){
            return false;
        }

        $linkedCategoriesIds = ReportCategorySubCategoryPivot::select(ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID)
            ->where(ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID, '=', $id)
            ->get()->pluck(ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID);

        $attributes = [
            'name'          => $params->get('name'),
            'comment'       => $params->get('comment'),
            'typeOfLink'    => $params->get('typeOfLink'),
        ];

        /**
         * Validate data
         */
        Validator::make($attributes, [
            'name'          => ['required','max:255'],
            'comment'       => ['nullable'],
            'typeOfLink'    => ['required','in:all,custom']
        ])->validate();
        Validator::make([
            'name'  => Crypt::encrypt($params->get('name')),
        ], [
            'name'  => ['unique:'.self::TABLE_NAME.','.self::FIELD_NAME.','.$id],
        ])->validate();

        $model->fill($attributes)->save();

        if($model->typeOfLink == self::TYPE_OF_LINK_ALL){
            ReportCategorySubCategoryPivot::where(ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID, '=', $model->id)->delete();
        }else{
            foreach (json_decode($params->get('categories'), true) as $key => $categoryId){
                $i = $linkedCategoriesIds->search($categoryId);
                if($i === false){
                    ReportCategorySubCategoryPivot::updateModel($categoryId, $model->id);
                }else{
                    $linkedCategoriesIds->pull($i);
                }
            }
            ReportCategorySubCategoryPivot::whereIn(
                ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID,
                $linkedCategoriesIds->toArray())
                ->where(ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID, '=', $model->id)
                ->delete();
        }

        return $model;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function deleteModel($id)
    {
        self::find($id)->delete();
        ReportCategorySubCategoryPivot::where(ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID, '=', $id)->delete();
        return true;
    }
}
