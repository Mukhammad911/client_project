<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\ReportAccount;

class ReportAccountController extends Controller
{
    use KeyRestorable;

    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters);
    }

    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.accounts', [
            'accounts' => ReportAccount::all(),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.accounts.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $account = ReportAccount::find($id);
//dd(ReportAccount::all());
        return view('accounting::reports.accounts.edit', [
            'account' => ReportAccount::find($id),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $reportAccount = ReportAccount::findOrFail($id);

        dd($reportAccount->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {

        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        ReportAccount::insertModel($params);

        return redirect()->route('accounting:cashboxes.accounts');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        ReportAccount::updateModel($id, $params);

        return redirect()->route('accounting:cashboxes.accounts');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        ReportAccount::deleteModel($id);

        return redirect()->route('accounting:cashboxes.accounts');
    }
}
