<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Traits\KeyRestorable;
#use Teleglobal\Iptvadmin\Models\Sunduk;

class StorageController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function reports(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::storages.reports', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }
}
