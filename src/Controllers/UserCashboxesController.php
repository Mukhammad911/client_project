<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\CashboxesIncoming;
use Teleglobal\Accounting\Models\CashboxesOutcoming;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\UserCashbox;
use Teleglobal\Accounting\Models\Breadcrumb;

class UserCashboxesController extends Controller
{
    use KeyRestorable;

    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $cashboxes = UserCashbox::select()
            ->with('user')
            ->with('currency')
            ->with('incoming')
            ->with('outcoming')
            ->whereNull(UserCashbox::FIELD_DELETED)
            ->get();

        $currencies = Currency::select()
            ->whereHas('cashboxes')
            ->with(['cashboxes' => function($query){
                return $query
                    ->whereNull(UserCashbox::FIELD_DELETED)
                    ->with('incoming')
                    ->with('outcoming');
            }])
            ->get()
        ;
        $total = [];
        foreach ($currencies as $currency){
            foreach ($currency->cashboxes as $cashbox){
                $total[$currency->id]['name'] = $currency->code . ' - ' . $currency->country;
                if(!empty($total[$currency->id]['incoming'])){
                    $total[$currency->id]['incoming'] += $cashbox->incoming->sum('e');
                }else{
                    $total[$currency->id]['incoming'] = $cashbox->incoming->sum('e');
                }
                if(!empty($total[$currency->id]['outcoming'])){
                    $total[$currency->id]['outcoming'] += $cashbox->outcoming->sum('e');
                }else{
                    $total[$currency->id]['outcoming'] = $cashbox->outcoming->sum('e');
                }
            }
        }

        return view('accounting::cashboxes.index', [
            'cashboxes' => $cashboxes,
            'total'     => $total,
            'fields'    => [],
            'data'      => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString()) ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function find(Request $request)
    {

        $this->restoreEncryptionKey($request);

        $cashboxes = [];
        $conditions = [];

        if(!$request->ajax()) {
            abort(405);
        }

        /*
         * Define search terms
         */
        $params = $request->get('params');

        if(isset($params['user_id'])) {
            $conditions = [
                'a' => $params['user_id']
            ];
        }

        if(isset($params['currency'])) {
            $conditions = [
                'c' => $params['currency']
            ];
        }

        /*
         * Find cashboxes
         */

        $userCashboxes = UserCashbox::where($conditions)->get();

        if($userCashboxes->isEmpty()) {
            abort(404);
        }
dd($userCashboxes);
        foreach ($userCashboxes->toArray() as $item) {
             $cashboxes[] = [
                'id' => $item['id'],
                'currency' => Crypt::decrypt($item['c']),
                'balance' => Crypt::decrypt($item['b']),
                'text' => Crypt::decrypt($item['e']). " " .Crypt::decrypt($item['b']) . " [". Crypt::decrypt($item['c']) ."]"
            ];
        }

        return response()->json($cashboxes);
    }
}
