<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\ReportCurrency;

class ReportCurrencyController extends Controller
{
    use KeyRestorable;

    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters);
    }

    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.currencies', [
            'currencies' => ReportCurrency::all(),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.currencies.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.currencies.edit', [
            'currency' => ReportCurrency::find($id),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $reportCurrency = ReportCurrency::findOrFail($id);

        dd($reportCurrency->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        ReportCurrency::insertModel($params);

        return redirect()->route('accounting:cashboxes.currencies');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        ReportCurrency::updateModel($id, $params);

        return redirect()->route('accounting:cashboxes.currencies');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        ReportCurrency::deleteModel($id);

        return redirect()->route('accounting:cashboxes.currencies');
    }
}
