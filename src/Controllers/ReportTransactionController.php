<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Models\Document;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\User;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\ReportTransaction;

class ReportTransactionController extends Controller
{
    use KeyRestorable;

    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters);
    }

    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.transactions.index', ReportTransaction::getActionIndexParams($request));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        return view('accounting::reports.transactions.create', ReportTransaction::getActionCreateParams($request));
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.transactions.edit', ReportTransaction::getActionEditParams($id, $request));
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $reportCurrency = ReportTransaction::findOrFail($id);

        dd($reportCurrency->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());

        ReportTransaction::insertModel($params);

        return redirect()->route('accounting:cashboxes.transactions');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());

        ReportTransaction::updateModel($id, $params);

        return redirect()->route('accounting:cashboxes.transactions');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        ReportTransaction::deleteModel($id);

        return redirect()->route('accounting:cashboxes.transactions');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function setValidated(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        if($request->ajax()){
            $params = collect($request->query());

            $model = ReportTransaction::setValidated($params);

            return response()->json([
                'status'    => true,
                'data'      => [
                    'transaction_id'    => $params->get('id'),
                    'validated_by'      => $model->validatedBy->getAttribute('name'),
                    'validated_at'      => $model->validated_at->format(config('accounting.app.datetime_format')),
                ],
            ]);
        }

        return null;
    }

    public function getHistory(Request $request)
    {
        $this->restoreEncryptionKey($request);

        if($request->ajax()){
            $params = collect($request->query());

            $history = ReportTransaction::getHistory($params->get('id'));

            return response()->json([
                'status'    => true,
                'data'      => [
                    'history'   => $history,
                ],
            ]);
        }

        return null;
    }
}
