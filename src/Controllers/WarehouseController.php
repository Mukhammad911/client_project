<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Document;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\UserGroup;
use Teleglobal\Accounting\Models\Warehouse;
use Teleglobal\Accounting\Traits\KeyRestorable;

class WarehouseController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::warehouses.index', Warehouse::getActionIndexParams($request));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::warehouses.create', Warehouse::getActionCreateParams($request));
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::warehouses.edit', Warehouse::getActionEditParams($id, $request));
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $warehouse = Warehouse::findOrFail($id);

        dd($warehouse->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());
        Warehouse::insertModel($params);

        return redirect()->route('accounting:warehouses.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());
        Warehouse::updateModel($id, $params);

        return redirect()->route('accounting:warehouses.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        Warehouse::deleteModel($id);

        return redirect()->route('accounting:warehouses.list');
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function move_remnants(int $id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::warehouses.move_remnants', [
            'warehouse_sender_id'=> $id,
            'warehouses' => Warehouse::with('history')
                ->where(Warehouse::FIELD_ID, '<>', $id)
                ->get(),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param int $senderId
     * @param int $recipientId
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function move_remnants_save(int $senderId, int $recipientId, Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        Document::insertModel(Document::DOC_TYPE_TRANSFERING_STOCKS, [
            'senderId'      => $senderId,
            'recipientId'   => $recipientId,
        ]);

        return redirect()->route('accounting:warehouses.list');
    }

}
