<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Models\Setting;
use Teleglobal\Accounting\Traits\KeyRestorable;

class SettingController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     * @param         $key
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $key)
    {
        $this->restoreEncryptionKey($request);

        $model = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt($key))->firstOrFail();

        $variables['data'] = [
            'route' => $request->route()->getName(),
            'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
            'query' => is_null($request->getQueryString()) ? '' : '?'. $request->getQueryString(),
        ];

        if('currencies' === $key) {
            $variables['currencies'] = Currency::whereIn('id', collect(json_decode($model->value)))->get();
        }

        return view('accounting::settings.'.$key.'.index', $variables);
    }

    /**
     * @param Request $request
     * @param         $key
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $key)
    {
        $this->restoreEncryptionKey($request);

        $model = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt($key))->firstOrFail();

        $variables['data'] = [
            'route' => $request->route()->getName(),
            'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
            'query' => is_null($request->getQueryString()) ? '' : '?'. $request->getQueryString(),
        ];

        if('currencies' === $key) {
            $variables['enabled_currencies_ids'] = collect(json_decode($model->value))->toArray();
            $variables['currencies'] = Currency::whereNotNull('code')->orderBy('code', 'asc')->get();
        }

        return view('accounting::settings.'.$key.'.edit', $variables);
    }

    /**
     * @param Request $request
     * @param         $key
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, $key)
    {
        $this->restoreEncryptionKey($request);

        $model = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt($key))->first();

        if($model) {
            $model->updateModel($request, $key);
        } else {
            (new Setting())->insertModel($request, $key);
        }

        $this->restoreEncryptionKey($request);

        return redirect()->route('accounting:settings.index', ['key' => $key]);
    }
}
