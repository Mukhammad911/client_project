<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\UserGroup;
use Teleglobal\Accounting\Traits\KeyRestorable;

class UserGroupController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::user_groups.index', [
            'user_groups'   => (new UserGroup())->all(),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $userGroup = UserGroup::findOrFail($id);

        dd($userGroup->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::user_groups.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::user_groups.edit', [
            'user_group' => (new UserGroup())->find($id),
            'data' => [
                'route' => $request->route()->getName(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        /*
         * Validate data
         */
        // TODO отладить проверку на уникальность
        $rule = [
            'name' => [
                'required',
                'unique:'.UserGroup::TABLE_NAME.','.UserGroup::FIELD_NAME,
                'max:255'
            ],
            'comment' => 'nullable',
        ];
        $this->validate($request, $rule);

        UserGroup::create([
            'name' => $params->get('name'),
            'comment' => $params->get('comment'),
        ]);

        return redirect()->route('accounting:user_groups.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        $userGroup = UserGroup::findOrFail($id);

        /*
         * Validate data
         */
        // TODO отладить проверку на уникальность
        $rule = [
            'name' => [
                'required',
                'unique:'.UserGroup::TABLE_NAME.','.UserGroup::FIELD_NAME.','.$id,
                'max:255'
            ],
            'comment' => 'nullable',
        ];
        $this->validate($request, $rule);

        $userGroup->update([
            'name' => $params->get('name'),
            'comment' => $params->get('comment'),
        ]);

        return redirect()->route('accounting:user_groups.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $userGroup = UserGroup::findOrFail($id);

        // TODO дописать обработку удаления в ивентах
        $userGroup->delete();

        return redirect()->route('accounting:user_groups.list');
    }

    public function showGroup($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::user_groups.show_group', [
            'userGroup' => UserGroup::findOrFail($id),
            'data'      => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function saveGroup($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO Дописать метод сохранения членов группы

        return redirect()->route('accounting:user_groups.list');
    }
}
