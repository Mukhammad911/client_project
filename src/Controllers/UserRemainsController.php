<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Document;
use Teleglobal\Accounting\Models\DocumentItem;
use Teleglobal\Accounting\Models\Income;
use Teleglobal\Accounting\Models\Product;
use Teleglobal\Accounting\Models\ProductIncome;
use Teleglobal\Accounting\Models\ProductsOutcoming;
use Teleglobal\Accounting\Models\ProductsIncoming;
use Teleglobal\Accounting\Models\Warehouse;
use Teleglobal\Accounting\Models\WarehouseProductPivot;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\User;
use Illuminate\Support\Facades\DB;
use Teleglobal\Accounting\Models\UserCashbox;
use Teleglobal\Accounting\Models\UserTransaction;

class UserRemainsController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::users.remains.index', [
            'data' => [
                //'transactions'  => UserTransaction::all(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'route'         => $request->route()->getName(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
                'warehouses'    => Warehouse::all()
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists(Request $request)
    {

        $this->restoreEncryptionKey($request);

        $warehouse_id = $request->input('warehouse_id');

        if($warehouse_id == 'all')
        {
            $_end_data = [];
            $_arr_warehouse = Warehouse::all();

            if($_arr_warehouse->count() == 0)
            {
                return view('accounting::users.remains.list', [
                    'data' => [
                        //'transactions'  => UserTransaction::all(),
                        'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                        'route' => $request->route()->getName(),
                        'query' => is_null($request->getQueryString())
                            ? '' : '?' . $request->getQueryString(),
                        'remains' => [
                            'end_data' => $_end_data,
                            'warehouse_name' => Warehouse::where('id', $warehouse_id)->first()->name
                        ]
                    ]
                ]);
            }

            foreach ($_arr_warehouse as $key => $warehouse)
            {
                if(!$this->getRemainsData($warehouse->id))
                {
                    return view('accounting::users.remains.list', [
                        'data' => [
                            //'transactions'  => UserTransaction::all(),
                            'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                            'route' => $request->route()->getName(),
                            'query' => is_null($request->getQueryString())
                                ? '' : '?' . $request->getQueryString(),
                            'remains' => [
                                'end_data' => $_end_data,
                                'warehouse_name' => Warehouse::all()
                                    //Warehouse::where('id', $warehouse_id)->first()->name
                            ]
                        ]
                    ]);
                }

                $_end_data[] = $this->getRemainsData($warehouse->id);
                $_end_data[$key]['warehouse_name'] = Warehouse::where('id', $warehouse->id)->first()->name;

            }

            return view('accounting::users.remains.list', [
                'data' => [
                    //'transactions'  => UserTransaction::all(),
                    'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                    'route' => $request->route()->getName(),
                    'query' => is_null($request->getQueryString())
                        ? '' : '?' . $request->getQueryString(),
                    'remains' => [
                        'end_data' => $_end_data,
                        'warehouse_name' => Warehouse::all()
                        //where('id', $warehouse_id)->first()->name
                    ]
                ]
            ]);

        }
        else{
            $_end_data[] = $this->getRemainsData($warehouse_id);
            $_end_data['warehouse_name'] = Warehouse::where('id', $warehouse_id)->first()->name;


        }
        return view('accounting::users.remains.list', [
            'data' => [
                //'transactions'  => UserTransaction::all(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'route' => $request->route()->getName(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?' . $request->getQueryString(),
                'remains' => [
                    'end_data' => $_end_data,
                    //'warehouse_name' => Warehouse::where('id', $warehouse_id)->first()->name
                ]
            ]
        ]);



    }

    protected function getRemainsData($warehouse_id)
    {
        $_end_data = [];

        $products_incoming = ProductsIncoming::get();

        if ($products_incoming->count() == 0) {
            return false;
        }

        $products_id = [];

        foreach ($products_incoming as $key => $product_incoming) {
            $data = WarehouseProductPivot::where('id', $product_incoming->pivot_id)
                ->where('a', $warehouse_id)->first();

            if (!is_null($data)) {
                $products_id[$key]['product_id'] = $data->product_id;
                $products_id[$key]['amount'] = $product_incoming->amount;
            }

        }

        $result_array_products_id = [];

        $pp = [];

        $i = 0;
        foreach ($products_id as $key => $p) {
            $pp[$i]['product_id'] = $p['product_id'];
            $pp[$i]['amount'] = $p['amount'];
            $i++;
        }

        $products_id = $pp;

        for ($i = 0; $i < count($products_id); $i++) {
            if ($this->countProductsDublicate($products_id[$i]['product_id'], $result_array_products_id)) {
                for ($j = 0; $j < count($result_array_products_id); $j++) {
                    if ($result_array_products_id[$j]['product_id'] == $products_id[$i]['product_id']) {
                        $result_array_products_id[$j]['amount'] = (float)$result_array_products_id[$j]['amount'] + (float)$products_id[$i]['amount'];
                    }
                }
            } else {
                $result_array_products_id[$i] = $products_id[$i];
            }
        }
//dd($result_array_products_id);// сумма приходов

        //Отгрузки
        $products_outcoming = ProductsOutcoming::get();

        if ($products_outcoming->count() == 0) {
            return false;
        }
        $products_outcoming_id = [];
        foreach ($products_outcoming as $key => $product_outcoming) {
            $data = WarehouseProductPivot::where('id', $product_outcoming->pivot_id)
                ->where('a', $warehouse_id)->first();

            if (!is_null($data)) {
                $products_outcoming_id[$key]['product_id'] = $data->product_id;
                $products_outcoming_id[$key]['amount'] = $product_outcoming->amount;
            }
        }

        $ppp = [];

        $l = 0;

        foreach ($products_outcoming_id as $key => $p) {
            //dd($p['amount']);
            $ppp[$l]['product_id'] = $p['product_id'];
            $ppp[$l]['amount'] = $p['amount'];
            $l++;
        }

        $products_outcoming_id = $ppp;

        $result_array = [];

        for ($i = 0; $i < count($products_outcoming_id); $i++) {

            if(!empty($products_outcoming_id[$i])){

            if ($this->countProductsDublicate($products_outcoming_id[$i]['product_id'], $result_array)) {

                for ($j = 0; $j < count($result_array); $j++) {

                    if ($result_array[$j]['product_id'] == $products_outcoming_id[$i]['product_id']) {
                        $result_array[$j]['amount'] = (float)$result_array[$j]['amount'] + (float)$products_outcoming_id[$i]['amount'];
                    }

                }
            } else {
                $result_array[$i] = $products_outcoming_id[$i];
            }
        }

        }

//dd($result_array); сумма отгрузок
        //Пересчет
        $end_data = [];

        if (count($result_array_products_id) > 0) {
            for ($k = 0; $k < count($result_array_products_id); $k++) {
                $end_data[$k]['product_id'] = $result_array_products_id[$k]['product_id'];
                $end_data[$k]['amount'] = $result_array_products_id[$k]['amount'];

                if (count($result_array) > 0) {
                    for ($j = 0; $j < count($result_array); $j++) {

                        if(!empty($result_array[$j])){
                            if ($result_array[$j]['product_id'] == $end_data[$k]['product_id']) {
                                $end_data[$k]['amount'] = (float)$end_data[$k]['amount'] - (float)$result_array[$j]['amount'];
                            }
                        }


                    }
                }
            }
        }



        //Get name of product by product_id

        $p = 0;
        foreach ($end_data as $data) {

            $_end_data[$p]['product_id'] = $data['product_id'];
            $_end_data[$p]['amount'] = $data['amount'];
            $_end_data[$p]['product_name'] = Product::where('id', $data['product_id'])->first()->name;

            $p++;

        }
//($_end_data); сумма остатков
       return $end_data;

    }

    private function countProductsDublicate($product_id, $exist_array)
    {
        if(count($exist_array) == 0)
        {
            return false;
        }

        for($i = 0; $i < count($exist_array); $i++)
        {
            if($exist_array[$i]['product_id'] == $product_id)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::users.transactions.create', [
            'users'     => User::all(),
            'data'      => [
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'route'         => $request->route()->getName(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        $params = collect($request->input());

        /*
         * Validate data
         */
        $request->validate([
            'from_user'       => ['required', 'exists:a,id'],
            'to_user'         => ['required', 'exists:a,id'],
            'from_cashbox'    => ['required', 'exists:e,id'],
            'to_cashbox'      => ['required', 'exists:e,id'],
            'amount'          => ['required', 'numeric'],
        ]);

        DB::beginTransaction();

        $from = UserCashbox::find($params->get('from_cashbox'));
        $to = UserCashbox::find($params->get('to_cashbox'));

        if($from->currency !== $to->currency) {
            return back()->withErrors([
                'Cross currency transfers are forbidden'
            ]);
        }

        if((float)$from->balance < (float)$params->get('amount')) {
            return back()->withErrors([
                'Insufficient funds to complete the transaction'
            ]);
        }

        /*
         * Update cashboxes
         */
        $from->balance = (string)((float)$from->balance - (float)$params->get('amount'));
        $from->save();

        $to->balance = (string)((float)$to->balance + (float)$params->get('amount'));
        $to->save();

        #dd(auth()->id());

        /*
         * Create transaction
         */
        UserTransaction::create([
            'user_id'      => auth()->id(),
            'from_cashbox' => $params->get('from_cashbox'),
            'to_cashbox'   => $params->get('to_cashbox'),
            'amount'       => $params->get('amount'),
            'timestamps'   => true
        ]);

        DB::commit();

        return redirect()->route('accounting:users.transactions.list');
    }
}
