<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Models\Document;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\Income;
use Teleglobal\Accounting\Models\Product;
use Teleglobal\Accounting\Models\ProductsIncoming;
use Teleglobal\Accounting\Models\Warehouse;
use Teleglobal\Accounting\Models\Setting;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;

class IncomeController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->query());

        if($request->ajax()){
            $docs = (new ProductsIncoming())->getDocumentsListAjax($params);
            return response()->json([
                'status'    => true,
                'data'      => [
                    'pagination' => [
                        'page'          => (int)$params->get('page'),
                        'pages_count'    => $docs->getPagesCount(),
                        'space'         => $docs->getSpace(),
                    ],
                    'order'         => $params->get('order'),
                    'date_from'     => $params->get('date_from'),
                    'date_to'       => $params->get('date_to'),
                    'table_body'    => $docs->getResult(),
                ],
            ]);
        }else{
            $docs = (new ProductsIncoming())->getDocumentsList($params);
            return view('accounting::incomes.index', [
                'docs'          => $docs->getResult(),
                'page'          => $docs->getPage(),
                'pagesCount'    => $docs->getPagesCount(),
                'space'         => $docs->getSpace(),
                'data'          => [
                    'route'         => $request->route()->getName(),
                    'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                    'query'         => is_null($request->getQueryString())
                        ? '' : '?'. $request->getQueryString(),
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings ? $currency_settings->value : null))->toArray();

        return view('accounting::incomes.create', [
            'warehouses' => Warehouse::all(),
            'currencies' => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'products' => Product::all(),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::products.edit', [
            'product' => (new Product())->find($id),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $product = Product::findOrFail($id);

        dd($product->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $this->validate($request, [
            'warehouse'             => 'required|integer|exists:'.Warehouse::TABLE_NAME.','.Warehouse::FIELD_ID,
            'income_date'           => 'required|date_format:d.m.Y',
            'products'              => 'required|array',
        ]);

        $attributes = $request->post();
        if(!empty($attributes['payment']) && $attributes['payment'] === 'on'){
            Validator::make($attributes, [
                'payment_amount'        => 'required_if:payment,on|numeric|min:0',
                'payment_date'          => 'required_if:payment,on|date_format:d.m.Y',
                'payment_currency_id'   => 'required_if:payment,on|integer|exists:'.Currency::TABLE_NAME.','.Currency::FIELD_ID,
            ])->validate();
        }

        for($i = 0; $i < count($attributes['products']['id']); $i++){
            Validator::make([
                'id'        => $attributes['products']['id'][$i],
                'quantity'  => $attributes['products']['quantity'][$i],
            ], [
                'id'        => 'required|integer|exists:'.Product::TABLE_NAME.','.Product::FIELD_ID,
                'quantity'  => 'required|numeric|min:0',
            ])->validate();
        }

        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($attributes);
        Document::insertModel(Document::DOC_TYPE_INCOMES, $params);

        return redirect()->route('accounting:incomes.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $product = Product::findOrFail($id);

        if($product){
            $product->updateModel($request);
        }

        return redirect()->route('accounting:products.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $product = Product::findOrFail($id);

        if($product){
            $product->deleteModel();
        }

        return redirect()->route('accounting:products.list');
    }

    public function remove($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $income = Document::findOrFail($id);

        if($income){
            $income->deleteModel();
        }

        return redirect()->route('accounting:incomes.list');
    }

    public function editDoc($id, Request $request)
    {
        $this->restoreEncryptionKey($request);
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings ? $currency_settings->value : null))->toArray();

        return view('accounting::incomes.edit', [
            'document' => (new Document())->find($id),
            'warehouses' => Warehouse::all(),
            'currencies' => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'products' => Product::all(),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

}
