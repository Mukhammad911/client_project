<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Models\ProductsIncoming;
use Teleglobal\Accounting\Models\SalaryRecipient;
use Teleglobal\Accounting\Models\SalaryReport;
use Teleglobal\Accounting\Models\Warehouse;
use Teleglobal\Accounting\Models\Setting;
use Teleglobal\Accounting\Traits\KeyRestorable;

class SalaryReportController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO перенести группировку товаров в SQL запрос

        $result = [];
        $allReports = SalaryReport::all();
        $incomes = new ProductsIncoming();

        foreach ($allReports as $report) {

            $warehouse = $report->warehouse()->first();
            $recipient = $report->recipient()->first();

            $result[$report->id]['recipient'] = $recipient->name;
            $result[$report->id]['currency'] = $recipient->currency()->first()->code;
            $result[$report->id]['warehouse'] = [
                'id' => $warehouse->id,
                'name' => $warehouse->name,
                'from' => $report->date_from,
                'to' => $report->date_to,
            ];
            // TODO исправить ошибку синтаксиса SQL возникающую при передаче параметров $dateFrom и $dateTo
            $incomingDocuments = $incomes->getIncomingProducts($warehouse->id, ['incomes']/*, $report->date_from, $report->date_to*/);

            foreach ($incomingDocuments as $incomingDocument) {

                foreach ($incomingDocument->productsIncoming()->get() as $incoming) {

                    $result[$report->id]['products'][$incoming->pivot->product->id]['name'] = $incoming->pivot->product->name;

                    if( isset($result[$report->id]['products'][$incoming->pivot->product->id]['quantity']) ){
                        $result[$report->id]['products'][$incoming->pivot->product->id]['quantity'] += $incoming->amount;
                    } else {
                        $result[$report->id]['products'][$incoming->pivot->product->id]['quantity'] = $incoming->amount;
                    }

                    $result[$report->id]['products'][$incoming->pivot->product->id]['bonus'] =
                        ($result[$report->id]['products'][$incoming->pivot->product->id]['quantity'] * $recipient->amount);

                }
            }
            $result[$report->id]['total'] = array_sum(array_column($result[$report->id]['products'], 'bonus'));
        }

        return view('accounting::salary.reports.index', [
            'reports' => $result,
            'data' => [
                'route'      => $request->route()->getName(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'      => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings->value))->toArray();

        return view('accounting::salary.reports.create', [
            'warehouses' => Warehouse::all(),
            'recipients' => SalaryRecipient::all(),
            'currencies' => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'data' => [
                'route'      => $request->route()->getName(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'      => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings->value))->toArray();

        return view('accounting::salary.reports.create', [
            'report' => SalaryReport::find($id),
            'currencies' => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'data' => [
                'route'      => $request->route()->getName(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'      => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        (new SalaryReport())->insertModel($request);

        return redirect()->route('accounting:salary.reports.list');
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->restoreEncryptionKey($request);

        $report = SalaryReport::findOrFail($id);

        $report->updateModel($request);

        return redirect()->route('accounting:salary.reports.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        // TODO: Сделать модальное окно с запросом подтверждения перед удалением.
        $this->restoreEncryptionKey($request);

        $report = SalaryReport::findOrFail($id);

        if($report){
            $report->deleteModel();
        }

        return redirect()->route('accounting:salary.reports.list');
    }
}
