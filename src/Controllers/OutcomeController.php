<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Client;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Models\Dealer;
use Teleglobal\Accounting\Models\Document;
use Teleglobal\Accounting\Models\DocumentItem;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\Product;
use Teleglobal\Accounting\Models\ProductsIncoming;
use Teleglobal\Accounting\Models\ProductsOutcoming;
use Teleglobal\Accounting\Models\Setting;
use Teleglobal\Accounting\Models\Transport;
use Teleglobal\Accounting\Models\UserCashbox;
use Teleglobal\Accounting\Models\Warehouse;
use Teleglobal\Accounting\Traits\KeyRestorable;

class OutcomeController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->query());

        if($request->ajax()){
            $docs = (new ProductsIncoming())->getDocumentsListAjax($params);
            return response()->json([
                'status'    => true,
                'data'      => [
                    'pagination' => [
                        'page'          => (int)$params->get('page'),
                        'pages_count'    => $docs->getPagesCount(),
                        'space'         => $docs->getSpace(),
                    ],
                    'order'         => $params->get('order'),
                    'date_from'     => $params->get('date_from'),
                    'date_to'       => $params->get('date_to'),
                    'table_body'    => $docs->getResult(),
                ],
            ]);
        }else{
            $docs = (new ProductsOutcoming())->getDocumentsList([Document::DOC_TYPE_OUTCOMES]);
            return view('accounting::outcomes.index', [
                'documents'     => $docs->getResult(),
                'page'          => (int)$params->get('page'),
                'pagesCount'    => $docs->getPagesCount(),
                'space'         => $docs->getSpace(), 
                'data' => [
                    'route'         => $request->route()->getName(),
                    'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                    'query'         => is_null($request->getQueryString())
                        ? '' : '?'. $request->getQueryString(),
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings ? $currency_settings->value : null))->toArray();

        return view('accounting::outcomes.create', [
            'warehouses'    => Warehouse::all(),
            'products'      => Product::all(),
            'clients'       => Client::all(),
            'currencies'    => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'cashboxes'     => UserCashbox::all(),
            'transports'    => Transport::all(),
            'data'          => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::products.edit', [
            'product' => (new Product())->find($id),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $product = Product::findOrFail($id);

        dd($product->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());
        Document::insertModel(Document::DOC_TYPE_OUTCOMES, $params);

        return redirect()->route('accounting:outcomes.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $product = Product::findOrFail($id);

        if($product){
            $product->updateModel($request);
        }

        return redirect()->route('accounting:products.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $product = Product::findOrFail($id);

        if($product){
            $product->deleteModel();
        }

        return redirect()->route('accounting:products.list');
    }

    public function remove($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $outcome = Document::findOrFail($id);

        if($outcome){
            $outcome->deleteModel();
        }

        return redirect()->route('accounting:outcomes.list');
    }
}
