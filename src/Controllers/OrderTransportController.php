<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\OrderTransport;
use Teleglobal\Accounting\Traits\KeyRestorable;

class OrderTransportController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::orders.transports.index', [
            'transports'   => OrderTransport::all(),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::orders.transports.create', [
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::orders.transports.edit', [
            'transport' => OrderTransport::find($id),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $transport = Transport::findOrFail($id);

        dd($transport->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        OrderTransport::insertModel($params);

        return redirect()->route('accounting:orders.transports');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        OrderTransport::updateModel($id, $params);

        return redirect()->route('accounting:orders.transports');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $transport = Transport::findOrFail($id);

        if($transport){
            $transport->deleteModel();
        }

        return redirect()->route('accounting:transports.list');
    }
}
