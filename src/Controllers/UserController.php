<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Session\Session;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\UserGroup;
use Teleglobal\Accounting\Models\UserGroupUser;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\User;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Models\Setting;

class UserController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $usersList = User::getUsersList();

        return view('accounting::users.index', [
            'users'     => $usersList['users'],
            'history'   => $usersList['history'],
            'fields'    => User::getFields(),
            'data'      => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString()) ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt(Setting::KEY_CURRENCIES))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings->value))->toArray();

        return view('accounting::users.create', [
            'userGroups'    => UserGroup::select()->get(),
            'currencies'    => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'data'          => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $user = User::select()
            ->where('id', '=', $id)
            ->with(['cashboxes' => function($query){
                return $query->with('incomingStartBalance');
            }])
            ->with('history')
            ->first();

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt(Setting::KEY_CURRENCIES))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings->value))->toArray();

        $oldGroupIdsArray = $user->groupsPivot()->select(UserGroupUser::FIELD_USER_GROUP_ID)->get()->pluck('user_group_id')->toArray();

        return view('accounting::users.edit', [
            'user'              => $user,
            'oldGroupIdsArray'  => $oldGroupIdsArray,
            'userGroups'        => UserGroup::select()->get(),
            'currencies'        => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'data' => [
                'route'         => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);
        $user = User::findOrFail($id);

        dd($user->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());
        $model = User::insertModel($params);

        return redirect()->route('accounting:users');
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());

        $user = User::updateModel($id, $params);

        return redirect()->route('accounting:users');
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        User::deleteModel($id);

        return redirect()->route('accounting:users');
    }
}
