<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Traits\KeyRestorable;
#use Teleglobal\Iptvadmin\Models\Sunduk;

class ReportController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currencies(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.currencies', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function categories(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.categories', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function categoriesCreate(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.categories.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function categoriesEdit(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.categories.edit', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function subcategories(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function subcategoriesCreate(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function subcategoriesEdit(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories.edit', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function accounts(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.accounts', [
            'data' => [
                'route' => $request->route()->getName(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function transactions(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.transactions', [
            'data' => [
                'route' => $request->route()->getName(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }
}
