<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\Setting;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\Currency;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\SalaryRecipient;

class SalaryRecipientController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::salary.recipients.index', [
            'recipients' => SalaryRecipient::all(),
            'data' => [
                'route'      => $request->route()->getName(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'      => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings->value))->toArray();

        return view('accounting::salary.recipients.create', [
            'currencies' => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'data' => [
                'route'      => $request->route()->getName(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'      => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $this->restoreEncryptionKey($request);

        // TODO переделать подгрузку настроек
        $currency_settings = Setting::where(Setting::FIELD_KEY, '=', Crypt::encrypt('currencies'))->first();
        $enabled_currencies_ids = collect(json_decode($currency_settings->value))->toArray();

        return view('accounting::salary.recipients.create', [
            'recipient' => SalaryRecipient::find($id),
            'currencies' => Currency::whereNotNull('code')->whereIn('id', $enabled_currencies_ids)->orderBy('code', 'asc')->get(),
            'data' => [
                'route'      => $request->route()->getName(),
                'breadcrumb' => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query'      => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        (new SalaryRecipient())->insertModel($request);

        return redirect()->route('accounting:salary.recipients.list');
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->restoreEncryptionKey($request);

        $recipient = SalaryRecipient::findOrFail($id);

        $recipient->updateModel($request);

        return redirect()->route('accounting:salary.recipients.list');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        // TODO: Сделать модальное окно с запросом подтверждения перед удалением.
        $this->restoreEncryptionKey($request);

        $recipient = SalaryRecipient::findOrFail($id);

        if($recipient){
            $recipient->deleteModel();
        }

        return redirect()->route('accounting:salary.recipients.list');
    }
}
