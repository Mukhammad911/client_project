<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\Order;
use Teleglobal\Accounting\Traits\KeyRestorable;
#use Teleglobal\Iptvadmin\Models\Sunduk;

class OrderController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::orders.order.index', Order::getActionIndexParams($request));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::orders.order.create', Order::getActionCreateParams($request));
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::orders.order.edit', Order::getActionEditParams($id, $request));
    }

    /**
     * @param $id
     *
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $orderPosition = Order::findOrFail($id);

        dd($orderPosition->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());

        Order::insertModel($params);

        return redirect()->route('accounting:orders.index');
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());

        Order::updateModel($id, $params);

        return redirect()->route('accounting:orders.index');
    }

    public function delete($id, Request $request)
    {

    }
}
