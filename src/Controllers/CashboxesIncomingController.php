<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\CashboxesIncoming;

class CashboxesIncomingController extends Controller
{
    use KeyRestorable;

    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters);
    }

    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::cashboxes.incoming_index', CashboxesIncoming::getActionIndexParams($request));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        return view('accounting::cashboxes.incoming_create', CashboxesIncoming::getActionCreateParams($request));
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::cashboxes.incoming_edit', CashboxesIncoming::getActionEditParams($id, $request));
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $reportCurrency = CashboxesIncoming::findOrFail($id);

        dd($reportCurrency->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());
        $params->put('key', CashboxesIncoming::KEY_CLIENT_TRANSFERRING);

        CashboxesIncoming::insertModel($params);

        return redirect()->route('accounting:cashboxes.incoming');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);
        HistoryOfEdits::$authUserId = Auth::user()->id;

        $params = collect($request->input());

        CashboxesIncoming::updateModel($id, $params);

        return redirect()->route('accounting:cashboxes.incoming');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        CashboxesIncoming::deleteModel($id);

        return redirect()->route('accounting:cashboxes.incoming');
    }
}
