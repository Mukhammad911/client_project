<?php

namespace Teleglobal\Accounting\Controllers\Auth;

use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\Middleware\StartSession;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Facades\Encrypter as Crypt;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\UserVisit;
use Carbon\Carbon;



class LoginController extends Controller
{
    use AuthenticatesUsers;
    use KeyRestorable;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('accounting:auth.logout');
        $this->redirectTo = route('accounting:dashboard.index');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('accounting::auth.auth');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin(Request $request)
    {
        return $this->showLoginForm();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postLogin(Request $request)
    {

        $this->restoreEncryptionKey($request);

        return $this->login($request);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        // TODO: разобраться почему не приходит запрос

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $this->encryptCredentials($request);
    }

    /**
     * Validate the user login request.
     *
     * users = a
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
          $validate = $request->validate([
            $this->username() => 'required|string',
            $this->password() => 'required|string',
            'key' => 'required',
        ]);



    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {

        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    protected function encryptCredentials(Request $request)
    {
        $credentials = collect([]);

        foreach ($request->only($this->username(), $this->password()) as $key => $value) {

            if($this->username() == $key) {
                $credentials->put($key, Crypt::encrypt($value));
            }
            if($this->password() == $key) {
                $credentials->put($key, $value);
            }
        }

        return $credentials->toArray();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * email = b
     *
     * @return string
     */
    public function username()
    {
        return 'b'; // 'email'
    }

    /**
     * Get the login password to be used by the controller.
     *
     * password = d
     *
     * @return string
     */
    public function password()
    {
        return 'd'; // 'password'
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function validateRecaptcha(Request $request)
    {
        /*if(config('uplinkeruser.g_recaptcha.enabled')) {
            $recaptcha_status = json_decode(
                file_get_contents(config('uplinkeruser.g_recaptcha.url')
                    . '?secret=' . config('uplinkeruser.g_recaptcha.secret_key')
                    . '&response=' . $request->input('g-recaptcha-response'))
            );

            if(!$recaptcha_status->success)
            {
                return redirect()->back()->withErrors(['incorrect' => 'The reCaptcha check error']);
            }
        }*/
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        // TODO: перенести в обработчик события
        UserVisit::create([
            'user_id' => $user->id,
            'time' => Carbon::now()->toDateTimeString(),
            'ip' => $_SERVER['REMOTE_ADDR'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ]);
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }
}
