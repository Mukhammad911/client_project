<?php

namespace Teleglobal\Accounting\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\ForgotPasswordController as Controller;

class ForgotPasswordController extends Controller
{
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        #return view('accounting::auth.passwords.email');
    }
}
