<?php

namespace Teleglobal\Accounting\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\RegisterController as Controller;

class RegisterController extends Controller
{
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        #return view('accounting::auth.register');
    }
}
