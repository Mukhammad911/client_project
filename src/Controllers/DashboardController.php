<?php

namespace Teleglobal\Accounting\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Carbon\Carbon;
use Teleglobal\Accounting\Models\User;

class DashboardController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request   $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return response(view('accounting::dashboard.index', [
            'data' => [
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'route'         => $request->route()->getName(),
                'query'         => is_null($request->getQueryString()) ? '' : '?'. $request->getQueryString(),
            ]
        ]));
    }
}
