<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\ReportCategory;
use Teleglobal\Accounting\Models\ReportCategorySubCategoryPivot;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Models\ReportSubCategory;

class ReportSubCategoryController extends Controller
{
    use KeyRestorable;

    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters);
    }

    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories', [
            'subcategories' => ReportSubCategory::all(),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories.create', [
            'categories' => ReportCategory::all(),
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function edit($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $subCategory = ReportSubCategory::find($id);
        $linkedCategoriesIds = ReportCategorySubCategoryPivot::select(ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID)
            ->where(ReportCategorySubCategoryPivot::FIELD_SUBCATEGORY_ID, '=', $subCategory->id)
            ->get()->pluck(ReportCategorySubCategoryPivot::FIELD_CATEGORY_ID)
            ->toArray();

        return view('accounting::reports.subcategories.edit', [
            'subCategory'           => $subCategory,
            'categories'            => ReportCategory::all(),
            'linkedCategoriesIds'   => $linkedCategoriesIds,
            'data'                  => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function get($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $subCategory = ReportSubCategory::findOrFail($id);

        dd($subCategory->name);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        ReportSubCategory::insertModel($params);

        return redirect()->route('accounting:cashboxes.subcategories');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        $params = collect($request->input());

        ReportSubCategory::updateModel($id, $params);

        return redirect()->route('accounting:cashboxes.subcategories');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id, Request $request)
    {
        $this->restoreEncryptionKey($request);

        // TODO дописать обработку удаления в ивентах
        ReportSubCategory::deleteModel($id);

        return redirect()->route('accounting:cashboxes.subcategories');
    }












    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currencies(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.currencies', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function categories(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.categories', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function categoriesCreate(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.categories.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function categoriesEdit(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.categories.edit', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function subcategories(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function subcategoriesCreate(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories.create', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function subcategoriesEdit(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.subcategories.edit', [
            'data' => [
                'route' => $request->route()->getName(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function accounts(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.accounts', [
            'data' => [
                'route' => $request->route()->getName(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    public function transactions(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::reports.transactions', [
            'data' => [
                'route' => $request->route()->getName(),
                'query' => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }
}
