<?php

namespace Teleglobal\Accounting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Teleglobal\Accounting\Models\Breadcrumb;
use Teleglobal\Accounting\Traits\KeyRestorable;
use Teleglobal\Accounting\Models\User;
use Illuminate\Support\Facades\DB;
use Teleglobal\Accounting\Models\UserCashbox;
use Teleglobal\Accounting\Models\UserTransaction;

class UserTransactionsController extends Controller
{
    use KeyRestorable;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::users.transactions.index', [
            'data' => [
                'transactions'  => UserTransaction::all(),
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'route'         => $request->route()->getName(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $this->restoreEncryptionKey($request);

        return view('accounting::users.transactions.create', [
            'users'     => User::all(),
            'data'      => [
                'breadcrumb'    => (new Breadcrumb($request->route()->getName()))->getBreadcrumb(),
                'route'         => $request->route()->getName(),
                'query'         => is_null($request->getQueryString())
                    ? '' : '?'. $request->getQueryString(),
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->restoreEncryptionKey($request);
        $params = collect($request->input());

        /*
         * Validate data
         */
        $request->validate([
            'from_user'       => ['required', 'exists:a,id'],
            'to_user'         => ['required', 'exists:a,id'],
            'from_cashbox'    => ['required', 'exists:e,id'],
            'to_cashbox'      => ['required', 'exists:e,id'],
            'amount'          => ['required', 'numeric'],
        ]);

        DB::beginTransaction();

        $from = UserCashbox::find($params->get('from_cashbox'));
        $to = UserCashbox::find($params->get('to_cashbox'));

        if($from->currency !== $to->currency) {
            return back()->withErrors([
                'Cross currency transfers are forbidden'
            ]);
        }

        if((float)$from->balance < (float)$params->get('amount')) {
            return back()->withErrors([
                'Insufficient funds to complete the transaction'
            ]);
        }

        /*
         * Update cashboxes
         */
        $from->balance = (string)((float)$from->balance - (float)$params->get('amount'));
        $from->save();

        $to->balance = (string)((float)$to->balance + (float)$params->get('amount'));
        $to->save();

        #dd(auth()->id());

        /*
         * Create transaction
         */
        UserTransaction::create([
            'user_id'      => auth()->id(),
            'from_cashbox' => $params->get('from_cashbox'),
            'to_cashbox'   => $params->get('to_cashbox'),
            'amount'       => $params->get('amount'),
            'timestamps'   => true
        ]);

        DB::commit();

        return redirect()->route('accounting:users.transactions.list');
    }
}
