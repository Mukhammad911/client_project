<?php

namespace Teleglobal\Accounting;

use Teleglobal\Accounting\Middleware\Authenticate;
use Teleglobal\Accounting\Console\Commands\GenerateAccounting;
use Teleglobal\Accounting\Console\Commands\InstallAccounting;
use Teleglobal\Accounting\Console\Commands\MigrateAccounting;
use Teleglobal\Accounting\Console\Commands\SeedAccounting;
use Teleglobal\Accounting\Console\Commands\PurgeAccounting;
use Teleglobal\Accounting\Console\Commands\PurgeDatabaseTables;
use Teleglobal\Accounting\Console\Commands\DeployAccountingDatabase;
use Teleglobal\Accounting\Facades\Encrypter as EncrypterFacade;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Models\User;
use Teleglobal\Accounting\Providers\AuthServiceProvider as AccountingAuthServiceProvider;
use Teleglobal\Accounting\Providers\EventServiceProvider;
use Teleglobal\Accounting\Providers\AccountingEncryptionServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Ultraware\Roles\RolesServiceProvider;
use Ultraware\Roles\Middleware\VerifyRole;
use Ultraware\Roles\Middleware\VerifyPermission;
use Ultraware\Roles\Middleware\VerifyLevel;

class AccountingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Routes
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');

        // Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'accounting');

        // Translations
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'accounting');

        // Migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        // Config
        $this->mergeConfigFrom(
            __DIR__.'/../config/database.php', 'database.connections'
        );

        $this->publishes([
            __DIR__.'/../config/accounting.php' => config_path('accounting.php'),
        ], 'accounting-config');

        // Publishing assets
        $this->publishes([
            __DIR__.'/resources/js' => public_path('teleglobal/accounting/js'),
            __DIR__.'/resources/css' => public_path('teleglobal/accounting/css'),
            __DIR__.'/resources/img' => public_path('teleglobal/accounting/img'),
            __DIR__.'/resources/plugins' => public_path('teleglobal/accounting/plugins')
        ], 'accounting-assets');

        // Register commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateAccounting::class,      // php artisan accounting:generate --key
                InstallAccounting::class,       // php artisan accounting:install
                MigrateAccounting::class,       // php artisan accounting:migrate {migration?} {--initial}
                SeedAccounting::class,          // php artisan accounting:seed {seeder?} {--initial}
                PurgeAccounting::class,         // php artisan accounting:purge --key
                PurgeDatabaseTables::class,     // php artisan accounting:purgeDatabaseTables
                DeployAccountingDatabase::class // php artisan accounting:deployAccountingDatabase
            ]);
        }

        // Set auth service provider
        config(['auth.providers.users.model' => User::class]);

        // Set default database driver
        config(['database.default' => 'accounting_mysql']);

        // Set Accounting application key
        if ($this->app->runningInConsole()) {
            config(['accounting.key' => env("ACCOUNTING_KEY", null)]);
        }

        // Set Encryption iv
        config(['accounting.iv' => env("ACCOUNTING_IV", null)]);

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Set default lang from the accounting-package config
        $default_lang = (include __DIR__.'/../config/accounting.php')['app']['fallback_locale'];
        $this->app['config']->set('app.fallback_locale', $default_lang);

        // Register Custom Auth Middleware
        $this->app['router']->aliasMiddleware('accounting.auth', Authenticate::class);

        // Register Custom Auth Service Provider
        $this->app->register(AccountingAuthServiceProvider::class, true);
        (AliasLoader::getInstance())->alias('Auth', Auth::class);

        // Register Events Service Provider
        $this->app->register(EventServiceProvider::class);

        // Register Accounting App Custom Encrypter
        $this->app->register(AccountingEncryptionServiceProvider::class);
        (AliasLoader::getInstance())->alias('Encrypter', EncrypterFacade::class);

        // Register Roles dependencies
        $this->app->register(RolesServiceProvider::class);
        $this->app['router']->aliasMiddleware('role', VerifyRole::class);
        $this->app['router']->aliasMiddleware('permission', VerifyPermission::class);
        $this->app['router']->aliasMiddleware('level', VerifyLevel::class);
    }
}
