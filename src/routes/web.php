<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::group([
    'namespace'  =>'Teleglobal\Accounting\Controllers',
    'middleware' => [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,

//        \Teleglobal\Accounting\Middleware\VerifyCsrfToken::class,
    ],
    'domain' => config('accounting.app.url'),
    'name'   => config('accounting.routes.name'),
], function() {
    $namespace = config('accounting.routes.name');

    Route::get('login','Auth\LoginController@getLogin')->name($namespace.':auth.get.login');
});

/**
 *
 */
Route::group([
    'namespace'  =>'Teleglobal\Accounting\Controllers',
    'middleware' => ['web'],
    'domain' => config('accounting.app.url'),
    'name'   => config('accounting.routes.name'),
], function() {
    $namespace = config('accounting.routes.name');

    Route::post('login','Auth\LoginController@postLogin')->name($namespace.':auth.post.login');


    // TODO: перенести logout в контроллер
    #Route::get('logout','Auth\LoginController@logout')->name($namespace.':auth.get.logout');
    Route::get('logout', [function(Illuminate\Http\Request $request) {

        $request->session()->invalidate();
        $request->session()->forget('acc_e_key');
        $request->session()->flush();

        config(['accounting.key' => null]);

        auth()->guard()->logout();

        return redirect()->route('accounting:dashboard.index');

    }])->name($namespace.':auth.get.logout');
});

/**
 *
 */
Route::group([
    'namespace'  =>'Teleglobal\Accounting\Controllers',
    'middleware' => ['web', 'accounting.auth'],
    'domain' => config('accounting.app.url'),
    'name'   => config('accounting.routes.name'),
], function() {
    $namespace = config('accounting.routes.name');

    /* Dashboard */
    Route::get('/', 'DashboardController@index')->name($namespace.':dashboard.index');

    /* Users */
    Route::get('users', 'UserController@index')->name($namespace.':users');
    Route::get('user/create', 'UserController@create')->name($namespace.':user.create');
    Route::post('user/save', 'UserController@save')->name($namespace.':user.save');
    Route::get('user/{id}', 'UserController@get')->name($namespace.':user.get');
    Route::get('user/edit/{id}', 'UserController@edit')->name($namespace.':user.edit');
    Route::post('user/edit/{id}', 'UserController@update')->name($namespace.':user.update');
    Route::get('user/delete/{id}', 'UserController@delete')->name($namespace.':user.delete');
    /* Users Groups */
    Route::get('user_groups', 'UserGroupController@index')->name($namespace.':user_groups');
    Route::get('user_group/create', 'UserGroupController@create')->name($namespace.':user_group.create');
    Route::post('user_group/save', 'UserGroupController@save')->name($namespace.':user_group.save');
    Route::get('user_group/{id}', 'UserGroupController@get')->name($namespace.':user_group.get');
    Route::get('user_group/edit/{id}', 'UserGroupController@edit')->name($namespace.':user_group.edit');
    Route::post('user_group/edit/{id}', 'UserGroupController@update')->name($namespace.':user_group.update');
    Route::get('user_group/delete/{id}', 'UserGroupController@delete')->name($namespace.':user_group.delete');
    Route::get('user_group/show_group/{id}', 'UserGroupController@showGroup')->name($namespace.':user_group.show_group');
    Route::post('user_group/save_group/{id}', 'UserGroupController@saveGroup')->name($namespace.':user_group.save_group');
    /* Users Transactions */
    Route::get('users/transactions', 'UserTransactionsController@index')->name($namespace.':users.transactions');
    Route::get('users/transaction/crate', 'UserTransactionsController@create')->name($namespace.':users.transaction.create');
    Route::post('users/transaction/save', 'UserTransactionsController@save')->name($namespace.':users.transaction.save');

    /* Users Remains */
    Route::get('users/remains', 'UserRemainsController@index')->name($namespace.':users.remains');
    Route::get('users/remains/lists', 'UserRemainsController@lists')->name($namespace.':users.remains.lists');
    /* Orders */
    Route::get('orders', 'OrderController@index')->name($namespace.':orders.index');
    Route::get('order/create', 'OrderController@create')->name($namespace.':order.create');
    Route::post('order/save', 'OrderController@save')->name($namespace.':order.save');
    Route::get('order/{id}', 'OrderController@get')->name($namespace.':order.get');
    Route::get('order/edit/{id}', 'OrderController@edit')->name($namespace.':order.edit');
    Route::post('order/edit/{id}', 'OrderController@update')->name($namespace.':order.update');
    Route::get('order/delete/{id}', 'OrderController@delete')->name($namespace.':order.delete');
    /* Position */
    Route::get('orders/positions', 'OrderPositionController@index')->name($namespace.':orders.positions');
    Route::get('orders/position/create', 'OrderPositionController@create')->name($namespace.':orders.position.create');
    Route::post('orders/position/save', 'OrderPositionController@save')->name($namespace.':orders.position.save');
    Route::get('orders/position/{id}', 'OrderPositionController@get')->name($namespace.':orders.position.get');
    Route::get('orders/position/edit/{id}', 'OrderPositionController@edit')->name($namespace.':orders.position.edit');
    Route::post('orders/position/edit/{id}', 'OrderPositionController@update')->name($namespace.':orders.position.update');
    Route::get('orders/position/delete/{id}', 'OrderPositionController@delete')->name($namespace.':orders.position.delete');
    /* Transports */
    Route::get('orders/transports', 'OrderTransportController@index')->name($namespace.':orders.transports');
    Route::get('orders/transport/create', 'OrderTransportController@create')->name($namespace.':orders.transport.create');
    Route::post('orders/transport/save', 'OrderTransportController@save')->name($namespace.':orders.transport.save');
    Route::get('orders/transport/{id}', 'OrderTransportController@get')->name($namespace.':orders.transport.get');
    Route::get('orders/transport/edit/{id}', 'OrderTransportController@edit')->name($namespace.':orders.transport.edit');
    Route::post('orders/transport/edit/{id}', 'OrderTransportController@update')->name($namespace.':orders.transport.update');
    Route::get('orders/transport/delete/{id}', 'OrderTransportController@delete')->name($namespace.':orders.transport.delete');

    /* Warehouses */
    Route::get('warehouses', 'WarehouseController@index')->name($namespace.':warehouses.list');
    Route::get('warehouse/create', 'WarehouseController@create')->name($namespace.':warehouse.create');
    Route::post('warehouse/save', 'WarehouseController@save')->name($namespace.':warehouse.save');
    Route::get('warehouse/{id}', 'WarehouseController@get')->name($namespace.':warehouse.get');
    Route::get('warehouse/edit/{id}', 'WarehouseController@edit')->name($namespace.':warehouse.edit');
    Route::post('warehouse/edit/{id}', 'WarehouseController@update')->name($namespace.':warehouse.update');
    Route::get('warehouse/edit_access/{id}', 'WarehouseController@editAccess')->name($namespace.':warehouse.edit_access');
    Route::post('warehouse/edit_access/{id}', 'WarehouseController@updateAccess')->name($namespace.':warehouse.update_access');
    Route::get('warehouse/delete/{id}', 'WarehouseController@delete')->name($namespace.':warehouse.delete');
    Route::get('warehouse/move_remnants/{id}', 'WarehouseController@move_remnants')->name($namespace.':warehouse.move_remnants');
    Route::get('warehouse/move_remnants/save/{sender_id}/{recipient_id}', 'WarehouseController@move_remnants_save')->name($namespace.':warehouse.move_remnants.save');
    Route::post('warehouse/move_remnants/cancel', 'WarehouseController@move_remnants_cancel')->name($namespace.':warehouse.move_remnants.cancel');

    /* Trademarks */
    Route::get('trademarks', 'TrademarkController@index')->name($namespace.':trademarks.list');
    Route::get('trademark/create', 'TrademarkController@create')->name($namespace.':trademark.create');
    Route::post('trademark/save', 'TrademarkController@save')->name($namespace.':trademark.save');
    Route::get('trademark/{id}', 'TrademarkController@get')->name($namespace.':trademark.get');
    Route::get('trademark/edit/{id}', 'TrademarkController@edit')->name($namespace.':trademark.edit');
    Route::post('trademark/edit/{id}', 'TrademarkController@update')->name($namespace.':trademark.update');
    Route::get('trademark/delete/{id}', 'TrademarkController@delete')->name($namespace.':trademark.delete');

    /* Clients */
    Route::get('clients', 'ClientController@index')->name($namespace.':clients.list');
    Route::get('client/create', 'ClientController@create')->name($namespace.':client.create');
    Route::post('client/save', 'ClientController@save')->name($namespace.':client.save');
    Route::get('client/{id}', 'ClientController@get')->name($namespace.':client.get');
    Route::get('client/edit/{id}', 'ClientController@edit')->name($namespace.':client.edit');
    Route::post('client/edit/{id}', 'ClientController@update')->name($namespace.':client.update');
    Route::get('client/delete/{id}', 'ClientController@delete')->name($namespace.':client.delete');

    /* Transports */
    Route::get('transports', 'TransportController@index')->name($namespace.':transports.list');
    Route::get('transport/create', 'TransportController@create')->name($namespace.':transport.create');
    Route::post('transport/save', 'TransportController@save')->name($namespace.':transport.save');
    Route::get('transport/{id}', 'TransportController@get')->name($namespace.':transport.get');
    Route::get('transport/edit/{id}', 'TransportController@edit')->name($namespace.':transport.edit');
    Route::post('transport/edit/{id}', 'TransportController@update')->name($namespace.':transport.update');
    Route::get('transport/delete/{id}', 'TransportController@delete')->name($namespace.':transport.delete');

    /* Dealers */
    Route::get('dealers', 'DealerController@index')->name($namespace.':dealers.list');
    Route::get('dealer/create', 'DealerController@create')->name($namespace.':dealer.create');
    Route::post('dealer/save', 'DealerController@save')->name($namespace.':dealer.save');
    Route::get('dealer/{id}', 'DealerController@get')->name($namespace.':dealer.get');
    Route::get('dealer/edit/{id}', 'DealerController@edit')->name($namespace.':dealer.edit');
    Route::post('dealer/edit/{id}', 'DealerController@update')->name($namespace.':dealer.update');
    Route::get('dealer/delete/{id}', 'DealerController@delete')->name($namespace.':dealer.delete');

    /* Products */
    Route::get('products', 'ProductController@index')->name($namespace.':products.list');
    Route::get('product/create', 'ProductController@create')->name($namespace.':product.create');
    Route::post('product/save', 'ProductController@save')->name($namespace.':product.save');
    Route::get('product/{id}', 'ProductController@get')->name($namespace.':product.get');
    Route::get('product/edit/{id}', 'ProductController@edit')->name($namespace.':product.edit');
    Route::post('product/edit/{id}', 'ProductController@update')->name($namespace.':product.update');
    Route::get('product/delete/{id}', 'ProductController@delete')->name($namespace.':product.delete');

    /* Product incomes */
    Route::get('incomes', 'IncomeController@index')->name($namespace.':incomes.list');
    Route::get('income/create', 'IncomeController@create')->name($namespace.':income.create');
    Route::post('income/form_validate', 'IncomeController@formValidate')->name($namespace.':income.form_validate');
    Route::post('income/save', 'IncomeController@save')->name($namespace.':income.save');
    Route::get('income/{id}', 'IncomeController@get')->name($namespace.':income.get');
    Route::get('income/delete/{id}', 'IncomeController@remove')->name($namespace.':income.delete');
    Route::get('income/update/{id}', 'IncomeController@editDoc')->name($namespace.':income.update');

    /* Product outcomes */
    Route::get('outcomes', 'OutcomeController@index')->name($namespace.':outcomes.list');
    Route::get('outcome/create', 'OutcomeController@create')->name($namespace.':outcome.create');
    Route::post('outcome/save', 'OutcomeController@save')->name($namespace.':outcome.save');
    Route::get('outcome/{id}', 'OutcomeController@get')->name($namespace.':outcome.get');
    Route::get('outcome/delete/{id}', 'OutcomeController@remove')->name($namespace.':outcome.delete');


    /* Salary recipients */
    Route::get('salary/recipients', 'SalaryRecipientController@index')->name($namespace.':salary.recipients.list');
    Route::get('salary/recipient/create', 'SalaryRecipientController@create')->name($namespace.':salary.recipient.create');
    Route::post('salary/recipient/save', 'SalaryRecipientController@save')->name($namespace.':salary.recipient.save');
    Route::get('salary/recipient/{id}', 'SalaryRecipientController@get')->name($namespace.':salary.recipient.get');
    Route::get('salary/recipient/edit/{id}', 'SalaryRecipientController@edit')->name($namespace.':salary.recipient.edit');
    Route::post('salary/recipient/edit/{id}', 'SalaryRecipientController@update')->name($namespace.':salary.recipient.update');
    Route::get('salary/recipient/delete/{id}', 'SalaryRecipientController@delete')->name($namespace.':salary.recipient.delete');

    /* Salary reports */
    Route::get('salary/reports', 'SalaryReportController@index')->name($namespace.':salary.reports.list');
    Route::get('salary/report/create', 'SalaryReportController@create')->name($namespace.':salary.report.create');
    Route::post('salary/report/save', 'SalaryReportController@save')->name($namespace.':salary.report.save');
    Route::get('salary/report/{id}', 'SalaryReportController@get')->name($namespace.':salary.report.get');
    Route::get('salary/report/edit/{id}', 'SalaryReportController@edit')->name($namespace.':salary.report.edit');
    Route::post('salary/report/edit/{id}', 'SalaryReportController@update')->name($namespace.':salary.report.update');
    Route::get('salary/report/delete/{id}', 'SalaryReportController@delete')->name($namespace.':salary.report.delete');

    /* Settings */
    // TODO Сделать проверку наличия ключа в базе
    Route::get('settings/{key}', 'SettingController@index')->name($namespace.':settings.index');
    Route::get('settings/{key}/edit', 'SettingController@edit')->name($namespace.':settings.edit');
    Route::post('settings/{key}/save', 'SettingController@save')->name($namespace.':settings.save');




    Route::get('storage/trademarks', 'StorageController@trademarks')->name($namespace.':storage.trademarks');
    Route::get('storage/clients', 'StorageController@clients')->name($namespace.':storage.clients');
    Route::get('storage/transport', 'StorageController@transport')->name($namespace.':storage.transport');
    Route::get('storage/dealers', 'StorageController@dealers')->name($namespace.':storage.dealers');
    Route::get('storage/income', 'StorageController@income')->name($namespace.':storage.income');
    Route::get('storage/expense', 'StorageController@expense')->name($namespace.':storage.expense');
    Route::get('storage/products', 'StorageController@products')->name($namespace.':storage.products');
    Route::get('storage/reports', 'StorageController@reports')->name($namespace.':storage.reports');
    # --------------------------------------------------------------------------------------------
    Route::get('storage', 'StorageController@index')->name($namespace.':storage.list');
    Route::get('storage/create', 'StorageController@create')->name($namespace.':storage.create');
    Route::post('storage/save', 'StorageController@save')->name($namespace.':storage.save');
    Route::get('storage/{id}', 'StorageController@get')->name($namespace.':storage.get');
    Route::put('storage/{id}', 'StorageController@update')->name($namespace.':storage.update');
    Route::delete('storage/{id}', 'StorageController@delete')->name($namespace.':storage.delete');

    /* Reports */
    /* ReportsCategory */
    Route::get('cashboxes/categories', 'ReportCategoryController@index')->name($namespace.':cashboxes.categories');
    Route::get('cashboxes/category/create', 'ReportCategoryController@create')->name($namespace.':cashboxes.category.create');
    Route::post('cashboxes/category/save', 'ReportCategoryController@save')->name($namespace.':cashboxes.category.save');
    Route::get('cashboxes/category/{id}', 'ReportCategoryController@get')->name($namespace.':cashboxes.category.get');
    Route::get('cashboxes/category/edit/{id}', 'ReportCategoryController@edit')->name($namespace.':cashboxes.category.edit');
    Route::post('cashboxes/category/edit/{id}', 'ReportCategoryController@update')->name($namespace.':cashboxes.category.update');
    Route::get('cashboxes/category/delete/{id}', 'ReportCategoryController@delete')->name($namespace.':cashboxes.category.delete');
    /* ReportsSubCategory */
    Route::get('cashboxes/subcategories', 'ReportSubCategoryController@index')->name($namespace.':cashboxes.subcategories');
    Route::get('cashboxes/subcategory/create', 'ReportSubCategoryController@create')->name($namespace.':cashboxes.subcategory.create');
    Route::post('cashboxes/subcategory/save', 'ReportSubCategoryController@save')->name($namespace.':cashboxes.subcategory.save');
    Route::get('cashboxes/subcategory/{id}', 'ReportSubCategoryController@get')->name($namespace.':cashboxes.subcategory.get');
    Route::get('cashboxes/subcategory/edit/{id}', 'ReportSubCategoryController@edit')->name($namespace.':cashboxes.subcategory.edit');
    Route::post('cashboxes/subcategory/edit/{id}', 'ReportSubCategoryController@update')->name($namespace.':cashboxes.subcategory.update');
    Route::get('cashboxes/subcategory/delete/{id}', 'ReportSubCategoryController@delete')->name($namespace.':cashboxes.subcategory.delete');
    /* ReportsAccount */
    Route::get('cashboxes/accounts', 'ReportAccountController@index')->name($namespace.':cashboxes.accounts');
    Route::get('cashboxes/account/create', 'ReportAccountController@create')->name($namespace.':cashboxes.account.create');
    Route::post('cashboxes/account/save', 'ReportAccountController@save')->name($namespace.':cashboxes.account.save');
    Route::get('cashboxes/account/{id}', 'ReportAccountController@get')->name($namespace.':cashboxes.account.get');
    Route::get('cashboxes/account/edit/{id}', 'ReportAccountController@edit')->name($namespace.':cashboxes.account.edit');
    Route::post('cashboxes/account/edit/{id}', 'ReportAccountController@update')->name($namespace.':cashboxes.account.update');
    Route::get('cashboxes/account/delete/{id}', 'ReportAccountController@delete')->name($namespace.':cashboxes.account.delete');
    /* ReportsAccount */
    Route::get('cashboxes/currencies', 'ReportCurrencyController@index')->name($namespace.':cashboxes.currencies');
    Route::get('cashboxes/currency/create', 'ReportCurrencyController@create')->name($namespace.':cashboxes.currency.create');
    Route::post('cashboxes/currency/save', 'ReportCurrencyController@save')->name($namespace.':cashboxes.currency.save');
    Route::get('cashboxes/currency/{id}', 'ReportCurrencyController@get')->name($namespace.':cashboxes.currency.get');
    Route::get('cashboxes/currency/edit/{id}', 'ReportCurrencyController@edit')->name($namespace.':cashboxes.currency.edit');
    Route::post('cashboxes/currency/edit/{id}', 'ReportCurrencyController@update')->name($namespace.':cashboxes.currency.update');
    Route::get('cashboxes/currency/delete/{id}', 'ReportCurrencyController@delete')->name($namespace.':cashboxes.currency.delete');
    /* Cashboxes */
    Route::get('cashboxes', 'UserCashboxesController@index')->name($namespace.':cashboxes');
    Route::get('cashboxes/find', 'UserCashboxesController@find')->name($namespace.':cashboxes.find');
    /* Incoming */
    Route::get('cashboxes/incoming', 'CashboxesIncomingController@index')->name($namespace.':cashboxes.incoming');
    Route::get('cashboxes/incoming/create', 'CashboxesIncomingController@create')->name($namespace.':cashboxes.incoming.create');
    Route::post('cashboxes/incoming/save', 'CashboxesIncomingController@save')->name($namespace.':cashboxes.incoming.save');
    Route::get('cashboxes/incoming/{id}', 'CashboxesIncomingController@get')->name($namespace.':cashboxes.incoming.get');
    Route::get('cashboxes/incoming/edit/{id}', 'CashboxesIncomingController@edit')->name($namespace.':cashboxes.incoming.edit');
    Route::post('cashboxes/incoming/edit/{id}', 'CashboxesIncomingController@update')->name($namespace.':cashboxes.incoming.update');
    /* Outcoming */
    Route::get('cashboxes/transactions', 'ReportTransactionController@index')->name($namespace.':cashboxes.transactions');
    Route::get('cashboxes/transaction/create', 'ReportTransactionController@create')->name($namespace.':cashboxes.transaction.create');
    Route::post('cashboxes/transaction/save', 'ReportTransactionController@save')->name($namespace.':cashboxes.transaction.save');
    Route::get('cashboxes/transaction/get_history', 'ReportTransactionController@getHistory')->name($namespace.':cashboxes.transaction.get_history');
    Route::get('cashboxes/transaction/set_validated', 'ReportTransactionController@setValidated')->name($namespace.':cashboxes.transaction.set_validated');
    Route::get('cashboxes/transaction/{id}', 'ReportTransactionController@get')->name($namespace.':cashboxes.transaction.get');
    Route::get('cashboxes/transaction/edit/{id}', 'ReportTransactionController@edit')->name($namespace.':cashboxes.transaction.edit');
    Route::post('cashboxes/transaction/edit/{id}', 'ReportTransactionController@update')->name($namespace.':cashboxes.transaction.update');
    Route::get('cashboxes/transaction/delete/{id}', 'ReportTransactionController@delete')->name($namespace.':cashboxes.transaction.delete');

    /* Movements */
    Route::get('movement', 'MovementController@index')->name($namespace.':movement.list');


});