<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Product;

class ProductUpdated extends Event
{
    use SerializesModels;

    public $product;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Product $product
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
}