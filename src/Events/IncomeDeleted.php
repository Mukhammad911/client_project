<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Income;

class IncomeDeleted extends Event
{
    use SerializesModels;

    public $income;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Income $income
     * @return void
     */
    public function __construct(Income $income)
    {
        $this->income = $income;
    }
}