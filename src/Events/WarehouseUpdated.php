<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Warehouse;

class WarehouseUpdated extends Event
{
    use SerializesModels;

    public $warehouse;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Warehouse $warehouse
     * @return void
     */
    public function __construct(Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
    }
}