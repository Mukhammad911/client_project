<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Client;

class ClientCreated extends Event
{
    use SerializesModels;

    public $client;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Client $client
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}