<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Trademark;

class TrademarkDeleted extends Event
{
    use SerializesModels;

    public $trademark;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Trademark $trademark
     * @return void
     */
    public function __construct(Trademark $trademark)
    {
        $this->trademark = $trademark;
    }
}