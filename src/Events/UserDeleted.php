<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\User;

class UserDeleted extends Event
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\User  $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}