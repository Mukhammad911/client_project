<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Transport;

class TransportCreated extends Event
{
    use SerializesModels;

    public $transport;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Transport $transport
     * @return void
     */
    public function __construct(Transport $transport)
    {
        $this->transport = $transport;
    }
}