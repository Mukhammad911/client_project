<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\UserGroup;

class UserGroupUpdated extends Event
{
    use SerializesModels;

    public $userGroup;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\UserGroup  $userGroup
     * @return void
     */
    public function __construct(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }
}