<?php

namespace Teleglobal\Accounting\Events;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Facades\Auth;
use Teleglobal\Accounting\Models\HistoryOfEdits;
use Teleglobal\Accounting\Models\User;

class UserUpdated extends Event
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\User  $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}