<?php

namespace Teleglobal\Accounting\Events;

use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Teleglobal\Accounting\Models\Dealer;

class DealerDeleted extends Event
{
    use SerializesModels;

    public $dealer;

    /**
     * Create a new event instance.
     *
     * @param  \Teleglobal\Accounting\Models\Dealer $dealer
     * @return void
     */
    public function __construct(Dealer $dealer)
    {
        $this->dealer = $dealer;
    }
}