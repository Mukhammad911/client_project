<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page title
    |--------------------------------------------------------------------------
    */
    'sidebar' => [
        'item_1' => 'Главная',
            'item_1_1' => 'Панель',
        'item_2' => 'Пользователи',
            'item_2_1' => 'Создать',
            'item_2_2' => 'Список',
            'item_2_3' => 'Транзакции',
        'item_8' => 'Группы пользователей',
            'item_8_1' => 'Создать',
            'item_8_2' => 'Список',
        'item_3' => 'Заказы',
            'item_3_1' => 'Создать',
            'item_3_2' => 'Список',
            'item_3_3' => 'Редактирование',
            'item_3_4' => 'Позиция',
            'item_3_5' => 'Создать позицию',
            'item_3_6' => 'Транспорт',
            'item_3_7' => 'Создать транспорт',
        'item_4' => 'Зарплаты',
            'item_4_1' => 'Получатели',
            'item_4_2' => 'Отчеты',
        'item_5' => 'Склад',
            'item_5_1' => 'Создать',
            'item_5_2' => 'Список',
            'item_5_3' => 'Бренды',
            'item_5_4' => 'Транспорт',
            'item_5_5' => 'Посредники',
            'item_5_6' => 'Клиенты',
            'item_5_7' => 'Приходы',
            'item_5_8' => 'Расходы',
            'item_5_9' => 'Марки',
            'item_5_10' => 'Отчеты',
            'item_5_11' => 'Движение товара',
        'item_6' => 'Отчеты',
            'item_6_1' => 'Категории',
            'item_6_2' => 'Подкатегории',
            'item_6_3' => 'Счета',
            'item_6_4' => 'Валюты',
            'item_6_5' => 'Транзакции',
        'item_7' => 'Настройки',
            'item_7_1' => 'Валюта',
    ],

    'header' => [
        'item_1' => 'Профиль',
        'item_2' => 'Календарь',
        'item_3' => 'Входящие',
        'item_4' => 'Задачи',
        'item_5' => 'Блокировка экрана',
        'item_6' => 'Выход',
    ]
];
