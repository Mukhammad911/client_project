<?php

return [
    'page_title' => 'Отчеты',
    'sidebar' => [
        'main'   => 'Склады',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Склады',
        'list'          => 'Список складов',
        'create'        => 'Создание склада',
        'edit'          => 'Редактирование склада',
        'edit_access'   => 'Изменение доступа',
        'move_remnants' => 'Переместить остатки',
        'movement'      => 'Движение товара',
        'reports'       => 'Отчеты',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление складами',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_created'         => 'Кем создан',
            'title_date_created'    => 'Дата создания',
            'title_move_remnants'   => 'Переместить остатки',
            'title_edit_access'     => 'Изменить доступ',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
            'title_move_cancel'     => 'Отменить остатки',
        ],
        'form' => [
            'all'       => 'Все',
            'groups'    => 'Группы пользователей',
            'users'     => 'Пользователи',
        ],
        'create' => [
            'title'         => 'Создание склада',
            'title_name'    => 'Название',
            ''
        ],
        'edit' => [
            'title' => 'Редактирование склада',
        ],
        'edit_access' => [
            'title' => 'Изменение доступа к складу',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить остатки',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];