<?php

return [
    'page_title' => 'Торговые марки',
    'sidebar' => [
        'main'   => 'Торговые марки',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Бренды',
        'list'          => 'Список брендов',
        'create'        => 'Создание бренда',
        'edit'          => 'Редактирование бренда',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление брендами',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
        ],
        'create' => [
            'title'         => 'Создание бренда',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
        ],
        'edit' => [
            'title' => 'Редактирование бренда',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];