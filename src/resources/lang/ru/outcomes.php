<?php

return [
    'page_title' => 'Отгрузки',
    'sidebar' => [
        'main'   => 'Товары',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Отгрузки',
        'list'          => 'Список отгрузок',
        'create'        => 'Создание отгрузки',
        'edit'          => 'Редактирование отгрузки',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление отгрузками',
            'title_doc_id'          => 'ID',
            'title_warehouse'       => 'Склад отправитель',
            'title_date'            => 'Дата отгрузки',
            'title_products'        => 'Товары',
            'title_created_by'      => 'Кем создан',
            'title_client'          => 'Получатель',
            'title_transport'       => 'Перевозчик',
            'delete'                => 'Удалить',
            'association'           => 'Ассоциация'
        ],
        'form' => [
            'title_warehouse'               => 'Склад отправитель',
            'title_client'                  => 'Клиент-получатель',
            'title_date'                    => 'Дата отгрузки',
            'title_currency'                => 'Валюта',
            'title_transport'               => 'Транспорт',
            'title_transport_not_selected'  => 'Не выбран',
            'assoc_source'                  => [
                'transport'         => 'Транспорт',
                'dealer'            => 'Посредник-транспорт',
                'travel_card'       => 'Проездной',
                'documents'         => 'Документы',
                'seller_dealer'     => 'Продавец-посредник',
                'warehouse_payd'    => 'Оплата склада',
                'outcome_ab'        => 'Расход Аб',
            ],
        ],
        'create' => [
            'title'             => 'Создание отгрузки',
            'title_name'        => 'Название',
            'title_trademark'   => 'Бренд',
            'marks'             => 'Наименование товара',
            'price'             => 'Цена',
            'quantity'          => 'Кол-во',
            'sum_quantity'      => 'Общее кол-во',
        ],
        'edit' => [
            'title' => 'Редактирование отгрузки',
        ],
    ],
    'placeholders' => [
        'enter_warehouse'   => 'Выбор склада',
        'enter_client'      => 'Выбор клиента',
        'enter_currency'    => 'Валюта',
        'enter_transport'   => 'Выбор транспорта',
        'enter_product'     => 'Выбор товара',
        'enter_cost'        => 'Цена',
        'enter_quantity'    => 'Кол-во',
        'total_quantity'    => 'Общее кол-во',
        'enter_amount'      => 'Сумма',
        'enter_date'        => 'Дата',
    ],
    'labels' => [
        'unit_thing'  => 'шт.',
    ],
    'buttons' => array_merge(
        include ('_button.php'),
        [
            'show'              => 'Показать список',
            'filter'            => 'Фильтр по дате отгрузки',
            'associate'         => 'Ассоциировать',
            'add_product'       => 'Добавить товар',
            'remove_product'    => 'Удалить товар',
            'add_warehouse'     => 'Добавить склад',
            'remove_warehouse'  => 'Удалить склад',
        ]
    )
];