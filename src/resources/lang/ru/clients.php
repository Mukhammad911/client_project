<?php

return [
    'page_title' => 'Клиенты',
    'sidebar' => [
        'main'   => 'Клиенты',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Клиенты',
        'list'          => 'Список клиентов',
        'create'        => 'Создание клиента',
        'edit'          => 'Редактирование клиента',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление клиентами',
            'title_name'            => 'Имя',
            'title_comment'         => 'Комментарий',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
        ],
        'create' => [
            'title'         => 'Создание клиента',
            'title_name'    => 'Имя',
            'title_comment' => 'Комментарий',
        ],
        'edit' => [
            'title' => 'Редактирование клиента',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите имя',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];