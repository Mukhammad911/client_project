<?php

return [
    'page_title' => 'Склады',
    'sidebar' => [
        'main'   => 'Склады',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'              => 'Склады',
        'list'              => 'Список складов',
        'create'            => 'Создание склада',
        'edit'              => 'Редактирование склада',
        'edit_access'       => 'Изменение доступа',
        'select_recepient'  => 'Перемещение остатков / Выбор склада-получателя',
        'movement'          => 'Движение товара',
        'reports'           => 'Отчеты',
    ],
    'page' => [
        'list' => [
            'title'                 => 'Управление складами',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_created'         => 'Кем создан',
            'title_date_created'    => 'Дата создания',
            'title_move_remnants'   => 'Переместить остатки',
            'title_edit_access'     => 'Изменить доступ',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
            'title_cancel'          => 'Отменить',
        ],
        'form' => [
            'access_type'   => 'Тип доступа',
            'all'           => 'Все',
            'groups'        => 'Группы пользователей',
            'users'         => 'Пользователи',
            'access'        => 'Сделать доступным',
        ],
        'create' => [
            'title'         => 'Создание склада',
            'title_name'    => 'Название',
        ],
        'edit' => [
            'title' => 'Редактирование склада',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];