<?php

return [
    'page_title' => 'Посредники',
    'sidebar' => [
        'main'   => 'Посредники',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Посредники',
        'list'          => 'Список посредников',
        'create'        => 'Создание посредника',
        'edit'          => 'Редактирование посредника',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление посредниками',
            'title_name'            => 'Имя',
            'title_comment'         => 'Комментарий',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
        ],
        'create' => [
            'title'         => 'Создание посредника',
            'title_name'    => 'Имя',
            'title_comment' => 'Комментарий',
        ],
        'edit' => [
            'title' => 'Редактирование посредника',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите имя',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];