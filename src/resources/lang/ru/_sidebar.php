<?php

return [

    'dashboard' => [
        'main'  => 'Главная',
        'panel' => 'Панель',
    ],
    'users' => [
        'main'          => 'Пользователи',
        'create'        => 'Создать',
        'users'         => 'Пользователи',
        'groups'        => 'Группы пользователей',
    ],
    'orders' => [
        'main'          => 'Заказы',
        'create'        => 'Создать',
        'orders'        => 'Заказы',
        'positions'     => 'Позиция',
        'transports'    => 'Транспорт',
    ],
    'salaries' => [
        'main'          => 'Зарплаты',
        'recipients'    => 'Получатели',
        'reports'       => 'Отчеты',
    ],
    'warehouses' => [
        'main'          => 'Склады',
        'create'        => 'Создать',
        'warehouses'    => 'Склады',
        'clients'       => 'Клиенты',
        'transports'    => 'Транспорт',
        'dealers'       => 'Посредники',
        'incomes'       => 'Приходы',
        'outcomes'      => 'Отгрузки',
        'products'      => 'Товар',
        'reports'       => 'Отчеты',
        'movements'     => 'Движения',
    ],
    'cashboxes' => [
        'main'          => 'Кассы пользователей',
        'categories'    => 'Категории',
        'subcategories' => 'Подкатегории',
        'accounts'      => 'Счета',
        'currencies'    => 'Валюты',
        'transactions'  => 'Транзакции',
        'list'          => 'Список касс',
        'incoming'      => 'Приходы',
        'outcoming'     => 'Расходы',
        'remains'       => 'Остатки',
    ],
    'settings' => [
        'main'      => 'Настройки',
        'currency'  => 'Валюта',
    ],
];



















