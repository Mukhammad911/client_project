<?php

return [
    'page_title' => 'Зарплаты',
    'sidebar' => [
        'main'   => 'Зарплаты',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'recipients'    => [
            'main'          => 'Зарплаты',
            'list'          => 'Список получателей',
            'create'        => 'Создание получателя',
            'edit'          => 'Редактирование получателя',
            'get'           => 'Получатель',
        ],
        'reports'    => [
            'main'          => 'Зарплаты',
            'list'          => 'Список отчетов',
            'create'        => 'Создание отчета',
            'edit'          => 'Редактирование отчета',
            'get'           => 'Отчет по зарплатам',
        ],
    ],
    'pages' => [
        'list' => [
            'recipients'    => [
                'title'                 => 'Список получателей',
                'title_name'            => 'Имя',
                'title_comment'         => 'Комментарий',
                'title_amount'          => 'Сумма',
                'title_currency'        => 'Валюта',
                'title_edit'            => 'Редактировать',
                'title_delete'          => 'Удалить',
            ],
            'reports'    => [
                'title'                 => 'Отчеты по зарплатам',
                'title_warehouse'       => 'Склад',
                'title_recipient'       => 'Получатель',
                'title_from'            => 'Приходы с',
                'title_to'              => 'Приходы по',
                'title_products'        => 'Приход марок на склад',
                'title_per_product'     => 'Выплата за каждую марку',
                'title_amount'          => 'Итого',
                'title_delete'          => 'Удалить',
            ],
        ],
        'form' => [
            'all'       => 'Все',
            'groups'    => 'Группы пользователей',
            'users'     => 'Пользователи',
        ],
        'create' => [
            'recipients'    => [
                'title'         => 'Создание получателя',
                'title_name'    => 'Название',
            ],
            'reports'    => [
                'title'         => 'Создание отчета',
                'title_name'    => 'Название',
            ],
        ],
        'edit' => [
            'recipients'    => [
                'title' => 'Редактирование получателя',
            ],
        ],
        'edit_access' => [
            'title' => 'Изменение доступа к складу',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите имя',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить остатки',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];