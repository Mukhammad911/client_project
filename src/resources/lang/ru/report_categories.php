<?php

return [
    'page_title' => 'Отчеты. Категории',
    'sidebar' => [
        'main'   => 'Категории',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Категории',
        'list'          => 'Список категорий',
        'create'        => 'Создание категории',
        'edit'          => 'Редактирование категории',
    ],
    'pages' => [
        'categories' => [
            'title'                 => 'Список категорий',
            'title_id'              => 'ID',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_action'          => 'Действия',
        ],
        'list' => [
            'title'                 => 'Управление категориями',
            'title_id'              => 'ID',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_created'         => 'Кем создан',
            'title_date_created'    => 'Дата создания',
            'title_move_remnants'   => 'Переместить остатки',
            'title_edit_access'     => 'Изменить доступ',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
            'title_move_cancel'     => 'Отменить остатки',
        ],
        'create' => [
            'title'             => 'Создание категории',
            'title_name'        => 'Название',
            'title_comment'     => 'Комментарий',
        ],
        'edit' => [
            'title' => 'Редактирование категории',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить остатки',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];