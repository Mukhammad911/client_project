<?php

return [
    'page_title' => 'Торговые марки',
    'sidebar' => [
        'main'   => 'Торговые марки',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Транспорт',
        'list'          => 'Список транспорта',
        'create'        => 'Создание транспорта',
        'edit'          => 'Редактирование транспорта',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление транспортом',
            'title_id'              => '№',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
            'title_action'          => 'Действия',
        ],
        'create' => [
            'title'         => 'Создание транспорта',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
        ],
        'edit' => [
            'title' => 'Редактирование транспорта',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];