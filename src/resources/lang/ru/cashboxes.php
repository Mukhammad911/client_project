<?php

return [
    'page_title' => 'Кассы пользователей',
    'breadcrumb' => [
        'main'  => 'Кассы пользователей',
        'list'  => 'Список касс',
        'transactions'          => 'Транзакции пользователей',
        'transaction_create'    => 'Новая транзакция',
        'remains'               => 'Остатки',
    ],
    'page' => [
        'list' => [
            'title'             => 'Список касс',
            'title_owner'       => 'Владелец',
            'title_alias'       => 'Название кассы',
            'title_incoming'    => 'Приход',
            'title_outcoming'   => 'Расход',
            'title_balance'     => 'Остаток',
            'title_currency'    => 'Валюта',
        ],
    ],
    'button' => array_merge(
        include ('_button.php'),
        [
        ]
    ),
];