<?php

return [
    'page_title' => 'Движение товара',
    'sidebar' => [
        'main'   => 'Товары',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Движение товара',
        'list'          => 'Список приходов',
        'create'        => 'Создание прихода',
        #'edit'          => 'Редактирование прихода',
        #'edit_access'   => 'Изменение доступа',
    ],
    'pages' => [
        'list' => [
            'title'                  => 'Управление приходами',
            'title_id'               => '№',
            'title_warehouse'        => 'Склад получатель',
            'title_date'             => 'Дата поставки',
            'title_payment'          => 'Оплата склада',
            'title_payment_date'     => 'Дата оплаты',
            'title_payment_amount'   => 'Сумма оплаты',
            'title_payment_currency' => 'Валюта оплаты',
            'title_products'         => 'Товары',
            'title_author'           => 'Кем создан',
        ],
        'form' => [
            'all'       => 'Все',
            'groups'    => 'Группы пользователей',
            'users'     => 'Пользователи',
        ],
        'create' => [
            'title'         => 'Создание прихода',
            'title_name'    => 'Название',
            'title_trademark' => 'Бренд',
        ],
        'edit' => [
            'title' => 'Редактирование прихода',
        ],
    ],
    'placeholders' => [
        'enter_warehouse'   => 'Выбор склада',
        'enter_currency'    => 'Выбор валюты',
        'enter_amount'      => 'Сумма',
        'quantity'          => 'Количество',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'show'          => 'Показать список',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить остатки',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];