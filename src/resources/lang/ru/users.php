<?php

return [
    'page_title' => 'Управление пользователями',
    'breadcrumb' => [
        'main'                  => 'Пользователи',
        'list'                  => 'Список пользователей',
        'create'                => 'Регистрация пользователя',
        'edit'                  => 'Редактирование пользователя',
        'remains'               => 'Остатки',

    ],
    'page' => [
        'list' => [
            'title'                 => 'Список пользователей',
            'title_name'            => 'Имя',
            'title_email'           => 'Email',
            'title_type'            => 'Тип',
            'title_cashboxes'       => 'Кассы',
            'title_comment'         => 'Комментарий',
            'title_created_by'      => 'Кем создан',
            'title_history'         => 'Правки',
            'title_last_visit'      => 'Последний визит',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
            'title_remains'         => 'Остатки складов',

            'fields_of_history' => [
                'title'             => 'История изменений',
                'action'            => 'Действие',
                'edited_by_user'    => 'Кем изменен',
                'action_date'       => 'Дата',
                'name'              => 'Имя',
                'email'             => 'Email',
                'password'          => 'Пароль',
                'type'              => 'Тип',
                'comment'           => 'Комментарий',
                'group'         => [
                    'action'    => 'Действие',
                    'name'      => 'Название',
                ],
                'cashbox'         => [
                    'action'    => 'Действие',
                    'balance'   => 'Баланс',
                    'currency'  => 'Валюта',
                    'date'      => 'Дата',
                    'alias'     => 'Псевдоним',
                ],
            ],
        ],
        'form' => [
            'name'      => 'Имя',
            'email'     => 'Email',
            'password'  => 'Пароль',
            'confirm'   => 'Повторить пароль',
            'type'      => 'Тип',
            'groups'    => 'Группы',
            'cashboxes' => 'Кассы',
            'comment'   => 'Комментарий',
        ],
        'create' => [
            'title' => 'Регистрация пользователя',
        ],
        'edit' => [
            'title' => 'Редактирование пользователя',
        ],
        'transactions' => [

        ],
    ],
    'placeholders' => [
        'enter_name' => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'button' => array_merge(
        include ('_button.php'),
        [
        ]
    ),
];