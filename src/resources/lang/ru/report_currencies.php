<?php

return [
    'page_title' => 'Отчеты. Валюта',
    'sidebar' => [
        'main'   => 'Валюта',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'      => 'Валюта',
        'list'      => 'Список валют',
        'create'    => 'Создание валюты',
        'edit'      => 'Редактирование валюты',
    ],
    'pages' => [
        'list' => [
            'title'         => 'Список валют',
            'title_id'      => 'ID',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
            'title_edit'    => 'Редактировать',
            'title_delete'  => 'Удалить',
        ],
        'create' => [
            'title'         => 'Создание валюты',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
        ],
        'edit' => [
            'title'         => 'Редактирование валюты',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];