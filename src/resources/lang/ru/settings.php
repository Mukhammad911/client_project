<?php

return [
    'page_title' => 'Настройки',
    'sidebar' => [
        'main'   => 'Зарплаты',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'currencies'    => [
            'main'          => 'Настройки',
            'list'          => 'Список валют',
            'edit'          => 'Редактирование валют',
        ],
    ],
    'pages' => [
        'list'    => [
            'currencies'    => [
                'title' => 'Настройки валют',
                'title_list' => 'Разрешенные валюты',
                'title_edit' => 'Редактировать',
            ],
        ],
        'edit'    => [
            'currencies'    => [
                'title' => 'Редактирование валют',
                'title_currency' => 'Разрешенные валюты',
            ],
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите имя',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить остатки',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];