<?php

return [
    'page_title' => 'Транспорт',
    'sidebar' => [
        'main'   => 'Транспорт',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Транспорт',
        'list'          => 'Список транспорта',
        'create'        => 'Создание транспорта',
        'edit'          => 'Редактирование транспорта',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление транспортом',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
        ],
        'create' => [
            'title'         => 'Создание транспорта',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
        ],
        'edit' => [
            'title' => 'Редактирование транспорта',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];