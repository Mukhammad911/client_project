<?php

return [
    'page_title' => 'Кассы пользователей. Приходы',
    'breadcrumb' => [
        'main'          => 'Приходы',
        'list'          => 'Список приходов',
        'create'        => 'Создание',
    ],
    'page' => [
        'list' => [
            'title'             => 'Управление приходами',
            'title_id'          => 'ID',
            'title_user'        => 'Пользователь',
            'title_cashbox'     => 'Касса пользователя',
            'title_account'     => 'Счет',
            'title_description' => 'Описание',
            'title_client'      => 'Клиент',
            'title_amount'      => 'Сумма',
            'title_currency'    => 'Валюта',
            'title_date'        => 'Дата',
            'title_comment'     => 'Комментарий',
        ],
        'form' => [
            'title_user'        => 'Пользователь',
            'title_cashbox'     => 'Касса пользователя',
            'title_account'     => 'Счет',
            'title_description' => 'Описание',
            'title_client'      => 'Клиент',
            'title_amount'      => 'Сумма',
            'title_currency'    => 'Валюта',
            'title_date'        => 'Дата',
            'title_comment'     => 'Комментарий',
        ],
        'create' => [
            'title' => 'Создание прихода',
        ],
        'edit' => [
            'title' => 'Редактирование прихода',
        ],
    ],
    'buttons' => array_merge(
        include '_button.php',
        [

        ]
    ),
];