<?php

return [
    'page_title' => 'Позиция',
    'sidebar' => [
        'main'   => 'Позиция',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Позиция',
        'list'          => 'Список позиции',
        'create'        => 'Создание позиции',
        'edit'          => 'Редактирование позиции',
    ],
    'page' => [
        'list' => [
            'title'                     => 'Управление позициями',
            'title_id'                  => '№',
            'title_name'                => 'Название',
            'title_unit'                => 'Единица измерения',
            'title_price_position'      => 'Цена позиции',
            'title_currency_position'   => 'Валюта позиции',
            'title_comment'             => 'Комментарий',
            'title_edit'                => 'Редактировать',
            'title_delete'              => 'Удалить',
            'title_action'              => 'Действия',
        ],
        'form' => [
            'title_name'                => 'Название',
            'title_comment'             => 'Комментарий',
            'title_unit'                => 'Единица измерения',
            'title_price_position'      => 'Цена позиции',
            'title_currency_position'   => 'Валюта позиции',
            'title_edit'                => 'Редактировать',
            'title_delete'              => 'Удалить',
            'title_action'              => 'Действия',
        ],
        'create' => [
            'title' => 'Создание позиции',
        ],
        'edit' => [
            'title' => 'Редактирование позиции',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
        'enter_unit'    => 'Введите единицу измерения',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];