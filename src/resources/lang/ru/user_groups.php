<?php

return [
    'page_title' => 'Группы пользователей',
    'sidebar' => [
        'main'   => 'Группы пользователей',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Группы пользователей',
        'list'          => 'Список групп',
        'create'        => 'Создание группы',
        'edit'          => 'Редактирование группы',
        'show_group'    => 'Обзор группы',
    ],
    'pages' => [
        'list' => [
            'title'             => 'Управление группами',
            'title_name'        => 'Название',
            'title_comment'     => 'Комментарий',
            'title_show_group'  => 'Обзор группы',
            'title_edit'        => 'Редактировать',
            'title_delete'      => 'Удалить',
        ],

        'create' => [
            'title' => 'Создание группы',
            'title_name' => 'Название',
        ],
        'edit' => [
            'title' => 'Редактирование группы',
        ],
    ],
    'placeholders' => [
        'enter_name' => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'    => 'СОЗДАТЬ',
        'get_group' => 'ОБЗОР ГРУППЫ',
        'edit'      => 'РЕДАКТИРОВАТЬ',
        'delete'    => 'УДАЛИТЬ',
        'save'      => 'Сохранить',
        'cancel'    => 'Отмена',
    ],
];