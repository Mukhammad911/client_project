<?php

return [
    'page_title' => 'Товары',
    'sidebar' => [
        'main'   => 'Товары',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Товары',
        'list'          => 'Список товаров',
        'create'        => 'Создание товара',
        'edit'          => 'Редактирование товара',
        #'edit_access'   => 'Изменение доступа',
    ],
    'pages' => [
        'list' => [
            'title'                 => 'Управление товарами',
            'title_name'            => 'Наименование товара',
            'title_comment'         => 'Комментарий',
            'title_category'        => 'Категория',
            'title_date_created'    => 'Дата создания',
            'title_trademark'       => 'Бренд',
            'title_edit'            => 'Редактировать',
            'title_delete'          => 'Удалить',
        ],
        'form' => [
            'all'       => 'Все',
            'groups'    => 'Группы пользователей',
            'users'     => 'Пользователи',
        ],
        'create' => [
            'title'         => 'Создание товара',
            'title_name'    => 'Название',
            'title_trademark' => 'Бренд',
        ],
        'edit' => [
            'title' => 'Редактирование товара',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'move_remnants' => 'Переместить остатки',
        'edit_access'   => 'Изменить доступ',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];