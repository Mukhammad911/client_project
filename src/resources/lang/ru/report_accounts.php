<?php

return [
    'page_title' => 'Отчеты. Счета',
    'sidebar' => [
        'main'   => 'Счета',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'      => 'Счета',
        'list'      => 'Список счетов',
        'create'    => 'Создание счета',
        'edit'      => 'Редактирование счета',
    ],
    'pages' => [
        'list' => [
            'title'         => 'Список счетов',
            'title_id'      => 'ID',
            'title_name'    => 'Название',
            'title_comment' => 'Комментарий',
            'title_edit'    => 'Редактировать',
            'title_delete'  => 'Удалить',
            'date'          => 'Дата',
        ],
        'create' => [
            'title'             => 'Создание счета',
            'title_name'        => 'Название',
            'title_comment'     => 'Комментарий',
            'title_status'      => 'Статус',
        ],
        'edit' => [
            'title' => 'Редактирование счета',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
    'select' => [
        'active'        => 'Активен',
        'inactive'      => 'Неактивен',
        'change'        => 'Поменять Статус',
    ]
];