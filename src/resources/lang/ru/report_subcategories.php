<?php

return [
    'page_title' => 'Отчеты. Подкатегории',
    'sidebar' => [
        'main'   => 'Склады',
        'create' => 'Создать',
        'list'   => 'Список',
    ],
    'breadcrumb' => [
        'main'          => 'Подкатегории',
        'list'          => 'Список подкатегорий',
        'create'        => 'Создание подкатегории',
        'edit'          => 'Редактирование подкатегории',
        'reports'       => 'Отчеты',
    ],
    'pages' => [
        'categories' => [
            'title'                 => 'Подкатегории',
            'title_id'              => 'ID',
            'title_name'            => 'Название',
            'title_comment'         => 'Комментарий',
            'title_action'          => 'Действия',
        ],
        'list' => [
            'title'                     => 'Управление подкатегориями',
            'title_id'                  => 'ID',
            'title_name'                => 'Название',
            'title_comment'             => 'Комментарий',
            'title_linked_categories'   => 'Связанные категории',
            'title_edit'                => 'Редактировать',
            'title_delete'              => 'Удалить',
        ],
        'form' => [
            'link_all'              => 'Все',
            'link_custom'           => 'Определенные',
            'title_type_of_link'    => 'Связь с категориями',
            'title_categories'      => 'Категории',
        ],
        'create' => [
            'title'             => 'Создание подкатегории',
            'title_name'        => 'Название',
            'title_comment'     => 'Комментарий',
            'title_category'    => 'Категория',
        ],
        'edit' => [
            'title' => 'Редактирование подкатегории',
        ],
    ],
    'placeholders' => [
        'enter_name'    => 'Введите название',
        'enter_comment' => 'Введите комментарий',
    ],
    'buttons' => [
        'create'        => 'Создать',
        'edit'          => 'Редактировать',
        'delete'        => 'Удалить',
        'save'          => 'Сохранить',
        'cancel'        => 'Отмена',
    ],
];