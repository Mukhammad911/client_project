@extends('accounting::layout.main')

@section('title', __('accounting::report_accounts.page_title') .' - '. config('accounting.app.name'))

@section('page_styles')
    @include('accounting::layout.partial.css')
@endsection

@section('content')

    @include('accounting::layout.partial.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">

                @include('accounting::layout.partial.breadcrumb')

                @include('accounting::layout.partial.toolbar')

            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light form-fit">

                        <div class="portlet-title">
                            <div class="caption bold font-blue-madison">
                                @lang('accounting::report_accounts.pages.create.title')
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
                                </a>
                            </div>
                        </div>

                        <!-- BEGIN FORM-->
                        <form id="user-form" action="{{ route("accounting:cashboxes.account.save") }}"
                              class="form-horizontal" autocomplete="off" method="POST">

                            @csrf

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_accounts.pages.create.title_name')
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <input name="name" id="name-form-control" type="text"
                                               placeholder="@lang('accounting::report_accounts.placeholders.enter_name')"
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_accounts.pages.create.title_comment')
                                    </label>
                                    <div class="col-md-6">
                                        <input name="comment" id="name-form-control" type="text"
                                               placeholder="@lang('accounting::report_accounts.placeholders.enter_comment')"
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn blue">
                                            <i class="fa fa-check"></i>
                                            @lang('accounting::report_accounts.buttons.save')
                                        </button>
                                        <a href="{{ route('accounting:cashboxes.accounts') }}" type="button" class="btn default">
                                            @lang('accounting::report_accounts.buttons.cancel')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

    @if(config('accounting.quicksidebar.enabled'))
        @include('accounting::layout.partial.quicksidebar')
    @endif

    <!-- BEGIN MODAL -->
    @include('accounting::layout.partial.modalsmall')
    <!-- END MODAL -->

@endsection

@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @include('accounting::layout.partial.js')
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
