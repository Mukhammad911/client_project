@extends('accounting::layout.main')

@section('title', __('accounting::report_accounts.page_title') .' - '. config('accounting.app.name'))

@section('page_styles')
    @include('accounting::layout.partial.css')
@endsection

@section('content')

    @include('accounting::layout.partial.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">

                @include('accounting::layout.partial.breadcrumb')

                @include('accounting::layout.partial.toolbar')

            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light form-fit">

                        <div class="portlet-title">
                            <div class="caption bold font-blue-madison">
                                @lang('accounting::report_accounts.pages.edit.title')
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
                                </a>
                            </div>
                        </div>

                        <!-- BEGIN FORM-->
                        <form id="user-form" action="{{ route('accounting:cashboxes.account.update', ['id' => $account->id]) }}" class="form-horizontal" autocomplete="off" method="POST">

                            @csrf

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_accounts.pages.create.title_name')
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <input name="name" id="name-form-control" type="text"
                                               placeholder="@lang('accounting::report_accounts.placeholders.enter_name')"
                                               class="form-control"
                                               value="{{ $account->name }}"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_accounts.pages.create.title_comment')
                                    </label>
                                    <div class="col-md-6">
                                        <input name="comment" id="name-form-control" type="text"
                                               placeholder="@lang('accounting::report_accounts.placeholders.enter_comment')"
                                               class="form-control"
                                               value="{{ $account->comment }}"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_accounts.pages.create.title_status')
                                    </label>
                                    <div class="col-md-6">
                                        {{--@if(\Teleglobal\Accounting\Models\ReportAccount::find($account->id))--}}

                                        <select name="status" class="form-control">
                                            <option value="" selected disabled>
                                                @lang('accounting::report_accounts.select.change')
                                            </option>
                                            <option value="@lang('accounting::report_accounts.select.active')">
                                                @lang('accounting::report_accounts.select.active')
                                            </option>
                                            <option value="@lang('accounting::report_accounts.select.inactive')">
                                                @lang('accounting::report_accounts.select.inactive')
                                            </option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group date">
                                    <label class="control-label col-md-3" for="comment-form-control">
                                        @lang('accounting::report_accounts.pages.list.date')
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-2 date">
                                        <div class="input-group date form_datetime">
                                            <input type="text" size="16" class="form-control" name="status_date"
                                                   style="background: #fff; cursor: text" readonly data-date-format="{{ config('accounting.app.date_format_js') }}">
                                            <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                {{--<input type="hidden" name="updated" value="{{\Carbon\Carbon::now()}}">--}}
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn blue">
                                            <i class="fa fa-check"></i>
                                            @lang('accounting::report_accounts.buttons.save')
                                        </button>
                                        <a href="{{ route('accounting:cashboxes.accounts') }}" type="button" class="btn default">
                                            @lang('accounting::report_accounts.buttons.cancel')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->


    @if(config('accounting.quicksidebar.enabled'))
        @include('accounting::layout.partial.quicksidebar')
    @endif

    <!-- BEGIN MODAL -->
    @include('accounting::layout.partial.modalsmall')
    <!-- END MODAL -->

@endsection

@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript" ></script>

    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.sparkline.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/select2/select2.min.js') }}" type="text/javascript" ></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('teleglobal/accounting/js/metronic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/layout.js') }}" type="text/javascript"></script>
    @if(config('accounting.quicksidebar.enabled'))
        <script src="{{ asset('teleglobal/accounting/js/quick-sidebar.js') }}" type="text/javascript"></script>
    @endif
    <!-- END PAGE LEVEL SCRIPTS -->

    <script src="{{ asset('teleglobal/accounting/js/page/outcome.js') }}" type="text/javascript"></script>
    {{--@include('accounting::layout.partial.js') --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
