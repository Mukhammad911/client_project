<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<div class="portlet light form-fit">

    @php
        $dateFormat = config('accounting.app.date_format');
        $dateTimeFormat = config('accounting.app.datetime_format');
        $fieldData = $data
    @endphp
    <!-- BEGIN MODAL -->
    @include('accounting::layout.partial.modalsmall')
    <!-- END MODAL -->

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::cashboxes_outcoming.page_title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:cashboxes.transaction.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::cashboxes_outcoming.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" data-content="{{ $historyFields }}">
            <thead>
            <tr>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_id')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_account')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_description')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_category')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_subcategory')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_date')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_amount')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_currency')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_association')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_history')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_edit')</th>
                <th>@lang('accounting::cashboxes_outcoming.page.list.title_validate')</th>
            </tr>
            </thead>
            <tbody>


         @foreach($transactions as $transaction)
                <tr tag="{{ $transaction->id }}">
                    <td>{{ $transaction->id }}</td>
                    <td>{{ $transaction->account->name }}</td>
                    <td>{{ $transaction->description }}</td>
                    <td>{{ $transaction->category->name }}</td>
                    <td>{{ $transaction->subcategory->name }}</td>
                    <td>{{ (new \Illuminate\Support\Carbon($transaction->date))->format($dateFormat) }}</td>
                    <td>{{ $transaction->amount }}</td>
                    <td>{{ $transaction->currency->name }}</td>
                    @php
                        $association = __('accounting::cashboxes_outcoming.page.form.link_none');
                        if($transaction->source == \Teleglobal\Accounting\Models\ReportTransaction::SOURCE_WAREHOUSE){
                            $association = __('accounting::cashboxes_outcoming.page.form.link_warehouse');
                        }elseif ($transaction->source == \Teleglobal\Accounting\Models\ReportTransaction::SOURCE_ORDER){
                            $association = __('accounting::cashboxes_outcoming.page.form.link_order');
                        }
                    @endphp
                    <td>{{ $association }}</td>
                    <td>
                        <a class="modal-history-trigger font-blue"
                           data-title="@lang('accounting::cashboxes_outcoming.page.list.title_history')">
                            {{ $transaction->history->count() }}
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route("accounting:cashboxes.transaction.edit", ['id' => $transaction->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"> </i>
                            @lang('accounting::cashboxes_outcoming.buttons.edit')
                        </a>
                    </td>
                    <td class="validated text-center" tag="{{ $transaction->id }}">
                        @if($transaction->validated)
                            <span>{{ $transaction->validatedBy->getAttribute('name') }}</span>
                            <br>
                            <span>{{ (new \Illuminate\Support\Carbon($transaction->validated_at))->format($dateTimeFormat) }}</span>
                        @else
                            <a class="default btn btn-xs btn-default validated" href="#">
                                <i class="fa fa-pencil font-blue-madison"> </i>
                                @lang('accounting::cashboxes_outcoming.buttons.validate')
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true,
            "order": [[ 0, "desc" ]]
        });
    });
</script>