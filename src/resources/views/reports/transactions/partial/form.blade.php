<div class="portlet light form-fit">

    @php
        $scenario = ('accounting:cashboxes.transaction.create' === $data['route']) ? 'create' : 'update';
        $dateFormat = config('accounting.app.date_format');
    @endphp

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if($scenario === 'create')
                @lang('accounting::cashboxes_outcoming.page.create.title')
            @elseif($scenario === 'update')
                @lang('accounting::cashboxes_outcoming.page.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- BEGIN FORM-->
        <form id="order-form"
              action="{{ ($scenario == 'create') ? route('accounting:cashboxes.transaction.save') : route('accounting:cashboxes.transaction.update', ['id' => $transaction->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST" scenario="{{ $scenario }}">

            @csrf

            <div class="form-body">
                <div class="form-group user">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_user')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control user" name="user_id">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}"{{
                                (($scenario === 'update') && ($user->id == $transaction->user_id))
                                ? ' selected'
                                : (($scenario === 'create') && ($user->id == $authUserId)) ? ' selected' : ''
                                }}>{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group cashbox">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_cashbox')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control cashbox" name="cashbox_id" data-cashboxes="{{ json_encode($cashboxes) }}">
                            @if($scenario === 'update')
                                @foreach($cashboxes[$transaction->user_id] as $cashbox)
                                    <option value="{{ $cashbox['id'] }}"{{
                                    (($scenario === 'update') && ($cashbox['id'] == $transaction->cashbox_id))
                                    ? ' selected'
                                    : ''
                                    }}>{{ $cashbox['alias'] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group account">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_account')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control account" name="account_id">
                            @foreach($accounts as $account)
                                <option value="{{ $account->id }}"{{ (($scenario === 'update') && ($account->id == $transaction->account_id)) ? ' selected="selected"' : '' }}>{{ $account->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_description')
                    </label>
                    <div class="col-md-6">
                        <textarea class="form-control" name="description">{{ (($scenario === 'update') && !empty($transaction->description)) ? $transaction->description : '' }}</textarea>
                    </div>
                </div>
                <div class="form-group category">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_category')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control category" name="category_id">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"{{ (($scenario === 'update') && ($category->id == $transaction->category_id)) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group subcategory">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_subcategory')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control subcategory" name="subcategory_id" data-subcategories="{{ json_encode($subcategories) }}">
                            @if($scenario === 'update')
                                @foreach($subcategories[$transaction->category_id] as $subcategory)
                                    <option value="{{ $subcategory['id'] }}"{{ (($scenario === 'update') && ($subcategory['id'] == $transaction->subcategory_id)) ? ' selected="selected"' : '' }}>{{ $subcategory['name'] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_amount')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <input type="number" class="form-control" name="amount" min="0" step="0.01"
                               value="{{ ($scenario === 'update') ? $transaction->amount : 0 }}">
                    </div>
                </div>
                <div class="form-group currency">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_currency')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control currency" name="currency_id">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}"{{ (($scenario === 'update') && ($currency->id == $transaction->currency_id)) ? ' selected="selected"' : '' }}>{{ $currency->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group date">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::cashboxes_outcoming.page.form.title_date')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control date" name="date"
                                   style="background: #fff; cursor: text" readonly data-date-format="{{ config('accounting.app.date_format_js') }}"
                                   value="{{ ($scenario === 'update') ? (new \Carbon\Carbon($transaction->date))->format($dateFormat) : '' }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_comment')
                    </label>
                    <div class="col-md-6">
                        <textarea class="form-control" name="comment">{{ (($scenario === 'update') && !empty($transaction->comment)) ? $transaction->comment : '' }}</textarea>
                    </div>
                </div>
                <div class="form-group choice_source">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.title_link_destination')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control choice_source" name="source">
                            <option value="none"{{ (($scenario === 'create') || (($scenario === 'update') && ($transaction->source === 'none'))) ? ' selected' : '' }}>@lang('accounting::cashboxes_outcoming.page.form.link_none')</option>
                            <option value="warehouse"{{ (($scenario === 'update') && ($transaction->source === 'warehouse')) ? ' selected' : '' }}>@lang('accounting::cashboxes_outcoming.page.form.link_warehouse')</option>
                            <option value="order"{{ (($scenario === 'update') && ($transaction->source === 'order')) ? ' selected' : '' }}>@lang('accounting::cashboxes_outcoming.page.form.link_order')</option>
                        </select>
                    </div>
                </div>

                <div class="form-group outcome hidden">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.link_warehouse')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select class="form-control" name="outcome_id">
                            @foreach($outcomes as $outcome)
                                <option value="{{ $outcome->id }}"
                                        {{ (($scenario === 'update') && ($outcome->id == $transaction->source_id)) ? ' selected' : '' }}>
                                    {{ 'Марка: ' . $outcome->pivot->product->name . ' - ' }}
                                    {{ 'Кол-во: ' . $outcome->amount . ' - ' }}
                                    @php
                                        $clientName = ($outcome->document->docItemsClients->count() > 0) ? $outcome->document->docItemsClients->first()->client->name . ' - ' : ' - ';
                                    @endphp
                                    {{ 'Клиент: ' . $clientName }}
                                    {{ 'Дата: ' . $outcome->date }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group order hidden">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_outcoming.page.form.link_order')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <select class="form-control" name="order_id">
                            @foreach($orders as $order)
                                <option value="{{ $order->id }}"{{ (($scenario === 'update') && ($order->id == $transaction->source_id)) ? ' selected="selected"' : '' }}>
                                    {{ 'ID: ' . $order->id . ' - ' }}
                                    {{ 'Счет: ' . $order->account->name . ' - ' }}
                                    {{ 'Позиция: ' . $order->position->name . ' - ' }}
                                    {{ 'Кол-во: ' . $order->quantity . ' - ' }}
                                    {{ 'Сумма: ' . $order->amount . ' - ' }}
                                    {{ 'Дата заказа: ' . $order->date_order . ' - ' }}
                                    {{ 'Комментарий: ' . $order->comment . ' - ' }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::orders.buttons.save')
                        </button>
                        <a href="{{ route('accounting:cashboxes.transactions') }}" type="button" class="btn default">
                            @lang('accounting::orders.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>