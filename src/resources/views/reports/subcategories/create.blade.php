@extends('accounting::layout.main')

@section('title', __('accounting::report_subcategories.page_title') .' - '. config('accounting.app.name'))

@section('page_styles')
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN PAGE STYLES -->
    <link href="{{ asset('teleglobal/accounting/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE STYLES -->

{{--    @include('accounting::layout.partial.css')--}}
@endsection

@section('content')

    @include('accounting::layout.partial.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">

                @include('accounting::layout.partial.breadcrumb')

                @include('accounting::layout.partial.toolbar')

            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light form-fit">

                        <div class="portlet-title">
                            <div class="caption bold font-blue-madison">
                                @lang('accounting::report_subcategories.pages.create.title')
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
                                </a>
                            </div>
                        </div>

                        <!-- BEGIN FORM-->
                        <form id="user-form"
                              action="{{ route("accounting:cashboxes.subcategory.save") }}"
                              class="form-horizontal" autocomplete="off" method="POST">

                            @csrf

                            <div class="form-body name">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_subcategories.pages.create.title_name')
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <input name="name" id="name-form-control" type="text"
                                               placeholder="@lang('accounting::report_subcategories.placeholders.enter_name')"
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group comment">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_subcategories.pages.create.title_comment')
                                    </label>
                                    <div class="col-md-6">
                                        <input name="comment" id="name-form-control" type="text"
                                               placeholder="@lang('accounting::report_subcategories.placeholders.enter_comment')"
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group type_of_link">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_subcategories.pages.form.title_type_of_link')
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="type_of_link" name="typeOfLink">
                                            <option value="all" selected="selected">
                                                @lang('accounting::report_subcategories.pages.form.link_all')
                                            </option>
                                            <option value="custom">
                                                @lang('accounting::report_subcategories.pages.form.link_custom')
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group categories hidden">
                                    <label class="control-label col-md-3">
                                        @lang('accounting::report_subcategories.pages.form.title_categories')
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-3" style="">
                                        <input type="hidden" id="categories" name="categories">
                                        <select class="form-control categories-selector" id="categories" multiple="multiple">
                                            @foreach($categories as $category)
                                                <option
                                                        value="{{ $category->id }}">{{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn blue">
                                            <i class="fa fa-check"></i>
                                            @lang('accounting::report_subcategories.buttons.save')
                                        </button>
                                        <a href="{{ route('accounting:cashboxes.subcategories') }}" type="button" class="btn default">
                                            @lang('accounting::report_subcategories.buttons.cancel')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

    @if(config('accounting.quicksidebar.enabled'))
        @include('accounting::layout.partial.quicksidebar')
    @endif

    <!-- BEGIN MODAL -->
    @include('accounting::layout.partial.modalsmall')
    <!-- END MODAL -->

@endsection

@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.sparkline.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/select2/select2.min.js') }}" type="text/javascript" ></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('teleglobal/accounting/js/metronic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/layout.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/index.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/tasks.js') }}" type="text/javascript"></script>
    @if(config('accounting.quicksidebar.enabled'))
        <script src="{{ asset('teleglobal/accounting/js/quick-sidebar.js') }}" type="text/javascript"></script>
    @endif
    <!-- END PAGE LEVEL SCRIPTS -->

    <script src="{{ asset('teleglobal/accounting/js/page/report_subcategory.js') }}" type="text/javascript"></script>
@endsection
