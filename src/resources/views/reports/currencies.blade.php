<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@extends('accounting::layout.main')

@section('title', __('accounting::report_currencies.page_title') .' - '. config('accounting.app.name'))

@section('page_styles')
    @include('accounting::layout.partial.css')
@endsection

@section('content')

    @include('accounting::layout.partial.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">

                @include('accounting::layout.partial.breadcrumb')

                @include('accounting::layout.partial.toolbar')

            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light form-fit">

                        <div class="portlet-title">
                            <div class="caption bold font-blue-madison">
                                @lang('accounting::report_currencies.pages.list.title')
                            </div>
                            <div class="actions">
                                <a href="{{ route('accounting:cashboxes.currency.create') }}" class="btn btn-circle blue ">
                                    <i class="fa fa-plus"></i>
                                    @lang('accounting::report_currencies.buttons.create')
                                </a>
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
                                </a>
                            </div>
                        </div>

                        <div class="portlet-body">
                            <table class="table table-hover table-bordered data-table" id="sample_editable_1">
                                <thead>
                                <tr>
                                    <th>@lang('accounting::report_currencies.pages.list.title_id')</th>
                                    <th>@lang('accounting::report_currencies.pages.list.title_name')</th>
                                    <th>@lang('accounting::report_currencies.pages.list.title_comment')</th>
                                    <th>@lang('accounting::report_currencies.pages.list.title_edit')</th>
                                    <th>@lang('accounting::report_currencies.pages.list.title_delete')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($currencies as $currency)
                                    <tr>
                                        <td>{{ $currency->id }}</td>
                                        <td>{{ $currency->name }}</td>
                                        <td>{{ $currency->comment }}</td>
                                        <td>
                                            <a class="default btn btn-xs btn-default"
                                               href="{{ route("accounting:cashboxes.currency.edit", ['id' => $currency->id]) }}">
                                                <i class="fa fa-pencil font-blue-madison"> </i>
                                                @lang('accounting::report_currencies.buttons.edit')
                                            </a>
                                        </td>
                                        <td>
                                            <a class="default btn btn-xs btn-default"
                                               href="{{ route("accounting:cashboxes.currency.delete", ['id' => $currency->id]) }}">
                                                <i class="fa fa-pencil font-blue-madison"> </i>
                                                @lang('accounting::report_currencies.buttons.delete')
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

    @if(config('accounting.quicksidebar.enabled'))
        @include('accounting::layout.partial.quicksidebar')
    @endif

@endsection

@section('page_scripts')
    @include('accounting::layout.partial.js')
@endsection
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>