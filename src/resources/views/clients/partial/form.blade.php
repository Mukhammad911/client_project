<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:client.create' === $data['route'])
                @lang('accounting::clients.pages.create.title')
            @elseif('accounting:client.edit' === $data['route'])
                @lang('accounting::clients.pages.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:client.create' === $data['route']) ? 'create' : 'update';
        @endphp

        <!-- BEGIN FORM-->
        <form id="user-form"
              action="{{ ($scenario == 'create') ? route('accounting:client.save') : route('accounting:client.update', ['id' => $client->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::clients.pages.list.title_name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::clients.placeholders.enter_name')" class="form-control"
                               value="{{ $client->name ?? '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::clients.pages.list.title_comment')
                    </label>
                    <div class="col-md-6">
                        @php
                            $comment = isset($client->comment) ? $client->comment : '' ;
                        @endphp
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::clients.placeholders.enter_comment')"
                                  class="form-control">{{ $comment }}</textarea>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::clients.buttons.save')
                        </button>
                        <a href="{{ route('accounting:clients.list') }}" type="button" class="btn default">
                            @lang('accounting::clients.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->