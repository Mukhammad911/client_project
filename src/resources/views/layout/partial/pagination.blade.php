{{-- Pagination container --}}
@php
    $page = $page ?? 1;
    $space = config('accounting.app.pagination.space', 3);
    $pagesCount = $pagesCount ?? null;
@endphp

<style>
    .pagination-container .pagination button{
        margin: 2px;
    }
</style>

<div class="text-center pagination-container" space="{{ $space }}">
    {{-- Buttons --}}
    <div class="btn-group pagination">
        @if($pagesCount !== null)
            @if( $page > ($space + 1) )
                <button type="button" class="btn btn-default pag-button" tag="1" title="@lang('accounting::incomes.buttons.first_page')">&laquo;</button>
            @endif
            @for( $i=1; $i<=$pagesCount; $i++ )
                @if( ($i - $space <= $page) && ($i + $space >= $page) )
                    <button type="button" class="btn btn-default pag-button" tag="{{ $i }}"{{ ($i === $page) ? ' disabled="disabled" style="background-color: #cccccc"' : '' }} title="@lang('accounting::incomes.buttons.page') {{ $i }}">{{ $i }}</button>
                @endif
            @endfor
            @if( ($page + $space) < $pagesCount )
                <button type="button" class="btn btn-default pag-button" tag="{{ $pagesCount }}" title="@lang('accounting::incomes.buttons.last_page')">&raquo;</button>
            @endif
        @endif
    </div>
    {{-- Page Input --}}
    <div style="width: 150px; margin-left: auto; margin-right: auto;">
        <div class="input-group">
            <input type="number" class="form-control input-current-page" min="1" max="{{ $pagesCount }}" value="{{ $page }}">
            <span class="input-group-btn">
                    <button class="btn btn-default button-current-page" type="button">Перейти</button>
                </span>
        </div>
    </div>
</div>
