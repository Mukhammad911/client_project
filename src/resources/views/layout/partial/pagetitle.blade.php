<h3 class="page-title">
    @if('accounting:users.list' === $data['route'])
        @lang('accounting::page.users.title_1')
    @elseif('accounting:users.create' === $data['route'])
        @lang('accounting::page.users.title_2')
    @endif
</h3>