<ul class="page-breadcrumb">
    @if(isset($data['breadcrumb']))
        @foreach($data['breadcrumb'] as $item)

            <li>
                <i class="fa{{ $item['class'] }}"></i>

                @if($item['route'])
                    <a href="{{ route($item['route']) }}">
                        @lang($item['title'])
                    </a>
                @else
                    <span>
                        @lang($item['title'])
                    </span>
                @endif

                @if(!$loop->last)
                    <i class="fa fa-angle-right"></i>
                @endif

            </li>

        @endforeach
    @endif
</ul>