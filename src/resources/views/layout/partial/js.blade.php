<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('teleglobal/accounting/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/morris/morris.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/morris/raphael-min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/flot/jquery.flot.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jquery.pulsate.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jquery.sparkline.min.js') }}" type="text/javascript" ></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('teleglobal/accounting/js/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/js/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/js/index.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/js/tasks.js') }}" type="text/javascript"></script>
@if(config('accounting.quicksidebar.enabled'))
    <script src="{{ asset('teleglobal/accounting/js/quick-sidebar.js') }}" type="text/javascript"></script>
@endif
<!-- END PAGE LEVEL SCRIPTS -->

<script src="{{ asset('teleglobal/accounting/js/page/dashboard.js') }}" type="text/javascript"></script>

<script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/jquery.sparkline.min.js') }}" type="text/javascript" ></script>

<script src="{{ asset('teleglobal/accounting/plugins/select2/select2.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('teleglobal/accounting/js/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/js/layout.js') }}" type="text/javascript"></script>

@if(config('accounting.quicksidebar.enabled'))
    <script src="{{ asset('teleglobal/accounting/js/quick-sidebar.js') }}" type="text/javascript"></script>
@endif