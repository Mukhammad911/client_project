<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu page-sidebar-menu-closed page-sidebar-menu-light {{--page-sidebar-menu-hover-submenu--}}"
            data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
{{--            <li class="sidebar-search-wrapper">--}}
{{--                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->--}}
{{--                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->--}}
{{--                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->--}}
{{--                <form class="sidebar-search" action="extra_search.html" method="POST">--}}
{{--                    <a href="javascript:;" class="remove">--}}
{{--                        <i class="icon-close"></i>--}}
{{--                    </a>--}}
{{--                    <div class="input-group">--}}
{{--                        <input type="text" class="form-control" placeholder="Search...">--}}
{{--                        <span class="input-group-btn">--}}
{{--							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>--}}
{{--							</span>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--                <!-- END RESPONSIVE QUICK SEARCH FORM -->--}}
{{--            </li>--}}

            @php
                $active = in_array($data['route'], [
                    'accounting:dashboard.index',
                ]);
                $li_class = $active ? 'start active open' : '';
                $span = $active ? '<span class="selected"></span>' : '';
                $arrow_class = $active ? 'open' : '';
            @endphp
            <li class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="icon-home"></i>
                    <span class="title">@lang('accounting::_sidebar.dashboard.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">
                    @php
                        $active = in_array($data['route'], [
                            'accounting:dashboard.index',
                        ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:dashboard.index') }}">
                            <i class="fa fa-dashboard"></i>
                            @lang('accounting::_sidebar.dashboard.panel')
                        </a>
                    </li>
                </ul>
            </li>

            {{--    USERS    --}}
            @php
                $active = in_array($data['route'], [
                    'accounting:users',
                    'accounting:user.create',
                    'accounting:user.edit',

                    'accounting:user_groups',
                    'accounting:user_group.create',
                    'accounting:user_group.edit',
                ]);
                $li_class = $active ? 'start active open' : '';
                $span = $active ? '<span class="selected"></span>' : '';
                $arrow_class = $active ? 'open' : '';
            @endphp
            <li class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="icon-users"></i>
                    <span class="title">@lang('accounting::_sidebar.users.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">

                    {{--    USERS - CREATE    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:user.create',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:user.create') }}">
                            <i class="fa fa-plus"></i>
                            @lang('accounting::_sidebar.users.create')
                        </a>
                    </li>

                    {{--    USERS - LIST    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:users',
                                'accounting:user.edit'
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:users') }}">
                            <i class="fa fa-list"></i>
                            @lang('accounting::_sidebar.users.users')
                        </a>
                    </li>

                    {{--    USERS - GROUPS    --}}
{{--                    @php--}}
{{--                        $active = in_array($data['route'], [--}}
{{--                                'accounting:user_groups',--}}
{{--                                'accounting:user_group.create',--}}
{{--                                'accounting:user_group.edit',--}}
{{--                            ]);--}}
{{--                        $li_class = $active ? 'active' : '';--}}
{{--                    @endphp--}}
{{--                    <li class="{{ $li_class }}">--}}
{{--                        <a href="{{ route('accounting:user_groups') }}">--}}
{{--                            <i class="fa fa-users"></i>--}}
{{--                            @lang('accounting::_sidebar.users.groups')--}}
{{--                        </a>--}}
{{--                    </li>--}}

                </ul>
            </li>

            {{--    ORDERS    --}}
            @php
                $active = in_array($data['route'], [
                    'accounting:orders.index',
                    'accounting:order.create',
                    'accounting:order.edit',

                    'accounting:orders.positions',
                    'accounting:orders.position.create',
                    'accounting:orders.position.edit',

                    'accounting:orders.transports',
                    'accounting:orders.transport.create',
                    'accounting:orders.transport.edit'
                ]);
                $li_class = $active ? 'start active open' : '';
                $span = $active ? '<span class="selected"></span>' : '';
                $arrow_class = $active ? 'open' : '';
            @endphp
            <li class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="icon-basket-loaded"></i>
                    <span class="title">@lang('accounting::_sidebar.orders.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">

                    {{--    ORDERS - CREATE    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:order.create',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:order.create') }}">
                            <i class="fa fa-plus"></i>
                            @lang('accounting::_sidebar.orders.create')
                        </a>
                    </li>

                    {{--    ORDERS - INDEX    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:orders.index',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:orders.index') }}">
                            <i class="fa fa-list"></i>
                            @lang('accounting::_sidebar.orders.orders')
                        </a>
                    </li>

                    {{--    ORDERS - INDEX    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:orders.positions',
                                'accounting:orders.position.create',
                                'accounting:orders.position.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:orders.positions') }}">
                            <i class="fa fa-check-circle-o"></i>
                            @lang('accounting::_sidebar.orders.positions')
                        </a>
                    </li>

                    {{--    ORDERS - INDEX    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:orders.transports',
                                'accounting:orders.transport.create',
                                'accounting:orders.transport.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:orders.transports') }}">
                            <i class="fa fa-truck"></i>
                            @lang('accounting::_sidebar.orders.transports')
                        </a>
                    </li>
                </ul>
            </li>

            {{--    SALARY    --}}
            @php
                $active = in_array($data['route'], [
                    'accounting:salary.recipients.list',
                    'accounting:salary.recipient.create',
                    'accounting:salary.recipient.edit',

                    'accounting:salary.reports.list',
                    'accounting:salary.report.create',
                    'accounting:salary.report.edit',
                ]);
                $li_class = $active ? 'start active open' : '';
                $span = $active ? '<span class="selected"></span>' : '';
                $arrow_class = $active ? 'open' : '';
            @endphp
            <li class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="icon-wallet"></i>
                    <span class="title">@lang('accounting::_sidebar.salaries.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">

                    {{--    SALARY - RECIPIENTS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:salary.recipients.list',
                            'accounting:salary.recipient.create',
                            'accounting:salary.recipient.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:salary.recipients.list') }}">
                            <i class="fa fa-user"></i>
                            @lang('accounting::_sidebar.salaries.recipients')
                        </a>
                    </li>

                    {{--    SALARY - REPORTS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:salary.reports.list',
                            'accounting:salary.report.create',
                            'accounting:salary.report.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:salary.reports.list') }}">
                            <i class="fa fa-bar-chart-o"></i>
                            @lang('accounting::_sidebar.salaries.reports')
                        </a>
                    </li>
                </ul>
            </li>

            {{--    WAREHOUSES    --}}
            @php
                $active = in_array($data['route'], [
                    'accounting:warehouses.list',
                    'accounting:warehouse.create',
                    'accounting:warehouse.edit',

                    'accounting:products.list',
                    'accounting:product.create',
                    'accounting:product.edit',

                    'accounting:incomes.list',
                    'accounting:income.create',
                    'accounting:income.get',

                    'accounting:outcomes.list',
                    'accounting:outcome.create',
                    'accounting:outcome.get',

                    'accounting:trademarks.list',
                    'accounting:trademark.create',
                    'accounting:trademark.edit',

                    'accounting:clients.list',
                    'accounting:client.create',
                    'accounting:client.edit',

                    'accounting:transports.list',
                    'accounting:transport.create',
                    'accounting:transport.edit',

                    'accounting:dealers.list',
                    'accounting:dealer.create',
                    'accounting:dealer.edit',

                    'accounting:movement.list',

                    'accounting:storage.reports',

                    'accounting:users.remains',
                    'accounting:users.remains.lists',
                ]);
                $li_class = $active ? 'start active open' : '' ;
                $span = $active ? '<span class="selected"></span>' : '' ;
                $arrow_class = $active ? 'open' : '' ;
            @endphp
            <li class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="icon-grid"></i>
                    <span class="title">@lang('accounting::_sidebar.warehouses.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">

                    {{--    WAREHOUSES - CREATE    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:warehouse.create',
                        ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:warehouse.create') }}">
                            <i class="fa fa-plus"></i>
                            @lang('accounting::_sidebar.warehouses.create')
                        </a>
                    </li>

                    {{--    WAREHOUSES - LIST    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:warehouses.list',
                            'accounting:warehouse.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:warehouses.list') }}">
                            <i class="fa fa-list"></i>
                            @lang('accounting::_sidebar.warehouses.warehouses')
                        </a>
                    </li>

                    {{--    WAREHOUSES - CLIENTS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:clients.list',
                            'accounting:client.create',
                            'accounting:client.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:clients.list') }}">
                            <i class="fa fa-user"></i>
                            @lang('accounting::_sidebar.warehouses.clients')
                        </a>
                    </li>

                    {{--    WAREHOUSES - TRANSPORTS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:transports.list',
                            'accounting:transport.create',
                            'accounting:transport.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:transports.list') }}">
                            <i class="fa fa-truck"></i>
                            @lang('accounting::_sidebar.warehouses.transports')
                        </a>
                    </li>

                    {{--    WAREHOUSES - DEALERS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:dealers.list',
                            'accounting:dealer.create',
                            'accounting:dealer.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:dealers.list') }}">
                            <i class="fa fa-dedent"></i>
                            @lang('accounting::_sidebar.warehouses.dealers')
                        </a>
                    </li>

                    {{--    WAREHOUSES - INCOMES    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:incomes.list',
                            'accounting:income.create',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:incomes.list') }}">
                            <i class="fa fa-sign-in"></i>
                            @lang('accounting::_sidebar.warehouses.incomes')
                        </a>
                    </li>

                    {{--    WAREHOUSES - OUTCOMES    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:outcomes.list',
                            'accounting:outcome.create',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:outcomes.list') }}">
                            <i class="fa fa-sign-out"></i>
                            @lang('accounting::_sidebar.warehouses.outcomes')
                        </a>
                    </li>

                    @php
                        $active = in_array($data['route'], [
                                'accounting:users.remains',
                                'accounting:users.remains.lists',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp

                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:users.remains') }}">
                            <i class="fa fa-exchange"></i>
                            @lang('accounting::_sidebar.cashboxes.remains')
                        </a>
                    </li>
                    {{--    WAREHOUSES - PRODUCTS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:products.list',
                            'accounting:product.create',
                            'accounting:product.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:products.list') }}">
                            <i class="fa fa-qrcode"></i>
                            @lang('accounting::_sidebar.warehouses.products')
                        </a>
                    </li>

                    {{--    WAREHOUSES - REPORTS    --}}
{{--                    @php--}}
{{--                        $active = in_array($data['route'], [--}}
{{--                            'accounting:warehouses.reports',--}}
{{--                            ]);--}}
{{--                        $li_class = $active ? 'active' : '';--}}
{{--                    @endphp--}}
{{--                    <li class="{{ $li_class }}">--}}
{{--                        <a href="{{ route('accounting:warehouses.reports') }}">--}}
{{--                            <i class="fa fa-bar-chart-o"></i>--}}
{{--                            @lang('accounting::_sidebar.warehouses.reports')--}}
{{--                        </a>--}}
{{--                    </li>--}}

                    {{--    WAREHOUSES - MOVEMENT    --}}
{{--                    @php--}}
{{--                        $active = in_array($data['route'], [--}}
{{--                            'accounting:warehouses.movements',--}}
{{--                            ]);--}}
{{--                        $li_class = $active ? 'active' : '';--}}
{{--                    @endphp--}}
{{--                    <li class="{{ $li_class }}">--}}
{{--                        <a href="{{ route('accounting:warehouses.movements') }}">--}}
{{--                            <i class="fa fa-qrcode"></i>--}}
{{--                            @lang('accounting::_sidebar.warehouses.movements')--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </li>

            {{--    CASHBOXES    --}}
            @php
                $active = in_array($data['route'], [
                    'accounting:cashboxes.categories',
                    'accounting:cashboxes.category.create',
                    'accounting:cashboxes.category.edit',

                    'accounting:cashboxes.subcategories',
                    'accounting:cashboxes.subcategory.create',
                    'accounting:cashboxes.subcategory.edit',

                    'accounting:cashboxes.accounts',
                    'accounting:cashboxes.account.create',
                    'accounting:cashboxes.account.edit',

                    'accounting:cashboxes.currencies',
                    'accounting:cashboxes.currency.create',
                    'accounting:cashboxes.currency.edit',

                    'accounting:cashboxes',

                    'accounting:cashboxes.incoming',
                    'accounting:cashboxes.incoming.create',
                    'accounting:cashboxes.incoming.edit',

                    'accounting:cashboxes.transactions',
                    'accounting:cashboxes.transaction.create',
                    'accounting:cashboxes.transaction.edit',

                    'accounting:users.transactions',
                    'accounting:users.transaction.create',


                ]);
                $li_class = $active ? 'start active open' : '';
                $span = $active ? '<span class="selected"></span>' : '' ;
                $arrow_class = $active ? 'open' : '' ;
            @endphp
            <li class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="fa fa-money"></i>
                    <span class="title">@lang('accounting::_sidebar.cashboxes.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">

                    {{--    CASHBOXES - CATEGORIES    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:cashboxes.categories',
                            'accounting:cashboxes.category.create',
                            'accounting:cashboxes.category.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes.categories') }}">
                            <i class="fa fa-tag"></i>
                            @lang('accounting::_sidebar.cashboxes.categories')
                        </a>
                    </li>

                    {{--    CASHBOXES - SUBCATEGORIES    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:cashboxes.subcategories',
                            'accounting:cashboxes.subcategory.create',
                            'accounting:cashboxes.subcategory.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes.subcategories') }}">
                            <i class="fa fa-tags"></i>
                            @lang('accounting::_sidebar.cashboxes.subcategories')
                        </a>
                    </li>

                    {{--    CASHBOXES - ACCOUNTS    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:cashboxes.accounts',
                            'accounting:cashboxes.account.create',
                            'accounting:cashboxes.account.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes.accounts') }}">
                            <i class="fa fa-bank"></i>
                            @lang('accounting::_sidebar.cashboxes.accounts')
                        </a>
                    </li>

                    {{--    CASHBOXES - CURRENCIES    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:cashboxes.currencies',
                            'accounting:cashboxes.currency.create',
                            'accounting:cashboxes.currency.edit',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes.currencies') }}">
                            <i class="fa fa-money"></i>
                            @lang('accounting::_sidebar.cashboxes.currencies')
                        </a>
                    </li>
                    {{--    CASHBOXES - LIST    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:cashboxes',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes') }}">
                            <i class="fa fa-list"></i>
                            @lang('accounting::_sidebar.cashboxes.list')
                        </a>
                    </li>

                    {{--    CASHBOXES - INCOMING    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:cashboxes.incoming',
                            'accounting:cashboxes.incoming.create',
                            'accounting:cashboxes.incoming.edit',
                        ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes.incoming') }}">
                            <i class="fa fa-plus"></i>
                            @lang('accounting::_sidebar.cashboxes.incoming')
                        </a>
                    </li>

                    {{--    CASHBOXES - OUTCOMING    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:cashboxes.transactions',
                            'accounting:cashboxes.transaction.create',
                            'accounting:cashboxes.transaction.edit',
                        ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:cashboxes.transactions') }}">
                            <i class="fa fa-minus"></i>
                            @lang('accounting::_sidebar.cashboxes.outcoming')
                        </a>
                    </li>

                    {{--    CASHBOXES - TRANSACTIONS    --}}
                    @php
                        $active = in_array($data['route'], [
                                'accounting:users.transactions',
                                'accounting:users.transaction.create',
                            ]);
                        $li_class = $active ? 'active' : '';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:users.transactions') }}">
                            <i class="fa fa-exchange"></i>
                            @lang('accounting::_sidebar.cashboxes.transactions')
                        </a>
                    </li>

                    {{--    CASHBOXES - REMAINS    --}}


                </ul>
            </li>

            {{--    SETTINGS    --}}
            @php
                $active = in_array($data['route'], [
                    'accounting:settings.index',
                    'accounting:settings.edit',
                ]);

                $li_class = $active ? 'start active open' : 'last' ;
                $span = $active ? '<span class="selected"></span>' : '' ;
                $arrow_class = $active ? 'open' : '' ;
            @endphp
            <li  class="{{ $li_class }}">
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">@lang('accounting::_sidebar.settings.main')</span>
                    {!! $span !!}
                    <span class="arrow {{ $arrow_class }}"></span>
                </a>
                <ul class="sub-menu">


                    {{--    SETTINGS - CURRENCIES    --}}
                    @php
                        $active = in_array($data['route'], [
                            'accounting:settings.index',
                            'accounting:settings.edit',
                        ]);
                        $li_class = $active ? 'start active open' : 'last';
                    @endphp
                    <li class="{{ $li_class }}">
                        <a href="{{ route('accounting:settings.index', ['key' => 'currencies']) }}">
                            <i class="fa fa-money"></i>
                            @lang('accounting::_sidebar.settings.currency')
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->