<div class="modal small fade" id="modal-small" tabindex="-1" role="dialog" aria-hidden="false" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal Title</h4>
            </div>
            <div class="modal-body">Modal body</div>
            <div class="modal-footer">
                <button type="button" class="btn default cancel" data-dismiss="modal">
                    <span class="md-click-circle md-click-animate" style="height: 65px; width: 65px; top: -15.5px; left: -6.48438px;"></span>
                    Close
                </button>
                <button type="button" class="btn blue confirm">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>