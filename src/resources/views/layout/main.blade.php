@extends('accounting::base')

@section('styles')
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    @yield('page_styles')

    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'material design' style just load 'components-md.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="{{ asset('teleglobal/accounting/css/components-md.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/css/plugins-md.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/css/blue.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <style>
        .page-sidebar .page-sidebar-menu > li > a {
            /*border-top: 0;*/
        }
        .modal.small {
            background-color: transparent;
            border: none;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .modal.small.fade.in {
            top: 15%;
        }
        .portlet.light {
            padding: 12px 20px 15px 20px !important;
        }
    </style>
@endsection

@section('layout')

    @include('accounting::layout.partial.header')

    <div class="clearfix"></div>

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        @yield('content')
    </div>
    <!-- END CONTAINER -->

    @include('accounting::layout.partial.footer')

@endsection

@section('scripts')
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="{{ asset('teleglobal/accounting/plugins/respond.min.js') }}"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/excanvas.min.js') }}"></script>
    <![endif]-->
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <script src="{{ asset('teleglobal/accounting/js/accounting.js') }}" type="text/javascript"></script>

    <script>

    </script>

    @yield('page_scripts')

    <!-- END JAVASCRIPTS -->
@endsection
