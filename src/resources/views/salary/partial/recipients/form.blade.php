<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:salary.recipient.create' === $data['route'])
                @lang('accounting::salary.pages.create.recipients.title')
            @elseif('accounting:salary.recipient.edit' === $data['route'])
                @lang('accounting::salary.pages.edit.recipients.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:salary.recipient.create' === $data['route']) ? 'create' : 'update';
        @endphp

    <!-- BEGIN FORM-->
        <form id="user-form"
              action="{{ ($scenario == 'create') ? route('accounting:salary.recipient.save') : route('accounting:salary.recipient.update', ['id' => $recipient->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::salary.pages.list.recipients.title_name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::salary.placeholders.enter_name')" class="form-control"
                               value="{{ $recipient->name ?? '' }}"/>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::salary.pages.list.recipients.title_comment')
                    </label>
                    <div class="col-md-6">
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::salary.placeholders.enter_comment')"
                                  class="form-control">{{ $recipient->comment ?? '' }}</textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group last">
                    <label class="control-label col-md-3">
                        @lang('accounting::salary.pages.list.recipients.title_amount')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2">
                        <input name="amount" id="amount" type="number" min="0" step=".01"
                               placeholder="@lang('accounting::page.users.placeholder_5')" class="form-control"
                               value="{{ isset($recipient) ? (number_format((float)$recipient->amount, 2, '.', '')) : '' }}"/>
                        {{--<span class="help-block">This is inline help </span>--}}
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group currencies">
                    <label class="control-label col-md-3">
                        @lang('accounting::salary.pages.list.recipients.title_currency')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2" style="">
                        <select class="form-control currencies-selector" name="currency" id="salary_currency">
                            @foreach($currencies as $currency)
                                <option @if(isset($recipient) && $currency->id == $recipient->currency_id)selected="selected"@endif
                                value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::products.buttons.save')
                        </button>
                        <a href="{{ route('accounting:salary.recipients.list') }}" type="button" class="btn default">
                            @lang('accounting::products.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->