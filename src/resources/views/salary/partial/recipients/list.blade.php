<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::salary.pages.list.recipients.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:salary.recipient.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::salary.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
            <tr>
                <th>@lang('accounting::salary.pages.list.recipients.title_name')</th>
                <th>@lang('accounting::salary.pages.list.recipients.title_amount')</th>
                <th>@lang('accounting::salary.pages.list.recipients.title_currency')</th>
                <th>@lang('accounting::salary.pages.list.recipients.title_comment')</th>
                <th>@lang('accounting::salary.pages.list.recipients.title_comment')</th>
                <th>@lang('accounting::salary.pages.list.recipients.title_comment')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($recipients as $recipient)
                <tr>
                    <td>{{ $recipient->name }}</td>
                    <td class="center">{{ number_format((float)$recipient->amount, 2, '.', '') }}</td>
                    <td>{{ $recipient->currency()->first()->code }}</td>
                    <td class="center">{{ $recipient->comment ?? '' }}</td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:salary.recipient.edit', ['id' => $recipient->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::salary.buttons.edit')
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs default modal-delete-trigger" data-content="{{ $recipient->name }}"
                           href="{{ route('accounting:salary.recipient.delete', ['id' => $recipient->id]) }}">
                            <i class="fa fa-trash-o font-red"></i>
                            @lang('accounting::salary.buttons.delete')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>
