<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::salary.pages.create.reports.title')
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <!-- BEGIN FORM-->
        <form id="user-form" action="{{ route('accounting:salary.report.save') }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::salary.pages.list.reports.title_warehouse')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3" id="warehouse_selector">
                        <select class="form-control warehouse-selector" name="warehouse" id="salary_warehouse">
                            @foreach($warehouses as $warehouse)
                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::salary.pages.list.reports.title_recipient')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3" id="recipient_selector">
                        <select class="form-control recipient-selector" name="recipient" id="salary_recipient">
                            @foreach($recipients as $recipient)
                                <option value="{{ $recipient->id }}">{{ $recipient->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group last">
                    <label class="control-label col-md-3">
                        @lang('accounting::salary.pages.list.reports.title_from')
                    </label>
                    <div class="col-md-2" style="" id="from_date">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control" name="from_date"
                                   data-date-format="dd-mm-yyyy" style="background: #fff;">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group currencies">
                    <label class="control-label col-md-3">
                        @lang('accounting::salary.pages.list.reports.title_to')
                    </label>
                    <div class="col-md-2" style="" id="to_date">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control" name="to_date"
                                   data-date-format="dd-mm-yyyy" style="background: #fff;">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::products.buttons.save')
                        </button>
                        <a href="{{ route('accounting:salary.reports.list') }}" type="button" class="btn default">
                            @lang('accounting::products.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->