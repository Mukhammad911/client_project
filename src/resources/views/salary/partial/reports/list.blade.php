<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::salary.pages.list.reports.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:salary.report.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::salary.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
            <tr>
                <th>@lang('accounting::salary.pages.list.reports.title_warehouse')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_recipient')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_from')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_to')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_products')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_per_product')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_amount')</th>
                <th>@lang('accounting::salary.pages.list.reports.title_delete')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reports as $id => $report)
                <tr>
                    <td>{{ $report['warehouse']['name'] }}</td>
                    <td class="center">{{ $report['recipient'] }}</td>
                    <td>{{ $report['warehouse']['from'] ?? '-' }}</td>
                    <td class="center">{{ $report['warehouse']['to'] ?? '-' }}</td>
                    <td>
                        @foreach($report['products'] as $product)
                            <div>{{ $product['name'] }} ({{ $product['quantity'] }} шт.) </div>
                        @endforeach
                    </td>
                    <td>
                        @foreach($report['products'] as $product)
                            <div>{{ $product['bonus'] }} {{ $report['currency'] }}</div>
                        @endforeach
                    </td>
                    <td>
                        {{ $report['total'] }} {{ $report['currency'] }}
                    </td>
                    <td>
                        <a class="default btn btn-xs default modal-delete-trigger" data-content=""
                           href="{{ route('accounting:salary.report.delete', ['id' => $id]) }}">
                            <i class="fa fa-trash-o font-red"></i>
                            @lang('accounting::salary.buttons.delete')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>