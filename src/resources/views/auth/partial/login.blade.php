<form class="login-form" action="{{ route('accounting:auth.post.login') }}" method="POST">

    {{csrf_field()}}
    <div class="form-title">
        <span class="form-title">@lang('accounting::page.auth.intro_1').</span>
        <span class="form-subtitle">@lang('accounting::page.auth.intro_2').</span>
    </div>
    @if($errors->count() > 0)

        @foreach($errors->default->toArray() as $error)

            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>
                    @if(strpos($error[0], ' b ') > 0 )
                        {{ str_replace(' b ', ' name ', $error[0])}}
                    @elseif(strpos($error[0], ' d ') > 0)
                        {{ str_replace(' d ', ' password ', $error[0])}}
                    @else
                        {{$error[0]}}
                    @endif
                </span>
            </div>

        @endforeach

    @endif
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">@lang('accounting::page.auth.label_1')</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="@lang('accounting::page.auth.label_1')" name="b"
{{--               value="romcrazy13@gmail.com"--}}
        />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">@lang('accounting::page.auth.label_2')</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="@lang('accounting::page.auth.label_2')" name="d"
{{--               value="crazy13"--}}
        />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">@lang('accounting::page.auth.label_3')</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
               placeholder="@lang('accounting::page.auth.label_3')" name="key"
{{--               value="VTlkc1RlejJETnUvL0xwdnBpeVNCWjkrVjB4eFBLOHBNUjhoU0R4eU9Fbw=="--}}
        />
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-primary btn-block uppercase">@lang('accounting::page.auth.button_1')</button>
    </div>
    <div class="form-actions">
        <div class="pull-left">
            <label class="rememberme check">
                <input type="checkbox" name="remember" value="1"/>@lang('accounting::page.auth.checkbox_1') </label>
        </div>
        {{--<div class="pull-right forget-password-block">
            <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
        </div>--}}
    </div>
    {{--<div class="login-options">
        <h4 class="pull-left">Or login with</h4>
        <ul class="social-icons pull-right">
            <li>
                <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
            </li>
            <li>
                <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
            </li>
            <li>
                <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
            </li>
            <li>
                <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
            </li>
        </ul>
    </div>
    <div class="create-account">
        <p>
            <a href="javascript:;" id="register-btn">Create an account</a>
        </p>
    </div>--}}
</form>
