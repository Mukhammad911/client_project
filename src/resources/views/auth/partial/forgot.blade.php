<form class="forget-form" action="index.html" method="POST">

    @csrf

    <div class="form-title">
        <span class="form-title">Forget Password ?</span>
        <span class="form-subtitle">Enter your e-mail to reset it.</span>
    </div>
    <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn btn-default">Back</button>
        <button type="submit" class="btn btn-primary uppercase pull-right">Submit</button>
    </div>
</form>