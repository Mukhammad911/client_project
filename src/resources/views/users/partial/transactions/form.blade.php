<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::page.users.title_5')
        </div>
        <div class="actions">
            {{--<a href="{{ route('accounting:users.transactions.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::page.users.button_1')
            </a>--}}

            {{--<div class="btn-group">
                <a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-user"></i> User <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-pencil"></i> Edit </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-trash-o"></i> Delete </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-ban"></i> Ban </a>
                    </li>
                    <li class="divider">
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="i"></i> Make admin </a>
                    </li>
                </ul>
            </div>--}}

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        {{--<div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button id="sample_editable_1_new" class="btn green">
                            @lang('accounting::page.users.button_1') <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    Print </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    Save as PDF </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    Export to Excel </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>--}}

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            /*dd(
                $data['users']
            );*/
        @endphp

        <!-- BEGIN FORM-->
        <form id="user-form" action="{{ route('accounting:users.transaction.save') }}"
              class="form-horizontal {{--form-bordered form-row-stripped--}}" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::page.users.th_16')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select id="from_user" class="form-control user-selector" name="from_user">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }} ({{ $user->email }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::page.users.th_18')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select id="from_user_cashbox" class="form-control cashbox-selector" name="from_cashbox"
                                disabled="disabled"  data-url="{{ route('accounting:cashboxes.find') }}">
                            <option value="">@lang('accounting::page.users.placeholder_7')</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::page.users.th_17')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select id="to_user" class="form-control user-selector" name="to_user">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }} ({{ $user->email }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::page.users.th_19')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select id="to_user_cashbox" class="form-control cashbox-selector" name="to_cashbox"
                                disabled="disabled" data-url="{{ route('accounting:cashboxes.find') }}">
                            <option value="">@lang('accounting::page.users.placeholder_7')</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group last">
                    <label class="control-label col-md-3">
                        @lang('accounting::page.users.th_20')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2">
                        <input name="amount" id="amount" type="number" min="0" step="0.01"
                               placeholder="@lang('accounting::page.users.placeholder_5')" class="form-control"
                               value=""/>
                        {{--<span class="help-block">This is inline help </span>--}}
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::page.users.button_6')
                        </button>
                        <a href="{{ route('accounting:users.transactions') }}" type="button" class="btn default">
                            @lang('accounting::page.users.button_5')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->

    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->