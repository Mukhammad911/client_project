<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::page.users.title_4')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:users.transaction.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::page.users.button_1')
            </a>

            {{--<div class="btn-group">
                <a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-user"></i> User <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-pencil"></i> Edit </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-trash-o"></i> Delete </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="fa fa-ban"></i> Ban </a>
                    </li>
                    <li class="divider">
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="i"></i> Make admin </a>
                    </li>
                </ul>
            </div>--}}

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        {{--<div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button id="sample_editable_1_new" class="btn green">
                            @lang('accounting::page.users.button_1') <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    Print </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    Save as PDF </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    Export to Excel </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>--}}
        <table class="table {{--table-striped--}} table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
                <th>@lang('accounting::page.users.th_20')</th>
                <th>@lang('accounting::page.users.th_21')</th>
                <th>@lang('accounting::page.users.th_22')</th>
                <th></th>
                <th>@lang('accounting::page.users.th_23')</th>
                <th>@lang('accounting::page.users.th_24')</th>
                <th>@lang('accounting::page.users.th_25')</th>
            </thead>
            <tbody>
                @if(isset($data['transactions']) && !empty($data['transactions']))
                    @foreach($data['transactions'] as $transaction)
                        @php
                            $fromCashbox = $transaction->fromCashboxes()->first();
                            $fromUser = $fromCashbox->users()->first();
                            $toCashbox = $transaction->toCashboxes()->first();
                            $toUser = $toCashbox->users()->first();
                        @endphp
                        <tr>
                            <td><b>{{ $transaction->amount }}</b> [{{ $fromCashbox->currency }}]</td>
                            <td>{{ $fromUser->a }} [{{ $fromUser->b }}]</td>
                            <td>{{ $fromCashbox->alias }}&nbsp;&nbsp;&nbsp;<b>{{ $fromCashbox->balance }}</b> [{{ $fromCashbox->currency }}]</td>
                            <td class="text-center"><i class="fa fa-arrow-right font-blue-madison"></i></td>
                            <td>{{ $toUser->a }} [{{ $fromUser->b }}]</td>
                            <td>{{ $toCashbox->alias }}&nbsp;&nbsp;&nbsp;<b>{{ $toCashbox->balance }}</b> [{{ $toCashbox->currency }}]</td>
                            <td>{{ $transaction->created_at }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->

<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>