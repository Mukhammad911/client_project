<!-- BEGIN EXAMPLE TABLE PORTLET-->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>
<div class="portlet light form-fit">

    @php
        $dateTimeFormat = config('accounting.app.datetime_format');
    @endphp

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::users.page.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:user.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::users.button.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1" data-content="{{ json_encode($fields) }}" data-date-format="{{ config('accounting.app.datetime_format_js') }}">
            <thead>
                <tr>
                    <th>@lang('accounting::users.page.list.title_name')</th>
                    <th>@lang('accounting::users.page.list.title_email')</th>
{{--                    <th>@lang('accounting::users.page.list.title_type')</th>--}}
                    <th>@lang('accounting::users.page.list.title_cashboxes')</th>
                    <th>@lang('accounting::users.page.list.title_comment')</th>
                    <th>@lang('accounting::users.page.list.title_created_by')</th>
                    <th>@lang('accounting::users.page.list.title_history')</th>
                    <th>@lang('accounting::users.page.list.title_last_visit')</th>
                    <th>@lang('accounting::users.page.list.title_edit')</th>
                    <th>@lang('accounting::users.page.list.title_delete')</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    {{--    NAME    --}}
                    <td>{{ $user->name }}</td>
                    {{--    EMAIL    --}}
                    <td>{{ $user->email }}</td>
                    {{--    TYPE    --}}
{{--                    <td class="center">{{ strtoupper($user->type ?? '') }}</td>--}}
                    {{--    CASHBOXES    --}}
                    <td class="center">{{ $user->cashboxes->count() }}</td>
                    {{--    COMMENT    --}}
                    <td class="center">{{ $user->comment ?? '' }}</td>
                    {{--    CREATED BY    --}}
                    <td>{{ $user->history->last()->user->name ?? '' }}</td>
                    {{--    EDITS HISTORY    --}}
                    <td>
                        @if($user->history->count() > 0)
                            <a class="modal-history-trigger font-blue"
                               data-content="{{ json_encode($history[$user->id]) }}"
                               data-title="@lang('accounting::users.page.list.fields_of_history.title')">
                                {{ $user->history->count() }}
                            </a>
                        @else
                            <span>{{ $user->history->count() }}</span>
                        @endif
                    </td>
                    {{--    LAST VISIT    --}}
                    @php
                        $date = ($user->visits->count() > 0) ? $user->visits->first()->time : '';
                        $date = ($date !== '') ? (new \Illuminate\Support\Carbon($date))->format($dateTimeFormat) : $date;
                    @endphp
                    <td>{{ $date }}</td>
                    {{--    ACTION EDIT    --}}
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:user.edit', ['id' => $user->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::users.button.edit')
                        </a>
                    </td>
                    {{--    ACTION DELETE    --}}
                    <td>
                        <a class="default btn btn-xs default modal-delete-trigger" data-content="{{ $user->name }}"
                           href="{{ route('accounting:user.delete', ['id' => $user->id]) }}">
                            <i class="fa fa-trash-o font-red"></i>
                            @lang('accounting::users.button.delete')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>

<!-- END EXAMPLE TABLE PORTLET-->