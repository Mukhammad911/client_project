<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light {{--bordered--}} form-fit">

    @php
        $dateFormat = config('accounting.app.date_format');
        $dateTimeFormat = config('accounting.app.datetime_format');

        $scenario = '';
        $action = '#';
        if($data['route'] === 'accounting:user.create'){
            $scenario = 'create';
            $action = route('accounting:user.save');
        }elseif($data['route'] === 'accounting:user.edit'){
            $scenario = 'edit';
            $action = route('accounting:user.update', ['id' => $user->id]);
            $currentUserGroups = [];
            foreach ($user->groups as $group){
                $currentUserGroups[] = $group->id;
            }
        }
    @endphp

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if($scenario === 'create')
                @lang('accounting::users.page.create.title')
            @elseif($scenario === 'edit')
                @lang('accounting::users.page.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <!-- BEGIN FORM-->
        <form id="user-form" action="{{ $action }}" class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                {{--    NAME    --}}
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::users.page.form.name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::users.page.form.name')" class="form-control"
                               value="{{ ($scenario === 'edit') ? $user->name : '' }}"/>
                    </div>
                </div>
                {{--    EMAIL    --}}
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::users.page.form.email')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="email" id="email-form-control" type="email"
                               placeholder="@lang('accounting::users.page.form.email')" class="form-control"
                               value="{{ ($scenario === 'edit') ? $user->email : '' }}"
                        />
                    </div>
                </div>
                {{--    PASSWORD    --}}
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::users.page.form.password')
                        @if('accounting:user.create' === $data['route'])
                            <span class="required" aria-required="true"> * </span>
                        @endif
                    </label>
                    <div class="col-md-6">
                        <input name="password" id="password-form-control" type="password" class="form-control"
                               placeholder="@lang('accounting::users.page.form.password')"/>
                    </div>
                </div>
                {{--    CONFIRM PASSWORD    --}}
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::users.page.form.confirm')
                        @if('accounting:users.create' === $data['route'])
                            <span class="required" aria-required="true"> * </span>
                        @endif
                    </label>
                    <div class="col-md-6">
                        <input name="password_confirmation" id="password_confirmation-form-control" type="password"
                               class="form-control" placeholder="@lang('accounting::users.page.form.confirm')"/>
                    </div>
                </div>
                {{--    TYPE    --}}
                {{--                <div class="form-group">--}}
                {{--                    <label class="control-label col-md-3">--}}
                {{--                        @lang('accounting::users.page.form.type')--}}
                {{--                    </label>--}}
                {{--                    <div class="col-md-2">--}}
                {{--                        <select class="form-control type-selector" name="type">--}}
                {{--                            <option value="a"{{ (($scenario === 'create') || ($user->type === 'a')) ? ' select' : '' }}>A</option>--}}
                {{--                            <option value="ga"{{ (($scenario === 'edit') && ($user->type === 'ga')) ? ' select' : '' }}>GA</option>--}}
                {{--                        </select>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--    GROUPS    --}}
                {{--                <div class="form-group user_groups">--}}
                {{--                    <label class="control-label col-md-3">--}}
                {{--                        @lang('accounting::users.page.form.groups')--}}
                {{--                        <span class="required" aria-required="true"> * </span>--}}
                {{--                    </label>--}}
                {{--                    <div class="col-md-3" style="">--}}
                {{--                        <input type="hidden" id="user_groups" name="user_groups"--}}
                {{--                                {{ ($scenario === 'edit') ? ' value=' . json_encode((object)$oldGroupIdsArray) : '' }}>--}}
                {{--                        <select class="form-control user_groups-selector" id="user_groups" multiple="multiple">--}}
                {{--                            @foreach($userGroups as $userGroup)--}}
                {{--                                <option value="{{ $userGroup->id }}"--}}
                {{--                                        {{ (($scenario === 'edit') && in_array($userGroup->id, $oldGroupIdsArray)) ? ' selected="selected"' : '' }}>--}}
                {{--                                    {{ $userGroup->name }}--}}
                {{--                                </option>--}}
                {{--                            @endforeach--}}
                {{--                        </select>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--    COMMENT    --}}
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::users.page.form.comment')
                    </label>
                    <div class="col-md-6">
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::users.page.form.comment')"
                                  class="form-control">{{ ($scenario === 'edit') ? $user->comment : '' }}</textarea>
                    </div>
                </div>
                {{--    CASHBOXES    --}}
                <div class="form-group last">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::page.users.th_14')
                    </label>
                    <div class="col-md-8">

                        <div id="cashboxes">

                            <script type="text/template" id="cashbox-template">
                                <div class="cashbox hidden" id="">
                                    {{--    CASHBOX_ID    --}}
                                    <div class="col-md-3" style="padding-left: 0;" hidden>
                                        <input type="text" class="form-control" name="new_cashboxes[id][]" value="new">
                                    </div>
                                    {{--    CASHBOX_BALANCE    --}}
                                    <div class="col-md-2" style="padding-left: 0;">
                                        <input type="number" step="any" class="form-control"
                                               name="new_cashboxes[balance][]"
                                               placeholder="@lang('accounting::page.users.placeholder_4')" value="0"
                                               min="0">
                                    </div>
                                    {{--    CASHBOX_CURRENCY_ID    --}}
                                    <div class="col-md-3" style="padding-left: 0;">
                                        <select class="form-control currencies-selector"
                                                name="new_cashboxes[currency_id][]">
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}">{{ $currency->code }}
                                                    - {{ $currency->country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{--    CASHBOX_DATE    --}}
                                    <div class="col-md-3" style="padding-left: 0;">
                                        <div class="input-group date form_datetime">
                                            <input type="text" size="16" class="form-control datetime"
                                                   name="new_cashboxes[datetime][]"
                                                   style="background: #fff; cursor: text" readonly
                                                   data-date-format="{{ config('accounting.app.date_format_js') }}">
                                            <span class="input-group-btn">
                                                <button class="btn default date-set" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    {{--    CASHBOX_ALIAS    --}}
                                    <div class="col-md-3" style="padding-left: 0;">
                                        <div class="input-group alias">
                                            <input type="text" class="form-control" name="new_cashboxes[alias][]"
                                                   style="background: #fff;" value=""
                                                   placeholder="@lang('accounting::page.users.placeholder_8')">
                                        </div>
                                    </div>
                                    {{--    CASHBOX - ACTION_REMOVE    --}}
                                    <div>
                                        <button class="btn remove-cashbox">
                                            <i class="fa fa-minus font-red"></i>
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </script>
                            @php
                                $cashboxes = (($scenario === 'edit') && ($user->cashboxes->count() > 0)) ? $user->cashboxes : null;
                            @endphp
                            @if( $cashboxes )
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($cashboxes as $key => $cashbox)
                                    @php
                                        $startBalance = $cashbox->incomingStartBalance->first();

                                    @endphp
                                    @if(!is_null($startBalance))
                                        <div class="cashbox @if(0 < $key) margin-top-10 @endif"
                                             id="cashbox_{{$cashbox->id}}">
                                            <div class="col-md-3" style="padding-left: 0;" hidden>
                                                <input type="text" class="form-control"
                                                       name="old_cashboxes[id][{{ $i }}]"
                                                       value="{{$cashbox->id ?? ''}}">
                                            </div>
                                            <div class="col-md-2" style="padding-left: 0;">
                                                <input type="number" step="any" class="form-control"
                                                       name="old_cashboxes[balance][{{ $i }}]"
                                                       placeholder="@lang('accounting::page.users.placeholder_4')"
                                                       value="{{$startBalance->amount}}">
                                            </div>
                                            <div class="col-md-3" style="padding-left: 0;" disabled>
                                                <input class="form-control" name="old_cashboxes[currency_id][{{ $i }}]"
                                                       value="{{ $cashbox->currency->code . ' - ' . $cashbox->currency->country }}"
                                                       disabled="disabled">
                                            </div>
                                            <div class="col-md-3" style="padding-left: 0;">
                                                <div class="input-group date form_datetime"
                                                     data-date-value="{{ $startBalance->date }}">
                                                    <input type="text" size="16" class="form-control datetime"
                                                           name="old_cashboxes[datetime][{{ $i }}]"
                                                           value="{{ (new \Carbon\Carbon($startBalance->date))->format($dateFormat) }}"
                                                           style="background: #fff; cursor: text" readonly
                                                           data-date-format="{{ config('accounting.app.date_format_js') }}">
                                                    <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="padding-left: 0;">
                                                <div class="input-group alias">
                                                    <input type="text" class="form-control"
                                                           name="old_cashboxes[alias][{{ $i }}]"
                                                           style="background: #fff;" value="{{ $cashbox->alias }}"
                                                           placeholder="@lang('accounting::page.users.placeholder_8')">
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn remove-cashbox">
                                                    <i class="fa fa-minus font-red"></i>
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <script>
                                            if (typeof newCashboxesID === 'undefined') {
                                                var newCashboxesID = [];
                                            }
                                            Array.prototype.push.apply(
                                                newCashboxesID,
                                                ["#cashbox_{{$cashbox->id}}"]
                                            );
                                        </script>
                                        @php
                                            $i++;
                                        @endphp
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div>
                            <button class="btn add-cashbox margin-top-10">
                                <i class="fa fa-plus font-blue"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::page.users.button_4')
                        </button>
                        <a href="{{ route('accounting:users') }}" type="button" class="btn default">
                            @lang('accounting::page.users.button_5')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->