@extends('accounting::layout.main')

@section('title', __('accounting::page.title.page_26') .' - '. config('accounting.app.name'))

@section('page_styles')
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/iptvadmin/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css"/>
    {{--<link href="{{ asset('teleglobal/iptvadmin/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/iptvadmin/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css"/>--}}
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN PAGE STYLES -->
    <link href="{{ asset('teleglobal/accounting/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE STYLES -->
@endsection

@section('content')

    @include('accounting::layout.partial.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">

                @include('accounting::layout.partial.breadcrumb')

                @include('accounting::layout.partial.toolbar')

            </div>

        {{--@include('accounting::layout.partial.pagetitle')--}}

        <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    @include('accounting::users.partial.transactions.list')
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

    @if(config('accounting.quicksidebar.enabled'))
        @include('accounting::layout.partial.quicksidebar')
    @endif

    <!-- BEGIN MODAL -->
    @include('accounting::layout.partial.modalsmall')
    <!-- END MODAL -->

@endsection

@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {{--<script src="{{ asset('teleglobal/accounting/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/morris/morris.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/morris/raphael-min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript" ></script>--}}
    {{--<script src="{{ asset('teleglobal/accounting/plugins/flot/jquery.flot.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.pulsate.min.js') }}" type="text/javascript" ></script>--}}
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.sparkline.min.js') }}" type="text/javascript" ></script>

    <script src="{{ asset('teleglobal/accounting/plugins/select2/select2.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
    {{--<script src="{{ asset('teleglobal/accounting/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript" ></script>--}}
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('teleglobal/accounting/js/metronic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/layout.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('teleglobal/accounting/js/index.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/tasks.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('teleglobal/accounting/js/table-editable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/ui-extended-modals.js') }}" type="text/javascript"></script>--}}

    @if(config('accounting.quicksidebar.enabled'))
        <script src="{{ asset('teleglobal/accounting/js/quick-sidebar.js') }}" type="text/javascript"></script>
    @endif
    <!-- END PAGE LEVEL SCRIPTS -->

    <script src="{{ asset('teleglobal/accounting/js/page/user.js') }}" type="text/javascript"></script>
@endsection
