<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<div class="portlet light form-fit">

    @php
        $dateTimeFormat = config('accounting.app.datetime_format');
    @endphp

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::cashboxes_incoming.page_title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:cashboxes.incoming.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::cashboxes_incoming.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
            <tr>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_id')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_user')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_cashbox')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_account')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_description')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_client')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_amount')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_currency')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_date')</th>
                <th>@lang('accounting::cashboxes_incoming.page.list.title_comment')</th>
            </tr>
            </thead>
            <tbody>


         @foreach($incomings as $incoming)
             @php
                $tmp = json_decode($incoming->comment);
             @endphp
                <tr>
                    <td>{{ $incoming->id }}</td>
                    <td>{{ $incoming->document->docItemsUser->user->name ?? '' }}</td>
                    <td>{{ $incoming->cashbox->alias }}</td>
                    <td>{{ $incoming->document->docItemsAccount->account->name ?? '' }}</td>
                    <td>{{ $tmp->description ?? '' }}</td>
                    <td>{{ $incoming->document->docItemsClient->client->name ?? '' }}</td>
                    <td>{{ $incoming->amount }}</td>
                    <td>{{ $incoming->document->docItemsReportCurrency->reportCurrency->name ?? '' }}</td>
                    <td>{{ (new \Carbon\Carbon($incoming->date))->format($dateTimeFormat) }}</td>
                    <td>{{ $tmp->comment ?? '' }}</td>
{{--                    <td>--}}
{{--                        <a class="default btn btn-xs btn-default"--}}
{{--                           href="{{ route("accounting:cashboxes.transaction.edit", ['id' => $incoming->id]) }}">--}}
{{--                            <i class="fa fa-pencil font-blue-madison"> </i>--}}
{{--                            @lang('accounting::cashboxes_incoming.buttons.edit')--}}
{{--                        </a>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        <a class="default btn btn-xs btn-default" href="#">--}}
{{--                            <i class="fa fa-pencil font-blue-madison"> </i>--}}
{{--                            @lang('accounting::cashboxes_incoming.buttons.validate')--}}
{{--                        </a>--}}
{{--                    </td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>
