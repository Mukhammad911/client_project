<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::users.page.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:user.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::users.button.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1" data-content="{{ json_encode($fields) }}">
            <thead>
                <tr>
                    <th>@lang('accounting::cashboxes.page.list.title_owner')</th>
                    <th>@lang('accounting::cashboxes.page.list.title_alias')</th>
                    <th>@lang('accounting::cashboxes.page.list.title_currency')</th>
                    <th>@lang('accounting::cashboxes.page.list.title_incoming')</th>
                    <th>@lang('accounting::cashboxes.page.list.title_outcoming')</th>
                    <th>@lang('accounting::cashboxes.page.list.title_balance')</th>
                </tr>
            </thead>
            <tbody>
            @foreach($cashboxes as $cashbox)
                <tr>
                    {{--    owner    --}}
                    <td>{{ $cashbox->user->name ?? '' }}</td>
                    {{--    alias    --}}
                    <td>{{ $cashbox->alias }}</td>
                    {{--    currency    --}}
                    <td>{{ $cashbox->currency->code . ' - ' . $cashbox->currency->country }}</td>
                    {{--    incoming    --}}
                    <td>{{ $incoming = $cashbox->incoming->sum('e') }}</td>
                    {{--    outcoming    --}}
                    <td>{{ $outcoming = $cashbox->outcoming->sum('e') }}</td>
                    {{--    balance    --}}
                    <td>{{ $incoming - $outcoming }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <table class="table table-hover table-bordered" id="sample_editable_1" data-content="{{ json_encode($fields) }}">
            <thead>
            <tr>
                <th>@lang('accounting::cashboxes.page.list.title_currency')</th>
                <th>@lang('accounting::cashboxes.page.list.title_incoming')</th>
                <th>@lang('accounting::cashboxes.page.list.title_outcoming')</th>
                <th>@lang('accounting::cashboxes.page.list.title_balance')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($total as $item)
                <tr>
                    <td>{{ $item['name'] }}</td>
                    <td>{{ $item['incoming'] }}</td>
                    <td>{{ $item['outcoming'] }}</td>
                    <td>{{ $item['incoming'] - $item['outcoming'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>