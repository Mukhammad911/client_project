<div class="portlet light form-fit">

    @php
        $scenario = ('accounting:cashboxes.incoming.create' === $data['route']) ? 'create' : 'update';
        $dateFormat = config('accounting.app.date_format');
    @endphp

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if($scenario === 'create')
                @lang('accounting::cashboxes_incoming.page.create.title')
            @elseif($scenario === 'update')
                @lang('accounting::cashboxes_incoming.page.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- BEGIN FORM-->
        <form id="cashbox_incoming-form"
              action="{{ ($scenario == 'create') ? route('accounting:cashboxes.incoming.save') : route('accounting:cashboxes.incoming.update', ['id' => $doc->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group user">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_user')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control" name="user_id">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}"{{
                                (($scenario === 'update') && ($user->id == $doc->docItemsUser->user->user_id))
                                ? ' selected'
                                : (($scenario === 'create') && ($user->id == $authUserId)) ? ' selected' : ''
                                }}>{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group cashbox">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_cashbox')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">  <select class="form-control" name="cashbox_id" data-cashboxes="{{ json_encode($cashboxes) }}">
                        @if($scenario === 'update')
                            @foreach($cashboxes[$doc->docItemsUser->user->user_id] as $cashbox)
                                <option value="{{ $cashbox['id'] }}"{{ (($scenario === 'update') && ($cashbox['id'] == $doc->docItemsCashbox->cashbox_id)) ? ' selected="selected"' : '' }}>{{ $cashbox['alias'] }}</option>
                            @endforeach
                        @endif
                    </select>
                    </div>
                </div>
                <div class="form-group account">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_account')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control" name="account_id">
                            @foreach($accounts as $account)
                                <option value="{{ $account->id }}"{{ (($scenario === 'update') && ($account->id == $transaction->account_id)) ? ' selected="selected"' : '' }}>{{ $account->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_description')
                    </label>
                    <div class="col-md-6">
                        <textarea class="form-control" name="description">{{ (($scenario === 'update') && !empty($transaction->description)) ? $transaction->description : '' }}</textarea>
                    </div>
                </div>
                <div class="form-group client">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_client')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control" name="client_id">
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}"{{ (($scenario === 'update') && ($client->id == $doc->docItemsClient->client->client_id)) ? ' selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_amount')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <input type="number" class="form-control" name="amount" min="0" step="0.01" value="{{ ($scenario === 'update') ? $transaction->amount : 0 }}">
                    </div>
                </div>
                <div class="form-group currency">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_currency')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control" name="currency_id">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}"{{ (($scenario === 'update') && ($currency->id == $transaction->currency_id)) ? ' selected="selected"' : '' }}>{{ $currency->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::cashboxes_incoming.page.form.title_date')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2" style="" id="date">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control date" name="date"
                                   value="{{ ($scenario === 'update') ? (new \Carbon\Carbon($transaction->date))->format($dateFormat) : '' }}"
                                   style="background: #fff; cursor: text" readonly data-date-format="{{ config('accounting.app.date_format_js') }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::cashboxes_incoming.page.form.title_comment')
                    </label>
                    <div class="col-md-6">
                        <textarea class="form-control" name="comment">{{ (($scenario === 'update') && !empty($transaction->comment)) ? $transaction->comment : '' }}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::orders.buttons.save')
                        </button>
                        <a href="{{ route('accounting:cashboxes.incoming') }}" type="button" class="btn default">
                            @lang('accounting::orders.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>