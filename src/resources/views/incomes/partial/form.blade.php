<style>
    .datetimepicker.dropdown-menu{
        cursor: default;
    }
    .datetimepicker.dropdown-menu .datetimepicker-days td{
        cursor: pointer;
    }
</style>

<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:income.create' === $data['route'])
                @lang('accounting::incomes.pages.create.title')
            @elseif('accounting:income.edit' === $data['route'])
                @lang('accounting::incomes.pages.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:income.create' === $data['route']) ? 'create' : 'update';
        @endphp

        <!-- BEGIN FORM-->
        <form id="products-incomes-form" onkeydown="return event.key != 'Enter';"
              action="{{ route('accounting:income.save') }}"
              class="form-horizontal" autocomplete="off" method="POST"
              scenario="{{ $scenario }}">

            @csrf

            <div class="form-body">
                <div class="form-group warehouses">
                    <label class="control-label col-md-3">
                        @lang('accounting::incomes.pages.list.title_warehouse')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select name="warehouse" id="warehouse" class="form-control warehouses-selector">
                            @foreach($warehouses as $warehouse)
                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::incomes.pages.list.title_date')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2" id="date">
                        <div class="input-group">
                            <input type="text" size="16" class="form-control datetime" name="income_date"
                                   style="background: #fff; cursor: text" readonly data-date-format="{{ config('accounting.app.date_format_js') }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group payment-children hidden">
                    <label class="control-label col-md-3">
                        @lang('accounting::incomes.pages.list.title_payment_amount')
                    </label>
                    <div class="col-md-2">
                        <input type="number" name="payment_amount"  min="0" step="0.01"
                               placeholder="@lang('accounting::incomes.placeholders.enter_amount')" class="form-control">
                    </div>
                </div>
                <div class="form-group payment-children hidden">
                    <label class="control-label col-md-3">
                        @lang('accounting::incomes.pages.list.title_payment_date')
                    </label>
                    <div class="col-md-2">
                        <div class="input-group">
                            <input type="text" size="16" class="form-control datetime" name="payment_date"
                                   style="background: #fff; cursor: text" readonly data-date-format="{{ config('accounting.app.date_format_js') }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group currencies payment-children hidden">
                    <label class="control-label col-md-3">
                        @lang('accounting::incomes.pages.list.title_payment_currency')
                    </label>
                    <div class="col-md-2" style="">
                        <select class="form-control currencies-selector" name="payment_currency_id" id="payment_currency">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::incomes.pages.list.title_products')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-8">

                        <div id="products">

                            {{-- Products template --}}
                            <script type="text/template" id="product-template">
                                <div class="product hidden" id="">
                                    {{-- Product --}}
                                    <div class="col-md-3" style="padding-left: 0;">
                                        <select class="form-control products-selector product_id" name="products[id][]">
                                            @foreach($products as $product)
                                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- Quantity --}}
                                    <div class="col-md-2" style="padding-left: 0;">
                                        <input type="number" min="1" step="1" class="form-control product_quantity" name="products[quantity][]"
                                               placeholder="@lang('accounting::incomes.placeholders.quantity')">
                                    </div>
                                    <div>
                                        <button class="btn remove-product">
                                            <i class="fa fa-minus font-red"></i>
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </script>

                        </div>
                        <div>
                            <button class="btn add-product margin-top-10">
                                <i class="fa fa-plus font-blue"></i>
                            </button>
                        </div>

                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::incomes.buttons.save')
                        </button>
                        <a href="{{ route('accounting:incomes.list') }}" type="button" class="btn default">
                            @lang('accounting::incomes.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->