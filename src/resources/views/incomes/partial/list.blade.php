<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@php
    $dateFormat = config('accounting.app.date_format');
@endphp

<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::incomes.pages.list.title')
        </div>
        <div class="actions">
            {{-- Daterange filter --}}
            <a href="#" class="btn">
                <div class="input-group date-range-group" title="@lang('accounting::incomes.buttons.filter')">
                    <input type="text" id="date-range-filter" class="form-control" style="background: #fff; width: 200px;"
                           date-format="{{ strtoupper(config('accounting.app.date_format_js')) }}">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn default">
                            <i class="fa fa-calendar"></i>
                        </button>
                        <button type="button" class="btn btn-default clear-filter" style="padding-top: 0; padding-bottom: 0; display: none;" title="@lang('accounting::incomes.buttons.clear_filter')">
                            <span class="align-middle" aria-hidden="true" style="font-size: 200%">&times;</span>
                        </button>
                    </div>
                </div>
            </a>
            <a href="{{ route('accounting:income.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::incomes.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    {{-- Top pagination container --}}
    @include('accounting::layout.partial.pagination')

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="product-incoming-table">
            <thead>
                <tr>
                    <th id="doc-id" class="text-center">@lang('accounting::incomes.pages.list.title_doc_id')</th>
                    <th id="warehouse" class="text-center">@lang('accounting::incomes.pages.list.title_warehouse')</th>
                    <th id="products" class="text-center">@lang('accounting::incomes.pages.list.title_products')</th>
                    <th id="incoming-date" class="text-center">@lang('accounting::incomes.pages.list.title_date')</th>
                    <th id="author" class="text-center">@lang('accounting::incomes.pages.list.title_author')</th>
                    <th  class="text-center">@lang('accounting::incomes.pages.list.delete')</th>
                </tr>
            </thead>
            <tbody>

                @foreach($docs as $doc)

                    <tr>
                        @if($doc->productsIncoming()->count() > 0)
                        <td class="text-center">{{ $doc->doc_number }}</td>
                        <td>{{ $doc->productsIncoming()->first()->pivot->warehouse->name }}</td>
                        <td>

                            @foreach($doc->productsIncoming as $item)

                                    <div>{{ $item->pivot->product->name }} ({{ $item->amount }} шт.)</div>

                            @endforeach
                        </td>
                        <td class="text-center">{{ (new \Illuminate\Support\Carbon($doc->productsIncoming()->first()->date))->format($dateFormat) }}</td>
                        <td>{{ $doc->history()->first()->user->name }}</td>
                        <td>
                            <a class="default btn btn-xs default modal-delete-trigger"
                               href="{{ route('accounting:income.delete', ['id' => $doc->id]) }}">
                                <i class="fa fa-trash-o font-red"></i>
                                @lang('accounting::warehouses.buttons.delete')
                            </a>
                        </td>
                            {{--<td>
                                <a class="default btn btn-xs default modal-delete-trigger"
                                   href="{{ route('accounting:income.update', ['id' => $doc->id]) }}">
                                    <i class="fa fa-pencil font-blue-madison"></i>
                                    @lang('accounting::warehouses.buttons.delete')
                                </a>
                            </td>--}}
                            @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- Bottom pagination container --}}
    @include('accounting::layout.partial.pagination')

</div>
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>

