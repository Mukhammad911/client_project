<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:order.create' === $data['route'])
                @lang('accounting::orders.pages.create.title')
            @elseif('accounting:order.edit' === $data['route'])
                @lang('accounting::orders.pages.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:order.create' === $data['route']) ? 'create' : 'update';
        @endphp

        <!-- BEGIN FORM-->
        <form id="order-form"
              action="{{ ($scenario == 'create') ? route('accounting:order.save') : route('accounting:order.update', ['id' => $order->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::orders.pages.form.title_account')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <div class="radio-list accounts">
                            <select class="form-control account-selector" name="account_id">
                                @foreach($accounts as $account)
                                    <option value="{{ $account->id }}"{{ (($scenario === 'update') && ($account->id == $order->account_id)) ? ' selected="selected"' : '' }}>{{ $account->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::orders.pages.form.title_position')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3 positions">
                        <select class="form-control position-selector" name="position_id">
                            @foreach($positions as $position)
                                <option data-attr-price="{{ $position->cost }}" id="option-{{ $position->id }}"
                                        value="{{ $position->id }}"{{ (($scenario === 'update') && ($position->id == $order->position_id)) ? ' selected="selected"' : '' }}>
                                    {{ $position->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::orders.pages.form.title_quantity')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2">
                        <input name="quantity" id="quantity" type="number" step="1" min="1"
                               placeholder="@lang('accounting::orders.pages.form.title_quantity')"
                               class="form-control"
                               value="{{ ($scenario === 'update') ? $order->quantity : '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::orders.pages.create.title_amount')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2">
                        <input name="amount" id="amount" type="number" step=".01" min="0.01"
                               placeholder="@lang('accounting::orders.placeholders.enter_amount')"
                               class="form-control"
                               value="{{ ($scenario === 'update') ? $order->amount : '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::orders.pages.create.title_date')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2" style="" id="date">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control form_datetime" name="date_order"
                                   data-date-format="dd-mm-yyyy" style="background: #fff;"
                                   value="{{ ($scenario === 'update') ? $order->date_order : '' }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::orders.pages.create.title_comment')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <textarea name="comment" id="comment"
                                  placeholder="@lang('accounting::orders.pages.create.title_comment')"
                                  class="form-control">{{ $order->comment ?? '' }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::orders.pages.form.title_date_end')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2" style="" id="execution_date">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control form_datetime" name="date_execution"
                                   data-date-format="dd-mm-yyyy" style="background: #fff;"
                                   value="{{ ($scenario === 'update') ? $order->date_execution : '' }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                {{--<div class="associations">--}}
                    {{--<label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::orders.pages.form.title_associations'):
                    </label>--}}
                    {{--<div class="form-group assoc_transport">--}}

                        @php
                            $checked = (isset($order->assoc_transport_id) || isset($order->assoc_transport_amount) || isset($order->assoc_documents_amount)
                            || isset($order->assoc_delivery_amount) || isset($order->assoc_service_amount)) ? 'checked' : '';
                            $hidden = (empty($checked)) ? 'hidden' : '';
                        @endphp

                        <div class="form-group">
                            <label class="control-label col-md-3">
                                @lang('accounting::orders.pages.form.title_associations')
                            </label>
                            <div class="col-md-6 transports">
                                <input type="checkbox" {{ $checked }} id="associations-checkbox" data-attr-child="association">
                            </div>
                        </div>
                        <div class="form-group association {{ $hidden }}">
                            <label class="control-label col-md-3">
                                @lang('accounting::orders.pages.form.title_assoc_transport')
                            </label>
                            <div class="col-md-3 transports">
                                <select class="form-control transport-selector" id="transport" name="assoc_transport_id">
                                    @foreach($transports as $transport)
                                        <option value="{{ $transport->id }}"
                                        @if(($scenario === 'update') && ($order->assoc_transport_id) && ($transport->id == $order->assoc_transport_id))
                                            {{ ' selected="selected"' }}
                                        @endif
                                        >{{ $transport->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group transport_amount association {{ $hidden }}">
                            <label class="control-label col-md-3">
                                @lang('accounting::orders.pages.form.title_assoc_transport_amount')
                            </label>
                            <div class="col-md-3">
                                <input name="assoc_transport_amount" id="transport-amount" type="number" min="0" step="0.01"
                                       placeholder="@lang('accounting::orders.placeholders.enter_amount')"
                                       class="form-control"
                                       value="{{ (($scenario === 'update') && ($order->assoc_transport_amount)) ? $order->assoc_transport_amount : '' }}"/>
                            </div>
                        </div>
                    {{--</div>--}}
                    {{--<div class="form-group association">--}}
                        <div class="form-group delivery_amount association {{ $hidden }}">
                            <label class="control-label col-md-3">
                                @lang('accounting::orders.pages.form.title_assoc_delivery_amount')
                            </label>
                            <div class="col-md-3">
                                <input name="assoc_delivery_amount" id="delivery-amount" type="number" min="0" step="0.01"
                                       placeholder="@lang('accounting::orders.placeholders.enter_amount')"
                                       class="form-control"
                                       value="{{ (($scenario === 'update') && ($order->assoc_delivery_amount)) ? $order->assoc_delivery_amount : '' }}"/>
                            </div>
                        </div>
                        <div class="form-group service_amount association {{ $hidden }}">
                            <label class="control-label col-md-3">
                                @lang('accounting::orders.pages.form.title_assoc_service_amount')
                            </label>
                            <div class="col-md-3">
                                <input name="assoc_service_amount" id="service-amount" type="number" min="0" step="0.01"
                                       placeholder="@lang('accounting::orders.placeholders.enter_amount')"
                                       class="form-control"
                                       value="{{ (($scenario === 'update') && ($order->assoc_service_amount)) ? $order->assoc_service_amount : '' }}"/>
                            </div>
                        </div>
                        <div class="form-group documents_amount association {{ $hidden }}">
                            <label class="control-label col-md-3">
                                @lang('accounting::orders.pages.form.title_assoc_documents_amount')
                            </label>
                            <div class="col-md-3">
                                <input name="assoc_documents_amount" id="documents-amount" type="number" min="0" step="0.01"
                                       placeholder="@lang('accounting::orders.placeholders.enter_amount')"
                                       class="form-control"
                                       value="{{ (($scenario === 'update') && ($order->assoc_documents_amount)) ? $order->assoc_documents_amount : '' }}"/>
                            </div>
                        </div>
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::orders.buttons.save')
                        </button>
                        <a href="{{ route('accounting:orders.index') }}" type="button" class="btn default">
                            @lang('accounting::orders.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>