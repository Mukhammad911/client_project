
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::orders.pages.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:order.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::orders.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
            <tr>
                <th>@lang('accounting::orders.pages.list.title_id')</th>
                <th>@lang('accounting::orders.pages.list.title_account')</th>
                <th>@lang('accounting::orders.pages.list.title_position')</th>
                <th>@lang('accounting::orders.pages.list.title_quantity')</th>
                <th>@lang('accounting::orders.pages.list.title_amount')</th>
                <th>@lang('accounting::orders.pages.list.title_date_order')</th>
                <th>@lang('accounting::orders.pages.list.title_comment')</th>
                <th>@lang('accounting::orders.pages.list.title_date_execution')</th>
{{--                <th>@lang('accounting::orders.pages.list.title_assoc_transport_name')</th>--}}
{{--                <th>@lang('accounting::orders.pages.list.title_assoc_transport_amount')</th>--}}
{{--                <th>@lang('accounting::orders.pages.list.title_assoc_delivery_amount')</th>--}}
{{--                <th>@lang('accounting::orders.pages.list.title_assoc_service_amount')</th>--}}
{{--                <th>@lang('accounting::orders.pages.list.title_assoc_documents_amount')</th>--}}
                <th>@lang('accounting::orders.pages.list.title_association')</th>
                <th>@lang('accounting::orders.pages.list.title_edit_count')</th>
                <th>@lang('accounting::orders.pages.list.title_action')</th>
            </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->account->name }}</td>
                        <td>{{ $order->position->name }}</td>
                        <td>{{ $order->quantity }}</td>
                        <td>{{ $order->amount }}</td>
                        <td>{{ $order->date_order }}</td>
                        <td>{{ $order->comment }}</td>
                        <td>{{ $order->date_execution }}</td>
                        <td>
                            @if(empty($order->transport) && empty($order->assoc_delivery_amount)
                                && empty($order->assoc_service_amount) && empty($order->assoc_documents_amount))
                                Нет
                            @endif
                            @if(!empty($order->transport))
                                @lang('accounting::orders.pages.list.title_assoc_transport_name'): {{ $order->transport->name }}<br>
                                @lang('accounting::orders.pages.list.title_assoc_transport_amount'): {{ $order->assoc_transport_amount }}<br>
                            @endif
                            @if(!empty($order->assoc_delivery_amount))
                                @lang('accounting::orders.pages.list.title_assoc_delivery_amount'): {{ $order->assoc_delivery_amount }}<br>
                            @endif
                            @if(!empty($order->assoc_service_amount))
                                @lang('accounting::orders.pages.list.title_assoc_service_amount'): {{ $order->assoc_service_amount }}<br>
                            @endif
                            @if(!empty($order->assoc_documents_amount))
                                @lang('accounting::orders.pages.list.title_assoc_documents_amount'): {{ $order->assoc_documents_amount }}<br>
                            @endif
                        </td>
                        <td>{{ $order->history()->count() }}</td>
                        <td>
                            <a class="default btn btn-xs btn-default" href="{{route("accounting:order.edit", ['id' => $order->id])}}">
                                <i class="fa fa-pencil font-blue-madison"></i>
                                @lang('accounting::orders.buttons.edit')
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>

<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>
