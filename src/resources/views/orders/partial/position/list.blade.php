<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::order_position.page.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:orders.position.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::orders.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
            <tr>
                <th>@lang('accounting::order_position.page.list.title_id')</th>
                <th>@lang('accounting::order_position.page.list.title_name')</th>
                <th>@lang('accounting::order_position.page.list.title_unit')</th>
                <th>@lang('accounting::order_position.page.list.title_price_position')</th>
                <th>@lang('accounting::order_position.page.list.title_currency_position')</th>
                <th>@lang('accounting::order_position.page.list.title_comment')</th>
                <th>@lang('accounting::order_position.page.list.title_action')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($positions as $position)
                <tr>
                    <td>{{ $position->id }}</td>
                    <td>{{ $position->name }}</td>
                    <td>{{ $position->unit }}</td>
                    <td>{{ $position->cost }}</td>
                    <td>{{ $position->currency->code }}</td>
                    <td>{{ $position->comment }}</td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:orders.position.edit', ['id' => $position->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::order_position.buttons.edit')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>
