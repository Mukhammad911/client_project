<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:orders.position.create' === $data['route'])
                @lang('accounting::order_position.page.create.title')
            @elseif('accounting:orders.position.edit' === $data['route'])
                @lang('accounting::order_position.page.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:orders.position.create' === $data['route']) ? 'create' : 'update';
            $action = ('create' === $scenario) ? route('accounting:orders.position.save') : route('accounting:orders.position.update', ['id'=>$position->id]) ;
        @endphp

        <!-- BEGIN FORM-->
        <form id="position-form" action="{{ $action }}" class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_position.page.form.title_name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::order_position.placeholders.enter_name')"
                               class="form-control"
                               value="{{ $position->name ?? '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_position.page.form.title_unit')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="unit" id="name-form-control" type="text" maxlength="20"
                               placeholder="@lang('accounting::order_position.placeholders.enter_unit')"
                               class="form-control"
                               value="{{ $position->unit ?? '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_position.page.form.title_price_position')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <input name="cost" id="name-form-control" type="number" step=".01" min="0.01"
                               placeholder="@lang('accounting::order_position.page.form.title_price_position')"
                               class="form-control"
                               value="{{ $position->cost ?? 0 }}"/>
                    </div>
                </div>
                <div class="form-group currencies">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_position.page.form.title_currency_position')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3" style="">
                        <select class="form-control currencies-selector" name="currency_id" id="currency_id">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}" @if(isset($position) && $position->currency_id == $currency->id) selected @endif >{{ $currency->code }} - {{ $currency->country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_position.page.form.title_comment')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::order_position.placeholders.enter_comment')"
                                  class="form-control">{{ $position->comment ?? '' }}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::order_position.buttons.save')
                        </button>
                        <a href="{{ route('accounting:orders.positions') }}" type="button" class="btn default">
                            @lang('accounting::order_position.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>