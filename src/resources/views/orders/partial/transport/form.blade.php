<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:orders.transport.create' === $data['route'])
                @lang('accounting::order_transport.pages.create.title')
            @elseif('accounting:orders.transport.edit' === $data['route'])
                @lang('accounting::order_transport.pages.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:orders.transport.create' === $data['route']) ? 'create' : 'update';
            $action = ('create' === $scenario) ? route('accounting:orders.transport.save') : route('accounting:orders.transport.update', ['id'=>$transport->id]) ;
        @endphp

        <!-- BEGIN FORM-->
        <form id="transport-form" action="{{ $action }}" class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_transport.pages.create.title_name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::order_transport.placeholders.enter_name')"
                               class="form-control"
                               value="{{ $transport->name ?? '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::order_transport.pages.create.title_comment')
                    </label>
                    <div class="col-md-6">
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::order_transport.placeholders.enter_comment')"
                                  class="form-control">{{ $transport->comment ?? '' }}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::order_position.buttons.save')
                        </button>
                        <a href="{{ route('accounting:orders.transports') }}" type="button" class="btn default">
                            @lang('accounting::order_position.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>