<!DOCTYPE html>
    <!--[if IE 8]> <html lang="ru" class="ie8 no-js"> <![endif]-->
    <!--[if IE 9]> <html lang="ru" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
    <html lang="ru" class="no-js">
    <!--<![endif]-->

    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('styles')

        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/>
    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo {{--page-container-bg-solid--}}">

        @yield('layout')

        @yield('scripts')
    </body>
    <!-- END BODY -->
</html>