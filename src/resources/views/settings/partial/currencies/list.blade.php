<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::settings.pages.list.currencies.title')
        </div>
        {{--<div class="actions">
            <a href="{{ route('accounting:salary.recipient.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::salary.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>--}}
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered" id="sample_editable_1">
            <thead>
            <tr>
                <th>@lang('accounting::settings.pages.list.currencies.title_list')</th>
                <th>@lang('accounting::settings.pages.list.currencies.title_edit')</th>
            </tr>
            </thead>
                <tbody>

                <tr>
                    <td>
                        @foreach($currencies as $currency)
                            <div>
                                {{ $currency->code }} - {{ $currency->country }}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:settings.edit', ['key' => 'currencies']) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::salary.buttons.edit')
                        </a>
                    </td>
                </tr>
                {{--@foreach($active_items as $recipient)
                    <tr>
                        <td>{{ $recipient->name }}</td>
                        <td class="center">{{ number_format((float)$recipient->amount, 2, '.', '') }}</td>
                        <td>{{ $recipient->currency()->first()->code }}</td>
                        <td class="center">{{ $recipient->comment ?? '' }}</td>
                        <td>
                            <a class="default btn btn-xs btn-default"
                               href="{{ route('accounting:salary.recipient.edit', ['id' => $recipient->id]) }}">
                                <i class="fa fa-pencil font-blue-madison"></i>
                                @lang('accounting::salary.buttons.edit')
                            </a>
                        </td>
                        <td>
                            <a class="default btn btn-xs default modal-delete-trigger" data-content="{{ $recipient->name }}"
                               href="{{ route('accounting:salary.recipient.delete', ['id' => $recipient->id]) }}">
                                <i class="fa fa-trash-o font-red"></i>
                                @lang('accounting::salary.buttons.delete')
                            </a>
                        </td>
                    </tr>
                @endforeach--}}
                </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->