<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @lang('accounting::settings.pages.edit.currencies.title')
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <!-- BEGIN FORM-->
        <form id="currencies-form" action="{{ route('accounting:settings.save', ['key' => 'currencies']) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group currencies">
                    <label class="control-label col-md-3">
                        @lang('accounting::settings.pages.edit.currencies.title_currency')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3" style="">
                        <input type="hidden" id="currencies" name="currencies">
                        <select class="form-control currencies-selector" id="setting_currency" multiple="multiple">
                            @foreach($currencies as $currency)
                                <option @if(in_array($currency->id, $enabled_currencies_ids))selected="selected"@endif
                                value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::products.buttons.save')
                        </button>
                        <a href="{{ route('accounting:settings.index', ['key' => 'currencies']) }}" type="button" class="btn default">
                            @lang('accounting::products.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->