<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="{{ asset('teleglobal/accounting/js/page/outcome_index.js') }}" type="text/javascript"></script>
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@php
    $dateFormat = config('accounting.app.date_format');
@endphp

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">
    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::outcomes.pages.list.title')
        </div>
        <div class="actions">
{{--            <a href="#" class="btn">--}}
{{--                <div class="input-group date-range-group" title="@lang('accounting::outcomes.buttons.filter')">--}}
{{--                    <input type="text" id="date-range-filter" class="form-control" style="background: #fff; width: 200px;"--}}
{{--                           date-format="{{ strtoupper(config('accounting.app.date_format_js')) }}">--}}
{{--                    <div class="btn-group" role="group" aria-label="Basic example">--}}
{{--                        <button type="button" class="btn default">--}}
{{--                            <i class="fa fa-calendar"></i>--}}
{{--                        </button>--}}
{{--                        <button type="button" class="btn btn-default clear-filter" style="padding-top: 0; padding-bottom: 0; display: none;" title="@lang('accounting::outcomes.buttons.clear_filter')">--}}
{{--                            <span class="align-middle" aria-hidden="true" style="font-size: 200%">&times;</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </a>--}}
            <a href="{{ route('accounting:outcome.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::outcomes.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    {{-- Top pagination container --}}
{{--    @include('accounting::layout.partial.pagination')--}}

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
                <tr>
                    <th>@lang('accounting::outcomes.pages.list.title_doc_id')</th>
                    <th>@lang('accounting::outcomes.pages.list.title_warehouse')</th>
                    <th>@lang('accounting::outcomes.pages.list.title_client')</th>
                    <th>@lang('accounting::outcomes.pages.list.title_products')</th>
                    <th>@lang('accounting::outcomes.pages.list.title_date')</th>
                    <th>@lang('accounting::outcomes.pages.list.title_created_by')</th>
                    <th>@lang('accounting::outcomes.pages.list.title_transport')</th>
                    <th>@lang('accounting::outcomes.pages.list.association')</th>
                    <th>@lang('accounting::outcomes.pages.list.delete')</th>
            </thead>
            <tbody>

            @foreach($documents as $doc)

                    <tr>
                        <td>{{ $doc->doc_number }}</td>
                        <td>{{ ($doc->productsOutcoming->count() > 0) ? $doc->productsOutcoming->first()->pivot->warehouse->name : '' }}</td>
                        <td>
                            @foreach($doc->docItemsClients as $item)
                                {{ $item->client->name }}
                            @endforeach
                        </td>
                        <td>
                            {{--@dd($doc->productsOutcoming[0]->getRelations()['pivot']->getRelations()['product'] != null)--}}
                            @foreach($doc->productsOutcoming as $item)
                                <div>
                                    {{ $item->pivot->product->name }}
                                    (
                                    {{ $item->amount }}  @lang('accounting::outcomes.labels.unit_thing')
                                    X {{ $item->cost  }}
                                    ) 
                                </div>

                            @endforeach
                        </td>
                        @php
                            $date = ($doc->productsOutcoming->count() > 0) ? $doc->productsOutcoming->first()->date : '';
                            $date = ($date !== '') ? (new \Illuminate\Support\Carbon($date))->format($dateFormat) : $date;
                        @endphp
                        <td>{{ $date }}</td>
                        <td>{{ $doc->history->last()->user->name }}</td>

                        <td>
                            @foreach($doc->docItemsTransports as $item)
                                {{ $item->transport->name }}
                            @endforeach
                        </td>
                        <td>
                            @php
                                $ass = \Teleglobal\Accounting\Models\Association::where(\Teleglobal\Accounting\Models\Association::FIELD_DOC_ID, $doc->id)->get();
                            @endphp
                            @if($ass->count() == 0)
                                Нет
                            @else
                                @foreach($ass as $item)
                                    {{ 'Name: ' . $item->name  . ', Amount: ' .$item->amount .', Currency ID: ' . $item->currency_id. ', Cashbox ID: '. $item->cashbox_id }} {!! '</br>' !!}

                                @endforeach
                            @endif

                        </td>
                        <td>
                            <a class="default btn btn-xs default modal-delete-trigger"
                               href="{{ route('accounting:outcome.delete', ['id' => $doc->id]) }}">
                                <i class="fa fa-trash-o font-red"></i>
                                @lang('accounting::incomes.pages.list.delete')
                            </a>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>

    {{-- Bottom pagination container --}}
{{--    @include('accounting::layout.partial.pagination')--}}
</div>

<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>