<div class="portlet light form-fit">

    <style>
        .datetimepicker.dropdown-menu{
            cursor: default;
        }
        .datetimepicker.dropdown-menu .datetimepicker-days td{
            cursor: pointer;
        }
    </style>

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:outcome.create' === $data['route'])
                @lang('accounting::outcomes.pages.create.title')
            @elseif('accounting:outcome.edit' === $data['route'])
                @lang('accounting::outcomes.pages.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ($data['route'] === 'accounting:outcome.create') ? 'create' : 'update';
        @endphp

        <!-- BEGIN FORM-->
        <form id="user-form"
              action="{{ route('accounting:outcome.save') }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">

                {{-- Client --}}
                <div class="form-group clients">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::outcomes.pages.form.title_client')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-4">
                        <select name="client_id" id="client" class="form-control clients-selector">
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{-- Date --}}
                <div class="form-group date">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::outcomes.pages.form.title_date')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-2 date">
                        <div class="input-group date form_datetime">
                            <input type="text" size="16" class="form-control" name="outcome_date"
                                   style="background: #fff; cursor: text" readonly data-date-format="{{ config('accounting.app.date_format_js') }}">
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                {{-- Currency --}}
                <div class="form-group currencies">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::outcomes.pages.form.title_currency')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select name="currency_id" id="currency" class="form-control currencies-selector">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{-- Warehouse --}}
                <div class="form-group warehouses">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::outcomes.pages.form.title_warehouse')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-4">
                        <select name="warehouse_id" id="warehouse" class="form-control warehouses-selector">
                            @foreach($warehouses as $warehouse)
                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{-- Transport --}}
                <div class="form-group transports">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::outcomes.pages.form.title_transport')
                    </label>
                    <div class="col-md-3">
                        <select name="transport_id" id="transport" class="form-control transports-selector">
                            <option value="" selected>@lang('accounting::outcomes.pages.form.title_transport_not_selected')</option>
                            @foreach($transports as $transport)
                                <option value="{{ $transport->id }}">{{ $transport->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <style>
                    label.button{
                        cursor: pointer;
                    }
                    hr{
                        margin: 0;
                    }
                </style>

                {{-- Products template --}}
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::outcomes.pages.list.title_products')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-8" style="padding: 0px 10px 10px 10px; border: solid 1px rgb(229, 229, 229)">
                        <div class="products-container">

                            {{-- Product tamplate --}}
                            <script type="text/template" id="product-template">
                                <div class="product-group hidden" status="old">
                                    <div class="col-md-11 float-left">
                                        <div class="form-group">
                                            {{-- Product --}}

                                            <div class="col-md-5" style="padding-left: 0;">
                                                <label>@lang('accounting::outcomes.pages.create.marks')</label>
                                                <select class="form-control products-selector" name="">
                                                    @foreach($products as $product)
                                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            {{-- Cost --}}
                                            <div class="col-md-2" style="padding-left: 0;">
                                                <label>@lang('accounting::outcomes.pages.create.price')</label>
                                                <input type="number" min="0.01" step="0.01" class="form-control cost" value="0"
                                                       title="@lang('accounting::outcomes.placeholders.enter_cost')">
                                            </div>
                                            {{-- Quantity --}}
                                            <div class="col-md-2" style="padding-left: 0;">
                                                <label>@lang('accounting::outcomes.pages.create.quantity')</label>
                                                <input type="number" min="1" step="1" class="form-control quantity" value="0"
                                                       title="@lang('accounting::outcomes.placeholders.enter_quantity')">
                                            </div>
                                            {{-- Total Quantity --}}
                                            <div class="col-md-2" style="padding-left: 0;">
                                                <label>@lang('accounting::outcomes.pages.create.sum_quantity')</label>
                                                <input type="text" class="form-control total_quantity" readonly value="0" style="cursor: default"
                                                       title="@lang('accounting::outcomes.placeholders.total_quantity')">
                                            </div>
                                            <div class="col-md-1 remove-product" title="@lang('accounting::outcomes.buttons.remove_product')">
                                                <label class="button margin-top-20" style="position: relative; top: 10px;">
                                                    <i class="fa fa-minus font-red"></i>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-11 warehouses-container">
                                            <label class="button add-warehouse">
                                                @lang('accounting::outcomes.buttons.add_warehouse')
                                                <i class="fa fa-plus font-blue" ></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                    <hr>
                                </div>
                            </script>

                            {{-- Warehouse template --}}
                            <script type="text/template" id="warehouse-template">
                                <div class="form-group warehouse-group" status="old">
                                    {{-- Warehouse --}}
                                    <div class="col-md-5" style="padding-left: 0;">
                                        <select name="warehouse" class="form-control warehouses-selector">
                                            @foreach($warehouses as $warehouse)
                                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- Quantity --}}
                                    <div class="col-md-3" style="padding-left: 0;">
                                        <input type="number" min="1" step="1" class="form-control quantity" value="0"
                                               title="@lang('accounting::outcomes.placeholders.enter_quantity')">
                                    </div>
                                    <div class="col-md-1" title="@lang('accounting::outcomes.buttons.remove_warehouse')">
                                        <label class="button margin-top-10 remove-warehouse">
                                            <i class="fa fa-minus font-red"></i>
                                        </label>
                                    </div>
                                </div>
                            </script>

                        </div>

                        <label class="button margin-top-10 add-product">
                            @lang('accounting::outcomes.buttons.add_product')
                            <i class="fa fa-plus font-blue"></i>
                        </label>
                    </div>
                </div>

                {{-- Association block --}}
                <div class="form-group collapse-group">
                    <a class="control-label assoc-collapse-button col-md-3" style="text-decoration: none">
                        @lang('accounting::outcomes.buttons.associate')
                        <i class="fa fa-plus font-blue"></i>
                    </a>
                    <div class="collapse col-md-9">
                        {{-- Association with transport --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.transport')
                                <input type="checkbox" id="assoc_transport_check" name="assoc_transport_check">
                            </label>
                            <div class="col-md-8 assoc_transport hidden" style="padding-left: 0;">
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_transport_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_transport_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- Association with dealer --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.dealer')
                                <input type="checkbox" id="assoc_dealer_check" name="assoc_dealer_check">
                            </label>
                            <div class="col-md-8 assoc_dealer hidden" style="padding-left: 0;">
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_dealer_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_dealer_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- Association with travel card --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.travel_card')
                                <input type="checkbox" id="assoc_travel_card_check" name="assoc_travel_card_check">
                            </label>
                            <div class="col-md-8 assoc_travel_card hidden" style="padding-left: 0;">
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_travel_card_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_travel_card_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- Association with documents --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.documents')
                                <input type="checkbox" id="assoc_documents_check" name="assoc_documents_check">
                            </label>
                            <div class="col-md-8 assoc_documents hidden" style="padding-left: 0;">
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_documents_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_documents_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- Association with seller-dealer --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.seller_dealer')
                                <input type="checkbox" id="assoc_seller_dealer_check" name="assoc_seller_dealer_check">
                            </label>
                            <div class="col-md-8 assoc_seller_dealer hidden" style="padding-left: 0;">
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_seller_dealer_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_seller_dealer_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- Association with warehouse payd --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.warehouse_payd')
                                <input type="checkbox" id="assoc_warehouse_payd_check" name="assoc_warehouse_payd_check">
                            </label>
                            <div class="col-md-8 assoc_warehouse_payd hidden" style="padding-left: 0;">
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_warehouse_payd_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_warehouse_payd_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- Association with outcome_ab --}}
                        <div class="form-group">
                            <label class="col-md-3 control-label checkbox-inline">
                                @lang('accounting::outcomes.pages.form.assoc_source.outcome_ab')
                                <input type="checkbox" id="assoc_outcome_ab_check" name="assoc_outcome_ab_check">
                            </label>
                            <div class="col-md-8 assoc_outcome_ab hidden" style="padding-left: 0;">
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_outcome_ab_cashbox">
                                        @foreach($cashboxes as $cashbox)
                                            <option value="{{ $cashbox->id }}">{{ $cashbox->alias }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2" style="padding-left: 0;">
                                    <input type="number" min="0.01" step="0.01" class="form-control" name="assoc_outcome_ab_amount"
                                           placeholder="@lang('accounting::outcomes.placeholders.enter_amount')">
                                </div>
                                <div class="col-md-4" style="padding-left: 0;">
                                    <select class="form-control" name="assoc_outcome_ab_currency">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::outcomes.buttons.save')
                        </button>
                        <a href="{{ route('accounting:outcomes.list') }}" type="button" class="btn default">
                            @lang('accounting::outcomes.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->
