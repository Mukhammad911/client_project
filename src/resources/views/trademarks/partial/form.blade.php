<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if('accounting:trademark.create' === $data['route'])
                @lang('accounting::trademarks.pages.create.title')
            @elseif('accounting:trademark.edit' === $data['route'])
                @lang('accounting::trademarks.pages.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @php
            $scenario = ('accounting:trademark.create' === $data['route']) ? 'create' : 'update';
        @endphp

        <!-- BEGIN FORM-->
        <form id="user-form"
              action="{{ ($scenario == 'create') ? route('accounting:trademark.save') : route('accounting:trademark.update', ['id' => $trademark->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::trademarks.pages.list.title_name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::trademarks.placeholders.enter_name')" class="form-control"
                               value="{{ $trademark->name ?? '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::trademarks.pages.list.title_comment')
                    </label>
                    <div class="col-md-6">
                        @php
                            $comment = isset($trademark->comment) ? $trademark->comment : '' ;
                        @endphp
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::trademarks.placeholders.enter_comment')"
                                  class="form-control">{{ $comment }}</textarea>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::trademarks.buttons.save')
                        </button>
                        <a href="{{ route('accounting:trademarks.list') }}" type="button" class="btn default">
                            @lang('accounting::trademarks.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->