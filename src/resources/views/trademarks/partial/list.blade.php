<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::trademarks.pages.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:trademark.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::trademarks.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered" id="sample_editable_1">
            <thead>
                <tr>
                    <th>@lang('accounting::trademarks.pages.list.title_name')</th>
                    <th>@lang('accounting::trademarks.pages.list.title_comment')</th>
                    <th>@lang('accounting::trademarks.pages.list.title_edit')</th>
                    <th>@lang('accounting::trademarks.pages.list.title_delete')</th>
                </tr>
            </thead>
            <tbody>
            @foreach($trademarks as $trademark)
                <tr>
                    <td>{{ $trademark->name }}</td>
                    <td>{{ $trademark->comment }}</td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:trademark.edit', ['id' => $trademark->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::trademarks.buttons.edit')
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs default modal-delete-trigger" data-content="{{ $trademark->name }}"
                           href="{{ route('accounting:trademark.delete', ['id' => $trademark->id]) }}">
                            <i class="fa fa-trash-o font-red"></i>
                            @lang('accounting::trademarks.buttons.delete')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->