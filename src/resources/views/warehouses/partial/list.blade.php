<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::warehouses.page.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:warehouse.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::warehouses.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered data-table" id="sample_editable_1">
            <thead>
                <tr>
                    <th>@lang('accounting::warehouses.page.list.title_name')</th>
                    <th>@lang('accounting::warehouses.page.list.title_comment')</th>
                    <th>@lang('accounting::warehouses.page.list.title_created')</th>
                    <th>@lang('accounting::warehouses.page.list.title_date_created')</th>
                    <th>@lang('accounting::warehouses.page.list.title_move_remnants')</th>
                    <th>@lang('accounting::warehouses.page.list.title_edit')</th>
                    <th>@lang('accounting::warehouses.page.list.title_delete')</th>
                </tr>
            </thead>
            <tbody>
            @foreach($warehouses as $warehouse)
                <tr>
                    <td>{{ $warehouse->name }}</td>
                    <td>{{ $warehouse->comment }}</td>
                    <td>{{ $warehouse->history->last()->user->name }}</td>
                    <td>{{ $warehouse->history->last()->date_edited }}</td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:warehouse.move_remnants', ['id' => $warehouse->id]) }}">
                            <i class="fa fa-truck font-blue-madison"></i>
                            @lang('accounting::warehouses.buttons.move_remnants')
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:warehouse.edit', ['id' => $warehouse->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::warehouses.buttons.edit')
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs default modal-delete-trigger" data-content="{{ $warehouse->name }}"
                           href="{{ route('accounting:warehouse.delete', ['id' => $warehouse->id]) }}">
                            <i class="fa fa-trash-o font-red"></i>
                            @lang('accounting::warehouses.buttons.delete')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->
<script src="{{ asset('teleglobal/accounting/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('teleglobal/accounting/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>
<script>    
    jQuery(document).ready(function($)
      {
        $.noConflict();
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });
</script>