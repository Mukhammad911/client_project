<div class="portlet light form-fit">

    @php
        $scenario = ('accounting:warehouse.create' === $data['route']) ? 'create' : 'update';
    @endphp

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            @if($scenario === 'create')
                @lang('accounting::warehouses.page.create.title')
            @elseif($scenario === 'update')
                @lang('accounting::warehouses.page.edit.title')
            @endif
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body form">

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- BEGIN FORM-->
        <form id="user-form"
              action="{{ ($scenario == 'create') ? route('accounting:warehouse.save') : route('accounting:warehouse.update', ['id' => $warehouse->id]) }}"
              class="form-horizontal" autocomplete="off" method="POST">

            @csrf

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        @lang('accounting::warehouses.page.list.title_name')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-6">
                        <input name="name" id="name-form-control" type="text"
                               placeholder="@lang('accounting::warehouses.placeholders.enter_name')" class="form-control"
                               value="{{ $warehouse->name ?? '' }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3" for="comment-form-control">
                        @lang('accounting::warehouses.page.list.title_comment')
                    </label>
                    <div class="col-md-6">
                        @php
                            $comment = isset($warehouse->comment) ? $warehouse->comment : '' ;
                        @endphp
                        <textarea name="comment" id="comment-form-control"
                                  placeholder="@lang('accounting::warehouses.placeholders.enter_comment')"
                                  class="form-control">{{ $comment }}</textarea>
                    </div>
                </div>
                <div class="form-group access_type hidden">
                    <label class="control-label col-md-3">
                        @lang('accounting::warehouses.page.form.access_type')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3">
                        <select class="form-control access_type-selector" id="access_type" name="access_type">
                            <option value="all" selected>@lang('accounting::warehouses.page.form.all')</option>
{{--                            <option value="all"{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'all')) ? ' selected' : '' }}>@lang('accounting::warehouses.page.form.all')</option>--}}
{{--                            <option value="groups"{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'groups')) ? ' selected' : '' }}>@lang('accounting::warehouses.page.form.groups')</option>--}}
{{--                            <option value="users"{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'users')) ? ' selected' : '' }}>@lang('accounting::warehouses.page.form.users')</option>--}}
                        </select>
                    </div>
                </div>
                <div class="form-group access_user_groups{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'groups')) ? '' : ' hidden' }}">
                    <label class="control-label col-md-3">
                        @lang('accounting::warehouses.page.form.access')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3" style="">
                        <input type="hidden" id="access_user_groups" name="access_user_groups">
                        <select class="form-control access_user_groups-selector" id="access_user_groups" multiple="multiple">
                            @foreach($userGroups as $userGroup)
                                <option value="{{ $userGroup->id }}"{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'groups') && in_array($userGroup->id, $sources)) ? ' selected' : '' }}>
                                    {{ $userGroup->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group access_users{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'users')) ? '' : ' hidden' }}">
                    <label class="control-label col-md-3">
                        @lang('')
                        <span class="required" aria-required="true"> * </span>
                    </label>
                    <div class="col-md-3" style="">
                        <input type="hidden" id="access_users" name="access_users">
                        <select class="form-control access_users-selector" id="access_users" multiple="multiple">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}"{{ (isset($warehouse->access_type) && ($warehouse->access_type == 'users') && in_array($user->id, $sources)) ? ' selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i>
                            @lang('accounting::warehouses.buttons.save')
                        </button>
                        <a href="{{ route('accounting:warehouses.list') }}" type="button" class="btn default">
                            @lang('accounting::warehouses.buttons.cancel')
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->