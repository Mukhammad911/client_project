<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light form-fit">

    <div class="portlet-title">
        <div class="caption bold font-blue-madison">
            {{--<i class="fa fa-edit"></i>--}}
            @lang('accounting::user_groups.page.list.title')
        </div>
        <div class="actions">
            <a href="{{ route('accounting:user_group.create') }}" class="btn btn-circle blue ">
                <i class="fa fa-plus"></i>
                @lang('accounting::user_groups.buttons.create')
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;">
            </a>
        </div>
    </div>

    <div class="portlet-body">
        <table class="table table-hover table-bordered" id="sample_editable_1">
            <thead>
                <tr>
                    <th>@lang('accounting::user_groups.pages.list.title_name')</th>
                    <th>@lang('accounting::user_groups.pages.list.title_comment')</th>
                    <th>@lang('accounting::user_groups.pages.list.title_show_group')</th>
                    <th>@lang('accounting::user_groups.pages.list.title_edit')</th>
                    <th>@lang('accounting::user_groups.pages.list.title_delete')</th>
                </tr>
            </thead>
            <tbody>
            @foreach($user_groups as $userGroup)
                <tr>
                    <td>{{ $userGroup->name }}</td>
                    <td>{{ $userGroup->comment }}</td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:user_group.show_group', ['id' => $userGroup->id]) }}">
                            <i class="fa icon-wrench font-blue-madison"></i>
                            @lang('accounting::user_groups.buttons.get_group')
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs btn-default"
                           href="{{ route('accounting:user_group.edit', ['id' => $userGroup->id]) }}">
                            <i class="fa fa-pencil font-blue-madison"></i>
                            @lang('accounting::user_groups.buttons.edit')
                        </a>
                    </td>
                    <td>
                        <a class="default btn btn-xs default modal-delete-trigger" data-content="{{ $userGroup->name }}"
                           href="{{ route('accounting:user_group.delete', ['id' => $userGroup->id]) }}">
                            <i class="fa fa-trash-o font-red"></i>
                            @lang('accounting::user_groups.buttons.delete')
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<!-- END EXAMPLE TABLE PORTLET-->