@extends('accounting::layout.main')

@section('title', __('accounting::user_groups.page_title') .' - '. config('accounting.app.name'))

@section('page_styles')
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('teleglobal/accounting/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN PAGE STYLES -->
    <link href="{{ asset('teleglobal/accounting/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE STYLES -->
@endsection

@section('content')

    @include('accounting::layout.partial.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">

                @include('accounting::layout.partial.breadcrumb')

                @include('accounting::layout.partial.toolbar')

            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN FORM-->
                    <form id="user_group-form" action="{{ route('accounting:user_group.save_group', ['id' => $userGroup->id]) }}" class="form-horizontal" autocomplete="off" method="POST">

                        @csrf

                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3">Default</label>
                                <div class="col-md-9">
                                    <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]" style="position: absolute; left: -9999px;">
                                        <option>Dallas Cowboys</option>
                                        <option>New York Giants</option>
                                        <option selected="">Philadelphia Eagles</option>
                                        <option selected="">Washington Redskins</option>
                                        <option>Chicago Bears</option>
                                        <option>Detroit Lions</option>
                                        <option>Green Bay Packers</option>
                                        <option>Minnesota Vikings</option>
                                        <option selected="">Atlanta Falcons</option>
                                        <option>Carolina Panthers</option>
                                        <option>New Orleans Saints</option>
                                        <option>Tampa Bay Buccaneers</option>
                                        <option>Arizona Cardinals</option>
                                        <option>St. Louis Rams</option>
                                        <option>San Francisco 49ers</option>
                                        <option>Seattle Seahawks</option>
                                    </select><div class="ms-container" id="ms-my_multi_select1"><div class="ms-selectable"><ul class="ms-list" tabindex="-1"><li class="ms-elem-selectable" id="823795905-selectable"><span>Dallas Cowboys</span></li><li class="ms-elem-selectable" id="-1890721249-selectable"><span>New York Giants</span></li><li selected="" class="ms-elem-selectable ms-selected" id="-1518976198-selectable" style="display: none;"><span>Philadelphia Eagles</span></li><li selected="" class="ms-elem-selectable ms-selected" id="-2136494795-selectable" style="display: none;"><span>Washington Redskins</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="980502473-selectable" style="display: none;"><span>Chicago Bears</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-1131466254-selectable" style="display: none;"><span>Detroit Lions</span></li><li class="ms-elem-selectable" id="185217194-selectable"><span>Green Bay Packers</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-383954085-selectable" style="display: none;"><span>Minnesota Vikings</span></li><li selected="" class="ms-elem-selectable ms-selected" id="-1425646635-selectable" style="display: none;"><span>Atlanta Falcons</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-1522070806-selectable" style="display: none;"><span>Carolina Panthers</span></li><li class="ms-elem-selectable" id="-738330712-selectable"><span>New Orleans Saints</span></li><li class="ms-elem-selectable" id="-2076057386-selectable"><span>Tampa Bay Buccaneers</span></li><li class="ms-elem-selectable" id="-2094873869-selectable"><span>Arizona Cardinals</span></li><li class="ms-elem-selectable" id="247588396-selectable"><span>St. Louis Rams</span></li><li class="ms-elem-selectable" id="-1874552895-selectable"><span>San Francisco 49ers</span></li><li class="ms-elem-selectable" id="-1391821265-selectable"><span>Seattle Seahawks</span></li></ul></div><div class="ms-selection"><ul class="ms-list" tabindex="-1"><li class="ms-elem-selection" id="823795905-selection" style="display: none;"><span>Dallas Cowboys</span></li><li class="ms-elem-selection" id="-1890721249-selection" style="display: none;"><span>New York Giants</span></li><li selected="" class="ms-elem-selection ms-selected" id="-1518976198-selection" style=""><span>Philadelphia Eagles</span></li><li selected="" class="ms-elem-selection ms-selected" id="-2136494795-selection" style=""><span>Washington Redskins</span></li><li class="ms-elem-selection ms-selected" id="980502473-selection" style=""><span>Chicago Bears</span></li><li class="ms-elem-selection ms-selected" id="-1131466254-selection" style=""><span>Detroit Lions</span></li><li class="ms-elem-selection" id="185217194-selection" style="display: none;"><span>Green Bay Packers</span></li><li class="ms-elem-selection ms-selected" id="-383954085-selection" style=""><span>Minnesota Vikings</span></li><li selected="" class="ms-elem-selection ms-selected" id="-1425646635-selection" style=""><span>Atlanta Falcons</span></li><li class="ms-elem-selection ms-selected" id="-1522070806-selection" style=""><span>Carolina Panthers</span></li><li class="ms-elem-selection" id="-738330712-selection" style="display: none;"><span>New Orleans Saints</span></li><li class="ms-elem-selection" id="-2076057386-selection" style="display: none;"><span>Tampa Bay Buccaneers</span></li><li class="ms-elem-selection" id="-2094873869-selection" style="display: none;"><span>Arizona Cardinals</span></li><li class="ms-elem-selection" id="247588396-selection" style="display: none;"><span>St. Louis Rams</span></li><li class="ms-elem-selection" id="-1874552895-selection" style="display: none;"><span>San Francisco 49ers</span></li><li class="ms-elem-selection" id="-1391821265-selection" style="display: none;"><span>Seattle Seahawks</span></li></ul></div></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    @lang('accounting::page.users.th_12')
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-3">
                                    <div class="radio-list">
                                        <select class="form-control type-selector" name="type">
                                            <option value="A" selected>A</option>
                                            <option value="GA">GA</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Пользователи</label>
                                <div class="col-md-4">
                                    <select class="bs-select form-control" multiple="true" style="display: none;">
                                        <option>Mustard</option>
                                        <option>Ketchup</option>
                                        <option>Relish</option>
                                    </select><div class="btn-group bootstrap-select show-tick bs-select form-control"><button type="button" class="btn dropdown-toggle selectpicker btn-default" data-toggle="dropdown" title="Mustard, Ketchup" aria-expanded="false"><span class="filter-option pull-left">Mustard, Ketchup</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open" style="max-height: 489px; overflow: hidden; min-height: 0px;"><ul class="dropdown-menu inner selectpicker" role="menu" style="max-height: 487px; overflow-y: auto; min-height: 0px;"><li data-original-index="0" class="ms-hover selected"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Mustard</span>"><span class="text">Mustard</span><span class="fa fa-check check-mark"></span></a></li><li data-original-index="1" class="ms-hover selected"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Ketchup</span>"><span class="text">Ketchup</span><span class="fa fa-check check-mark"></span></a></li><li data-original-index="2" class="ms-hover"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Relish</span>"><span class="text">Relish</span><span class="fa fa-check check-mark"></span></a></li></ul></div></div>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn blue">
                                        <i class="fa fa-check"></i>
                                        @lang('accounting::user_groups.buttons.save')
                                    </button>
                                    <a href="{{ route('accounting:user_groups.list') }}" type="button" class="btn default">
                                        @lang('accounting::user_groups.buttons.cancel')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->

                </div>
            </div>
            <!-- END PAGE CONTENT -->

        </div>
    </div>
    <!-- END CONTENT -->

    @if(config('accounting.quicksidebar.enabled'))
        @include('accounting::layout.partial.quicksidebar')
    @endif

@endsection

@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript" ></script>
{{--    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript" ></script>--}}
{{--    <script src="{{ asset('teleglobal/accounting/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript" ></script>--}}
{{--    <script src="{{ asset('teleglobal/accounting/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript" ></script>--}}
    <script src="{{ asset('teleglobal/accounting/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/jquery.sparkline.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('teleglobal/accounting/plugins/select2/select2.min.js') }}" type="text/javascript" ></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('teleglobal/accounting/js/metronic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/layout.js') }}" type="text/javascript"></script>
    <script src="{{ asset('teleglobal/accounting/js/tasks.js') }}" type="text/javascript"></script>--}}
    @if(config('accounting.quicksidebar.enabled'))
        <script src="{{ asset('teleglobal/accounting/js/quick-sidebar.js') }}" type="text/javascript"></script>
    @endif
    <!-- END PAGE LEVEL SCRIPTS -->

    <script src="{{ asset('teleglobal/accounting/js/page/user-group.js') }}" type="text/javascript"></script>
@endsection
