jQuery(document).ready(function() {
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout

    var handleDatetimePicker = function (items = null) {
        if (!jQuery().datetimepicker) { return; }
        if(items.length === 0) { return false; }
        items.each(function(index, value){
            var item = $(value);
            var options = {
                immediateUpdates: true,
                todayHighlight: true,
                autoclose: true,
                timepicker: false,
                isRTL: Metronic.isRTL(),
                pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
                maxView: 4,
                minView: 2
            };
            item.datetimepicker(options).on('changeDate', function(e) {
                $(this).datetimepicker('update', $(this).data('date'));
            });
            if(item.attr('data-date-value')) {
                item.datetimepicker('update', new Date(item.attr('data-date-value')));
            }
        });
    };
    var handleSelect = function (item = null, placeholder) {
        if (!jQuery().select2) { return false; }
        if(null == item) { return false; }
        item = item.find('select');
        item.select2({
            placeholder: placeholder,
            allowClear: true
        });
    };

    var handleSelectUser = function(item = null, placeholder){
        if (!jQuery().select2) { return false; }
        if(null == item) { return false; }

        item = item.find('select');
        item.select2({
            placeholder: placeholder,
            allowClear: true
        });

        var userSelect = $('div.form-group.user > div > select')[0];
        var cashboxesSelect = $('div.form-group.cashbox > div > select')[0];
        if(typeof cashboxesSelect === 'undefined'){ return false; }
        var data = JSON.parse($(cashboxesSelect).attr('data-cashboxes'));
        userSelect.onchange = function(e){
            var html = '';
            if(
                typeof data[userSelect.value] !== 'undefined'
                && data[userSelect.value].length > 0
            ){
                data[userSelect.value].forEach(function(item, i){
                    html += '<option value="' + item.id + '">' + item.alias + '</option>';
                });
            }
            cashboxesSelect.innerHTML = html;
        }
    };

    var handleHistoryModal = function() {
        if (!jQuery().modal) { return false; }
        $(document).on('click', '.modal-history-trigger', function(e){
            e.preventDefault();
            var data = $(this).attr('data-'),
                fields = JSON.parse($(this).closest('table').attr('data-content')).fields,
                cashboxesFields = JSON.parse($(this).closest('table').attr('data-content')).cashboxes_fields,
                content = JSON.parse($(this).attr('data-content')),
                modal = $('#modal-small').clone();
            if(title) {
                modal.find('.modal-title').html(title);
            }
            if(content) {

                var html = '<ul style="padding-left: 30px;">';
                $.each(content, function( index, editItem ) {
                    html += '<li><p><h5>';
                    if(editItem._scenario == 'create'){
                        html += '<span class="fa fa-plus font-green"></span>';
                    }else if(editItem._scenario == 'update'){
                        html += '<span class="fa fa-edit font-yellow"></span>';
                    }else if(editItem._scenario == 'delete'){
                        html += '<span class="fa fa-minus font-red"></span>';
                    }
                    html += '<b>'+ editItem.edited_by_user + '</b><br> ['+ editItem.edited_date +']</h5></p>';
                    delete editItem._scenario;
                    delete editItem.edited_by_user;
                    delete editItem.edited_date;
                    $.each(editItem, function( key, value ) {
                        if(key != 'cashboxes'){
                            html += '<p><b>' + fields[key] + ' :   </b>'+ value +'</p>';
                        }else{
                            html += '<h5 style="padding-left: 20px; font-weight: bold;">Кассы</h5>';
                            $.each( value, function( index, cashbox ) {
                                html += '<div style="padding-left: 20px;">';
                                if(cashbox._scenario == 'create'){
                                    html += '<span class="fa fa-plus font-green"></span>';
                                }else if(cashbox._scenario == 'update'){
                                    html += '<span class="fa fa-edit font-yellow"></span>';
                                }else if(cashbox._scenario == 'delete'){
                                    html += '<span class="fa fa-minus font-red"></span>';
                                }
                                if(typeof cashbox.balance !== 'undefined'){ html += '   ' + cashbox.balance + '<br>'}
                                if(typeof cashbox.currency !== 'undefined'){ html += '   ' + cashbox.currency + '<br>'}
                                if(typeof cashbox.date !== 'undefined'){ html += '   ' + cashbox.date + '<br>'}
                                if(typeof cashbox.alias !== 'undefined'){ html += '   ' + cashbox.alias + '<br>'}
                                html += '</div><br>';
                            });
                        }
                    });
                    html += '</li>';
                });
                html += '</ul>';

                modal.find('.modal-body').html(html);
            }
            modal.find('.modal-footer').remove();
            modal.modal('show');
        });
    };

    var bindEventHandlers = function() {
        handleDatetimePicker($('input.date'));
        handleSelect($('.cashbox'), 'Касса');
        handleSelect($('.account'), 'Счет');
        handleSelect($('.client'), 'Клиент');
        handleSelect($('.currency'), 'Валюта');
        handleSelect($('.outcome'), 'Отгрузка');
        handleSelectUser($('.user'), 'Пользователь');
        handleHistoryModal();

        $('.user select').trigger('change');
        $('.choice_source select').trigger('change');
    };

    bindEventHandlers();
});