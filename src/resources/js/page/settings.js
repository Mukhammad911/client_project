jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout

  var handleCurrenciesSelect = function (item = null) {
    if (!jQuery().select2) {
      return;
    }
    if(null == item) {
      return false;
    }
    item = item.find(".currencies-selector");
    item.select2({
      placeholder: "Валюта",
      allowClear: true,
    }).on("change.select2", function (e) {
      var selected;
      selected = window.selected = [];
      $.each($(".currencies-selector option:selected"), function(key, option) {
        window.selected.push($(option).val())
      });
      //console.log(JSON.stringify(Object.assign({}, window.selected)));
      $('#currencies').val(JSON.stringify(Object.assign({}, window.selected)));
    });
  };

  var bindEventHandlers = function() {
    handleCurrenciesSelect($('.currencies'));
  };

  bindEventHandlers();
});