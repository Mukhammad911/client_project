jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout

  var handleCategoriesSelect = function (item = null) {
    if (!jQuery().select2) {
      return;
    }
    if(null == item) {
      return false;
    }
    item = item.find(".categories-selector");
    item.select2({
      placeholder: "Категории",
      allowClear: true,
    }).on("change.select2", function (e) {
      var selected;
      selected = window.selected = [];
      $.each($(".categories-selector option:selected"), function(key, option) {
        window.selected.push($(option).val())
      });
      //console.log(JSON.stringify(Object.assign({}, window.selected)));
      $('#categories').val(JSON.stringify(Object.assign({}, window.selected)));
    });
  };

  var handleTypeOfLinkSelect = function (item = null) {
    $(document).on('change', '.type_of_link', function (e) {
      e.preventDefault();
      var element = $(e.currentTarget).find('select').first();
      // $(document).find('#typeOfLink').val = element.value;
      if(element.value == 'all'){
        $(document).find('.categories').addClass('hidden');
      }else{
        $(document).find('.categories').removeClass('hidden');
      }
    });
  };

  var bindEventHandlers = function() {
    handleCategoriesSelect($('.categories'));
    handleTypeOfLinkSelect($('.type_of_link'));
  };

  bindEventHandlers();
});