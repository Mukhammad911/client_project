jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout
  //Index.init(); // init index page
  //Tasks.initDashboardWidget(); // init tash dashboard widget

  var handleCurrenciesSelect = function (item = null) {
    if (!jQuery().select2) {
      return;
    }
    if(null == item) {
      return false;
    }
    item = item.find(".currencies-selector");
    item.select2({
      placeholder: "Валюта",
      allowClear: true
    });
  };

  var bindEventHandlers = function() {
    handleCurrenciesSelect($('.currencies'));
  };

  bindEventHandlers();
});