jQuery(document).ready(function() {
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout
    //Index.init(); // init index page
    //Tasks.initDashboardWidget(); // init tash dashboard widget

    var handleDatetimePicker = function (item = null) {
        if (!jQuery().datetimepicker) {
            return;
        }
        if(null == item) {
            return false;
        }
        var options = {
            immediateUpdates: true,
            todayHighlight: true,
            autoclose: true,
            timepicker: false,
            isRTL: Metronic.isRTL(),
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            maxView: 4,
            minView: 2
        };
        item = item.find(".form_datetime");
        item.datetimepicker(options).on('changeDate', function(e) {
            $(this).datetimepicker('update', $(this).data('date'));
        });

        if(item.attr('data-date-value')) {
            item.datetimepicker('update', new Date(item.attr('data-date-value')));
        }
    };

    var handleAccountsSelect = function (item = null) {
        if (!jQuery().select2) {
            return;
        }
        if(null == item) {
            return false;
        }
        item = item.find(".account-selector");
        item.select2({
            placeholder: "Счет",
            allowClear: true
        });
    };

    var handlePositionsSelect = function (item = null) {
        if (!jQuery().select2) {
            return;
        }
        if(null == item) {
            return false;
        }
        item = item.find(".position-selector");
        item.select2({
            placeholder: "Позиция",
            allowClear: true
        }).on("change.select2", function (e) {
            handleAmountCalculation();
        });
    };

    var handleTransportSelect = function (item = null) {
        if (!jQuery().select2) {
            return;
        }
        if(null == item) {
            return false;
        }
        item = item.find(".transport-selector");
        item.select2({
            placeholder: "Транспорт",
            allowClear: true
        });
    };

    var handleAssociationCheckbox = function () {
        $('#associations-checkbox').on('click', function (e) {
            var children = $(this).attr('data-attr-child');
            if($(this).attr('checked')) {
                $('.'+children).removeClass('hidden');
            } else {
                $('.'+children).addClass('hidden');
            }
        });
    };

    var handleAmountCalculation = function () {
        var positionID = $("select.position-selector").val();
        var price = $("#option-"+positionID).attr('data-attr-price');
        var quantity = $("#quantity").val();
        var amount = $("#amount");

        amount.val(Math.round(price*quantity * 100) / 100);
    };

    var handleQuantityInput = function () {
        $('#quantity').on('change', function (e) {
            handleAmountCalculation();
        });
    };

    var bindEventHandlers = function() {
        handleDatetimePicker($('#date'));
        handleDatetimePicker($('#execution_date'));
        handleAccountsSelect($('.accounts'));
        handlePositionsSelect($('.positions'));
        handleTransportSelect($('.transports'));
        handleAssociationCheckbox();
        handleQuantityInput();
        handleAmountCalculation();
    };

    bindEventHandlers();
});