jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout
  //Index.init(); // init index page
  //Tasks.initDashboardWidget(); // init dashboard widget

  var handleDatetimePicker = function (items = null) {
    if (!jQuery().datetimepicker) { return; }
    if(items.length === 0) { return false; }
    items.each(function(index, value){
      var item = $(value);
      var options = {
        immediateUpdates: true,
        todayHighlight: true,
        autoclose: true,
        timepicker: false,
        isRTL: Metronic.isRTL(),
        pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
        maxView: 4,
        minView: 2
      };
      item.datetimepicker(options).on('changeDate', function(e) {
        $(this).datetimepicker('update', $(this).data('date'));
      });
      if(item.attr('data-date-value')) {
        item.datetimepicker('update', new Date(item.attr('data-date-value')));
      }
    });
  };
  var handleSelect = function (item = null) {
    if (!jQuery().select2) { return; }
    if(null == item) { return false; }
    item.select2({
      allowClear: true
    });
  };
  var handlePaymentCheckbox = function () {
    $('#payment').on('click', function (e) {
      if($(this).attr('checked')) {
        $('div.payment-children').removeClass('hidden');
      } else {
        $('div.payment-children').addClass('hidden');
      }
    });
  };
  var handleProductButtons = function () {
    if (!jQuery()) { return; }
    var productsContainer = $('#products');
    var product = $($('#product-template').html());

    $(document).on('click', '.add-product', function (e) {
      e.preventDefault();
      var newProduct = product.clone().removeClass('hidden');
      newProduct.addClass('margin-top-10').addClass('active');
      newProduct = $(productsContainer).append(newProduct);
      handleSelect(newProduct.find('select'));
    });
    $(document).on('click', '.remove-product', function (e) {
      e.preventDefault();
      $(this).closest('.product').remove();
    });
  };
  var bindEventHandlers = function() {
    handleDatetimePicker($('.datetime'));
    handleSelect($('select.form-control'));
    handlePaymentCheckbox();
    handleProductButtons();
  };

  bindEventHandlers();
});