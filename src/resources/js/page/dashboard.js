jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout
  Index.init(); // init index page
  Tasks.initDashboardWidget(); // init tash dashboard widget

  if(
      typeof iptvAdminApp !== 'undefined'
      && iptvAdminApp.quickSidebarEnabled
  ) {
    QuickSidebar.init(); // init quick sidebar
  }
});