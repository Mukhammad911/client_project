jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout
  //Index.init(); // init index page
  //Tasks.initDashboardWidget(); // init tash dashboard widget

  var handleDatetimePicker = function (item = null) {
    if (!jQuery().datetimepicker) {
      return;
    }
    if(null == item) {
      return false;
    }
    var options = {
      immediateUpdates: true,
      todayHighlight: true,
      autoclose: true,
      timepicker: false,
      isRTL: Metronic.isRTL(),
      pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
      maxView: 4,
      minView: 2
    };
    item = item.find(".form_datetime");
    item.datetimepicker(options).on('changeDate', function(e) {
      $(this).datetimepicker('update', $(this).data('date'));
    });

    if(item.attr('data-date-value')) {
      item.datetimepicker('update', new Date(item.attr('data-date-value')));
    }
  };

  var handleCurrenciesSelect = function (item = null) {
    if (!jQuery().select2) {
      return;
    }
    if(null == item) {
      return false;
    }
    item = item.find(".currencies-selector");
    item.select2({
      placeholder: "Валюта",
      allowClear: true
    });
  };

  var handleTypeSelect = function () {
    if (!jQuery().select2) {
      return;
    }
    $(".type-selector").select2();
  };

  var handleUserEditsModal = function() {
    if (!jQuery().modal) {
      return;
    }
    $(document).on('click', '.modal-edits-trigger', function(e){
      e.preventDefault();
      var title = $(this).attr('data-title'),
        content = JSON.parse($(this).attr('data-content')),
        modal = $('#modal-small').clone();
      if(title) {
        modal.find('.modal-title').html(title);
      }
      if(content) {
        var html = '<ul style="padding-left: 30px;">';
        $.each(content, function( index, value ) {
          html += '<li>'
            +'<p><b>'+ value.editedBy + '</b> ['+ value.createdAt +']</p>'
            +'<p>'+ value.fields +'</p>'
            +'</li>';
        });
        html += '</ul>';
        modal.find('.modal-body').html(html);
      }
      modal.find('.modal-footer').remove();
      modal.modal('show');
    });
  };

  var handleUserDeleteModal = function() {
    if (!jQuery().modal) {
      return;
    }
    $(document).on('click', '.modal-delete-trigger', function(e){
      e.preventDefault();
      var modal = $('#modal-small').clone(),
        url = $(this).attr('href'),
        content = $(this).attr('data-content');

      modal.find('.modal-title').html('Удаление пользователя');
      modal.find('.modal-body').html('Удалить пользователя <b>'+ content +'</b> и все данные связанные с его учетной записью?');
      modal.find('button.cancel').html('Отмена');
      var confirm = modal.find('button.confirm').html('Удалить');
      confirm.on('click', confirm, function(e){
        e.preventDefault();
        window.location.href = url;
      });
      //modal.find('.modal-footer').remove();
      modal.modal('show');
    });
  };

  var handleCashboxButtons = function () {
    if (!jQuery()) {
      return;
    }
    var cashboxesContainer = $('#cashboxes');
    var cashbox = $($('#cashbox-template').html());

    $(document).on('click', '.add-cashbox', function (e) {
      e.preventDefault();
      var cashboxesCount = $('.cashbox').length;
      var newCashboxID = 'cashbox_' + (cashboxesCount+1);
      var newCashbox = cashbox.clone().removeClass('hidden');
      newCashbox.addClass('margin-top-10').addClass('active');
      newCashbox.attr('id', newCashboxID);
      $(cashboxesContainer).append(newCashbox);
      handleCurrenciesSelect($('#'+newCashboxID));
      handleDatetimePicker($('#'+newCashboxID));
    });
    $(document).on('click', '.remove-cashbox', function (e) {
      e.preventDefault();
      $(this).closest('.cashbox').remove();
    });
  };

  var handleUserSelect = function () {
    if (!jQuery().select2) {
      return;
    }
    var cashboxurrency;
    cashboxCurrency = window.cashboxCurrency;

    var selector = $(".user-selector");
    selector.select2({
      placeholder: 'Выбор пользователя'
    });
    selector.on('change', function (e) {
      var id = $(this).attr('id');
      var cashbox = $('#'+ id +'_cashbox');

      $.ajax({
        url: cashbox.attr('data-url'),
        type: "get",
        data: {
          params: {
            'user_id': $(e.added.element).val()
          },
        },
        success: function(result){
          cashbox.html('<option value="">Выбор кассы</option>');
          $.each(result, function(index, value) {
            cashbox.append(
              '<option value="'+ value.id +'" data-limit="'+ value.balance +'">'+ value.text +'</option>'
            );
          });
          cashbox.removeAttr('disabled');
          cashbox.select2().on('change', function (e) {
            if('from_user_cashbox' === $(this).attr('id')) {
              $('#amount').attr('max', $(this).children("option:selected").attr('data-limit'));
            }
          });
        },
        error: function(xhr) {
          cashbox.html('<option value="">Кассы не найдены</option>');
          cashbox.attr('disabled', 'disabled');
        }
      });
    });
  };

  var bindEventHandlers = function() {
    if('undefined' !== typeof newCashboxesID) {
      newCashboxesID.forEach(function(cashbox) {
        handleDatetimePicker($(cashbox));
        handleCurrenciesSelect($(cashbox));
      });
    }
    handleTypeSelect();
    handleUserSelect();
    handleCashboxButtons();
    handleUserEditsModal();
    handleUserDeleteModal();
  };

  bindEventHandlers();
});