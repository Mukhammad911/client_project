jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout
  //Index.init(); // init index page
  //Tasks.initDashboardWidget(); // init tash dashboard widget

  var handleDatetimePicker = function (items = null) {
    if (!jQuery().datetimepicker) { return; }
    if(items.length === 0) { return false; }
    items.each(function(index, value){
      var item = $(value);
      var options = {
        immediateUpdates: true,
        todayHighlight: true,
        autoclose: true,
        timepicker: false,
        isRTL: Metronic.isRTL(),
        pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
        maxView: 4,
        minView: 2
      };
      item.datetimepicker(options).on('changeDate', function(e) {
        $(this).datetimepicker('update', $(this).data('date'));
      });
      if(item.attr('data-date-value')) {
        item.datetimepicker('update', new Date(item.attr('data-date-value')));
      }
    });
  };

  var handleCurrenciesSelect = function (item = null) {
    if (!jQuery().select2) { return; }
    if(null == item) { return false; }
    item.select2({
      placeholder: "Валюта",
      allowClear: true
    });
  };

  var handleTypeSelect = function () {
    if (!jQuery().select2) {
      return;
    }
    $(".type-selector").select2();
  };

  var handleUserHistoryModal = function() {
    if (!jQuery().modal) {
      return;
    }
    $(document).on('click', '.modal-history-trigger', function(e){
      e.preventDefault();
      var title = $(this).attr('data-title'),
          fields = JSON.parse($(this).closest('table').attr('data-content')).fields,
          cashboxesFields = JSON.parse($(this).closest('table').attr('data-content')).cashboxes_fields,
          dateFormat = $(this).closest('table').attr('data-date-format'),
          content = JSON.parse($(this).attr('data-content')),
          modal = $('#modal-small').clone();
      if(title) {
        modal.find('.modal-title').html(title);
      }
      if(content) {

        var html = '<ul style="padding-left: 30px;">';
        $.each(content, function( index, editItem ) {
          html += '<li style="list-style-type: none"><p><h4>';
          if(editItem._scenario == 'create'){
            html += '<span class="fa fa-plus font-green"></span>';
          }else if(editItem._scenario == 'update'){
            html += '<span class="fa fa-edit font-yellow"></span>';
          }else if(editItem._scenario == 'delete'){
            html += '<span class="fa fa-minus font-red"></span>';
          }
          html += '<b style="font-size: 130%">'+ editItem.edited_by_user + '</b><br> ['+  editItem.edited_date +']</h4></p>';
          delete editItem._scenario;
          delete editItem.edited_by_user;
          delete editItem.edited_date;
          $.each(editItem, function( key, value ) {
            if(key == 'groups'){
              // html += '<h5 style="padding-left: 20px; font-weight: bold;">Группы</h5>';
              // $.each( value, function( index, group ) {
              //   html += '<div style="padding-left: 20px;">';
              //   if(group._scenario == 'create'){
              //     html += '<span class="fa fa-plus font-green"></span>';
              //   }else if(group._scenario == 'delete'){
              //     html += '<span class="fa fa-minus font-red"></span>';
              //   }
              //   if(typeof group.name !== 'undefined'){ html += '   ' + group.name + '<br>'}
              //   html += '</div>';
              // });
            }else if(key == 'cashboxes'){
              html += '<h5 style="padding-left: 20px; font-weight: bold;">Кассы</h5>';
              $.each( value, function( index, cashbox ) {
                html += '<div style="padding-left: 20px;">';
                if(cashbox._scenario == 'create'){
                  html += '<span class="fa fa-plus font-green"></span>';
                }else if(cashbox._scenario == 'update'){
                  html += '<span class="fa fa-edit font-yellow"></span>';
                }else if(cashbox._scenario == 'delete'){
                  html += '<span class="fa fa-minus font-red"></span>';
                }
                if(typeof cashbox.balance !== 'undefined'){ html += '   ' + cashbox.balance + '<br>'}
                if(typeof cashbox.currency !== 'undefined'){ html += '   ' + cashbox.currency + '<br>'}
                if(typeof cashbox.date !== 'undefined'){ html += '   ' + cashbox.date + '<br>'}
                if(typeof cashbox.alias !== 'undefined'){ html += '   ' + cashbox.alias + '<br>'}
                html += '</div><br>';
              });
            }else if(key == 'type'){
              // html += '<p><b>' + fields[key] + ' :   </b>'+ value.toUpperCase() +'</p>';
            }else{
              html += '<p><b>' + fields[key] + ' :   </b>'+ value +'</p>';
            }
          });
          html += '</li>';
        });
        html += '</ul>';

        modal.find('.modal-body').html(html);
      }
      modal.find('.modal-footer').remove();
      modal.modal('show');
    });
  };

  var handleUserDeleteModal = function() {
    if (!jQuery().modal) {
      return;
    }
    $(document).on('click', '.modal-delete-trigger', function(e){
      e.preventDefault();
      var modal = $('#modal-small').clone(),
        url = $(this).attr('href'),
        content = $(this).attr('data-content');

      modal.find('.modal-title').html('Удаление пользователя');
      modal.find('.modal-body').html('Удалить пользователя <b>'+ content +'</b> и все данные связанные с его учетной записью?');
      modal.find('button.cancel').html('Отмена');
      var confirm = modal.find('button.confirm').html('Удалить');
      confirm.on('click', confirm, function(e){
        e.preventDefault();
        window.location.href = url;
      });
      //modal.find('.modal-footer').remove();
      modal.modal('show');
    });
  };

  var handleCashboxButtons = function () {
    if (!jQuery()) {
      return;
    }
    var cashboxesContainer = $('#cashboxes');
    var cashbox = $($('#cashbox-template').html());

    $(document).on('click', '.add-cashbox', function (e) {
      e.preventDefault();
      var cashboxesCount = $('.cashbox').length;
      var newCashboxID = 'cashbox_' + (cashboxesCount+1);
      var newCashbox = cashbox.clone().removeClass('hidden');
      newCashbox.addClass('margin-top-10 active new');
      newCashbox.attr('id', newCashboxID);
      $(cashboxesContainer).append(newCashbox);
      newCashbox = $(cashboxesContainer).find('.new');

      handleCurrenciesSelect(newCashbox.find('select.currencies-selector'));
      handleDatetimePicker(newCashbox.find('input.datetime'));
    });
    $(document).on('click', '.remove-cashbox', function (e) {
      e.preventDefault();
      $(this).closest('.cashbox').remove();
    });
  };

  var handleUserSelect = function () {
    if (!jQuery().select2) {
      return;
    }
    var cashboxurrency;
    cashboxCurrency = window.cashboxCurrency;

    var selector = $(".user-selector");
    selector.select2({
      placeholder: 'Выбор пользователя'
    });
    selector.on('change', function (e) {
      var id = $(this).attr('id');
      var cashbox = $('#'+ id +'_cashbox');

      $.ajax({
        url: cashbox.attr('data-url'),
        type: "get",
        data: {
          params: {
            'user_id': $(e.added.element).val()
          },
        },
        success: function(result){
          cashbox.html('<option value="">Выбор кассы</option>');
          $.each(result, function(index, value) {
            cashbox.append(
              '<option value="'+ value.id +'" data-limit="'+ value.balance +'">'+ value.text +'</option>'
            );
          });
          cashbox.removeAttr('disabled');
          cashbox.select2().on('change', function (e) {
            if('from_user_cashbox' === $(this).attr('id')) {
              $('#amount').attr('max', $(this).children("option:selected").attr('data-limit'));
            }
          });
        },
        error: function(xhr) {
          cashbox.html('<option value="">Кассы не найдены</option>');
          cashbox.attr('disabled', 'disabled');
        }
      });
    });
  };

  var handleUserGroupsSelect = function (item = null) {
    if (!jQuery().select2) { return; }
    if(null == item) { return false; }
    item = item.find(".user_groups-selector");
    item.select2({
      placeholder: "Группы пользователей",
      allowClear: true,
    }).on("change.select2", function (e) {
      var selected;
      selected = window.selected = [];
      $.each($(".user_groups-selector option:selected"), function(key, option) {
        window.selected.push($(option).val())
      });
      $('#user_groups').val(JSON.stringify(Object.assign({}, window.selected)));
    });
  };

  var bindEventHandlers = function() {
    handleDatetimePicker($('input.datetime'));
    handleTypeSelect();
    handleUserSelect();
    handleCashboxButtons();
    handleUserHistoryModal();
    handleUserDeleteModal();
    handleUserGroupsSelect($('.user_groups'));
  };

  bindEventHandlers();
});