jQuery(document).ready(function() {
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout
    //Index.init(); // init index page
    //Tasks.initDashboardWidget(); // init tash dashboard widget

    var handleDatetimePicker = function (item = null) {
        if (!jQuery().datetimepicker) { return; }
        if(null == item) { return false; }
        var options = {
            immediateUpdates: true,
            todayHighlight: true,
            autoclose: true,
            timepicker: false,
            isRTL: Metronic.isRTL(),
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            maxView: 4,
            minView: 2
        };
        item = item.find(".form_datetime");
        item.datetimepicker(options).on('changeDate', function(e) {
            $(this).datetimepicker('update', $(this).data('date'));
        });

        if(item.attr('data-date-value')) {
            item.datetimepicker('update', new Date(item.attr('data-date-value')));
        }
    };

    var handleCurrenciesSelect = function (item = null) {
        if (!jQuery().select2) { return; }
        if(null == item) { return false; }
        item = item.find(".currencies-selector");
        item.select2({
            placeholder: "Валюта",
            allowClear: true
        });
    };

    var handleUserGroupEditsModal = function() {
        if (!jQuery().modal) { return; }
        $(document).on('click', '.modal-edits-trigger', function(e){
            e.preventDefault();
            var title = $(this).attr('data-title'),
                content = JSON.parse($(this).attr('data-content')),
                modal = $('#modal-small').clone();
            if(title) {
                modal.find('.modal-title').html(title);
            }
            if(content) {
                var html = '<ul style="padding-left: 30px;">';
                $.each(content, function( index, value ) {
                    html += '<li>'
                        +'<p><b>'+ value.editedBy + '</b> ['+ value.createdAt +']</p>'
                        +'<p>'+ value.fields +'</p>'
                        +'</li>';
                });
                html += '</ul>';
                modal.find('.modal-body').html(html);
            }
            modal.find('.modal-footer').remove();
            modal.modal('show');
        });
    };

    var handleUserGroupDeleteModal = function() {
        if (!jQuery().modal) { return; }
        $(document).on('click', '.modal-delete-trigger', function(e){
            e.preventDefault();
            var modal = $('#modal-small').clone(),
                url = $(this).attr('href'),
                content = $(this).attr('data-content');

            modal.find('.modal-title').html('Удаление склада');
            modal.find('.modal-body').html('Удалить склад <b>`'+ content +'`</b>?');
            modal.find('button.cancel').html('Отмена');
            var confirm = modal.find('button.confirm').html('Удалить');
            confirm.on('click', confirm, function(e){
                e.preventDefault();
                window.location.href = url;
            });
            modal.modal('show');
        });
    };

    var handleAccessTypeSelect = function(item = null) {
        if (!jQuery().select2) { return; }
        if(null == item) { return false; }

        item = item.find(".access_type-selector");
        item.on('change', function(e){
            switch(this.value){
                case 'all':
                    $(document).find('.access_user_groups').addClass('hidden');
                    $(document).find('.access_users').addClass('hidden');
                    break;
                case 'groups':
                    $(document).find('.access_user_groups').removeClass('hidden');
                    $(document).find('.access_users').addClass('hidden');
                    break;
                case 'users':
                    $(document).find('.access_user_groups').addClass('hidden');
                    $(document).find('.access_users').removeClass('hidden');
                    break;
            }
        })

    };

    var handleAccessUsersSelect = function (item = null) {
        if (!jQuery().select2) { return; }
        if(null == item) { return false; }
        item = item.find(".access_users-selector");
        item.select2({
            placeholder: "Пользователи",
            allowClear: true,
        }).on("change.select2", function (e) {
            var selected;
            selected = window.selected = [];
            $.each($(".access_users-selector option:selected"), function(key, option) {
                window.selected.push($(option).val())
            });
            $('#access_users').val(JSON.stringify(Object.assign({}, window.selected)));
        });
    };

    var handleAccessUserGroupsSelect = function (item = null) {
        if (!jQuery().select2) { return; }
        if(null == item) { return false; }
        item = item.find(".access_user_groups-selector");
        item.select2({
            placeholder: "Группы пользователей",
            allowClear: true,
        }).on("change.select2", function (e) {
            var selected;
            selected = window.selected = [];
            $.each($(".access_user_groups-selector option:selected"), function(key, option) {
                window.selected.push($(option).val())
            });
            $('#access_user_groups').val(JSON.stringify(Object.assign({}, window.selected)));
        });
    };

    var bindEventHandlers = function() {
        handleDatetimePicker();
        handleCurrenciesSelect();
        handleUserGroupEditsModal();
        handleUserGroupDeleteModal();
        handleAccessTypeSelect($('.access_type'));
        handleAccessUsersSelect($('.access_users'));
        handleAccessUserGroupsSelect($('.access_user_groups'));
    };

    bindEventHandlers();
});