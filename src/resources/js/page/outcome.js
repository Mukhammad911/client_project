jQuery(document).ready(function() {
  Metronic.init(); // init metronic core componets
  Layout.init(); // init layout
  //Index.init(); // init index page
  //Tasks.initDashboardWidget(); // init tash dashboard widget

  var handleSelect = function (item = null, placeholder = '') {
    if (!jQuery().select2) { return; }
    if(null == item) { return false; }
    item.select2({
      placeholder: placeholder,
      allowClear: true
    });
  };
  var handleDatetimePicker = function (item = null) {
    if (!jQuery().datetimepicker) { return; }
    if(null == item) { return; }
    var options = {
      immediateUpdates: true,
      todayHighlight: true,
      autoclose: true,
      timepicker: false,
      isRTL: Metronic.isRTL(),
      pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
      maxView: 4,
      minView: 2
    };
    item = item.find(".form_datetime");
    item.datetimepicker(options).on('changeDate', function(e) {
      $(this).datetimepicker('update', $(this).data('date'));
    });

    if(item.attr('data-date-value')) {
      item.datetimepicker('update', new Date(item.attr('data-date-value')));
    }
  };
  function setNewNames(productGroup, i, isNew = true){
    var isNew = '[' + (isNew ? 'new' : 'old') + ']';
    var namePrefix = 'products' + isNew + '[' + i + ']';

    productGroup.find('select.products-selector').attr('name', namePrefix + '[product_id]');
    productGroup.find('input.cost').attr('name', namePrefix + '[cost]');
    productGroup.find('input.quantity').attr('name', namePrefix + '[quantity]');

    productGroup.find('.warehouses-container .warehouse-group[status="new"]').each(function(i){
      $(this).find('.warehouses-selector').attr('name', namePrefix + '[addWarehouses][' + i + '][warehouse_id]');
      $(this).find('.quantity').attr('name', namePrefix + '[addWarehouses][' + i + '][quantity]');
    });
  }
  function reCalcNewProductIds(){
    var newProducts = $('.products-container').find('.product-group[status="new"]');
    newProducts.each(function(i){
      setNewNames($(this), i, true);
    });
  }
  var handleProductButtons = function () {
    if (!jQuery()) { return; }
    var productsContainer = $('.products-container');
    var product = $($('#product-template').html());

    $(document).on('click', '.add-product', function (e) {
      e.preventDefault();
      var newProductsCount = $('.new_product').length;
      var newProduct = product.clone().removeClass('hidden');
      newProduct.addClass('margin-top-10 active new');
      newProduct.attr('tag', newProductsCount);
      newProduct.attr('status', 'new');
      $(productsContainer).append(newProduct);
      var newProduct = $($(productsContainer).find('.new[status="new"]')[0]);
      handleSelect(newProduct.find('select'), 'Товар');
      // setNewNames(newProduct, newProductsCount, true);
      handleInputQuantity($(newProduct.find('input.quantity')));
      newProduct.removeClass('new');
      reCalcNewProductIds();
    });
    $(document).on('click', '.remove-product', function (e) {
      e.preventDefault();
      $(this).closest('.product-group').remove();
      reCalcNewProductIds();
    });
  };
  var handleWarehouseButtons = function () {
    if (!jQuery()) { return; }

    $(document).on('click', '.add-warehouse', function (e) {
      e.preventDefault();

      var activeButton = this;
      var warehousesContainer = $(this).closest('.warehouses-container');
      var warehouse = $($('#warehouse-template').html());

      var newWarehouse = warehouse.clone().removeClass('hidden');
      newWarehouse.addClass('active new');
      newWarehouse.attr('status', 'new');
      $(newWarehouse).insertBefore(activeButton);
      newWarehouse = warehousesContainer.find('.warehouse-group.new[status="new"]');
      handleSelect($(newWarehouse.find('select.warehouses-selector')), 'Склад');
      handleInputQuantity($(newWarehouse.find('input.quantity')));
      newWarehouse.removeClass('new');
      reCalcNewProductIds();
    });
    $(document).on('click', '.remove-warehouse', function (e) {
      e.preventDefault();
      var mainQuantityInput = $($(this).closest('.product-group').find('input.quantity')[0]);
      $(this).closest('.warehouse-group').remove();
      mainQuantityInput.trigger('change');
      reCalcNewProductIds();
    });
  };
  var handleAssocCheckboxes = function() {
    if (!jQuery()) { return; }
    $(document).on('click', '#assoc_dealer_check', function (e) {
      if(this.checked){
        $('.assoc_dealer').removeClass('hidden');
      }else{
        $('.assoc_dealer').addClass('hidden');
      }
    });
    $(document).on('click', '#assoc_transport_check', function (e) {
      if(this.checked){
        $('.assoc_transport').removeClass('hidden');
      }else{
        $('.assoc_transport').addClass('hidden');
      }
    });
    $(document).on('click', '#assoc_travel_card_check', function (e) {
      if(this.checked){
        $('.assoc_travel_card').removeClass('hidden');
      }else{
        $('.assoc_travel_card').addClass('hidden');
      }
    });
    $(document).on('click', '#assoc_documents_check', function (e) {
      if(this.checked){
        $('.assoc_documents').removeClass('hidden');
      }else{
        $('.assoc_documents').addClass('hidden');
      }
    });
    $(document).on('click', '#assoc_seller_dealer_check', function (e) {
      if(this.checked){
        $('.assoc_seller_dealer').removeClass('hidden');
      }else{
        $('.assoc_seller_dealer').addClass('hidden');
      }
    });
    $(document).on('click', '#assoc_warehouse_payd_check', function (e) {
      if(this.checked){
        $('.assoc_warehouse_payd').removeClass('hidden');
      }else{
        $('.assoc_warehouse_payd').addClass('hidden');
      }
    });
    $(document).on('click', '#assoc_outcome_ab_check', function (e) {
      if(this.checked){
        $('.assoc_outcome_ab').removeClass('hidden');
      }else{
        $('.assoc_outcome_ab').addClass('hidden');
      }
    });
  };
  var handleAssocCollapseToggle = function(item = null) {
    if (!jQuery().collapse) { return; }
    if(null == item) { return false; }

    $(item).on('click', function (e) {
      var collapseBlock = $(this).closest('.collapse-group').find('.collapse');
      if(typeof collapseBlock !== 'undefined'){
        collapseBlock.collapse('toggle');
        $(collapseBlock).on('shown.bs.collapse', function(){
          var i = $(this).closest('.collapse-group').find('.assoc-collapse-button i');
          i.removeAttr('class');
          i.addClass('fa fa-minus font-red');
        });
        $(collapseBlock).on('hidden.bs.collapse', function(){
          var i = $(this).closest('.collapse-group').find('.assoc-collapse-button i');
          i.removeAttr('class');
          i.addClass('fa fa-plus font-blue');
        });
      }
    });
  };
  var handleInputQuantity = function(elements = null){
    if (!jQuery()) { return; }
    if(elements === null || elements.length == 0) { return false; }

    elements.each(function(){
      $(this).on('input change keyup', function(){
        var productGroup = $(this).closest('.product-group');
        var totalQuantity = productGroup.find('.total_quantity');
        var value = 0;
        productGroup
            .find('input.quantity')
            .each(function(){
              value += !isNaN(parseFloat(this.value)) ? parseFloat(this.value) : 0;
            });
        totalQuantity[0].value = value.toFixed(0);
      })
    });
  };

  var bindEventHandlers = function() {
    handleSelect($('select.clients-selector'), 'Клиент');
    handleSelect($('select.currencies-selector'), 'Валюта');
    handleSelect($('select.transports-selector'), 'Транспорт');
    handleSelect($('select.warehouses-selector'), 'Склад');
    handleDatetimePicker($('.date'));
    handleProductButtons();
    handleWarehouseButtons();
    handleAssocCheckboxes();
    handleAssocCollapseToggle($('.assoc-collapse-button'));
    handleInputQuantity($('input.quantity'));
  };

  bindEventHandlers();
});