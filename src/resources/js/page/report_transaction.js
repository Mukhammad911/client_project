jQuery(document).ready(function() {
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout

    var handleDatetimePicker = function (items = null) {
        if (!jQuery().datetimepicker) { return; }
        if(items.length === 0) { return false; }
        items.each(function(index, value){
            var item = $(value);
            var options = {
                immediateUpdates: true,
                todayHighlight: true,
                autoclose: true,
                timepicker: false,
                isRTL: Metronic.isRTL(),
                pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
                maxView: 4,
                minView: 2
            };
            item.datetimepicker(options).on('changeDate', function(e) {
                $(this).datetimepicker('update', $(this).data('date'));
            });
            if(item.attr('data-date-value')) {
                item.datetimepicker('update', new Date(item.attr('data-date-value')));
            }
        });
    };
    var handleSelect = function (item = null, placeholder) {
        if (!jQuery().select2) { return false; }
        if(null == item) { return false; }
        item.select2({
            placeholder: placeholder,
            allowClear: true
        });
    };
    var handleHistoryModal = function(elements = null) {
        if (!jQuery().modal) { return false; }
        if ((elements == null) && (elements.lengths > 0)) { return false; }

        elements.each(function(index, element){
            $(element).on('click', function(e){
                e.preventDefault();

                var data = {
                    id: $(this).closest('tr').attr('tag')
                };

                $.ajax({
                    type: 'GET',
                    url: 'transaction/get_history',
                    data: data,
                    success: function (response){
                        if(response.status == true){
                            var fieldsData = $('table').attr('data-content');
                            fieldsData = JSON.parse(fieldsData);
                            var html = '<ul style="padding-left: 30px;">';
                            $.each(response.data.history, function( index, editItem ) {
                                html += '<li><p><h5>';
                                if(editItem._scenario == 'create'){
                                    html += '<span class="fa fa-plus font-green"></span>';
                                }else if(editItem._scenario == 'update'){
                                    html += '<span class="fa fa-edit font-yellow"></span>';
                                }else if(editItem._scenario == 'delete'){
                                    html += '<span class="fa fa-minus font-red"></span>';
                                }
                                html += '<b>'+ editItem.edited_by + '</b><br> ['+ editItem.edited_at +']</h5></p>';

                                // $.each(editItem.changes, function( key, value ) {
                                //     if(typeof fieldsData[key] === 'undefined'){
                                //         var a=1;
                                //     }
                                //     html += '<p><b>' + fieldsData[key] + ' :   </b>'+ value +'</p>';
                                // });

                                if(typeof editItem.changes.user !== 'undefined'){
                                    html += '<p><b>' + fieldsData['user'] + ' :   </b>'+ editItem.changes.user +'</p>';
                                }
                                if(typeof editItem.changes.cashbox !== 'undefined'){
                                    html += '<p><b>' + fieldsData['cashbox'] + ' :   </b>'+ editItem.changes.cashbox +'</p>';
                                }
                                if(typeof editItem.changes.account !== 'undefined'){
                                    html += '<p><b>' + fieldsData['account'] + ' :   </b>'+ editItem.changes.account +'</p>';
                                }
                                if(typeof editItem.changes.description !== 'undefined'){
                                    html += '<p><b>' + fieldsData['description'] + ' :   </b>'+ editItem.changes.description +'</p>';
                                }
                                if(typeof editItem.changes.category !== 'undefined'){
                                    html += '<p><b>' + fieldsData['category'] + ' :   </b>'+ editItem.changes.category +'</p>';
                                }
                                if(typeof editItem.changes.subcategory !== 'undefined'){
                                    html += '<p><b>' + fieldsData['subcategory'] + ' :   </b>'+ editItem.changes.subcategory +'</p>';
                                }
                                if(typeof editItem.changes.amount !== 'undefined'){
                                    html += '<p><b>' + fieldsData['amount'] + ' :   </b>'+ editItem.changes.amount +'</p>';
                                }
                                if(typeof editItem.changes.currency !== 'undefined'){
                                    html += '<p><b>' + fieldsData['currency'] + ' :   </b>'+ editItem.changes.currency +'</p>';
                                }
                                if(typeof editItem.changes.date !== 'undefined'){
                                    html += '<p><b>' + fieldsData['date'] + ' :   </b>'+ editItem.changes.date +'</p>';
                                }
                                if(typeof editItem.changes.comment !== 'undefined'){
                                    html += '<p><b>' + fieldsData['comment'] + ' :   </b>'+ editItem.changes.comment +'</p>';
                                }

                                html += '</li>';
                            });
                            html += '</ul>';

                            modal = $('#modal-small').clone();
                            modal.find('.modal-title').html('История изменений');
                            modal.find('.modal-body').html(html);
                            modal.find('.modal-footer').remove();
                            modal.modal('show');
                        }
                    },
                    error: function (errors){
                        var a=1;
                    }
                });
            });
        });
    };

    $(document)
        .on('change', 'select.user', function(){
            var userSelector = $('select.user');
            var cashboxesSelector = $('select.cashbox');
            var data = JSON.parse($(cashboxesSelector).attr('data-cashboxes'));

            cashboxesSelector.select2('destroy').find('option').remove();
            if(
                typeof data[userSelector[0].value] !== 'undefined'
                && data[userSelector[0].value].length > 0
            ){
                $(data[userSelector[0].value]).each(function(){
                    $(cashboxesSelector).append('<option value="' + this.id + '">' + this.alias + '</option>');
                });
            }
            handleSelect($(cashboxesSelector), 'Касса пользователя');
        })
        .on('change', 'select.category', function(){
            var categorySelector = $('select.category');
            var subcategorySelector = $('select.subcategory');
            var data = JSON.parse($(subcategorySelector).attr('data-subcategories'));

            subcategorySelector.select2('destroy').find('option').remove();
            if(
                typeof data[categorySelector[0].value] !== 'undefined'
                && data[categorySelector[0].value].length > 0
            ){
                $(data[categorySelector[0].value]).each(function(){
                    $(subcategorySelector).append('<option value="' + this.id + '">' + this.name + '</option>');
                });
            }
            handleSelect($(subcategorySelector), 'Касса пользователя');
        })
        .on('change', 'select.choice_source', function(){
            switch(this.value){
                case 'none':
                    $('.form-group.outcome').addClass('hidden');
                    $('.form-group.order').addClass('hidden');
                    break;
                case 'warehouse':
                    $('.form-group.outcome').removeClass('hidden');
                    $('.form-group.order').addClass('hidden');
                    break;
                case 'order':
                    $('.form-group.outcome').addClass('hidden');
                    $('.form-group.order').removeClass('hidden');
                    break;
            }
        })
        .on('click', 'a.validated', function(e){
            e.preventDefault();
            var data = {
                id: $(this).closest('td').attr('tag')
            };
            $.ajax({
                type: 'GET',
                url: 'transaction/set_validated',
                data: data,
                success: function (response){
                    if(response.status == true){
                        var date = response.data;
                        var td = $('td.validated[tag="' + date.transaction_id + '"]');
                        td.find('a').remove();
                        td.append('<span>' + date.validated_by + '</span>');
                        td.append('<br>');
                        td.append('<span>' + date.validated_at + '</span>');
                    }
                },
                error: function (errors){
                    var a=1;
                }
            });
        });

    var bindEventHandlers = function() {
        handleDatetimePicker($('input.date'));
        handleSelect($('select.user'), 'Пользователь');
        handleSelect($('select.cashbox'), 'Касса пользователя');
        handleSelect($('select.account'), 'Счет');
        handleSelect($('select.category'), 'Категория');
        handleSelect($('select.subcategory'), 'Подкатегория');
        handleSelect($('select.currency'), 'Валюта');
        // handleSelect($('select.choice_source'), 'Привязка');
        handleSelect($('select.outcome'), 'Отгрузка');
        handleSelect($('select.order'), 'Заказ');
        handleHistoryModal($('a.modal-history-trigger'));

        if($('form#order-form').attr('scenario') == 'create'){
            $('select.user').trigger('change');
            $('select.category').trigger('change');
            $('select.choice_source').trigger('change');
        }
    };

    bindEventHandlers();
});