$(document).ready(function() {
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout

    var tableBody = $('#product-incoming-table tbody');
    var currentParams = {
        pagination: {
            page:           null,
            pagesCount:     null,
            recordsCount:   null,
            recordsPerPage: null,
            space:          null
        },
        order:      null,
        filter: {
            input:          null,
            picker:         null,
            clearButton:    null,
            dateFrom:       null,
            dateTo:         null,
        }
    };

    var initDateRangePicker = function () {
        var filter = currentParams.filter;
        if (!jQuery().daterangepicker) { return; }
        filter.input = $('input#date-range-filter');
        if(filter.input === null) { return false; }
        filter.clearButton = filter.input.closest('.date-range-group').find('button.clear-filter');

        var dateFormat = filter.input.attr('date-format');
        filter.input
            .daterangepicker({
                'showDropdowns': true,
                'format': (typeof dateFormat !== 'undefined') ? dateFormat : 'YYYY-MM-DD',
                locale: {
                    'format': (typeof dateFormat !== 'undefined') ? dateFormat : 'YYYY-MM-DD',
                    'separator': ' - ',
                    'applyLabel': 'Применить',
                    'cancelLabel': 'Отмена',
                    'fromLabel': 'От',
                    'toLabel': 'До',
                    'customRangeLabel': 'Custom',
                    'weekLabel': 'W',
                    'firstDay': 0,
                    'daysOfWeek': [ 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс' ],
                    'monthNames': [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ]
                },
                'timePicker': false,
                'opens': 'left'
            })
            .on('apply.daterangepicker', function(e, picker){
                currentParams.filter.picker = picker;
                onChangeFilter();
            })
            .trigger('cancel.daterangepicker');
        $('<br/>').insertBefore($('div.daterangepicker.dropdown-menu .ranges'));
        $('div.actions .date-range-group span button').on('click', function(){
            $('.date-range').focus();
        });
        filter.input.closest('.date-range-group').find('button.clear-filter').on('click', function(e){
            onClearFilter(this);
        });
    };
    var onChangeFilter = function(){
        var picker = currentParams.filter.picker;
        var range = currentParams.filter.input[0].value.split(picker.separator);
        var data = {
            page:           1,
            date_from:      range[0],
            date_to:        range[1],
            order:          currentParams.order
        };
        getDataQuery(data);
    };
    var onClearFilter = function(item){
        currentParams.filter.input.val('');
        $(item).hide();
        onChangeFilter();
    };
    var clearTableBody = function(){
        tableBody.find('tr').remove();
    };
    var buildTableBody = function(data = []){
        if(!jQuery()) { return; }
        if(typeof tableBody === 'undefined'){ return false; }

        clearTableBody();

        data.forEach(function(item, index, array){
            var tr = $('<tr/>', { 'doc-id': item.id });
            $('<td/>', { 'class': 'text-center', 'html': item.doc_number }).appendTo(tr);
            $('<td/>', { 'html': item.warehouse }).appendTo(tr);
            var td = $('<td/>', {});
            item.products.forEach(function(product){
                $('<div/>', { 'html': product.name + ' (' + product.amount + ' шт.)' }).appendTo(td);
            });
            td.appendTo(tr);
            $('<td/>', { 'class': 'text-center', 'html': item.date }).appendTo(tr);
            $('<td/>', { 'html': item.created_by }).appendTo(tr);
            $('<td/>', { 'class': 'text-center', 'html': item.payment }).appendTo(tr);
            $('<td/>', { 'class': 'text-center', 'html': item.payment_date }).appendTo(tr);
            $('<td/>', { 'html': item.payment_amount }).appendTo(tr);
            $('<td/>', { 'html': item.payment_currency }).appendTo(tr);

            tr.appendTo(tableBody);
        });
    };
    var getDataQuery = function(data){
        $.ajax({
            type: 'GET',
            url: 'incomes',
            data: data,
            success: function (response){
                if(response.status == true){
                    currentParams.order = response.data.order;
                    currentParams.filter.dateFrom = response.data.date_from;
                    currentParams.filter.dateTo = response.data.date_to;
                    currentParams.pagination.page = response.data.pagination.page;
                    currentParams.pagination.pagesCount = response.data.pagination.pages_count;
                    currentParams.pagination.space = response.data.pagination.space;
                    if(response.data.date_from && response.data.date_to){
                        currentParams.filter.input.data('daterangepicker').setStartDate(currentParams.filter.dateFrom);
                        currentParams.filter.input.data('daterangepicker').setEndDate(currentParams.filter.dateTo);
                        currentParams.filter.clearButton.show();
                    }
                    showPaginationBars();
                    buildTableBody(response.data.table_body);
                }
            },
            error: function (errors){
                var a=1;
            }
        });
    };
    var handleListOrder = function(items = null){
        if(!jQuery()) { return; }
        if(items.length === 0){ return false; }
    };
    var showPaginationBars = function(){
        if(!jQuery()) { return; }
        var containers = $('div.pagination-container>.pagination');
        if(containers.length === 0){ return false; }

        // Параметры для отображения пагинации
        var params = currentParams.pagination;

        // Удаляем старую пагинацию
        containers.empty();

        // Создание кнопки 'Первая страница'
        if(params.page > (params.space + 1)){
            $('<button/>', {
                'type':     'button',
                'class':    'btn btn-default pag-button',
                'tag':      1,
                'html':     '&laquo;',
                'title':    'Первая страница',
                'style':    'margin: 2px;'
            }).appendTo(containers);
        }
        // Создание кнопок
        for (var i = 1; i <= params.pagesCount; i++){
            if( (i - params.space <= params.page) && (i + params.space >= params.page) ){
                var attributes = {
                    'type':     'button',
                    'class':    'btn btn-default pag-button',
                    'tag':      i,
                    'html':     i,
                    'title':    'Cтраница ' + i,
                    'style':    'margin: 2px;'
                };
                if(params.page == i){
                    attributes.disabled = 'disabled';
                    attributes.style = 'margin: 2px; background-color: #cccccc;';
                }
                $('<button/>', attributes).appendTo(containers);
            }
        }
        // Создание кнопки 'Последняя страница'
        if( (params.page + params.space) < params.pagesCount ){
            $('<button/>', {
                'type':     'button',
                'class':    'btn btn-default pag-button',
                'tag':      params.pagesCount,
                'html':     '&raquo;',
                'title':    'Последняя страница',
                'style':    'margin: 2px;'
            }).appendTo(containers);
        }

        $('.input-current-page').each(function(){
            this.value = params.page;
            this.max = params.pagesCount;
        });
        handlePaginationButton($('.pagination-container>.pagination>button.pag-button'));
    };
    var handlePaginationButton = function(item = null){
        if(!jQuery()) { return; }
        if(item == null) { return false; }

        item.on('click', function(){
            var data = {
                page:           $(this).attr('tag'),
                date_from:      currentParams.filter.dateFrom,
                date_to:        currentParams.filter.dateTo,
                order:          currentParams.order
            };
            getDataQuery(data);
        });
    };
    var handlePaginationInput = function(item = null){
        if(!jQuery()) { return; }
        if(item == null) { return false; }

        item.on('click', function(){
            var page = $(this).closest('div.input-group').find('input.input-current-page')[0].value;
            var data = {
                page:           page,
                date_from:      currentParams.filter.dateFrom,
                date_to:        currentParams.filter.dateTo,
                order:          currentParams.order
            };
            getDataQuery(data);
        });
    };
    var initHandlers = function() {
        initDateRangePicker();

        handlePaginationInput($('button.button-current-page'));
        handlePaginationButton($('.pagination-container>.pagination>button.pag-button'));

        handleListOrder($('#product-incoming-table th'));
    };

    initHandlers();
});