<?php

return [

    'app' => [
        'name' => 'Система внутреннего учета',
        'url' => 'localhost',
        'path' => base_path(),
        'env' => env('APP_ENV', 'production'),
        'timezone' => 'UTC',
        'version' => '1.0.0',
        'fallback_locale' => 'ru',
        'date_format' => 'd.m.Y',
        'datetime_format' => 'd.m.Y H:i:s',
        'date_format_js' => 'dd.mm.yyyy',
        'datetime_format_js' => 'dd.mm.yyyy hh:ii:ss',
        'pagination' => [
            'records_per_page' => 20,
            'space' => 2,
        ],
    ],

    'package' => [
        'path' => base_path().'/vendor/teleglobal/accounting',
    ],

    'routes' => [
        'name' => 'accounting'
    ],

    'quicksidebar' => [
        'enabled' => 0
    ],

    'users' => [
        'role' => [
            'owner',
            'admin',
            'support',
            'operator',
            'user'
        ],
        'level' => [
            'owner'    => 5,
            'admin'    => 4,
            'support'  => 3,
            'operator' => 2,
            'user'     => 1
        ],
        'status' => [
            'banned'  => 0,
            'pending' => 1,
            'active'  => 2,
            'deleted' => 3
        ]
    ],
    'key' => env('ACCOUNTING_KEY',null),
    'accounting_iv' => env('ACCOUNTING_IV',null),

    // M2JyV2k2NnphRjNyRDN0YnVaR3FuQ0FFMFJHeTZVRDJCeEtoVmQ4YmJ2Zw==

    #'token' => 'md2fwikjLAPrpUwAaGVF+oUa/bOCBuHPTF2z5Ys9qVw='
];
