# accounting-package

Accounting Laravel Framework Package


### Package installation

Add a private repository to the composer.json file in the root project directory.
```sh
"repositories": [
    {
        "type": "git",
        "url": "https://YOUR_USERNAME@bitbucket.org/Mukhammad911/client_project.git"
    }
]
```

Install the package with all dependencies
```sh
$ composer require teleglobal/accounting
```

Generate and store application key.
```sh
$ php artisan accounting:generate --key
```

Run package package deployment
```sh
$ php artisan accounting:install
```

Attention! The cryptographic key must be removed after the project is deployed on the production server and the key is transferred to the site administrator.
DO NOT FORGET to remove the key from the .env file on the production server!
```sh
$ php artisan accounting:purge --key
```


### Requirements

Laravel = 5.7
MariaDB = 10.3
PHP >= 7.1
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension
BCMath PHP Extension